# WetSponge Information
## What is *WetSponge*?
It’s a whole new and powerful API for developers to let them make their plugins compatible with Spigot +1.8 and Sponge +1.9 Before you think that’s it, it also gives a new powerful system to call for Spigot events, optimizing and adapting to Java8 some of its methods.

Also it gives you custom functions to get and use attributes and events not processed by default by Spigot or Sponge.

## How to download
Go to the [Tags](https://gitlab.com/degoos/WetSponge/tags) tab and look for your desired version. Click on the download icon and take a look below *Artifacts*, there you will find the download link.

Inside the zip file you will find everything you need to start using *WetSponge* in your server or projects.

## Maven Repository
If you want to use WetSponge in your Maven Project you don't need to download the plugin itself, you can add our repository and add WetSponge as a dependency.
```html
    <repositories>
        <repository>
            <id>com.degoos.mvn.repo</id>
            <url>https://degoos.com/mvn-repo/</url>
        </repository>
    </repositories>

    <dependencies>
        <!-- WetSponge -->
        <dependency>
            <groupId>com.degoos</groupId>
            <artifactId>WetSponge</artifactId>
            <version>LATEST</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
```

## Interesting links
* [Degoos Team](https://degoos.com)
* [WetSponge Javadocs](https://degoos.com/javadocs/WetSponge/)

[![Buy us a Pizza](https://degoos.com/img/degoos-userbar-pizza.png)](https://www.paypal.me/DeathlyFlags)
[![UserBar](https://degoos.com/img/degoos-userbar.png)](https://degoos.com)