#!/bin/bash

file="./build/resources/main/version.properties"

if [ -f "$file" ]
then
  echo "$file found."

  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')
    eval "${key}='${value}'"
  done < "$file"

  echo "Deploying ${ARTIFACT} ${VERSION}"
    if [ -f build/libs/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ]
    then
        cp build/libs/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ${ARTIFACT}-${VERSION}.jar
    else
        cp build/libs/${ARTIFACT}-${VERSION}.jar ${ARTIFACT}-${VERSION}.jar
    fi
else
  echo "$file not found."
fi

#ARTIFACT=`mvn -q -Dexec.executable="echo" -Dexec.args='${project.artifactId}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec 2>/dev/null`
#VERSION=`mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec 2>/dev/null`
#echo "Deploying ${ARTIFACT} ${VERSION}"
#if [ -f target/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ]
#then
#    mv build/libs/${ARTIFACT}-${VERSION}-jar-with-dependencies.jar ${ARTIFACT}-${VERSION}.jar
#else
#    mv build/libs/${ARTIFACT}-${VERSION}.jar ${ARTIFACT}-${VERSION}.jar
#fi