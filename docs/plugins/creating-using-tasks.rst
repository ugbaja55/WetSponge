Creating and using Tasks
========================

Tasks will allow you to defer the execution of a block of code to a later time. Blocks of code may also be scheduled to be executed
repeatedly at a fixed interval, with or without a delay. They will continue to execute until completed, or canceled, or your plugin is disabled.
You should take a look to the `WSTask Javadocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/task/WSTask.html>`_ to see methods
and understand the following snippets.


.. toctree::
    :maxdepth: 4
    :caption: Contents:



Basic usage
-----------

Creating a Task is something really simple and powerful. In the following example we will create a task which will send
a "This server is using WetSponge!" message to every online targets at the moment.
Once the task is created we can run it using any property we want filling our needs.

.. code-block:: java

    // Main task - Send a message to all online targets
    WSTask wsTask = WSTask.of((task) -> {
        WetSponge.getServer().getOnlinePlayers().forEach(p -> p.sendMessage("This server is using WetSponge!"));
    });

    // Run the task with a delay of 20 ticks = 1 second
    wsTask.runTaskLater(20, this);

    // Create a runnable task wich will be executed every 6000 ticks = 300 seconds after waiting 60 ticks = 3 seconds
    wsTask.runTaskTimer(60, 6000, this);

    // Create a runnable task wich will be executed every 6000 ticks = 300 seconds but only 10 times instantly
    wsTask.runTaskTimer(0, 600, 10, this);
