.. WetSponge documentation master file, created by
   sphinx-quickstart on Fri Jul  7 23:48:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :maxdepth: 4
    :caption: Contents:

How to install WetSponge
========================

If you know how to add a dependency to your plugin you know how to add
WetSponge. But we encourage you to use our Maven Repository to include
WetSponge. Just add our repository to your plugin pom.xml and add
WetSponge as a dependency.

.. code-block:: html

        <repositories>
            <repository>
                <id>com.degoos.mvn.repo</id>
                <url>https://degoos.com/mvn-repo/</url>
            </repository>
        </repositories>

        <dependencies>
            <!-- WetSponge -->
            <dependency>
                <groupId>com.degoos</groupId>
                <artifactId>WetSponge</artifactId>
                <version>LATEST</version>
                <scope>provided</scope>
            </dependency>
        </dependencies>

Plugins folder
==============

Once you launch your server for the first time using WetSponge, a new
plugins directory named *WetSpongePlugins* will be created on your
server root folder.

Any WetSponge plugin must go in that folder. They won’t work if you put
them inside the original server folder as they aren’t coded as a
*“normal”* plugin.