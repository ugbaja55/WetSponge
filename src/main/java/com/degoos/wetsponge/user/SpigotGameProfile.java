package com.degoos.wetsponge.user;


import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.mojang.authlib.GameProfile;
import com.degoos.wetsponge.util.Validate;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.UUID;

public class SpigotGameProfile implements WSGameProfile {

    static WSGameProfile of(UUID uniqueId, @Nullable String name) {
        return new SpigotGameProfile(new GameProfile(uniqueId, name));
    }

    private GameProfile gameProfile;


    public SpigotGameProfile(GameProfile gameProfile) {
        this.gameProfile = gameProfile;
    }

    @Override
    public Optional<String> getName() {
        return Optional.ofNullable(gameProfile.getName());
    }

    @Override
    public Multimap<String, WSProfileProperty> getPropertyMap() {
        Multimap<String, WSProfileProperty> map = LinkedHashMultimap.create();
        gameProfile.getProperties().keySet().forEach(key -> gameProfile.getProperties().get(key).forEach(prop -> map.put(key, new SpigotProfileProperty(prop))));
        return map;
    }


    @Override
    public WSGameProfile addProperty(String name, WSProfileProperty property) {
        Validate.notNull(name, "Name cannot be null!");
        Validate.notNull(property, "Property cannot be null!");
        gameProfile.getProperties().put(name, ((SpigotProfileProperty) property).getHandled());
        return this;
    }

    @Override
    public boolean isFilled() {
        return gameProfile.isComplete();
    }

    @Override
    public GameProfile getHandled() {
        return gameProfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpigotGameProfile that = (SpigotGameProfile) o;

        return gameProfile.equals(that.gameProfile);
    }

    @Override
    public int hashCode() {
        return gameProfile.hashCode();
    }
}
