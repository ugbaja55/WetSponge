package com.degoos.wetsponge.user;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerType;

import java.util.Optional;
import java.util.UUID;

/**
 * The WSUser class represents an user. The user can be online or offline.
 */
public interface WSUser {

    /**
     * Creates a new {@link WSUser user} based on their {@link UUID uuid}.
     * The {@link String name} will be null.
     *
     * @param uuid the {@link UUID unique id}.
     * @return the new {@link WSUser user}.
     */
    static WSUser of(UUID uuid) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return SpigotUser.of(uuid);
            case SPONGE:
                return SpongeUser.of(uuid);
            default:
                return null;
        }
    }

    /**
     * Creates a new {@link WSUser user} based on their {@link UUID uuid} and {@link String name}.
     *
     * @param uuid the {@link UUID unique id}.
     * @param name the {@link String name}.
     * @return the new {@link WSUser user}.
     */
    static WSUser of(UUID uuid, String name) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return SpigotUser.of(uuid);
            case SPONGE:
                return SpongeUser.of(uuid, name);
            default:
                return null;
        }
    }

    /**
     * Checks if the {@link WSUser user} is online.
     *
     * @return whether the {@link WSUser user} is online.
     */
    boolean isOnline();

    /**
     * @return the {@link WSUser user}'s name, or null if not present.
     */
    String getName();

    /**
     * @return the {@link WSUser user}'s {@link UUID unique id}.
     */
    UUID getUniqueId();

    /**
     * Returns whether the {@link WSUser user} is banned.
     * If true, the {@link WSUser user} will not be allowed
     * to enter the server.
     *
     * @return whether the {@link WSUser user} is banned.
     */
    boolean isBanned();

    /**
     * Returns whether the {@link WSUser user} is in the whitelist.
     * If false, the {@link WSUser user} will not be allowed
     * to enter the server.
     *
     * @return whether the {@link WSUser user} in the whitelist.
     */
    boolean isWhitelisted();

    /**
     * Sets whether the {@link WSUser user} is in the whitelist.
     *
     * @param whitelisted the boolean.
     */
    void setWhitelisted(boolean whitelisted);

    /**
     * Returns the linked {@link WSPlayer player} to the {@link WSUser}, if them is connected.
     *
     * @return the linked {@link WSPlayer player}, if present.
     */
    Optional<WSPlayer> getPlayer();

    /**
     * Returns the date of the {@link WSUser}'s first join, in millis.
     *
     * @return the date of the {@link WSUser}'s first join.
     */
    long getFirstPlayed();

    /**
     * Returns the date of the {@link WSUser}'s last join, in millis.
     *
     * @return the date of the {@link WSUser}'s last join.
     */
    long getLastPlayed();

    /**
     * Returns whether the {@link WSUser user} has played before to the server.
     *
     * @return whether the {@link WSUser user} has played before.
     */
    boolean hasPlayedBefore();

    /**
     * @return the handled object.
     */
    Object getHandled();
}
