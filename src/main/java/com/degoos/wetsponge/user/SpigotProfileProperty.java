package com.degoos.wetsponge.user;

import com.mojang.authlib.properties.Property;

import javax.annotation.Nullable;
import java.util.Optional;

public class SpigotProfileProperty implements WSProfileProperty {


    static WSProfileProperty of(String name, String value, @Nullable String signature) {
        return new SpigotProfileProperty(new Property(name, value, signature));
    }

    private Property profileProperty;


    public SpigotProfileProperty(Property profileProperty) {
        this.profileProperty = profileProperty;
    }

    @Override
    public String getName() {
        return profileProperty.getName();
    }

    @Override
    public String getValue() {
        return profileProperty.getValue();
    }

    @Override
    public Optional<String> getSignature() {
        return profileProperty.hasSignature() ? Optional.empty() : Optional.of(profileProperty.getSignature());
    }

    @Override
    public Property getHandled() {
        return profileProperty;
    }
}
