package com.degoos.wetsponge.entity;

import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.enums.EnumPotionEffectType;

import java.util.List;

public interface WSPotionEffectApplicable {

    /**
     * Adds a {@link WSPotionEffect} to this {@link WSLivingEntity}.
     *
     * @param effect the {@link WSPotionEffect}.
     */
    void addPotionEffect(WSPotionEffect effect);

    /**
     * Returns a {@link List} with all current {@link WSPotionEffect potion effect}s of this {@link WSLivingEntity}.
     *
     * @return the {@link List}.
     */
    List<WSPotionEffect> getPotionEffects();

    /**
     * Clears every @link WSPotionEffect potion effect} this {@link WSLivingEntity} has.
     */
    void clearAllPotionEffects();

    /**
     * Removes the {@link WSPotionEffect potion effect} with the target {@link EnumPotionEffectType type}.
     * @param potionEffectType the {@link EnumPotionEffectType effect type}.
     */
    void removePotionEffect(EnumPotionEffectType potionEffectType);

    /**
     * Returns true if this Entity has the target {@link EnumPotionEffectType potion effect type}.
     *
     * @param effectType the {@link EnumPotionEffectType potion effect type}.
     * @return true if the Entity has the target {@link EnumPotionEffectType potion effect type}.
     */
    default boolean hasCustomEffect(EnumPotionEffectType effectType) {
        return getPotionEffects().stream().anyMatch(effect -> effect.getType().equals(effectType));
    }

}
