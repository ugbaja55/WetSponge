package com.degoos.wetsponge.entity.modifier;

import com.degoos.wetsponge.enums.EnumDamageModifierType;
import com.degoos.wetsponge.enums.EnumHealthModifierType;
import com.degoos.wetsponge.util.Validate;

import java.util.function.DoubleUnaryOperator;

public class WSHealthModifier {

    private EnumHealthModifierType healthModifierType;
    private DoubleUnaryOperator function;

    public WSHealthModifier(EnumHealthModifierType healthModifierType, DoubleUnaryOperator function) {
        Validate.notNull(healthModifierType, "Health modifier type cannot be null!");
        this.healthModifierType = healthModifierType;
        this.function = function == null ? t -> t : function;
    }

    public EnumHealthModifierType getHealthModifierType() {
        return healthModifierType;
    }

    public void setHealthModifierType(EnumHealthModifierType healthModifierType) {
        this.healthModifierType = healthModifierType;
    }

    public DoubleUnaryOperator getFunction() {
        return function;
    }

    public void setFunction(DoubleUnaryOperator function) {
        this.function = function == null ? t -> t : function;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSHealthModifier that = (WSHealthModifier) o;

        return healthModifierType == that.healthModifierType;
    }

    @Override
    public int hashCode() {
        return healthModifierType.hashCode();
    }
}
