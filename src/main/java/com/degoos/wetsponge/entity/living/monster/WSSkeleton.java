package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.entity.living.WSEquipable;

public interface WSSkeleton extends WSMonster, WSEquipable {
}
