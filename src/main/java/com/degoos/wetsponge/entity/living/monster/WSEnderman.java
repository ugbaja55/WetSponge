package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.material.WSMaterial;

import java.util.Optional;

public interface WSEnderman extends WSMonster {

    /**
     * Return an immutable copy of the {@link WSMaterial material} the {@link WSEnderman enderman} is holding.
     *
     * @return an immutable copy of the {@link WSMaterial material}.
     */
    Optional<WSMaterial> getCarriedMaterial();

    /**
     * Sets the {@link WSMaterial material} the {@link WSEnderman enderman} is holding. It can be null.
     *
     * @param carriedMaterial the {@link WSMaterial material}.
     */
    void setCarriedMaterial(WSMaterial carriedMaterial);

}
