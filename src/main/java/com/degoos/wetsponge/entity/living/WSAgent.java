package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.entity.WSEntity;

import java.util.Optional;

public interface WSAgent extends WSLivingEntity {

	/**
	 * Sets if the entity has its AI enabled.
	 *
	 * @param ai
	 * 		true to enable the AI
	 */
	void setAI (boolean ai);

	/**
	 * @return true if the entity has its AI enabled.
	 */
	boolean hasAI ();

	/**
	 * @return the targeted entity of this entity.
	 */
	Optional<WSEntity> getTarget ();

	/**
	 * Sets the targeted entity of this entity.
	 *
	 * @param entity
	 * 		the targeted entity.
	 */
	void setTarget (WSEntity entity);

}
