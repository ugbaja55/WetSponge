package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Cow.
 */
public interface WSCow extends WSAnimal {}
