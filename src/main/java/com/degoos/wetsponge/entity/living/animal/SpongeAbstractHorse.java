package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinAbstractHorse;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.passive.AbstractHorse;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.animal.Horse;

public class SpongeAbstractHorse extends SpongeAnimal implements WSAbstractHorse {


	public SpongeAbstractHorse(Horse entity) {
		super(entity);
	}

	@Override
	public int getDomestication() {
		return ((AbstractHorse) getHandled()).getTemper();
	}

	@Override
	public void setDomestication(int domestication) {
		((AbstractHorse) getHandled()).setTemper(domestication);
	}

	@Override
	public int getMaxDomestication() {
		return ((AbstractHorse) getHandled()).getMaxTemper();
	}

	@Override
	public void setMaxDomestication(int maxDomestication) {
		((WSMixinAbstractHorse) getHandled()).setMaxTemper(maxDomestication);
	}

	@Override
	public double getJumpStrength() {
		return ((AbstractHorse) getHandled()).getHorseJumpStrength();
	}

	@Override
	public void setJumpStrength(double jumpStrength) {
		try {
			Field field = ReflectionUtils.getFirstField(AbstractHorse.class, IAttribute.class);
			field.setAccessible(true);
			((AbstractHorse) getHandled()).getEntityAttribute((IAttribute) field.get(null)).setBaseValue(jumpStrength);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred white WetSponge was setting the jump strength of a horse!");
		}
	}

	@Override
	public boolean isTamed() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty()).isPresent();
	}

	@Override
	public Optional<UUID> getTamer() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty());
	}

	@Override
	public void setTamer(UUID tamer) {
		getHandled().offer(Keys.TAMED_OWNER, Optional.ofNullable(tamer));
	}

	@Override
	public boolean hasSaddle() {
		return getHandled().get(Keys.PIG_SADDLE).orElse(false);
	}

	@Override
	public void setSaddle(boolean saddle) {
		getHandled().offer(Keys.PIG_SADDLE, saddle);
	}

	@Override
	public Horse getHandled() {
		return (Horse) super.getHandled();
	}
}
