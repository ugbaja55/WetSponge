package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.material.itemType.WSItemType;
import org.bukkit.entity.Enderman;
import org.bukkit.material.MaterialData;

import java.util.Optional;

public class SpigotEnderman extends SpigotMonster implements WSEnderman {


    public SpigotEnderman(Enderman entity) {
        super(entity);
    }

    @Override
    public Optional<WSMaterial> getCarriedMaterial() {
        MaterialData materialData = getHandled().getCarriedMaterial();
        if (materialData == null) return Optional.empty();
        WSMaterial material = WSMaterial.of(materialData.getItemTypeId(),
                materialData.getData()).orElse(null);
        if (material == null) material = new WSItemType(materialData.getItemTypeId(), "", 64);
        return Optional.of(material);
    }

    @Override
    public void setCarriedMaterial(WSMaterial carriedMaterial) {
        if (carriedMaterial == null) getHandled().setCarriedMaterial(null);
        else getHandled().setCarriedMaterial(new MaterialData(carriedMaterial.getId(),
                    (byte) (carriedMaterial instanceof WSDataValuable ? ((WSDataValuable) carriedMaterial).getDataValue() : 0)));
    }

    @Override
    public Enderman getHandled() {
        return (Enderman) super.getHandled();
    }


}
