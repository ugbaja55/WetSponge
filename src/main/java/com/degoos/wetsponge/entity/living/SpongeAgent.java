package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import java.util.Optional;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Agent;

public class SpongeAgent extends SpongeLivingEntity implements WSAgent {


	public SpongeAgent(Agent entity) {
		super(entity);
	}


	@Override
	public void setAI(boolean ai) {
		getHandled().offer(Keys.AI_ENABLED, ai);
	}


	@Override
	public boolean hasAI() {
		return getHandled().aiEnabled().get();
	}


	@Override
	public Optional<WSEntity> getTarget() {
		return getHandled().getTarget().map(SpongeEntityParser::getWSEntity);
	}


	@Override
	public void setTarget(WSEntity entity) {
		if (entity == null) getHandled().setTarget(null);
		else getHandled().setTarget((Entity) entity.getHandled());
	}


	@Override
	public Agent getHandled() {
		return (Agent) super.getHandled();
	}
}
