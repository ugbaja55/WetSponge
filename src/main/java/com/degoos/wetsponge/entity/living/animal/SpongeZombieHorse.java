package com.degoos.wetsponge.entity.living.animal;


import org.spongepowered.api.entity.living.animal.ZombieHorse;

public class SpongeZombieHorse extends SpongeAbstractHorse implements WSSkeletonHorse {

	public SpongeZombieHorse(ZombieHorse entity) {
		super(entity);
	}

	@Override
	public ZombieHorse getHandled() {
		return (ZombieHorse) super.getHandled();
	}
}
