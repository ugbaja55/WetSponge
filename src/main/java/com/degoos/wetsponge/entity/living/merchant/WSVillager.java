package com.degoos.wetsponge.entity.living.merchant;

import com.degoos.wetsponge.entity.living.WSAgeable;
import com.degoos.wetsponge.enums.EnumVillagerProfession;
import com.degoos.wetsponge.merchant.WSMerchant;

/**
 * This class represents a Villager. A villager is an entity that extends {@link WSMerchant}.
 * Players can interact to a villager and open the trade GUI.
 */
public interface WSVillager extends WSAgeable, WSMerchant {

    /**
     * Returns the profession of this villager.
     * This method will work better on {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge}.
     * @return the profession.
     */
    EnumVillagerProfession getProfession();

    /**
     * Sets the profession of this villager.
     * This method will work better on {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge}.
     * @param profession the profession..
     */
    void setProfession(EnumVillagerProfession profession);

}
