package com.degoos.wetsponge.entity.living.player;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.inventory.SpigotInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.particle.WSParticle;
import com.degoos.wetsponge.resource.spigot.TitleAPI;
import com.degoos.wetsponge.scoreboard.SpigotScoreboard;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.WSTitle;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.PacketUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3f;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachmentInfo;

public class SpigotPlayer extends SpigotLivingEntity implements WSPlayer {

	private Map<WSLocation, WSBlockType> fakeBlocks;


	public SpigotPlayer(Player entity) {
		super(entity);
		fakeBlocks = new HashMap<>();
	}


	@Override
	public String getName() {
		return getHandled().getName();
	}


	@Override
	public WSText getDisplayedName() {
		return SpigotText.getByFormattingText(getHandled().getDisplayName());
	}


	@Override
	public void setDisplayedName(WSText displayedName) {
		getHandled().setDisplayName(displayedName.toFormattingText());
	}


	@Override
	public int getFoodLevel() {
		return getHandled().getFoodLevel();
	}


	@Override
	public void setFoodLevel(int foodLevel) {
		getHandled().setFoodLevel(foodLevel);
	}


	@Override
	public void spawnParticle(WSLocation location, float speed, int amount, WSParticle particle) {
		particle.spawnParticle(location, speed, amount, this);
	}


	@Override
	public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSParticle particle) {
		particle.spawnParticle(location, speed, amount, radius, this);
	}


	@Override
	public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
		ItemStack itemStack = null;
		switch (type) {
			case HELMET:
				itemStack = getHandled().getEquipment().getHelmet();
				break;
			case CHESTPLATE:
				itemStack = getHandled().getEquipment().getChestplate();
				break;
			case LEGGINGS:
				itemStack = getHandled().getEquipment().getLeggings();
				break;
			case BOOTS:
				itemStack = getHandled().getEquipment().getBoots();
				break;
			case MAIN_HAND:
				itemStack = getHandled().getEquipment().getItemInHand();
				break;
			case OFF_HAND:
				itemStack = !WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? null : getHandled().getEquipment().getItemInOffHand();
				break;
		}
		return Optional.ofNullable(itemStack).map(SpigotItemStack::new);
	}


	@Override
	public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
		switch (type) {
			case HELMET:
				getHandled().getEquipment().setHelmet(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case CHESTPLATE:
				getHandled().getEquipment().setChestplate(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case LEGGINGS:
				getHandled().getEquipment().setLeggings(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case BOOTS:
				getHandled().getEquipment().setBoots(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case MAIN_HAND:
				getHandled().getEquipment().setItemInHand(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
			case OFF_HAND:
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
					getHandled().getEquipment().setItemInOffHand(itemStack == null ? null : ((SpigotItemStack) itemStack).getHandled().clone());
				break;
		}
	}


	@Override
	public WSInventory getInventory() {
		return new SpigotInventory(getHandled().getInventory());
	}

	@Override
	public WSInventory getEnderChestInventory() {
		return new SpigotInventory(getHandled().getEnderChest());
	}

	@Override
	public Optional<WSInventory> getOpenInventory() {
		return Optional.ofNullable(getHandled().getOpenInventory().getTopInventory()).map(SpigotInventory::new);
	}

	@Override
	public void openInventory(WSInventory inventory) {
		if (inventory == null) closeInventory();
		else getHandled().openInventory(((SpigotInventory) inventory).getHandled());
	}

	@Override
	public void closeInventory() {
		getHandled().closeInventory();
	}

	@Override
	public EnumGameMode getGameMode() {
		return EnumGameMode.getByValue(getHandled().getGameMode().getValue()).orElse(EnumGameMode.SURVIVAL);
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {
		getHandled().setGameMode(GameMode.getByValue(gameMode.getValue()));
	}

	@Override
	public void kick() {
		getHandled().kickPlayer(null);
	}

	@Override
	public void kick(WSText message) {
		getHandled().kickPlayer(message.toFormattingText());
	}

	@Override
	public void simulateMessage(WSText message) {
		getHandled().chat(message.toFormattingText());
	}

	@Override
	public WSScoreboard getScoreboard() {
		return new SpigotScoreboard(getHandled().getScoreboard());
	}

	@Override
	public void setScoreboard(WSScoreboard scoreboard) {
		getHandled().setScoreboard(((SpigotScoreboard) scoreboard).getHandled());
	}

	@Override
	public WSGameProfile getProfile() {
		return WSGameProfile.of(getUniqueId(), getName());
	}

	@Override
	public void sendTitle(WSTitle wsTitle) {
		if (wsTitle.isClear() || wsTitle.isReset()) {
			TitleAPI.sendTitle(getHandled(), wsTitle.isReset() ? 20 : null, wsTitle.isReset() ? 60 : null, wsTitle.isReset() ? 20 : null, "", "");
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_11_2)) TitleAPI.sendActionBarPost112(getHandled(), "");
			else TitleAPI.sendActionBarPre112(getHandled(), "");
		} else {
			TitleAPI.sendTitle(getHandled(), wsTitle.getFadeIn().orElse(null), wsTitle.getStay().orElse(null), wsTitle.getFadeOut().orElse(null), wsTitle.getTitle()
				.map(WSText::toFormattingText).orElse(null), wsTitle.getSubtitle().map(WSText::toFormattingText).orElse(null));
			if (wsTitle.getActionBar().isPresent()) if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_11_2)) {
				TitleAPI.sendActionBarPost112(getHandled(), wsTitle.getActionBar().map(WSText::toFormattingText).orElse(null));
			} else {
				TitleAPI.sendActionBarPre112(getHandled(), wsTitle.getActionBar().map(WSText::toFormattingText).orElse(null));
			}
		}
	}

	@Override
	public void setResourcePack(URI uri) {
		getHandled().setResourcePack(uri.toString());
	}

	@Override
	public String getLanguageCode() {
		try {
			Object playerHandled = HandledUtils.getPlayerHandle(getHandled());
			Field field = playerHandled.getClass().getDeclaredField("locale");
			field.setAccessible(true);
			return (String) field.get(playerHandled);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return "en_US";
		}
	}

	@Override
	public boolean isOnline() {
		return getHandled().isOnline();
	}

	@Override
	public float getExperience() {
		return getHandled().getExp();
	}

	@Override
	public void setExperience(float experience) {
		getHandled().setExp(experience);
	}

	@Override
	public int getExperienceLevel() {
		return getHandled().getLevel();
	}

	@Override
	public void setExperienceLevel(int experienceLevel) {
		getHandled().setLevel(experienceLevel);
	}

	@Override
	public double getWalkingSpeed() {
		return getHandled().getWalkSpeed();
	}

	@Override
	public void setWalkingSpeed(double walkingSpeed) {
		getHandled().setWalkSpeed((float) walkingSpeed);
	}

	@Override
	public double getFlyingSpeed() {
		return getHandled().getFlySpeed();
	}

	@Override
	public void setFlyingSpeed(double flyingSpeed) {
		getHandled().setFlySpeed((float) flyingSpeed);
	}

	@Override
	public boolean canFly() {
		return getHandled().getAllowFlight();
	}

	@Override
	public void setCanFly(boolean canFly) {
		getHandled().setAllowFlight(canFly);
	}

	@Override
	public boolean isFlying() {
		return getHandled().isFlying();
	}

	@Override
	public void setFlying(boolean flying) {
		getHandled().setFlying(flying);
	}

	@Override
	public void addFakeBlock(WSLocation location, WSBlockType material) {
		fakeBlocks.put(location.getBlockLocation(), material);
		getHandled().sendBlockChange(((SpigotLocation) location).getLocation(), material.getId(),
			material instanceof WSDataValuable ? (byte) ((WSDataValuable) material).getDataValue() : 0);
	}

	@Override
	public Optional<WSBlockType> getFakeBlock(WSLocation location) {
		return Optional.ofNullable(fakeBlocks.get(location.getBlockLocation()));
	}


	@Override
	public Map<WSLocation, WSBlockType> getFakeBlocks() {
		Map<WSLocation, WSBlockType> map = new HashMap<>();
		fakeBlocks.forEach((location, type) -> map.put(location.clone(), type.clone()));
		return map;
	}

	@Override
	public boolean containsFakeBlock(WSLocation location) {
		return fakeBlocks.containsKey(location.getBlockLocation());
	}

	@Override
	public void refreshFakeBlock(WSLocation location) {
		WSLocation blockLocation = location.getBlockLocation();
		if (fakeBlocks.containsKey(blockLocation)) addFakeBlock(blockLocation, fakeBlocks.get(blockLocation));
	}

	@Override
	public void removeFakeBlock(WSLocation location) {
		WSLocation blockLocation = location.getBlockLocation();
		fakeBlocks.remove(blockLocation);
		WSBlockType type = location.getBlock().createState().getBlockType();
		if (location.getWorld().equals(getWorld())) getHandled()
			.sendBlockChange(((SpigotLocation) location).getLocation(), type.getId(), type instanceof WSDataValuable ? (byte) ((WSDataValuable) type).getDataValue() :
			                                                                          0);
	}

	@Override
	public void clearFakeBlocks() {
		fakeBlocks.keySet().stream().filter(location -> location.getWorld().equals(getWorld())).forEach(this::removeFakeBlock);
		fakeBlocks.clear();
	}

	@Override
	public void sendPacket(WSPacket packet) {
		packet.update();
		PacketUtils.sendPacket(getHandled(), packet.getHandler());
	}

	@Override
	public Optional<WSItemStack> getItemOnCursor() {
		ItemStack itemStack = getHandled().getItemOnCursor();
		if (itemStack == null || itemStack.getType() == Material.AIR) return Optional.empty();
		return Optional.of(new SpigotItemStack(itemStack));
	}

	@Override
	public void setItemOnCursor(WSItemStack itemOnCursor) {
		if (itemOnCursor == null) getHandled().setItemOnCursor(null);
		else getHandled().setItemOnCursor(((SpigotItemStack) itemOnCursor).getHandled());
	}

	@Override
	public int getSelectedHotbarSlot() {
		return getHandled().getInventory().getHeldItemSlot();
	}

	@Override
	public void setSelectedHotbarSlot(int slot) {
		getHandled().getInventory().setHeldItemSlot(slot);
	}

	@Override
	public void sendSignChange(WSLocation location, WSText[] lines) {
		String[] newLines = new String[lines.length];
		for (int i = 0; i < lines.length; i++) newLines[i] = lines[i].toFormattingText();
		getHandled().sendSignChange(((SpigotLocation) location).getLocation(), newLines);
	}

	@Override
	public boolean hasPermission(String name) {
		return getHandled().hasPermission(name);
	}

	@Override
	public Set<String> getPermissions() {
		return getHandled().getEffectivePermissions().stream().map(PermissionAttachmentInfo::getPermission).collect(Collectors.toSet());
	}

	@Override
	public void sendMessage(String message) {
		getHandled().sendMessage(message);
	}


	@Override
	public void sendMessage(WSText text) {
		getHandled().spigot().sendMessage(((SpigotText) text).getHandled());
	}


	@Override
	public void sendMessages(String... messages) {
		for (String message : messages) sendMessage(message);
	}


	@Override
	public void sendMessages(WSText... texts) {
		for (WSText text : texts) sendMessage(text);
	}

	@Override
	public void performCommand(String command) {
		Bukkit.dispatchCommand(getHandled(), command);
	}


	@Override
	public boolean isBanned() {
		return getHandled().isBanned();
	}

	@Override
	public boolean isWhitelisted() {
		return getHandled().isWhitelisted();
	}

	@Override
	public void setWhitelisted(boolean whitelisted) {
		getHandled().setWhitelisted(true);
	}

	@Override
	public Optional<WSPlayer> getPlayer() {
		return Optional.of(this);
	}

	@Override
	public long getFirstPlayed() {
		return getHandled().getFirstPlayed();
	}

	@Override
	public long getLastPlayed() {
		return getHandled().getLastPlayed();
	}

	@Override
	public boolean hasPlayedBefore() {
		return getHandled().hasPlayedBefore();
	}


	@Override
	public Player getHandled() {
		return (Player) super.getHandled();
	}

	public void setHandled(Object object) {
		try {
			Field field = SpigotEntity.class.getDeclaredField("entity");
			field.setAccessible(true);
			field.set(this, object);
			field.setAccessible(false);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getPing() {
		try {
			Object player = HandledUtils.getPlayerHandle(this.getHandled());
			Field ping = player.getClass().getDeclaredField("ping");
			ping.setAccessible(true);
			return ping.getInt(player);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
