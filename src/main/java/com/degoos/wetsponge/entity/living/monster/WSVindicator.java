package com.degoos.wetsponge.entity.living.monster;

public interface WSVindicator extends WSMonster {

    /**
     * Returns true if the vindicator is a "Jonny" vindicator. Due to Spigot doesn't have implemented this method, it will return false on this server type.
     *
     * @return if the blaze is a "Jonny" vindicator.
     */
    boolean isJonny();

    /**
     * Sets if the vindicator is a "Jonny" vindicator. Due to Spigot doesn't have implemented this method, it won't apply any change on this server type.
     *
     * @param jonny the boolean.
     */
    void setJonny(boolean jonny);

}
