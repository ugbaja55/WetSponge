package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Chicken.
 */
public interface WSChicken extends WSAnimal {

	/**
	 * Returns the time until the next egg of the {@link WSChicken chicken} in ticks.
	 *
	 * @return the time until the next egg.
	 */
	int getTimeUntilNextEgg();

	/**
	 * Sets the time until the next egg of the {@link WSChicken chicken} in ticks.
	 *
	 * @param timeUntilNextEgg the time until the next egg.
	 */
	void setTimeUntilNextEgg(int timeUntilNextEgg);

}
