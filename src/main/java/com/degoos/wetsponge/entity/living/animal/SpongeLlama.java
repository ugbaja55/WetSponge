package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumLlamaType;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.passive.AbstractChestHorse;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.LlamaVariant;
import org.spongepowered.api.data.type.LlamaVariants;
import org.spongepowered.api.entity.living.animal.Llama;

public class SpongeLlama extends SpongeAbstractHorse implements WSLlama {


	public SpongeLlama(Llama entity) {
		super(entity);
	}


	@Override
	public boolean isTamed() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty()).isPresent();
	}


	@Override
	public Optional<UUID> getTamer() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty());
	}


	@Override
	public void setTamer(UUID tamer) {
		getHandled().offer(Keys.TAMED_OWNER, Optional.ofNullable(tamer));
	}

	@Override
	public EnumLlamaType getLlamaType() {
		return EnumLlamaType.getByName(getHandled().llamaVariant().get().getName()).orElse(EnumLlamaType.CREAMY);
	}

	@Override
	public boolean hasChest() {
		return ((AbstractChestHorse) getHandled()).hasChest();
	}

	@Override
	public void setChested(boolean chested) {
		((AbstractChestHorse) getHandled()).setChested(chested);
	}

	@Override
	public void setLlamaType(EnumLlamaType llamaType) {
		getHandled().offer(Keys.LLAMA_VARIANT, Sponge.getRegistry().getType(LlamaVariant.class, llamaType.name()).orElse(LlamaVariants.CREAMY));
	}

	@Override
	public WSInventory getInventory() {
		return new SpongeInventory(getHandled().getInventory());
	}

	@Override
	public Llama getHandled() {
		return (Llama) super.getHandled();
	}

}
