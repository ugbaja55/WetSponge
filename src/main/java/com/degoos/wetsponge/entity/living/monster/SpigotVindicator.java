package com.degoos.wetsponge.entity.living.monster;

import org.bukkit.entity.Vindicator;

public class SpigotVindicator extends SpigotMonster implements WSVindicator {

    public SpigotVindicator(Vindicator entity) {
        super(entity);
    }

    @Override
    public boolean isJonny() {
        return false;
    }

    @Override
    public void setJonny(boolean jonny) {

    }

    @Override
    public Vindicator getHandled() {
        return (Vindicator) super.getHandled();
    }


}
