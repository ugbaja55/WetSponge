package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Zombie Horse.
 */
public interface WSZombieHorse extends WSAbstractHorse {}
