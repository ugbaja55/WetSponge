package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.entity.vehicle.WSVehicle;

/**
 * Represents a Pig.
 */
public interface WSPig extends WSAnimal, WSVehicle {

	/**
	 * @return true if the pig has a saddle.
	 */
	boolean isSaddled ();

	/**
	 * Sets a saddle to the pig if the boolean is true.
	 * @param saddled the boolean.
	 */
	void setSaddled (boolean saddled);

}
