package com.degoos.wetsponge.entity.living.monster;


import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Blaze;

public class SpongeBlaze extends SpongeMonster implements WSBlaze {


    public SpongeBlaze(Blaze entity) {
        super(entity);
    }


    @Override
    public boolean isAflame() {
        return getHandled().aflame().get();
    }

    @Override
    public void setAflame(boolean aflame) {
        getHandled().offer(Keys.IS_AFLAME, aflame);
    }

    @Override
    public Blaze getHandled() {
        return (Blaze) super.getHandled();
    }
}
