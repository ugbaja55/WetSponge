package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumRabbitType;

import java.util.Optional;

/**
 * Represents a Rabbit.
 */
public interface WSRabbit extends WSAnimal {

	/**
	 * @return the rabbit type of this rabbit.
	 */
	Optional<EnumRabbitType> getRabbitType ();

	/**
	 * Sets the rabbit type of this rabbit.
	 * @param rabbitType the rabbit type.
	 */
	void setRabbitType (EnumRabbitType rabbitType);

}
