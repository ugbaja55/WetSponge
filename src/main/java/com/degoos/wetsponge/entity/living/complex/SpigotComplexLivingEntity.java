package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import java.util.Set;
import java.util.stream.Collectors;
import org.bukkit.entity.ComplexLivingEntity;

public class SpigotComplexLivingEntity extends SpigotLivingEntity implements WSComplexLivingEntity {


	public SpigotComplexLivingEntity(ComplexLivingEntity entity) {
		super(entity);
	}

	@Override
	public Set<? extends WSComplexLivingEntityPart> getParts() {
		return getHandled().getParts().stream().map(SpigotEntityParser::getWSEntity).map(entity -> (WSComplexLivingEntityPart) entity).collect(Collectors.toSet());
	}

	@Override
	public ComplexLivingEntity getHandled() {
		return (ComplexLivingEntity) super.getHandled();
	}
}
