package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import org.bukkit.entity.Squid;

public class SpigotSquid extends SpigotLivingEntity implements WSSquid {


    public SpigotSquid(Squid entity) {
        super(entity);
    }


    @Override
    public Squid getHandled() {
        return (Squid) super.getHandled();
    }
}
