package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.entity.vehicle.WSVehicle;

/**
 * Represents a horse, a donkey, a skeleton horse or a zombie horse.
 */
public interface WSAbstractHorse extends WSAnimal, WSVehicle, WSTameable {

	/**
	 * Returns the domestication level.
	 *
	 * @return the domestication level.
	 */
	int getDomestication();

	/**
	 * Sets the domestication level.
	 *
	 * @param domestication the domestication level.
	 */
	void setDomestication(int domestication);

	/**
	 * Returns the maximum domestication level.
	 *
	 * @return the maximum domestication level.
	 */
	int getMaxDomestication();

	/**
	 * Sets the maximum domestication level.
	 *
	 * @param maxDomestication the maximum domestication level.
	 */
	void setMaxDomestication(int maxDomestication);

	/**
	 * Returns the jump Strength. In Vanilla its maximum value is 1.0D.
	 *
	 * @return the jump Strength.
	 */
	double getJumpStrength();

	/**
	 * Sets the jump Strength. In Vanilla its maximum value is 1.0D.
	 *
	 * @param jumpStrength the jump Strength.
	 */
	void setJumpStrength(double jumpStrength);

	boolean hasSaddle();

	void setSaddle(boolean saddle);

}
