package com.degoos.wetsponge.entity.living.ambient;


import org.spongepowered.api.entity.living.Bat;

public class SpongeBat extends SpongeAmbient implements WSBat {


	public SpongeBat (Bat entity) {
		super(entity);
	}


	@Override
	public boolean isAwake () {
		return false;
	}


	@Override
	public void setAwake (boolean awake) {
	}


	@Override
	public Bat getHandled () {
		return (Bat) super.getHandled();
	}

}
