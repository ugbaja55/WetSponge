package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.entity.Chicken;

public class SpigotChicken extends SpigotAnimal implements WSChicken {

	public SpigotChicken(Chicken entity) {
		super(entity);
	}

	@Override
	public int getTimeUntilNextEgg() {
		try {
			return ReflectionUtils.getFirstField(NMSUtils.getNMSClass("EntityChicken"), int.class).getInt(HandledUtils.getEntityHandle(getHandled()));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the time until next egg of a chicken!");
			return 0;
		}
	}

	@Override
	public void setTimeUntilNextEgg(int timeUntilNextEgg) {
		try {
			ReflectionUtils.getFirstField(NMSUtils.getNMSClass("EntityChicken"), int.class).setInt(HandledUtils.getEntityHandle(getHandled()), timeUntilNextEgg);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the time until next egg of a chicken!");
		}
	}

	@Override
	public Chicken getHandled() {
		return (Chicken) super.getHandled();
	}
}
