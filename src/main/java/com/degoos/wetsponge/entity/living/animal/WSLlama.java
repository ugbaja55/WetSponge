package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumLlamaType;
import com.degoos.wetsponge.inventory.WSInventory;

/**
 * Represents a Llama.
 */
public interface WSLlama extends WSChestedHorse {

	/**
	 * Returns the {@link EnumLlamaType llama type} of the {@link WSLlama llama}.
	 *
	 * @return the {@link EnumLlamaType llama type}.
	 */
	EnumLlamaType getLlamaType();

	/**
	 * Sets the {@link EnumLlamaType llama type} of the {@link WSLlama llama}.
	 *
	 * @param type the {@link EnumLlamaType llama type}.
	 */
	void setLlamaType(EnumLlamaType type);

	/**
	 * Returns the {@link WSInventory inventory} of the {@link WSLlama llama}.
	 *
	 * @return the {@link WSInventory inventory}.
	 */
	WSInventory getInventory();
}
