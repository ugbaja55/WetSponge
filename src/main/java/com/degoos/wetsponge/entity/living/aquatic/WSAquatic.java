package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.living.WSLivingEntity;

/**
 * Represents an aquatic entity. In Vanilla only the Squid extends this class.
 */
public interface WSAquatic extends WSLivingEntity {
}
