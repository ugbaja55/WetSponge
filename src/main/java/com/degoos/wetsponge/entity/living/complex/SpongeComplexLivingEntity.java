package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import java.util.Set;
import java.util.stream.Collectors;
import org.spongepowered.api.entity.living.complex.ComplexLiving;

public class SpongeComplexLivingEntity extends SpongeLivingEntity implements WSComplexLivingEntity {


	public SpongeComplexLivingEntity(ComplexLiving entity) {
		super(entity);
	}

	@Override
	public Set<? extends WSComplexLivingEntityPart> getParts() {
		return getHandled().getParts().stream().map(SpongeEntityParser::getWSEntity).map(entity -> (WSComplexLivingEntityPart) entity).collect(Collectors.toSet());
	}

	@Override
	public ComplexLiving getHandled() {
		return (ComplexLiving) super.getHandled();
	}
}
