package com.degoos.wetsponge.entity.living.monster;

import org.bukkit.entity.Blaze;

public class SpigotBlaze extends SpigotMonster implements WSBlaze {


    public SpigotBlaze(Blaze entity) {
        super(entity);
    }


    @Override
    public boolean isAflame() {
        return false;
    }

    @Override
    public void setAflame(boolean aflame) {
    }


    @Override
    public Blaze getHandled() {
        return (Blaze) super.getHandled();
    }
}
