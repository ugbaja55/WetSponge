package com.degoos.wetsponge.entity.living.player;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.WSSPacketHeldItemChange;
import com.degoos.wetsponge.particle.WSParticle;
import com.degoos.wetsponge.scoreboard.SpongeScoreboard;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.WSTitle;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3f;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.DisplayNameData;
import org.spongepowered.api.data.manipulator.mutable.entity.JoinData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameMode;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.resourcepack.ResourcePacks;
import org.spongepowered.api.service.ban.BanService;
import org.spongepowered.api.service.whitelist.WhitelistService;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.text.title.Title;
import org.spongepowered.common.text.SpongeTexts;

public class SpongePlayer extends SpongeLivingEntity implements WSPlayer {


	private Map<WSLocation, WSBlockType> fakeBlocks;

	public SpongePlayer(Player entity) {
		super(entity);
		fakeBlocks = new HashMap<>();
	}


	@Override
	public String getName() {
		return getHandled().getName();
	}

	@Override
	public UUID getUniqueId() {
		return getHandled().getUniqueId();
	}

	@Override
	public WSText getDisplayedName() {
		return SpongeText.of(getHandled().getDisplayNameData().displayName().get());
	}


	@Override
	public void setDisplayedName(WSText displayedName) {
		DisplayNameData data = getHandled().getDisplayNameData();
		data.set(Keys.DISPLAY_NAME, ((SpongeText) displayedName).getHandled());
		getHandled().offer(data);
	}


	@Override
	public int getFoodLevel() {
		return getHandled().foodLevel().get();
	}


	@Override
	public void setFoodLevel(int foodLevel) {
		getHandled().offer(Keys.FOOD_LEVEL, foodLevel);
	}


	@Override
	public void spawnParticle(WSLocation location, float speed, int amount, WSParticle particle) {
		particle.spawnParticle(location, speed, amount, this);
	}


	@Override
	public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSParticle particle) {
		particle.spawnParticle(location, speed, amount, radius, this);
	}


	@Override
	public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
		Optional<ItemStack> itemStack;
		switch (type) {
			case HELMET:
				itemStack = getHandled().getHelmet();
				break;
			case CHESTPLATE:
				itemStack = getHandled().getChestplate();
				break;
			case LEGGINGS:
				itemStack = getHandled().getLeggings();
				break;
			case BOOTS:
				itemStack = getHandled().getBoots();
				break;
			case MAIN_HAND:
				itemStack = getHandled().getItemInHand(HandTypes.MAIN_HAND);
				break;
			case OFF_HAND:
				itemStack = getHandled().getItemInHand(HandTypes.OFF_HAND);
				break;
			default:
				itemStack = Optional.empty();
		}
		return itemStack.map(SpongeItemStack::new);
	}


	@Override
	public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
		switch (type) {
			case HELMET:
				getHandled().setHelmet(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case CHESTPLATE:
				getHandled().setChestplate(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case LEGGINGS:
				getHandled().setLeggings(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case BOOTS:
				getHandled().setBoots(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case MAIN_HAND:
				getHandled().setItemInHand(HandTypes.MAIN_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case OFF_HAND:
				getHandled().setItemInHand(HandTypes.OFF_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
		}
	}


	@Override
	public WSInventory getInventory() {
		return new SpongeInventory(getHandled().getInventory());
	}

	@Override
	public WSInventory getEnderChestInventory() {
		return new SpongeInventory(getHandled().getEnderChestInventory());
	}

	@Override
	public Optional<WSInventory> getOpenInventory() {
		return getHandled().getOpenInventory().map(inventory -> new SpongeInventory(inventory));
	}

	@Override
	public void openInventory(WSInventory inventory) {
		if (inventory == null) closeInventory();
		else getHandled().openInventory(((SpongeInventory) inventory).getHandled());
	}

	@Override
	public void closeInventory() {
		getHandled().closeInventory();
	}

	@Override
	public EnumGameMode getGameMode() {
		return EnumGameMode.valueOf(getHandled().gameMode().get().getName().toUpperCase());
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {
		getHandled().offer(Keys.GAME_MODE, Sponge.getRegistry().getType(GameMode.class, gameMode.name()).orElse(GameModes.SURVIVAL));
	}

	@Override
	public void kick() {
		getHandled().kick();
	}

	@Override
	public void kick(WSText message) {
		getHandled().kick(((SpongeText) message).getHandled());
	}

	@Override
	public void simulateMessage(WSText message) {
		getHandled().simulateChat(((SpongeText) message).getHandled(), Cause.of(EventContext.empty(), SpongeWetSponge.getInstance()));
	}

	@Override
	public WSScoreboard getScoreboard() {
		return new SpongeScoreboard(getHandled().getScoreboard());
	}

	@Override
	public void setScoreboard(WSScoreboard scoreboard) {
		getHandled().setScoreboard(((SpongeScoreboard) scoreboard).getHandled());
	}

	@Override
	public WSGameProfile getProfile() {
		return new SpongeGameProfile(getHandled().getProfile());
	}

	@Override
	public void sendTitle(WSTitle title) {
		getHandled().sendTitle(Title.builder().title(title.getTitle().map(text -> ((SpongeText) text).getHandled()).orElse(null))
			.subtitle(title.getSubtitle().map(text -> ((SpongeText) text).getHandled()).orElse(null))
			.actionBar(title.getActionBar().map(text -> ((SpongeText) text).getHandled()).orElse(null)).fadeIn(title.getFadeIn().orElse(null))
			.stay(title.getStay().orElse(null)).fadeOut(title.getFadeOut().orElse(null)).clear(title.isClear()).reset(title.isReset()).build());
	}

	@Override
	public void setResourcePack(URI uri) {
		getHandled().sendResourcePack(ResourcePacks.fromUriUnchecked(uri));
	}

	@Override
	public String getLanguageCode() {
		return getHandled().getLocale().toString();
	}

	@Override
	public boolean isOnline() {
		return getHandled().isOnline();
	}

	@Override
	public float getExperience() {
		return (float) getHandled().get(Keys.EXPERIENCE_SINCE_LEVEL).orElse(0) / (float) getHandled().get(Keys.EXPERIENCE_FROM_START_OF_LEVEL).orElse(1);
	}

	@Override
	public void setExperience(float experience) {
		getHandled().offer(Keys.EXPERIENCE_SINCE_LEVEL, (int) (experience * getHandled().get(Keys.EXPERIENCE_FROM_START_OF_LEVEL).orElse(1)));
	}

	@Override
	public int getExperienceLevel() {
		return getHandled().get(Keys.EXPERIENCE_LEVEL).orElse(0);
	}

	@Override
	public void setExperienceLevel(int experienceLevel) {
		getHandled().offer(Keys.EXPERIENCE_LEVEL, experienceLevel);
	}

	@Override
	public double getWalkingSpeed() {
		return getHandled().get(Keys.WALKING_SPEED).orElse(0D);
	}

	@Override
	public void setWalkingSpeed(double walkingSpeed) {
		getHandled().offer(Keys.WALKING_SPEED, walkingSpeed);
	}

	@Override
	public double getFlyingSpeed() {
		return getHandled().get(Keys.FLYING_SPEED).orElse(0D);
	}

	@Override
	public void setFlyingSpeed(double flyingSpeed) {
		getHandled().offer(Keys.FLYING_SPEED, flyingSpeed);
	}

	@Override
	public boolean canFly() {
		return getHandled().get(Keys.CAN_FLY).orElse(false);
	}

	@Override
	public void setCanFly(boolean canFly) {
		getHandled().offer(Keys.CAN_FLY, canFly);
	}

	@Override
	public boolean isFlying() {
		return getHandled().get(Keys.IS_FLYING).orElse(false);
	}

	@Override
	public void setFlying(boolean flying) {
		getHandled().offer(Keys.IS_FLYING, flying);
	}

	@Override
	public void addFakeBlock(WSLocation location, WSBlockType material) {
		fakeBlocks.put(location.getBlockLocation(), material);
		if (getWorld().equals(location.getWorld())) {
			BlockType type = Sponge.getRegistry().getType(BlockType.class, material.getStringId()).orElse(BlockTypes.AIR);
			BlockState state = type.getDefaultState();
			getHandled().sendBlockChange(location.toVector3i(), SpongeDataValueConverter.update(material, state));
		}
	}

	@Override
	public Optional<WSBlockType> getFakeBlock(WSLocation location) {
		return Optional.ofNullable(fakeBlocks.get(location.getBlockLocation()));
	}

	@Override
	public Map<WSLocation, WSBlockType> getFakeBlocks() {
		Map<WSLocation, WSBlockType> map = new HashMap<>();
		fakeBlocks.forEach((location, type) -> map.put(location.clone(), type.clone()));
		return map;
	}

	@Override
	public boolean containsFakeBlock(WSLocation location) {
		return fakeBlocks.containsKey(location.getBlockLocation());
	}

	@Override
	public void refreshFakeBlock(WSLocation location) {
		if (!getWorld().equals(location.getWorld())) return;
		WSLocation blockLocation = location.getBlockLocation();
		if (fakeBlocks.containsKey(blockLocation)) {
			addFakeBlock(blockLocation, fakeBlocks.get(blockLocation));
		}
	}

	@Override
	public void removeFakeBlock(WSLocation location) {
		WSLocation blockLocation = location.getBlockLocation();
		fakeBlocks.remove(blockLocation);
		if (location.getWorld().equals(getWorld())) getHandled().resetBlockChange(blockLocation.toVector3i());
	}

	@Override
	public void clearFakeBlocks() {
		fakeBlocks.keySet().stream().filter(location -> location.getWorld().equals(getWorld())).forEach(location -> getHandled().resetBlockChange(location.toVector3i
			()));
		fakeBlocks.clear();
	}

	@Override
	public void sendPacket(WSPacket packet) {
		packet.update();
		((EntityPlayerMP) getHandled()).connection.sendPacket(((SpongePacket) packet).getHandler());
	}

	@Override
	public Optional<WSItemStack> getItemOnCursor() {
		ItemStack itemStack = (ItemStack) (Object) ((EntityPlayerMP) getHandled()).inventory.getItemStack();
		if (itemStack == null || itemStack.getItem().getId().equals(ItemTypes.AIR.getId())) return Optional.empty();
		return Optional.of(new SpongeItemStack(itemStack));
	}

	@Override
	public void setItemOnCursor(WSItemStack itemOnCursor) {
		if (itemOnCursor == null) ((EntityPlayerMP) getHandled()).inventory.setItemStack(net.minecraft.item.ItemStack.EMPTY);
		else ((EntityPlayerMP) getHandled()).inventory.setItemStack((net.minecraft.item.ItemStack) (Object) ((SpongeItemStack) itemOnCursor).getHandled());
		((EntityPlayerMP) getHandled()).updateHeldItem();
	}

	@Override
	public int getSelectedHotbarSlot() {
		return ((EntityPlayerMP) getHandled()).inventory.currentItem;
	}

	@Override
	public void setSelectedHotbarSlot(int slot) {
		Validate.isTrue(slot >= 0 && slot < 9, "Slot is not between 0 and 8 inclusive");
		((EntityPlayerMP) getHandled()).inventory.currentItem = slot;
		sendPacket(WSSPacketHeldItemChange.of(slot));
	}

	@Override
	public void sendSignChange(WSLocation location, WSText[] lines) {
		Validate.notNull(location, "Location cannot be null!");
		Validate.notNull(lines, "Lines cannot be null!");
		Validate.isTrue(lines.length > 3, "Lines length must be 4!");
		EntityPlayerMP playerMP = (EntityPlayerMP) getHandled();
		if (playerMP.connection != null) {
			TileEntitySign signTileEntity = new TileEntitySign();
			signTileEntity.setPos(new BlockPos(getLocation().getX(), getLocation().getY(), getLocation().getZ()));
			ITextComponent[] components = new ITextComponent[4];
			for (int i = 0; i < 4; i++)
				components[i] = SpongeTexts.toComponent(((SpongeText) lines[i]).getHandled());
			System.arraycopy(components, 0, signTileEntity.signText, 0, signTileEntity.signText.length);
			playerMP.connection.sendPacket(signTileEntity.getUpdatePacket());
		}
	}

	@Override
	public boolean hasPermission(String name) {
		return getHandled().hasPermission(name);
	}

	@Override
	public Set<String> getPermissions() {
		//todo: get player permissions set
		return new HashSet<>();
	}

	@Override
	public void sendMessage(String message) {
		getHandled().sendMessage(TextSerializers.LEGACY_FORMATTING_CODE.deserialize(message));
	}


	@Override
	public void sendMessage(WSText text) {
		getHandled().sendMessage(((SpongeText) text).getHandled());
	}


	@Override
	public void sendMessages(String... messages) {
		for (String message : messages) sendMessage(message);
	}


	@Override
	public void sendMessages(WSText... texts) {
		for (WSText text : texts) sendMessage(text);
	}

	@Override
	public void performCommand(String command) {
		Sponge.getGame().getCommandManager().process(getHandled(), command);
	}

	@Override
	public boolean isBanned() {
		return Sponge.getGame().getServiceManager().provideUnchecked(BanService.class).isBanned(getHandled().getProfile());
	}


	@Override
	public boolean isWhitelisted() {
		return Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).isWhitelisted(getHandled().getProfile());
	}


	@Override
	public void setWhitelisted(boolean whitelisted) {
		if (whitelisted) Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).addProfile(getHandled().getProfile());
		else Sponge.getGame().getServiceManager().provideUnchecked(WhitelistService.class).removeProfile(getHandled().getProfile());
	}


	@Override
	public Optional<WSPlayer> getPlayer() {
		return Optional.of(this);
	}


	@Override
	public long getFirstPlayed() {
		Optional<JoinData> optional = getHandled().get(JoinData.class);
		return optional.map(joinData -> joinData.firstPlayed().get().toEpochMilli()).orElse((long) 0);
	}


	@Override
	public long getLastPlayed() {
		Optional<JoinData> optional = getHandled().get(JoinData.class);
		return optional.map(joinData -> joinData.lastPlayed().get().toEpochMilli()).orElse((long) 0);
	}


	@Override
	public boolean hasPlayedBefore() {
		return getHandled().get(JoinData.class).isPresent();
	}


	@Override
	public Player getHandled() {
		return (Player) super.getHandled();
	}

	public void setHandled(Object object) {
		try {
			Field field = SpongeEntity.class.getDeclaredField("entity");
			field.setAccessible(true);
			field.set(this, object);
			field.setAccessible(false);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getPing() {
		return ((EntityPlayerMP) this.getHandled()).ping;
	}
}
