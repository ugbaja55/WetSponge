package com.degoos.wetsponge.entity.living.aerial;

import com.degoos.wetsponge.entity.living.WSLivingEntity;

/**
 * Represents an aerial entity.
 */
public interface WSAerial extends WSLivingEntity {}
