package com.degoos.wetsponge.entity.living.animal;


import org.bukkit.entity.Pig;

public class SpigotPig extends SpigotAnimal implements WSPig {


	public SpigotPig (Pig entity) {
		super(entity);
	}


	@Override
	public boolean isSaddled () {
		return getHandled().hasSaddle();
	}


	@Override
	public void setSaddled (boolean saddled) {
		getHandled().setSaddle(saddled);
	}


	@Override
	public Pig getHandled () {
		return (Pig) super.getHandled();
	}
}
