package com.degoos.wetsponge.entity.living.monster;


public interface WSSpider extends WSMonster {


    /**
     * Returns true if the spider is climbing. Due to Spigot doesn't have implemented this method, it will return false on this server type.
     * @return true if the blaze is climbing.
     */
    boolean isClimbing ();

}
