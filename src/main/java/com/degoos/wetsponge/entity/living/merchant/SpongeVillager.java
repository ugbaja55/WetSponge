package com.degoos.wetsponge.entity.living.merchant;

import com.degoos.wetsponge.entity.living.SpongeAgeable;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumVillagerProfession;
import com.degoos.wetsponge.merchant.SpongeTrade;
import com.degoos.wetsponge.merchant.WSTrade;
import com.degoos.wetsponge.parser.player.PlayerParser;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.Career;
import org.spongepowered.api.entity.living.Villager;
import org.spongepowered.api.item.merchant.TradeOffer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeVillager extends SpongeAgeable implements WSVillager {


    public SpongeVillager(Villager entity) {
        super(entity);
    }

    @Override
    public EnumVillagerProfession getProfession() {
        return EnumVillagerProfession.getBySpongeName(getHandled().career().get().getName()).orElseThrow(NullPointerException::new);
    }

    @Override
    public void setProfession(EnumVillagerProfession profession) {
        getHandled().offer(Keys.CAREER, Sponge.getRegistry().getType(Career.class, profession.getSpongeName()).<NullPointerException>orElseThrow(NullPointerException::new));
    }

    @Override
    public Optional<WSPlayer> getCustomer() {
        return getHandled().getCustomer().map(target -> PlayerParser.getPlayer(target.getUniqueId()).orElse(null)).filter(Objects::nonNull);
    }

    @Override
    public void setCustomer(WSPlayer player) {
        if (player == null) getHandled().setCustomer(null);
        else {
            ((EntityVillager) getHandled()).processInteract((EntityPlayer) player.getHandled(), EnumHand.MAIN_HAND);
            getHandled().setCustomer(((SpongePlayer) player).getHandled());
        }
    }

    @Override
    public List<WSTrade> getTrades() {
        return getHandled().get(Keys.TRADE_OFFERS).orElse(new ArrayList<>()).stream().map(SpongeTrade::new).collect(Collectors.toList());
    }

    @Override
    public void setTrades(List<WSTrade> trades) {
        getHandled().offer(Keys.TRADE_OFFERS, trades.stream().map(trade -> ((SpongeTrade) trade).getHandled()).collect(Collectors.toList()));
    }

    @Override
    public void addTrade(WSTrade trade) {
        List<TradeOffer> trades = getHandled().get(Keys.TRADE_OFFERS).orElse(new ArrayList<>());
        trades.add(((SpongeTrade) trade).getHandled());
        getHandled().offer(Keys.TRADE_OFFERS, trades);
    }

    @Override
    public void removeTrade(WSTrade trade) {
        getHandled().offer(Keys.TRADE_OFFERS, getHandled().get(Keys.TRADE_OFFERS).orElse(new ArrayList<>()).stream()
                .filter(target -> target.equals(((SpongeTrade) trade).getHandled())).collect(Collectors.toList()));
    }

    @Override
    public void clearTrades() {
        getHandled().offer(Keys.TRADE_OFFERS, new ArrayList<>());
    }

    @Override
    public Villager getHandled() {
        return (Villager) super.getHandled();
    }
}
