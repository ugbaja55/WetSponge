package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a chested horse. In Vanilla only the donkey extends this class.
 */
public interface WSChestedHorse extends WSAbstractHorse {

	/**
	 * Returns whether the {@link WSChestedHorse chested horse} has a chest.
	 *
	 * @return whether the {@link WSChestedHorse chested horse} has a chest.
	 */
	boolean hasChest();

	/**
	 * Sets whether the {@link WSChestedHorse chested horse} has a chest.
	 *
	 * @param chested whether the {@link WSChestedHorse chested horse} has a chest.
	 */
	void setChested(boolean chested);

}
