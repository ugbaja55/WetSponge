package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.entity.living.SpongeAgeable;
import org.spongepowered.api.entity.living.animal.Animal;

public class SpongeAnimal extends SpongeAgeable implements WSAnimal {


	public SpongeAnimal (Animal entity) {
		super(entity);
	}


	@Override
	public Animal getHandled () {
		return (Animal) super.getHandled();
	}
}
