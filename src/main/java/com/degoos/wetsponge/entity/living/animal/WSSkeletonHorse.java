package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Skeleton Horse.
 */
public interface WSSkeletonHorse extends WSAbstractHorse {}
