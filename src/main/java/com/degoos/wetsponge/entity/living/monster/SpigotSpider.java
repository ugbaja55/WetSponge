package com.degoos.wetsponge.entity.living.monster;


import org.bukkit.entity.Spider;

public class SpigotSpider extends SpigotMonster implements WSSpider {


    public SpigotSpider(Spider entity) {
        super(entity);
    }


    @Override
    public boolean isClimbing() {
        return false;
    }

    @Override
    public Spider getHandled() {
        return (Spider) super.getHandled();
    }
}
