package com.degoos.wetsponge.entity.living;

import com.flowpowered.math.vector.Vector3d;

public interface WSArmorStand extends WSLivingEntity, WSEquipable {

    Vector3d getHeadDirection();

    void setHeadDirection(Vector3d headDirection);

    Vector3d getBodyRotation();

    void setBodyRotation(Vector3d bodyRotation);

    Vector3d getLeftArmDirection();

    void setLeftArmDirection(Vector3d leftArmDirection);

    Vector3d getRightArmDirection();

    void setRightArmDirection(Vector3d rightArmDirection);

    Vector3d getLeftLegDirection();

    void setLeftLegDirection(Vector3d leftLegDirection);

    Vector3d getRightLegDirection();

    void setRightLegDirection(Vector3d rightLegDirection);

    boolean isMarker();

    void setMarker(boolean marker);

    boolean isSmall();

    void setSmall(boolean small);

    boolean hasBasePlate();

    void setBasePlate(boolean basePlate);

    boolean hasArms();

    void setArms(boolean arms);

    boolean hasGravity();

    void setGravity(boolean gravity);

}
