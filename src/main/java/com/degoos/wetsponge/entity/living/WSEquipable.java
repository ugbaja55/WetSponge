package com.degoos.wetsponge.entity.living;

import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.particle.WSParticle;

import java.util.Optional;

public interface WSEquipable {


    /**
     * Returns a new instance of the equipped {@link WSItemStack} of the entity. The handled ItemStack is also a new instance.
     *
     * @param type the equipment section.
     * @return a new instance of the {@link WSItemStack}, if found.
     */
    Optional<WSItemStack> getEquippedItem(EnumEquipType type);

    /**
     * Sets a new instance of the {@link WSItemStack} into the equipment section.
     * Any changes of the set {@link WSItemStack} after use this method won't be applied in the equipped item.
     *
     * @param type      the equipment section.
     * @param itemStack the {@link WSParticle} to set.
     */
    void setEquippedItem(EnumEquipType type, WSItemStack itemStack);
}
