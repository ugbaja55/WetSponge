package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumHorseArmorType;
import com.degoos.wetsponge.enums.EnumHorseColor;
import com.degoos.wetsponge.enums.EnumHorseStyle;
import com.degoos.wetsponge.inventory.SpigotInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.SpigotItemStack;
import org.bukkit.entity.Horse;

public class SpigotHorse extends SpigotAbstractHorse implements WSHorse {

	public SpigotHorse(Horse entity) {
		super(entity);
	}

	@Override
	public EnumHorseColor getHorseColor() {
		return EnumHorseColor.getByName(getHandled().getColor().name()).orElse(EnumHorseColor.WHITE);
	}

	@Override
	public void setHorseColor(EnumHorseColor horseColor) {
		getHandled().setColor(Horse.Color.valueOf(horseColor.name()));
	}

	@Override
	public EnumHorseStyle getHorseStyle() {
		return EnumHorseStyle.getByName(getHandled().getStyle().name()).orElse(EnumHorseStyle.NONE);
	}

	@Override
	public void setHorseStyle(EnumHorseStyle horseStyle) {
		getHandled().setStyle(Horse.Style.valueOf(horseStyle.name()));
	}

	@Override
	public EnumHorseArmorType getArmor() {
		return EnumHorseArmorType.getByMaterial(getHandled().getInventory().getArmor().getType().getId());
	}

	@Override
	public void setArmor(EnumHorseArmorType armor) {
		getHandled().getInventory().setArmor(((SpigotItemStack) armor.getArmorItemStack()).getHandled());
	}

	@Override
	public WSInventory getInventory() {
		return new SpigotInventory(getHandled().getInventory());
	}

	@Override
	public Horse getHandled() {
		return (Horse) super.getHandled();
	}
}
