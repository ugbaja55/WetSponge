package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import org.bukkit.entity.ComplexEntityPart;

public class SpigotComplexLivingEntityPart extends SpigotEntity implements WSComplexLivingEntityPart {


	public SpigotComplexLivingEntityPart(ComplexEntityPart entity) {
		super(entity);
	}

	@Override
	public WSComplexLivingEntity getParent() {
		return (WSComplexLivingEntity) SpigotEntityParser.getWSEntity(getHandled().getParent());
	}

	@Override
	public ComplexEntityPart getHandled() {
		return (ComplexEntityPart) super.getHandled();
	}
}
