package com.degoos.wetsponge.entity.living;


import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.entity.AgeableData;
import org.spongepowered.api.entity.living.Ageable;

public class SpongeAgeable extends SpongeCreature implements WSAgeable {


	public SpongeAgeable (Ageable entity) {
		super(entity);
	}


	@Override
	public int getAge () {
		return getHandled().age().get();
	}


	@Override
	public void setAge (int age) {
		AgeableData ageableData = getHandled().getAgeData();
		ageableData.set(Keys.AGE, age);
		getHandled().offer(ageableData);
	}


	@Override
	public boolean isBaby () {
		return !getHandled().adult().get();
	}


	@Override
	public void setBaby (boolean baby) {
		setAge(-24000);
	}


	@Override
	public boolean isAdult () {
		return getHandled().getAgeData().adult().get();
	}


	@Override
	public void setAdult (boolean adult) {
		setAge(0);
	}


	@Override
	public Ageable getHandled () {
		return (Ageable) super.getHandled();
	}
}
