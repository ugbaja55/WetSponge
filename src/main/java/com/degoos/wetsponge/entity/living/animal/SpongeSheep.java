package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.animal.Sheep;

import java.util.Optional;

public class SpongeSheep extends SpongeAnimal implements WSSheep {


	public SpongeSheep (Sheep entity) {
		super(entity);
	}


	@Override
	public Optional<EnumDyeColor> getColor () {
		return EnumDyeColor.getByName(getHandled().color().get().getName());
	}


	@Override
	public void setColor (EnumDyeColor color) {
		Validate.notNull(color, "WSColor cannot be null!");
		getHandled().offer(Keys.DYE_COLOR, getDyeColor(color));
	}


	@Override
	public boolean isSheared () {
		Optional<Boolean> optional = getHandled().get(Keys.IS_SHEARED);
		return optional.orElse(false);
	}


	@Override
	public void setSheared (boolean sheared) {
		getHandled().offer(Keys.IS_SHEARED, sheared);
	}


	@Override
	public Sheep getHandled () {
		return (Sheep) super.getHandled();
	}


	private org.spongepowered.api.data.type.DyeColor getDyeColor (EnumDyeColor color) {
		switch (color) {
			case WHITE:
				return DyeColors.WHITE;
			case ORANGE:
				return DyeColors.ORANGE;
			case MAGENTA:
				return DyeColors.MAGENTA;
			case LIGHT_BLUE:
				return DyeColors.LIGHT_BLUE;
			case YELLOW:
				return DyeColors.YELLOW;
			case LIME:
				return DyeColors.LIME;
			case PINK:
				return DyeColors.PINK;
			case GRAY:
				return DyeColors.GRAY;
			case SILVER:
				return DyeColors.SILVER;
			case CYAN:
				return DyeColors.CYAN;
			case PURPLE:
				return DyeColors.PURPLE;
			case BLUE:
				return DyeColors.BLUE;
			case BROWN:
				return DyeColors.BROWN;
			case GREEN:
				return DyeColors.GREEN;
			case RED:
				return DyeColors.RED;
			case BLACK:
				return DyeColors.BLACK;
			default:
				return DyeColors.WHITE;
		}
	}
}
