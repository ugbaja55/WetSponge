package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.WSEntity;

public interface WSComplexLivingEntityPart extends WSEntity {

	WSComplexLivingEntity getParent();

}
