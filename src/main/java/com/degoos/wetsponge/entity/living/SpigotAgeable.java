package com.degoos.wetsponge.entity.living;


import org.bukkit.entity.Ageable;

public class SpigotAgeable extends SpigotCreature implements WSAgeable {


	public SpigotAgeable (Ageable entity) {
		super(entity);
	}


	@Override
	public int getAge () {
		return getHandled().getAge();
	}


	@Override
	public void setAge (int age) {
		getHandled().setAge(age);
	}


	@Override
	public boolean isBaby () {
		return !isAdult();
	}


	@Override
	public void setBaby (boolean baby) {
		if (baby) getHandled().setBaby();
		else setAdult(true);
	}


	@Override
	public boolean isAdult () {
		return getHandled().isAdult();
	}


	@Override
	public void setAdult (boolean adult) {
		if (adult) getHandled().setAdult();
		else setBaby(true);
	}


	@Override
	public Ageable getHandled () {
		return (Ageable) super.getHandled();
	}
}
