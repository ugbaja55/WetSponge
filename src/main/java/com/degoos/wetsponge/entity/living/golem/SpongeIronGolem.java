package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.SpongeCreature;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.golem.IronGolem;

public class SpongeIronGolem extends SpongeCreature implements WSIronGolem {


    public SpongeIronGolem(IronGolem entity) {
        super(entity);
    }

    @Override
    public boolean isPlayerCreated() {
        return getHandled().playerCreated().get();
    }

    @Override
    public void setPlayerCreated(boolean playerCreated) {
        getHandled().offer(Keys.PLAYER_CREATED, playerCreated);
    }

    @Override
    public IronGolem getHandled() {
        return (IronGolem) super.getHandled();
    }


}
