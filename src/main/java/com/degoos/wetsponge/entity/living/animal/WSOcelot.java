package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumOcelotType;

/**
 * Represents a Ocelot.
 */
public interface WSOcelot extends WSAnimal, WSTameable, WSSittable {

	/**
	 * Returns the {@link EnumOcelotType ocelot type} of the {@link WSOcelot ocelot}.
	 *
	 * @return the {@link EnumOcelotType ocelot type}.
	 */
	EnumOcelotType getOcelotType();

	/**
	 * Sets the {@link EnumOcelotType ocelot type} of the {@link WSOcelot ocelot}.
	 *
	 * @param ocelotType the {@link EnumOcelotType ocelot type}.
	 */
	void setOcelotType(EnumOcelotType ocelotType);

}
