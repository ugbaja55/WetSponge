package com.degoos.wetsponge.entity.living.monster;

public interface WSBlaze extends WSMonster {

    /**
     * Returns true if the blaze is aflame. Due to Spigot doesn't have implemented this method, it will return false on this server type.
      * @return true if the blaze is aflame.
     */
    boolean isAflame ();

    /**
     * Sets if the blaze is aflame. Due to Spigot doesn't have implemented this method, it won't apply any change on this server type.
     * @param aflame the boolean.
     */
    void setAflame (boolean aflame);

}
