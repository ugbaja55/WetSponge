package com.degoos.wetsponge.entity.living.monster;


import java.util.Optional;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Creeper;

public class SpongeCreeper extends SpongeMonster implements WSCreeper {


	public SpongeCreeper(Creeper entity) {
		super(entity);
	}

	@Override
	public boolean isPowered() {
		return getHandled().get(Keys.CREEPER_CHARGED).orElse(false);
	}

	@Override
	public void setPowered(boolean powered) {
		getHandled().offer(Keys.CREEPER_CHARGED, powered);
	}


	@Override
	public int getFuseDuration() {
		return getHandled().getFuseData().fuseDuration().get();
	}

	@Override
	public void setFuseDuration(int fuseDuration) {
		getHandled().offer(Keys.FUSE_DURATION, fuseDuration);
	}

	@Override
	public int getTicksRemaining() {
		return getHandled().getFuseData().ticksRemaining().get();
	}

	@Override
	public void setTicksRemaining(int ticksRemaining) {
		getHandled().offer(Keys.TICKS_REMAINING, ticksRemaining);
	}

	@Override
	public boolean isPrimed() {
		return getHandled().isPrimed();
	}

	@Override
	public void prime() {
		getHandled().prime();
	}

	@Override
	public void defuse() {
		getHandled().defuse();
	}

	@Override
	public int setExplosionRadius() {
		return getHandled().explosionRadius().get().orElse(2);
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().offer(Keys.EXPLOSION_RADIUS, Optional.of(explosionRadius));
	}

	@Override
	public void detonate() {
		getHandled().detonate();
	}

	@Override
	public Creeper getHandled() {
		return (Creeper) super.getHandled();
	}
}
