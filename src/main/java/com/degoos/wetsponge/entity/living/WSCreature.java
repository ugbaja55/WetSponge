package com.degoos.wetsponge.entity.living;


public interface WSCreature extends WSAgent {

	/**
	 * Sets if the entity has its AI enabled.
	 */
	void setAI (boolean ai);

	/**
	 * @return true if the entity has its AI enabled.
	 */
	boolean hasAI ();

}
