package com.degoos.wetsponge.entity.living.ambient;


public interface WSBat extends WSAmbient {

	/**
	 * @return true if the bat is awake.
	 */
	boolean isAwake ();

	/**
	 * Sets if the bat is awake or not
	 *
	 * @param awake
	 * 		true to awake the bat
	 */
	void setAwake (boolean awake);

}
