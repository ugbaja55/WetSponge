package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.WSCreature;

/**
 * Represents a golem. In Vanilla it can be a Snow Golem or a Iron Golem.
 */
public interface WSGolem extends WSCreature {
}
