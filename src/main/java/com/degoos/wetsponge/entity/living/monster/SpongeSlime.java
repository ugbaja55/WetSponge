package com.degoos.wetsponge.entity.living.monster;


import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Slime;

public class SpongeSlime extends SpongeMonster implements WSSlime {


    public SpongeSlime(Slime entity) {
        super(entity);
    }


    @Override
    public int getSize() {
        return getHandled().slimeSize().get();
    }

    @Override
    public void setSize(int size) {
        getHandled().offer(Keys.SLIME_SIZE, size);
    }

    @Override
    public Slime getHandled() {
        return (Slime) super.getHandled();
    }
}
