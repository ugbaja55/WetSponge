package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.entity.explosive.WSFusedExplosive;

public interface WSCreeper extends WSMonster, WSFusedExplosive {

    /**
     * @return true if the creeper is powered.
     */
    boolean isPowered();

    /**
     * Sets if the creeper is powered.
     * @param powered the boolean.
     */
    void setPowered(boolean powered);

}
