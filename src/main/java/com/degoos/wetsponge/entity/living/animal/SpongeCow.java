package com.degoos.wetsponge.entity.living.animal;


import org.spongepowered.api.entity.living.animal.Cow;

public class SpongeCow extends SpongeAnimal implements WSCow {


	public SpongeCow (Cow entity) {
		super(entity);
	}


	@Override
	public Cow getHandled () {
		return (Cow) super.getHandled();
	}
}
