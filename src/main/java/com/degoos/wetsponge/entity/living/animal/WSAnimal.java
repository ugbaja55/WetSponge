package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.entity.living.WSAgeable;

/**
 * Represents an animal.
 */
public interface WSAnimal extends WSAgeable {}
