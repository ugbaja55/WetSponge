package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.living.WSLivingEntity;
import java.util.Set;
import org.spongepowered.api.entity.living.complex.ComplexLivingPart;

public interface WSComplexLivingEntity extends WSLivingEntity {

	Set<? extends WSComplexLivingEntityPart> getParts();

}
