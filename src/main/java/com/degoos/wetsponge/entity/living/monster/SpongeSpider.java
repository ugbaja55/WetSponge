package com.degoos.wetsponge.entity.living.monster;


import org.spongepowered.api.entity.living.monster.Spider;

public class SpongeSpider extends SpongeMonster implements WSSpider {


    public SpongeSpider(Spider entity) {
        super(entity);
    }


    @Override
    public boolean isClimbing() {
        return getHandled().isClimbing();
    }

    @Override
    public Spider getHandled() {
        return (Spider) super.getHandled();
    }
}
