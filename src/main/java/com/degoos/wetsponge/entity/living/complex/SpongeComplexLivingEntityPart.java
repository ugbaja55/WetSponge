package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import org.spongepowered.api.entity.living.complex.ComplexLivingPart;

public class SpongeComplexLivingEntityPart extends SpongeEntity implements WSComplexLivingEntityPart {


	public SpongeComplexLivingEntityPart(ComplexLivingPart entity) {
		super(entity);
	}

	@Override
	public WSComplexLivingEntity getParent() {
		return (WSComplexLivingEntity) SpongeEntityParser.getWSEntity(getHandled().getParent());
	}

	@Override
	public ComplexLivingPart getHandled() {
		return (ComplexLivingPart) super.getHandled();
	}
}
