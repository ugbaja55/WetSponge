package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import org.spongepowered.api.entity.living.Squid;

public class SpongeSquid extends SpongeLivingEntity implements WSSquid {


    public SpongeSquid(Squid entity) {
        super(entity);
    }


    @Override
    public Squid getHandled() {
        return (Squid) super.getHandled();
    }
}
