package com.degoos.wetsponge.entity.living.animal;

import org.spongepowered.api.entity.living.animal.ZombieHorse;

public class SpongeSkeletonHorse extends SpongeAbstractHorse implements WSSkeletonHorse {

	public SpongeSkeletonHorse(ZombieHorse entity) {
		super(entity);
	}

	@Override
	public ZombieHorse getHandled() {
		return (ZombieHorse) super.getHandled();
	}
}
