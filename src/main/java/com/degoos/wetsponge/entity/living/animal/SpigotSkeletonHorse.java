package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.SkeletonHorse;

public class SpigotSkeletonHorse extends SpigotAbstractHorse implements WSSkeletonHorse {

	public SpigotSkeletonHorse(SkeletonHorse entity) {
		super(entity);
	}

	@Override
	public SkeletonHorse getHandled() {
		return (SkeletonHorse) super.getHandled();
	}
}
