package com.degoos.wetsponge.entity.living.monster;


import com.degoos.wetsponge.entity.living.SpongeCreature;
import org.spongepowered.api.entity.living.monster.Monster;

public class SpongeMonster extends SpongeCreature implements WSMonster {


	public SpongeMonster (Monster entity) {
		super(entity);
	}


	@Override
	public Monster getHandled () {
		return (Monster) super.getHandled();
	}
}
