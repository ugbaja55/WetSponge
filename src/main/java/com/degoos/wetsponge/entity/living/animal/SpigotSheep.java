package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.entity.Sheep;

import java.util.Optional;

public class SpigotSheep extends SpigotAnimal implements WSSheep {


	public SpigotSheep (Sheep entity) {
		super(entity);
	}


	@Override
	public Optional<EnumDyeColor> getColor () {
		return EnumDyeColor.getByWoolData(getHandled().getColor().getWoolData());
	}


	@Override
	public void setColor (EnumDyeColor color) {
		Validate.notNull(color, "WSColor cannot be null!");
		getHandled().setColor(org.bukkit.DyeColor.getByWoolData(color.getWoolData()));
	}


	@Override
	public boolean isSheared () {
		return getHandled().isSheared();
	}


	@Override
	public void setSheared (boolean sheared) {
		getHandled().setSheared(sheared);
	}


	@Override
	public Sheep getHandled () {
		return (Sheep) super.getHandled();
	}
}
