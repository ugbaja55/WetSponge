package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.material.WSMaterial;
import org.spongepowered.api.entity.living.monster.Enderman;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Optional;

public class SpongeEnderman extends SpongeMonster implements WSEnderman {


    public SpongeEnderman(Enderman entity) {
        super(entity);
    }

    @Override
    public Optional<WSMaterial> getCarriedMaterial() {
        Optional<ItemStack> optional = getHandled().getInventory().peek();
        return optional.map(itemStack -> new SpongeItemStack(itemStack).getMaterial());
    }

    @Override
    public void setCarriedMaterial(WSMaterial carriedMaterial) {
        if (carriedMaterial == null) getHandled().getInventory().set(null);
        else getHandled().getInventory().set(new SpongeItemStack(carriedMaterial).getHandled());
    }

    @Override
    public Enderman getHandled() {
        return (Enderman) super.getHandled();
    }


}
