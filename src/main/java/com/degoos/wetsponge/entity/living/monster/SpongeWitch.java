package com.degoos.wetsponge.entity.living.monster;

import org.spongepowered.api.entity.living.monster.Witch;

public class SpongeWitch extends SpongeMonster implements WSGuardian {


    public SpongeWitch(Witch entity) {
        super(entity);
    }


    @Override
    public Witch getHandled() {
        return (Witch) super.getHandled();
    }
}
