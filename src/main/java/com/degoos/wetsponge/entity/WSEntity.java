package com.degoos.wetsponge.entity;


import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.WSTranslatable;
import com.degoos.wetsponge.world.WSLocatable;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface WSEntity extends WSLocatable, WSTranslatable {

    /**
     * @return the unique {@link UUID} of this entity.
     */
    UUID getUniqueId();

    /**
     * @return get the {@link EnumEntityType} of this entity.
     */
    EnumEntityType getEntityType();

    /**
     * @return true if the entity is dead.
     */
    boolean isDead();

    /**
     * @return whether the {@link WSEntity entity} is on ground.
     */
    boolean isOnGround();

    /**
     * Teleports the entity to a new location.
     * If the set location is modified after use this method,
     * changes won't be applied to the location of this entity.
     *
     * @param location the new location.
     */
    void setLocation(WSLocation location);

    /**
     * @return a new instance of the velocity of this entity.
     */
    Vector3d getVelocity();

    /**
     * Sets the velocity of this entity.
     * If the set velocity is modified after use this method,
     * changes won't be applied to the velocity of this entity.
     *
     * @param velocity the new velocity
     */
    void setVelocity(Vector3d velocity);

    /**
     * @return a new instance of the head rotation of this entity.
     */
    Vector3d getRotation();

    /**
     * Sets the head rotation of this entity.
     * If the set rotation os modified after use this method,
     * changes won't be applied to the head rotation of this entity.
     *
     * @param rotation the head rotation.
     */
    void setRotation(Vector3d rotation);

    /**
     * @return the {@link WSText display name} of the {@link WSEntity entity}, if present.
     */
    Optional<WSText> getCustomName();

    /**
     * Sets the {@link WSText display name} of the entity.
     * <p>If the {@link WSText display name} to set is null,
     * the current {@link WSText display name} of this entity will be erased.</p>
     *
     * @param customName the {@link WSText display name} to set.
     */
    void setCustomName(@Nullable WSText customName);

    /**
     * @return whether the {@link WSText display name} of the {@link WSEntity entity} is visible.
     */
    boolean isCustomNameVisible();

    /**
     * Sets if the {@link WSText display name} of the {@link WSEntity entity} is visible.
     *
     * @param customNameVisible the {@link Boolean boolean}.
     */
    void setCustomNameVisible(boolean customNameVisible);

    /**
     * @return a list of all passengers of the {@link WSEntity entity}.
     */
    Set<WSEntity> getPassengers();

    /**
     * Adds a passenger to the {@link WSEntity entity}.
     *
     * @param entity the entity to add.
     * @return whether the passenger was added.
     */
    boolean addPassenger(WSEntity entity);

    Optional<WSEntity> getRidingEntity();

    void mountEntity(WSEntity entity);

    void dismountRidingEntity();

    boolean isRiding();

    boolean isBeingRidden();

    /**
     * Returns the id of the {@link WSEntity entity}.
     *
     * @return the id of the {@link WSEntity entity}.
     */
    int getEntityId();

    /**
     * Adds a property to the {@link WSEntity entity}.
     * WetSponge's entities have a map which plugins can use it for data storage.
     * If a value with the same key already exists, it would do nothing.
     *
     * @param id    the id of the property.
     * @param value the property.
     */
    void addProperty(String id, Object value);

    /**
     * Adds a property to the {@link WSEntity entity}.
     * WetSponge's entities have a map which plugins can use it for data storage.
     *
     * @param id    the id of the property.
     * @param value the property.
     * @param force whether the value will be added even though a value with the same key already exists.
     */
    void addProperty(String id, Object value, boolean force);

    /**
     * Returns the target property by its id, if found.
     * WetSponge's entities have a map which plugins can use it for data storage.
     *
     * @param id the id.
     * @return the property.
     */
    Optional<Object> getProperty(String id);

    /**
     * Returns the target property by its id, if found.
     * If the property doesn't extends the expected class, this method
     * will return {@link Optional#empty()}.
     *
     * @param id       the id.
     * @param expected the expected class of the property.
     * @param <T>      the expected class of the property.
     * @return the property.
     */
    <T> Optional<T> getProperty(String id, Class<T> expected);

    /**
     * Removes a property by its id.
     * If the property doesn't exist, it will do nothing.
     *
     * @param id the id.
     */
    void removeProperty(String id);

    /**
     * Returns all properties the {@link WSEntity entity} has.
     *
     * @return all properties.
     */
    Map<String, Object> getProperties();

    /**
     * Returns whether the {@link WSEntity entity} has gravity.
     *
     * @return true if the {@link WSEntity entity} has gravity.
     */
    boolean hasGravity();

    /**
     * Sets whether the {@link WSEntity entity} has gravity.
     *
     * @param gravity whether the {@link WSEntity entity} has gravity.
     */
    void setGravity(boolean gravity);

    /**
     * Returns whether the {@link WSEntity entity} is glowing.
     *
     * @return true if the {@link WSEntity entity} is glowing.
     */
    boolean isGlowing();

    /**
     * Sets whether the {@link WSEntity entity} is glowing.
     *
     * @param glowing whether the {@link WSEntity entity} is glowing.
     */
    void setGlowing(boolean glowing);

    /**
     * Returns whether the {@link WSEntity entity} is silent.
     *
     * @return true if the {@link WSEntity entity} is silent.
     */
    boolean isSilent();

    /**
     * Sets whether the {@link WSEntity entity} is silent.
     *
     * @param silent whether the {@link WSEntity entity} is silent.
     */
    void setSilent(boolean silent);

    /**
     * Returns whether the {@link WSEntity entity} is invisible.
     *
     * @return true if the {@link WSEntity entity} is invisible.
     */
    boolean isInvisible();

    /**
     * Sets whether the {@link WSEntity entity} is invisible.
     *
     * @param invisible whether the {@link WSEntity entity} is invisible.
     */
    void setInvisible(boolean invisible);

    /**
     * Returns whether the {@link WSEntity entity} cannot receive damage.
     *
     * @return whether the {@link WSEntity entity} is invincible.
     */
    boolean isInvincible();

    /**
     * Sets whether the {@link WSEntity entity} cannot receive damage.
     * This method will override the {@link #setInvincibleMillis(int)} method.
     *
     * @param invincible the boolean.
     */
    void setInvincible(boolean invincible);

    /**
     * Sets the amount of time in millis the {@link WSEntity entity} will be invincible.
     * This method will override the {@link #setInvincible(boolean)} method.
     *
     * @param millis the amount of time.
     */
    void setInvincibleMillis(int millis);

    /**
     * Returns the amount of remaining air ticks the {@link WSEntity entity} has.
     *
     * @return the amount of remaining air ticks.
     */
    int getAir();

    /**
     * Sets the amount of remaining air ticks the {@link WSEntity entity} has.
     *
     * @param air the amount of remaining air ticks.
     */
    void setAir(int air);

    /**
     * Returns the remaining amount of ticks the {@link WSEntity entity} will be on fire.
     *
     * @return the remaining amount of ticks.
     */
    int getFireTicks();

    /**
     * Sets the remaining amount of ticks the {@link WSEntity entity} will be on fire
     *
     * @param fireTicks the remaining amount of ticks.
     */
    void setFireTicks(int fireTicks);

    /**
     * Sets the maximun amount of ticks the {@link WSEntity entity} can be on fire.
     *
     * @return the maximun amount of ticks.
     */
    int getMaxFireTicks();


    /**
     * Removes the {@link WSEntity entity}.
     * The {@link WSEntity entity} will disappear without being killed.
     */
    void remove();

    /**
     * @return the handled entity.
     */
    Object getHandled();
}
