package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.effect.potion.SpongePotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.projectile.arrow.TippedArrow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpongeTippedArrow extends SpongeArrow implements WSTippedArrow {


    public SpongeTippedArrow(TippedArrow entity) {
        super(entity);
    }

    @Override
    public void addPotionEffect(WSPotionEffect effect) {
        List<PotionEffect> list = getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
        list.add(((SpongePotionEffect) effect).getHandled());
        getHandled().offer(Keys.POTION_EFFECTS, list);
    }

    @Override
    public List<WSPotionEffect> getPotionEffects() {
        return getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>()).stream().map(SpongePotionEffect::new).collect(Collectors.toList());
    }

    @Override
    public void clearAllPotionEffects() {
        getHandled().offer(Keys.POTION_EFFECTS, new ArrayList<>());
    }

    @Override
    public void removePotionEffect(EnumPotionEffectType potionEffectType) {
        getHandled().offer(Keys.POTION_EFFECTS, getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>()).stream().filter(potionEffect ->
                !potionEffect.getType().getName().equals(potionEffectType.name())).collect(Collectors.toList()));
    }

    @Override
    public TippedArrow getHandled() {
        return (TippedArrow) super.getHandled();
    }

}
