package com.degoos.wetsponge.entity.projectile;


import org.spongepowered.api.entity.projectile.Snowball;

public class SpongeSnowball extends SpongeProjectile implements WSSnowball {


    public SpongeSnowball(Snowball entity) {
        super(entity);
    }


    @Override
    public Snowball getHandled() {
        return (Snowball) super.getHandled();
    }

}
