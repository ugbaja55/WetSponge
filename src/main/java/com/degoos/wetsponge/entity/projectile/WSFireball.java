package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.entity.explosive.WSExplosive;
import com.flowpowered.math.vector.Vector3d;

public interface WSFireball extends WSProjectile, WSExplosive {

	Vector3d getDirection();

	void setDirection(Vector3d direction);

}
