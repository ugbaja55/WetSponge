package com.degoos.wetsponge.entity.projectile;

import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public interface WSProjectileSource {

    <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile);

    <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity);

}
