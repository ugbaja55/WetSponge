package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.effect.potion.SpigotPotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import org.bukkit.entity.TippedArrow;
import org.bukkit.potion.PotionEffectType;

import java.util.List;
import java.util.stream.Collectors;

public class SpigotTippedArrow extends SpigotArrow implements WSTippedArrow {


    public SpigotTippedArrow(TippedArrow entity) {
        super(entity);
    }

    @Override
    public void addPotionEffect(WSPotionEffect effect) {
        getHandled().addCustomEffect(((SpigotPotionEffect) effect).getHandled(), true);
    }

    @Override
    public List<WSPotionEffect> getPotionEffects() {
        return getHandled().getCustomEffects().stream().map(SpigotPotionEffect::new).collect(Collectors.toList());
    }

    @Override
    public void clearAllPotionEffects() {
        getHandled().clearCustomEffects();
    }

    @Override
    public void removePotionEffect(EnumPotionEffectType potionEffectType) {
        getHandled().removeCustomEffect(PotionEffectType.getById(potionEffectType.getValue()));
    }

    @Override
    public TippedArrow getHandled() {
        return (TippedArrow) super.getHandled();
    }

}
