package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.entity.WSPotionEffectApplicable;

public interface WSTippedArrow extends WSArrow, WSPotionEffectApplicable {

}
