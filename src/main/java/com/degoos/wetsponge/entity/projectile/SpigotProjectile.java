package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.block.tileentity.SpigotTileEntityDispenser;
import com.degoos.wetsponge.block.tileentity.WSTileEntityDispenser;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;

public class SpigotProjectile extends SpigotEntity implements WSProjectile {


    public SpigotProjectile(Projectile entity) {
        super(entity);
    }


    @Override
    public Projectile getHandled() {
        return (Projectile) super.getHandled();
    }

    @Override
    public WSProjectileSource getShooter() {
        if (getHandled().getShooter() instanceof LivingEntity)
            return (WSProjectileSource) SpigotEntityParser.getWSEntity((LivingEntity) getHandled().getShooter());
        if (getHandled().getShooter() instanceof Dispenser)
            return new SpigotTileEntityDispenser(new SpigotBlock(((Dispenser) getHandled().getShooter()).getBlock()));
        else return new WSUnkownProjectileSource();
    }

    @Override
    public void setShooter(WSProjectileSource source) {
        if (source instanceof WSLivingEntity)
            getHandled().setShooter(((SpigotLivingEntity) source).getHandled());
        if (source instanceof WSTileEntityDispenser)
            getHandled().setShooter(((SpigotTileEntityDispenser) source).getHandled().getBlockProjectileSource());
    }
}
