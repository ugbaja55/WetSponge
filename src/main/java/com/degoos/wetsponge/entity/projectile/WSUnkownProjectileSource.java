package com.degoos.wetsponge.entity.projectile;

import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public class WSUnkownProjectileSource implements WSProjectileSource {

    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile) {
        return Optional.empty();
    }

    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity) {
        return Optional.empty();
    }
}
