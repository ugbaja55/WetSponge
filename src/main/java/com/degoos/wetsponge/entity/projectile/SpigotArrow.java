package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumPickupStatus;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.entity.Arrow;

public class SpigotArrow extends SpigotProjectile implements WSArrow {


	public SpigotArrow(Arrow entity) {
		super(entity);
	}


	@Override
	public double getDamage() {
		return getHandled().spigot().getDamage();
	}

	@Override
	public void setDamage(double damage) {
		getHandled().spigot().setDamage(damage);
	}

	@Override
	public int getKnockbackStrength() {
		return getHandled().getKnockbackStrength();
	}

	@Override
	public void setKnockbackStrength(int knockbackStrength) {
		getHandled().setKnockbackStrength(knockbackStrength);
	}

	@Override
	public EnumPickupStatus getPickupStatus() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return EnumPickupStatus.getByName(getHandled().getPickupStatus().name()).orElse(EnumPickupStatus.ALLOWED);
		try {
			return ReflectionUtils.setAccessible(NMSUtils.getNMSClass("EntityArrow").getField("h")).getInt(getHandled()) == 1 ? EnumPickupStatus.ALLOWED
			                                                                                                                  : EnumPickupStatus.DISALLOWED;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the PickupStatus of the arrow " + getHandled().getUniqueId() + "!");
		}
		return EnumPickupStatus.ALLOWED;
	}

	@Override
	public void setPickupStatus(EnumPickupStatus status) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setPickupStatus(org.bukkit.entity.Arrow.PickupStatus.valueOf(status.name()));
		else {
			try {
				ReflectionUtils.setAccessible(NMSUtils.getNMSClass("EntityArrow").getField("h")).setInt(getHandled(), status == EnumPickupStatus.ALLOWED ? 1 : 0);
			} catch (Exception ex) {
				InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the PickupStatus of the arrow " + getHandled().getUniqueId() + "!");
			}
		}
	}


	@Override
	public Arrow getHandled() {
		return (Arrow) super.getHandled();
	}
}
