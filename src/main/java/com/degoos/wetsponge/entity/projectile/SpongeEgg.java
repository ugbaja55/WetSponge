package com.degoos.wetsponge.entity.projectile;

import org.spongepowered.api.entity.projectile.Egg;

public class SpongeEgg extends SpongeProjectile implements WSEgg {


    public SpongeEgg(Egg entity) {
        super(entity);
    }


    @Override
    public Egg getHandled() {
        return (Egg) super.getHandled();
    }
}
