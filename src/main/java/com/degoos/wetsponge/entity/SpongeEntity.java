package com.degoos.wetsponge.entity;


import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.SpongeTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.SpongeWorld;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class SpongeEntity implements WSEntity {


    private Entity entity;
    private EnumEntityType entityType;
    private Map<String, Object> properties;
    private boolean invincible;
    private long invincibleMillis;


    public SpongeEntity(Entity entity) {
        Validate.notNull(entity, "Entity cannot be null!");
        this.entity = entity;
        this.entityType = SpongeEntityParser.getEntityType(entity);
        this.properties = new HashMap<>();
    }


    @Override
    public UUID getUniqueId() {
        return entity.getUniqueId();
    }


    @Override
    public EnumEntityType getEntityType() {
        return entityType;
    }


    @Override
    public boolean isDead() {
        return entity.isRemoved();
    }

    @Override
    public boolean isOnGround() {
        return entity.isOnGround();
    }


    @Override
    public WSLocation getLocation() {
        return new SpongeLocation(entity.getLocation(), entity.getRotation());
    }


    @Override
    public void setLocation(WSLocation location) {
        entity.setLocationAndRotation(((SpongeLocation) location).getLocation().getLocation(), new Vector3d(location.getPitch(), location.getYaw(), 0));
    }


    @Override
    public WSWorld getWorld() {
        return WorldParser.getOrCreateWorld(entity.getWorld().getName(), entity.getWorld());
    }


    @Override
    public Vector3d getVelocity() {
        return entity.getVelocity();
    }


    @Override
    public void setVelocity(Vector3d velocity) {
        entity.setVelocity(velocity);
    }

    @Override
    public Vector3d getRotation() {
        Vector3d vector3d = entity.getRotation();
        return new Vector3d(vector3d.getY(), vector3d.getX(), vector3d.getZ());
    }

    @Override
    public void setRotation(Vector3d rotation) {
        entity.setRotation(new Vector3d(rotation.getY(), rotation.getX(), rotation.getZ()));
    }

    @Override
    public Optional<WSText> getCustomName() {
        return entity.get(Keys.DISPLAY_NAME).map(SpongeText::of);
    }

    @Override
    public void setCustomName(@Nullable WSText customName) {
        entity.offer(Keys.DISPLAY_NAME, ((SpongeText) customName).getHandled());
    }

    @Override
    public boolean isCustomNameVisible() {
        return entity.get(Keys.CUSTOM_NAME_VISIBLE).orElse(false);
    }

    @Override
    public void setCustomNameVisible(boolean customNameVisible) {
        entity.offer(Keys.CUSTOM_NAME_VISIBLE, customNameVisible);
    }

    @Override
    public Set<WSEntity> getPassengers() {
        return entity.getPassengers().stream().map(SpongeEntityParser::getWSEntity).collect(Collectors.toSet());
    }

    @Override
    public boolean addPassenger(WSEntity entity) {
        return this.entity.addPassenger(((SpongeEntity) entity).entity);
    }

    @Override
    public Optional<WSEntity> getRidingEntity() {
        return Optional.ofNullable(((net.minecraft.entity.Entity) entity).getRidingEntity()).map(target -> SpongeEntityParser.getWSEntity((Entity) target));
    }

    @Override
    public void mountEntity(WSEntity entity) {
        ((net.minecraft.entity.Entity) this.entity).startRiding((net.minecraft.entity.Entity) ((SpongeEntity) entity).entity);
    }

    @Override
    public void dismountRidingEntity() {
        ((net.minecraft.entity.Entity) entity).dismountRidingEntity();
    }

    @Override
    public boolean isRiding() {
        return ((net.minecraft.entity.Entity) entity).isRiding();
    }

    @Override
    public boolean isBeingRidden() {
        return ((net.minecraft.entity.Entity) entity).isBeingRidden();
    }


    @Override
    public int getEntityId() {
        return ((net.minecraft.entity.Entity) entity).getEntityId();
    }

    @Override
    public void addProperty(String id, Object value) {
        properties.putIfAbsent(id, value);
    }

    @Override
    public void addProperty(String id, Object value, boolean force) {
        if (force) properties.put(id, value);
        else properties.putIfAbsent(id, value);
    }

    @Override
    public Optional<Object> getProperty(String id) {
        return Optional.ofNullable(properties.get(id));
    }

    @Override
    public <T> Optional<T> getProperty(String id, Class<T> expected) {
        Optional<Object> optional = Optional.ofNullable(properties.get(id));
        if (!optional.isPresent() || !expected.isInstance(optional.get())) return Optional.empty();
        return optional.map(o -> (T) o);
    }

    @Override
    public void removeProperty(String id) {
        properties.remove(id);
    }

    @Override
    public Map<String, Object> getProperties() {
        return new HashMap<>(properties);
    }

    @Override
    public boolean hasGravity() {
        return entity.gravity().get();
    }

    @Override
    public void setGravity(boolean gravity) {
        entity.offer(Keys.HAS_GRAVITY, gravity);
    }

    @Override
    public boolean isGlowing() {
        return entity.get(Keys.GLOWING).orElse(false);
    }

    @Override
    public void setGlowing(boolean glowing) {
        entity.offer(Keys.GLOWING, glowing);
    }

    @Override
    public boolean isSilent() {
        return entity.get(Keys.IS_SILENT).orElse(false);
    }

    @Override
    public void setSilent(boolean silent) {
        entity.offer(Keys.IS_SILENT, silent);
    }

    @Override
    public boolean isInvisible() {
        return entity.get(Keys.INVISIBLE).orElse(false);
    }

    @Override
    public void setInvisible(boolean invisible) {
        entity.offer(Keys.INVISIBLE, invisible);
    }

    @Override
    public boolean isInvincible() {
        return invincible || invincibleMillis > System.currentTimeMillis();
    }

    @Override
    public void setInvincible(boolean invincible) {
        invincibleMillis = 0;
        this.invincible = invincible;
    }

    @Override
    public void setInvincibleMillis(int millis) {
        invincibleMillis = System.currentTimeMillis() + millis;
        invincible = false;
    }

    @Override
    public int getAir() {
        return entity.get(Keys.REMAINING_AIR).orElse(0);
    }

    @Override
    public void setAir(int air) {
        entity.offer(Keys.REMAINING_AIR, air);
    }

    @Override
    public int getFireTicks() {
        return entity.get(Keys.FIRE_TICKS).orElse(0);
    }

    @Override
    public void setFireTicks(int fireTicks) {
        entity.offer(Keys.FIRE_TICKS, fireTicks);
    }

    @Override
    public int getMaxFireTicks() {
        try {
            Method method = net.minecraft.entity.Entity.class.getMethod("getFireImmuneTicks");
            method.setAccessible(true);
            return (int) method.invoke(entity);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return 0;
        }
    }


    @Override
    public void remove() {
        entity.remove();
    }

    @Override
    public Entity getHandled() {
        return entity;
    }


    @Override
    public WSTranslation getTranslation() {
        return new SpongeTranslation(entity.getTranslation());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpongeEntity that = (SpongeEntity) o;

        return entity.equals(that.entity);
    }

    @Override
    public int hashCode() {
        return entity.hashCode();
    }
}
