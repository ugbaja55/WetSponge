package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumRotation;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.util.rotation.Rotation;
import org.spongepowered.api.util.rotation.Rotations;

import java.util.Optional;

public class SpongeItemFrame extends SpongeHanging implements WSItemFrame {


    public SpongeItemFrame(ItemFrame entity) {
        super(entity);
    }

    @Override
    public Optional<WSItemStack> getItemStack() {
        Optional<ItemStackSnapshot> snapshot = getHandled().get(Keys.REPRESENTED_ITEM);
	    return snapshot.flatMap(itemStackSnapshot -> Optional.ofNullable(itemStackSnapshot.createStack()).map(SpongeItemStack::new));
    }

    @Override
    public void setItemStack(WSItemStack itemStack) {
        if (itemStack == null) getHandled().offer(Keys.REPRESENTED_ITEM, null);
        else getHandled().offer(Keys.REPRESENTED_ITEM, ((SpongeItemStack) itemStack).getHandled().createSnapshot());
    }

    @Override
    public EnumRotation getFrameRotation() {
        return EnumRotation.getBySpongeName(getHandled().get(Keys.ROTATION).orElse(Rotations.TOP).getName()).orElse(EnumRotation.TOP);
    }

    @Override
    public void setRotation(EnumRotation rotation) {
        getHandled().offer(Keys.ROTATION, Sponge.getRegistry().getType(Rotation.class, rotation.getSpongeName()).orElse(Rotations.TOP));
    }

    public ItemFrame getHandled() {
        return (ItemFrame) super.getHandled();
    }


}
