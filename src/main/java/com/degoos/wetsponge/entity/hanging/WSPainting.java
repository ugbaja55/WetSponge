package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumArtType;

public interface WSPainting extends WSHanging {

    /**
     * @return the {@link EnumArtType} of this {@link WSPainting}.
     */
    EnumArtType getArt();

    /**
     * Sets the {@link EnumArtType} of this {@link WSPainting}.
     *
     * @param art the art type.
     */
    void setArt(EnumArtType art);

}
