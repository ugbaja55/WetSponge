package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumArtType;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.Art;
import org.spongepowered.api.data.type.Arts;
import org.spongepowered.api.entity.hanging.Painting;

public class SpongePainting extends SpongeHanging implements WSPainting {


    public SpongePainting(Painting entity) {
        super(entity);
    }

    @Override
    public EnumArtType getArt() {
        return EnumArtType.getByName(getHandled().art().get().getName()).orElse(EnumArtType.KEBAB);
    }

    @Override
    public void setArt(EnumArtType art) {
        getHandled().offer(Keys.ART, Sponge.getRegistry().getType(Art.class, art.name()).orElse(Arts.KEBAB));
    }

    @Override
    public Painting getHandled() {
        return (Painting) super.getHandled();
    }


}
