package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;

public interface WSHanging extends WSEntity {

    /**
     * @return the direction of the entity.
     */
    EnumBlockDirection getDirection();

    /**
     * Sets the direction of the entity.
     *
     * @param direction the new direction.
     */
    void setDirection(EnumBlockDirection direction);

}
