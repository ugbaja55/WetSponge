package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.hanging.Hanging;
import org.spongepowered.api.util.Direction;

public class SpongeHanging extends SpongeEntity implements WSHanging {


    public SpongeHanging(Hanging entity) {
        super(entity);
    }

    @Override
    public EnumBlockDirection getDirection() {
        return EnumBlockDirection.getByName(getHandled().direction().get().name()).orElse(EnumBlockDirection.EAST);
    }

    @Override
    public void setDirection(EnumBlockDirection direction) {
        getHandled().offer(Keys.DIRECTION, Direction.valueOf(direction.name()));
    }

    @Override
    public Hanging getHandled() {
        return (Hanging) super.getHandled();
    }
}
