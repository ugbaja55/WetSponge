package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumRotation;
import com.degoos.wetsponge.item.WSItemStack;

import java.util.Optional;

public interface WSItemFrame extends WSHanging {

    /**
     * @return a new instance of the represented {@link WSItemStack} in this {@link WSItemFrame}, if present.
     */
    Optional<WSItemStack> getItemStack();

    /**
     * Sets a new instance of the represented {@link WSItemStack} of this {@link WSItemStack}.
     * Set NULL to remove the {@link WSItemStack}.
     *
     * @param itemStack the {@link WSItemStack}.
     */
    void setItemStack(WSItemStack itemStack);

    /**
     * @return the {@link WSItemStack} {@link EnumRotation}.
     */
    EnumRotation getFrameRotation();

    /**
     * Sets the {@link WSItemStack} {@link EnumRotation} of this {@link WSItemFrame}.
     *
     * @param rotation the {@link EnumRotation}.
     */
    void setRotation(EnumRotation rotation);

}
