package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Hanging;

public class SpigotHanging extends SpigotEntity implements WSHanging {


    public SpigotHanging(Hanging entity) {
        super(entity);
    }

    @Override
    public EnumBlockDirection getDirection() {
        return EnumBlockDirection.getByName(getHandled().getFacing().name()).orElse(EnumBlockDirection.EAST);
    }

    @Override
    public void setDirection(EnumBlockDirection direction) {
        getHandled().setFacingDirection(BlockFace.valueOf(direction.name()));
    }

    @Override
    public Hanging getHandled() {
        return (Hanging) super.getHandled();
    }
}
