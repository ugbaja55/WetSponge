package com.degoos.wetsponge.entity.explosive;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.util.Optional;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.explosive.PrimedTNT;

public class SpongePrimedTNT extends SpongeEntity implements WSPrimedTNT {

	public SpongePrimedTNT(PrimedTNT entity) {
		super(entity);
	}

	@Override
	public Optional<WSLivingEntity> getDetonator() {
		return getHandled().getDetonator().map(SpongeEntityParser::getWSEntity).map(t -> (WSLivingEntity) t);
	}

	@Override
	public void setDetonator(WSLivingEntity entity) {
		try {
			ReflectionUtils.setFirstObject(EntityTNTPrimed.class, EntityLivingBase.class, getHandled(), entity == null ? null : entity.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the detonator of a primed TNT!");
		}
	}

	@Override
	public int getFuseDuration() {
		return getHandled().getFuseData().fuseDuration().get();
	}

	@Override
	public void setFuseDuration(int fuseDuration) {
		getHandled().offer(Keys.FUSE_DURATION, fuseDuration);
	}

	@Override
	public int getTicksRemaining() {
		return getHandled().getFuseData().ticksRemaining().get();
	}

	@Override
	public void setTicksRemaining(int ticksRemaining) {
		getHandled().offer(Keys.TICKS_REMAINING, ticksRemaining);
	}

	@Override
	public boolean isPrimed() {
		return getHandled().isPrimed();
	}

	@Override
	public void prime() {
		getHandled().prime();
	}

	@Override
	public void defuse() {
		getHandled().defuse();
	}

	@Override
	public int setExplosionRadius() {
		return getHandled().getExplosionRadiusData().explosionRadius().get().orElse(2);
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().offer(Keys.EXPLOSION_RADIUS, Optional.of(explosionRadius));
	}

	@Override
	public void detonate() {
		getHandled().detonate();
	}

	@Override
	public PrimedTNT getHandled() {
		return (PrimedTNT) super.getHandled();
	}
}
