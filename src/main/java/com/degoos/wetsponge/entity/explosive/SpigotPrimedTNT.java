package com.degoos.wetsponge.entity.explosive;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.util.Optional;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.TNTPrimed;

public class SpigotPrimedTNT extends SpigotEntity implements WSPrimedTNT {


	public SpigotPrimedTNT(TNTPrimed entity) {
		super(entity);
	}

	@Override
	public Optional<WSLivingEntity> getDetonator() {
		Entity entity = getHandled().getSource();
		if (entity == null || !(entity instanceof LivingEntity)) return Optional.empty();
		return Optional.of((WSLivingEntity) SpigotEntityParser.getWSEntity(entity));
	}

	@Override
	public void setDetonator(WSLivingEntity entity) {
		try {
			Object handled = HandledUtils.getEntityHandle(getHandled());
			ReflectionUtils.setFirstObject(handled.getClass(), NMSUtils.getNMSClass("EntityLiving"), handled,
				entity == null ? null : HandledUtils.getEntityHandle(((SpigotEntity) entity).getHandled()));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the detonator of a primed TNT!");
		}
	}

	@Override
	public int getFuseDuration() {
		return 100;
	}

	@Override
	public void setFuseDuration(int fuseDuration) {
	}

	@Override
	public int getTicksRemaining() {
		return getHandled().getFuseTicks();
	}

	@Override
	public void setTicksRemaining(int ticksRemaining) {
		getHandled().setFuseTicks(ticksRemaining);
	}

	@Override
	public boolean isPrimed() {
		return true;
	}

	@Override
	public void prime() {
	}

	@Override
	public void defuse() {
		getHandled().remove();
	}

	@Override
	public int setExplosionRadius() {
		return (int) getHandled().getYield();
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().setYield(explosionRadius);
	}

	@Override
	public void detonate() {
		try {
			ReflectionUtils.invokeMethod(HandledUtils.getEntityHandle(getHandled()), "explode");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was exploding a primed TNT!");
		}
	}

	@Override
	public TNTPrimed getHandled() {
		return (TNTPrimed) super.getHandled();
	}
}
