package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.entity.WSPotionEffectApplicable;
import com.degoos.wetsponge.particle.WSParticle;

public interface WSAreaEffectCloud extends WSPotionEffectApplicable {

    WSColor getColor();

    void setColor(WSColor color);

    double getRadius();

    void setRadius(double radius);

    WSParticle getParticle();

    void setParticle(WSParticle particle);

    int getDuration();

    void setDuration(int duration);

    int getWaitTime();

    void setWaitTime(int waitTime);

    double getRadiusOnUse();

    void setRadiusOnUse(double radiusOnUse);

    double getRadiusPerTick();

    void setRadiusPerTick(double radiusPerTick);

    int getDurationOnUse();

    void setDurationOnUse(int durationOnUse);

    int getApplicationDelay();

    void setApplicationDelay(int applicationDelay);

    int getAge();

    void setAge(int age);


}
