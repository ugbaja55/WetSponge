package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.item.WSItemStack;

public interface WSItem extends WSEntity {

    WSItemStack getItemStack();

    void setItemStack(WSItemStack itemStack);

    int getPickupDelay();

    void setPickupDelay(int pickupDelay);

}
