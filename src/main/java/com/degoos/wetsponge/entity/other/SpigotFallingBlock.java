package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.entity.FallingBlock;

public class SpigotFallingBlock extends SpigotEntity implements WSFallingBlock {

	private static Class<?> NMS_CLASS = NMSUtils.getNMSClass("EntityFallingBlock");

	public SpigotFallingBlock(FallingBlock entity) {
		super(entity);
	}

	@Override
	public WSBlockType getBlockType() {
		return WSBlockTypes.getType(getHandled().getMaterial().getId(), getHandled().getBlockData()).orElse(WSBlockTypes.AIR.getDefaultType());
	}

	@Override
	public void setBlockType(WSBlockType blockType) {
		Validate.notNull(blockType, "Block type cannot be null!");
		try {
			ReflectionUtils
				.setFirstObject(NMS_CLASS, NMSUtils.getNMSClass("IBlockData"), HandledUtils.getEntityHandle(getHandled()), HandledUtils.getBlockState(blockType));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getFallDamagePerBlock() {
		try {
			return (double) ReflectionUtils.getObject(HandledUtils.getEntityHandle(getHandled()), "fallHurtAmount");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallDamagerPerBlock(double fallDamagerPerBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtAmount"))
				.setDouble(HandledUtils.getEntityHandle(getHandled()), fallDamagerPerBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getMaxFallDamage() {
		try {
			return (double) ReflectionUtils.getObject(HandledUtils.getEntityHandle(getHandled()), "fallHurtMax");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setMaxFallDamage(double maxFallDamage) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtMax")).setDouble(HandledUtils.getEntityHandle(getHandled()), maxFallDamage);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canPlaceAsBlock() {
		try {
			return (boolean) ReflectionUtils
				.getObject(HandledUtils.getEntityHandle(getHandled()), WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return false;
		}
	}

	@Override
	public void setCanPlaceAsBlock(boolean canPlaceAsBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e"))
				.setBoolean(HandledUtils.getEntityHandle(getHandled()), canPlaceAsBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canDropAsItem() {
		return getHandled().getDropItem();
	}

	@Override
	public void setCanDropAsItem(boolean canDropAsItem) {
		getHandled().setDropItem(canDropAsItem);
	}

	@Override
	public int getFallTime() {
		try {
			return (int) ReflectionUtils.getObject(HandledUtils.getEntityHandle(getHandled()), "ticksLived");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallTime(int fallTime) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "ticksLived")).setInt(HandledUtils.getEntityHandle(getHandled()), fallTime);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canHurtEntities() {
		return getHandled().canHurtEntities();
	}

	@Override
	public void setCanHurtEntities(boolean canHurtEntities) {
		getHandled().setHurtEntities(canHurtEntities);
	}

	@Override
	public FallingBlock getHandled() {
		return (FallingBlock) super.getHandled();
	}

}
