package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.entity.Item;

public class SpigotItem extends SpigotEntity implements WSItem {


    public SpigotItem(Item entity) {
        super(entity);
    }

    @Override
    public WSItemStack getItemStack() {
        return new SpigotItemStack(getHandled().getItemStack());
    }

    @Override
    public void setItemStack(WSItemStack itemStack) {
        getHandled().setItemStack(((SpigotItemStack)itemStack).getHandled());
    }

    @Override
    public int getPickupDelay() {
        return getHandled().getPickupDelay();
    }

    @Override
    public void setPickupDelay(int pickupDelay) {
        getHandled().setPickupDelay(pickupDelay);
    }

    @Override
    public Item getHandled() {
        return (Item) super.getHandled();
    }
}
