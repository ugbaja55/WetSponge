package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Item;

public class SpongeItem extends SpongeEntity implements WSItem {

    public SpongeItem(Item entity) {
        super(entity);
    }

    @Override
    public WSItemStack getItemStack() {
        return new SpongeItemStack(getHandled().getItemData().item().get().createStack());
    }

    @Override
    public void setItemStack(WSItemStack itemStack) {
        getHandled().offer(Keys.REPRESENTED_ITEM, ((SpongeItemStack) itemStack).getHandled().createSnapshot());
    }

    @Override
    public int getPickupDelay() {
        return getHandled().get(Keys.PICKUP_DELAY).orElse(0);
    }

    @Override
    public void setPickupDelay(int pickupDelay) {
        getHandled().offer(Keys.PICKUP_DELAY, pickupDelay);
    }

    @Override
    public Item getHandled() {
        return (Item) super.getHandled();
    }
}
