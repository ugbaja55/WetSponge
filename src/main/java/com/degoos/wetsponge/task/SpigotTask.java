package com.degoos.wetsponge.task;


import co.aikar.wetspongetimings.Timing;
import co.aikar.wetspongetimings.WetSpongeTimings;
import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.UUID;
import java.util.function.Consumer;
import org.bukkit.scheduler.BukkitRunnable;

public class SpigotTask implements WSTask {

	private BukkitRunnable task;
	private UUID uniqueId;
	private long times, timesExecuted;
	private WSPlugin plugin;
	private boolean async;
	private Timing taskTimer;
	private StackTraceElement caller;


	public SpigotTask(Runnable runnable) {
		Consumer<WSTask> consumer = (wsTask -> runnable.run());
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[3];
		task = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if (times > -1) {
						if (times <= timesExecuted) {
							cancel();
							return;
						}
					}
					Timing timing = getTimingsHandler();
					if (timing == null) consumer.accept(SpigotTask.this);
					else {
						timing.startTiming();
						consumer.accept(SpigotTask.this);
						timing.stopTiming();
					}
					SpigotTask.this.timesExecuted++;

				} catch (Throwable ex) {
					InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
						.append(WSText.of(String.valueOf(task.getTaskId()), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
				}
			}
		};
		uniqueId = UUID.randomUUID();
	}


	public SpigotTask(Consumer<WSTask> consumer) {
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[3];
		task = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					if (times > -1) {
						if (times <= timesExecuted) {
							cancel();
							return;
						}
					}
					Timing timing = getTimingsHandler();
					if (timing == null) consumer.accept(SpigotTask.this);
					else {
						timing.startTiming();
						consumer.accept(SpigotTask.this);
						timing.stopTiming();
					}
					SpigotTask.this.timesExecuted++;

				} catch (Throwable ex) {
					InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
						.append(WSText.of(String.valueOf(task.getTaskId()), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
				}
			}
		};
		uniqueId = UUID.randomUUID();
	}


	@Override
	public void run(WSPlugin plugin) {
		this.plugin = plugin;
		this.async = false;
		task.run();
	}


	@Override
	public void runAsynchronously(WSPlugin plugin) {
		this.plugin = plugin;
		this.async = true;
		task.runTaskAsynchronously(SpigotWetSponge.getInstance());
	}


	@Override
	public void runTaskLater(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.async = false;
		task.runTaskLater(SpigotWetSponge.getInstance(), delay);
	}


	@Override
	public void runTaskLaterAsynchronously(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.async = true;
		task.runTaskLaterAsynchronously(SpigotWetSponge.getInstance(), delay);
	}


	@Override
	public void runTaskTimer(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.async = false;
		task.runTaskTimer(SpigotWetSponge.getInstance(), delay, interval);
	}

	@Override
	public void runTaskTimer(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		runTaskTimer(delay, interval, plugin);
	}


	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		this.async = true;
		task.runTaskTimerAsynchronously(SpigotWetSponge.getInstance(), delay, interval);
	}

	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		runTaskTimerAsynchronously(delay, interval, plugin);
	}

	@Override
	public UUID getUniqueId() {
		return uniqueId;
	}

	@Override
	public StackTraceElement getCallerStackTraceElement() {
		return caller;
	}

	@Override
	public long getTimesExecuted() {
		return timesExecuted;
	}

	@Override
	public boolean isAsynchronous() {
		return async;
	}

	@Override
	public WSPlugin getPlugin() {
		return plugin;
	}


	@Override
	public void cancel() {
		task.cancel();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotTask that = (SpigotTask) o;

		return uniqueId.equals(that.uniqueId);
	}

	@Override
	public int hashCode() {
		return uniqueId.hashCode();
	}

	public Timing getTimingsHandler() {
		if (this.taskTimer == null) {
			this.taskTimer = WetSpongeTimings.getPluginTaskTimings(this, times);
		}
		return this.taskTimer;
	}
}
