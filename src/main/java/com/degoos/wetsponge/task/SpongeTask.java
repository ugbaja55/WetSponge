package com.degoos.wetsponge.task;


import co.aikar.wetspongetimings.Timing;
import co.aikar.wetspongetimings.WetSpongeTimings;
import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.UUID;
import java.util.function.Consumer;
import org.spongepowered.api.scheduler.Task;

public class SpongeTask implements WSTask {

	private Consumer<WSTask> consumer;
	private Task task;
	private long times, timesExecuted;
	private WSPlugin plugin;
	private Timing taskTimer;
	private StackTraceElement caller;


	public SpongeTask(Runnable runnable) {
		this.times = -1;
		this.timesExecuted = 0;
		caller = Thread.currentThread().getStackTrace()[3];
		consumer = (wsTask -> {
			try {
				if (times > -1) {
					if (times <= timesExecuted) {
						cancel();
						return;
					}
				}
				runnable.run();
				this.timesExecuted++;
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
					.append(WSText.of(task.getUniqueId().toString(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
		});
	}


	public SpongeTask(Consumer<WSTask> consumer) {
		this.times = -1;
		caller = Thread.currentThread().getStackTrace()[3];
		this.consumer = (wsTask -> {
			try {
				if (times > -1) {
					if (times <= timesExecuted) {
						cancel();
						return;
					}
				}

				Timing timing = getTimingsHandler();
				if (timing == null) consumer.accept(SpongeTask.this);
				else {
					timing.startTiming();
					consumer.accept(SpongeTask.this);
					timing.stopTiming();
				}
				this.timesExecuted++;
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was executing the task ")
					.append(WSText.of(task.getUniqueId().toString(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
		});
	}


	@Override
	public void run(WSPlugin plugin) {
		this.plugin = plugin;
		task = Task.builder().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runAsynchronously(WSPlugin plugin) {
		this.plugin = plugin;
		task = Task.builder().async().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskLater(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		task = Task.builder().delayTicks(delay).execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskLaterAsynchronously(long delay, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		task = Task.builder().delayTicks(delay).async().execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}


	@Override
	public void runTaskTimer(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		task = Task.builder().delayTicks(delay).intervalTicks(interval).execute((task1 -> consumer.accept(SpongeTask.this))).submit(SpongeWetSponge.getInstance());
	}

	@Override
	public void runTaskTimer(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		runTaskTimer(delay, interval, plugin);
	}


	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, WSPlugin plugin) {
		if (!WSTaskPool.addTask(plugin, this)) return;
		this.plugin = plugin;
		task = Task.builder().delayTicks(delay).intervalTicks(interval).async().execute((task1 -> consumer.accept(SpongeTask.this)))
			.submit(SpongeWetSponge.getInstance());
	}

	@Override
	public void runTaskTimerAsynchronously(long delay, long interval, long times, WSPlugin plugin) {
		this.times = times;
		runTaskTimerAsynchronously(delay, interval, plugin);
	}

	@Override
	public UUID getUniqueId() {
		return task == null ? null : task.getUniqueId();
	}

	@Override
	public StackTraceElement getCallerStackTraceElement() {
		return caller;
	}

	@Override
	public long getTimesExecuted() {
		return timesExecuted;
	}

	@Override
	public boolean isAsynchronous() {
		return task != null && task.isAsynchronous();
	}

	@Override
	public WSPlugin getPlugin() {
		return plugin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeTask that = (SpongeTask) o;

		return task != null ? task.equals(that.task) : that.task == null;
	}

	@Override
	public int hashCode() {
		return task != null ? task.hashCode() : 0;
	}

	@Override
	public void cancel() {
		if (task != null) task.cancel();
	}

	public Timing getTimingsHandler() {
		if (this.taskTimer == null) {
			this.taskTimer = WetSpongeTimings.getPluginTaskTimings(this, times);
		}
		return this.taskTimer;
	}
}
