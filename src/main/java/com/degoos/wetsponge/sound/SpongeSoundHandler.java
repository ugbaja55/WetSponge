package com.degoos.wetsponge.sound;

import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumSoundCategory;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.sound.SoundCategories;
import org.spongepowered.api.effect.sound.SoundCategory;
import org.spongepowered.api.effect.sound.SoundType;
import us.myles.ViaVersion.api.ViaVersion;

public class SpongeSoundHandler {

    public static void playSound(WSSound sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
        String soundName;
        if (Sponge.getPluginManager().isLoaded("ViaVersion"))
            soundName = ViaVersion.getInstance().getPlayerVersion(player.getUniqueId()) == 47 ?
                    sound.getMinecraft1_8Sound() : sound.getMinecraft1_9Sound();
        else soundName = sound.getMinecraft1_9Sound();

        playSound(soundName, category, player, position, volume, pitch);
    }

    public static void playSound(String sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
        ((SpongePlayer) player).getHandled().playSound(SoundType.of(sound), Sponge.getRegistry().getType(SoundCategory.class,
                category.getSpongeName()).orElse(SoundCategories.VOICE), position, volume, pitch);
    }

}
