package com.degoos.wetsponge.sound;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.SpigotPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumSoundCategory;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.PacketUtils;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import us.myles.ViaVersion.api.ViaVersion;

import java.lang.reflect.InvocationTargetException;

public class SpigotSoundHandler {


    public static void playSound(WSSound sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
        Player spigotPlayer = ((SpigotPlayer) player).getHandled();
        Location location = new Location(spigotPlayer.getWorld(), position.getX(), position.getY(), position.getZ());
        if (Bukkit.getPluginManager().isPluginEnabled("ViaVersion")) {
            spigotPlayer.playSound(location,
                    ViaVersion.getInstance().getPlayerVersion(spigotPlayer) == 47 ? sound.getMinecraft1_8Sound() : sound.getMinecraft1_9Sound(), volume, pitch);
        } else if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
            try {
                PacketUtils.sendPacket(spigotPlayer, NMSUtils.getNMSClass("PacketPlayOutCustomSoundEffect")
                        .getConstructor(String.class, NMSUtils.getNMSClass("SoundCategory"), double.class, double.class, double.class, float.class, float.class)
                        .newInstance(sound.getMinecraft1_9Sound(), category.getSpigotHandle(), position.getX(), position.getY(), position.getZ(), volume,
                                pitch));
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
                    e) {
                e.printStackTrace();
            }
        } else spigotPlayer.playSound(location, sound.getMinecraft1_8Sound(), volume, pitch);

    }

    public static void playSound(String sound, EnumSoundCategory category, WSPlayer player, Vector3d position, float volume, float pitch) {
        Player spigotPlayer = ((SpigotPlayer) player).getHandled();
        Location location = new Location(spigotPlayer.getWorld(), position.getX(), position.getY(), position.getZ());
        if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
            try {
                PacketUtils.sendPacket(spigotPlayer, NMSUtils.getNMSClass("PacketPlayOutCustomSoundEffect")
                        .getConstructor(String.class, NMSUtils.getNMSClass("SoundCategory"), double.class, double.class, double.class, float.class, float.class)
                        .newInstance(sound, category.getSpigotHandle(), position.getX(), position.getY(), position.getZ(), volume,
                                pitch));
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
                    e) {
                e.printStackTrace();
            }
        } else spigotPlayer.playSound(location, sound, volume, pitch);

    }
}
