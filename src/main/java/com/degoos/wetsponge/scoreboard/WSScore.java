package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.text.WSText;

public interface WSScore {

    WSText getName();

    int getScore();

    void setScore(int score);

    Object getHandled();
}
