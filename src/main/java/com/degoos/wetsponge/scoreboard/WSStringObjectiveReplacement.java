package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;

public abstract class WSStringObjectiveReplacement extends WSObjectiveReplacement {

	public WSStringObjectiveReplacement(String key) {
		super(key);
	}

	@Override
	public WSText getReplacement(WSPlayer player) {
		return WSText.of(getStringReplacement(player));
	}

	@Override
	public WSText apply(WSText text, WSPlayer player) {
		return text.replace(getKey(), getStringReplacement(player));
	}

	public abstract String getStringReplacement(WSPlayer player);
}
