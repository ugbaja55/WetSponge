package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.text.SpongeText;
import org.spongepowered.api.scoreboard.Score;

public class SpongeScore implements WSScore {

    private Score score;

    public SpongeScore(Score score) {
        Validate.notNull(score, "Score cannot be null!");
        this.score = score;
    }

    public WSText getName() {
        return SpongeText.of(score.getName());
    }

    public int getScore() {
        return score.getScore();
    }

    public void setScore(int score) {
        this.score.setScore(score);
    }

    @Override
    public Score getHandled() {
        return score;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpongeScore that = (SpongeScore) o;

        return score.equals(that.score);
    }

    @Override
    public int hashCode() {
        return score.hashCode();
    }
}
