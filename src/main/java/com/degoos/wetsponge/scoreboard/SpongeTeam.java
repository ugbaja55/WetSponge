package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCollisionRule;
import com.degoos.wetsponge.enums.EnumVisibility;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scoreboard.CollisionRule;
import org.spongepowered.api.scoreboard.CollisionRules;
import org.spongepowered.api.scoreboard.Team;
import org.spongepowered.api.scoreboard.Visibilities;
import org.spongepowered.api.scoreboard.Visibility;

public class SpongeTeam implements WSTeam {

	private Team team;

	public SpongeTeam(Team team) {
		Validate.notNull(team, "Team cannot be null!");
		this.team = team;
	}

	@Override
	public String getName() {
		return team.getName();
	}

	@Override
	public WSText getDisplayName() {
		return SpongeText.of(team.getDisplayName());
	}

	@Override
	public void setDisplayName(@Nullable WSText name) {
		if (name == null) team.setDisplayName(null);
		else team.setDisplayName(((SpongeText) name).getHandled());
	}

	@Override
	public WSText getPrefix() {
		return SpongeText.of(team.getPrefix());
	}

	@Override
	public void setPrefix(@Nullable WSText prefix) {
		if (prefix == null) team.setPrefix(null);
		else team.setPrefix(((SpongeText) prefix).getHandled());
	}

	@Override
	public WSText getSuffix() {
		return SpongeText.of(team.getSuffix());
	}

	@Override
	public void setSuffix(@Nullable WSText suffix) {
		if (suffix == null) team.setSuffix(null);
		else team.setSuffix(((SpongeText) suffix).getHandled());
	}

	@Override
	public boolean allowFriendlyFire() {
		return team.allowFriendlyFire();
	}

	@Override
	public void setAllowFriendlyFire(boolean enabled) {
		team.setAllowFriendlyFire(enabled);
	}

	@Override
	public boolean canSeeFriendlyInvisibles() {
		return team.canSeeFriendlyInvisibles();
	}

	@Override
	public void setCanSeeFriendlyInvisibles(boolean enabled) {
		team.setCanSeeFriendlyInvisibles(enabled);
	}

	@Override
	public EnumVisibility getNameTagVisibility() {
		return EnumVisibility.getBySpongeName(team.getNameTagVisibility().getName()).orElse(EnumVisibility.ALWAYS);
	}

	@Override
	public void setNameTagVisibility(EnumVisibility visibility) {
		Validate.notNull(visibility, "Visibility cannot be null!");
		team.setNameTagVisibility(Sponge.getRegistry().getType(Visibility.class, visibility.getSpongeName()).orElse(Visibilities.ALWAYS));
	}

	@Override
	public EnumVisibility getDeathMessageVisibility() {
		return EnumVisibility.getBySpongeName(team.getDeathMessageVisibility().getName()).orElse(EnumVisibility.ALWAYS);
	}

	@Override
	public void setDeathMessageVisibility(EnumVisibility visibility) {
		Validate.notNull(visibility, "Visibility cannot be null!");
		team.setDeathMessageVisibility(Sponge.getRegistry().getType(Visibility.class, visibility.getSpongeName()).orElse(Visibilities.ALWAYS));
	}

	@Override
	public EnumCollisionRule getCollisionRule() {
		return EnumCollisionRule.getBySpongeName(team.getCollisionRule().getName()).orElse(EnumCollisionRule.ALWAYS);
	}

	@Override
	public void setCollisionRule(EnumCollisionRule collisionRule) {
		Validate.notNull(collisionRule, "Collision rule cannot be null!");
		team.setCollisionRule(Sponge.getRegistry().getType(CollisionRule.class, collisionRule.getSpongeName()).orElse(CollisionRules.ALWAYS));
	}

	@Override
	public Set<WSText> getMembers() {
		return team.getMembers().stream().map(SpongeText::of).collect(Collectors.toSet());
	}

	@Override
	public void addMember(WSText member) {
		Validate.notNull(member, "Member cannot be null!");
		team.addMember(((SpongeText) member).getHandled());
	}

	@Override
	public boolean removeMember(WSText member) {
		Validate.notNull(member, "Member cannot be null!");
		return team.removeMember(((SpongeText) member).getHandled());
	}

	@Override
	public boolean hasMember(WSText member) {
		Validate.notNull(member, "Member cannot be null!");
		return getMembers().contains(member);
	}

	@Override
	public Optional<WSScoreboard> getScoreboard() {
		return team.getScoreboard().map(SpongeScoreboard::new);
	}

	@Override
	public boolean unregister() {
		return team.unregister();
	}

	@Override
	public Team getHandled() {
		return team;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeTeam that = (SpongeTeam) o;

		return team.equals(that.team);
	}

	@Override
	public int hashCode() {
		return team.hashCode();
	}
}
