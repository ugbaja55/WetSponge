package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCollisionRule;
import com.degoos.wetsponge.enums.EnumVisibility;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.scoreboard.Team;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SpigotTeam implements WSTeam {

    private Team team;

    public SpigotTeam(Team team) {
        Validate.notNull(team, "Team cannot be null!");
        this.team = team;
    }

    @Override
    public String getName() {
        return team.getName();
    }

    @Override
    public WSText getDisplayName() {
        return WSText.getByFormattingText(team.getDisplayName());
    }

    @Override
    public void setDisplayName(@Nullable WSText name) {
        if (name == null) team.setDisplayName(null);
        else team.setDisplayName(name.toFormattingText());
    }

    @Override
    public WSText getPrefix() {
        return WSText.getByFormattingText(team.getPrefix());
    }

    @Override
    public void setPrefix(@Nullable WSText prefix) {
        if (prefix == null) team.setPrefix(null);
        else team.setPrefix(prefix.toFormattingText());
    }

    @Override
    public WSText getSuffix() {
        return WSText.getByFormattingText(team.getSuffix());
    }

    @Override
    public void setSuffix(@Nullable WSText suffix) {
        if (suffix == null) team.setSuffix(null);
        else team.setSuffix(suffix.toFormattingText());
    }

    @Override
    public boolean allowFriendlyFire() {
        return team.allowFriendlyFire();
    }

    @Override
    public void setAllowFriendlyFire(boolean enabled) {
        team.setAllowFriendlyFire(enabled);
    }

    @Override
    public boolean canSeeFriendlyInvisibles() {
        return team.canSeeFriendlyInvisibles();
    }

    @Override
    public void setCanSeeFriendlyInvisibles(boolean enabled) {
        team.setCanSeeFriendlyInvisibles(enabled);
    }

    @Override
    public EnumVisibility getNameTagVisibility() {
        return EnumVisibility.getBySpigotName(team.getOption(Team.Option.NAME_TAG_VISIBILITY).name()).orElse(EnumVisibility.ALWAYS);
    }

    @Override
    public void setNameTagVisibility(EnumVisibility visibility) {
        Validate.notNull(visibility, "Visibility cannot be null!");
        team.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.valueOf(visibility.getSpigotName()));
    }

    @Override
    public EnumVisibility getDeathMessageVisibility() {
        return EnumVisibility.getBySpigotName(team.getOption(Team.Option.DEATH_MESSAGE_VISIBILITY).name()).orElse(EnumVisibility.ALWAYS);
    }

    @Override
    public void setDeathMessageVisibility(EnumVisibility visibility) {
        Validate.notNull(visibility, "Visibility cannot be null!");
        team.setOption(Team.Option.DEATH_MESSAGE_VISIBILITY, Team.OptionStatus.valueOf(visibility.getSpigotName()));
    }

    @Override
    public EnumCollisionRule getCollisionRule() {
        return EnumCollisionRule.getBySpigotName(team.getOption(Team.Option.COLLISION_RULE).name()).orElse(EnumCollisionRule.ALWAYS);
    }

    @Override
    public void setCollisionRule(EnumCollisionRule collisionRule) {
        Validate.notNull(collisionRule, "Collision rule cannot be null!");
        team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.valueOf(collisionRule.getSpigotName()));
    }

    @Override
    public Set<WSText> getMembers() {
        return team.getEntries().stream().map(WSText::getByFormattingText).collect(Collectors.toSet());
    }

    @Override
    public void addMember(WSText member) {
        Validate.notNull(member, "Member cannot be null!");
        team.addEntry(member.toFormattingText());
    }

    @Override
    public boolean removeMember(WSText member) {
        Validate.notNull(member, "Member cannot be null!");
        return team.removeEntry(member.toFormattingText());
    }

    @Override
    public boolean hasMember(WSText member) {
        Validate.notNull(member, "Member cannot be null!");
        return team.hasEntry(member.toFormattingText());
    }

    @Override
    public Optional<WSScoreboard> getScoreboard() {
        try {
            return Optional.of(new SpigotScoreboard(team.getScoreboard()));
        } catch (Throwable ex) {
            return Optional.empty();
        }
    }

    @Override
    public boolean unregister() {
        try {
            team.unregister();
            return true;
        } catch (Throwable ex) {
            return false;
        }
    }

    @Override
    public Team getHandled() {
        return team;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpigotTeam that = (SpigotTeam) o;

        return team.equals(that.team);
    }

    @Override
    public int hashCode() {
        return team.hashCode();
    }
}
