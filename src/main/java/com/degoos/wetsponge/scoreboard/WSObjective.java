package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.enums.EnumCriteria;

import java.util.Map;
import java.util.Optional;

public interface WSObjective {

    String getName();

    WSText getDisplayName();

    void setDisplayName(WSText text);

    EnumCriteria getCriteria();

    Map<WSText, WSScore> getScores();

    boolean hasScore(WSText name);

    default Optional<WSScore> getScore(WSText name) {
        if (!hasScore(name)) return Optional.empty();
        return Optional.of(getOrCreateScore(name));
    }

    WSScore getOrCreateScore(WSText name);

    boolean removeScore(WSText name);

    boolean removeScore(WSScore score);

    void removeAllScoresWithScore (int score);

    Optional<WSScoreboard> getScoreboard();

    Object getHandled();
}
