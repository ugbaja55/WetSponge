package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.enums.EnumCollisionRule;
import com.degoos.wetsponge.enums.EnumVisibility;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;

public interface WSTeam {

    String getName();

    WSText getDisplayName();

    void setDisplayName(@Nullable WSText name);

    WSText getPrefix();

    void setPrefix(@Nullable WSText prefix);

    WSText getSuffix();

    void setSuffix(@Nullable WSText suffix);

    boolean allowFriendlyFire();

    void setAllowFriendlyFire(boolean enabled);

    boolean canSeeFriendlyInvisibles();

    void setCanSeeFriendlyInvisibles(boolean enabled);

    EnumVisibility getNameTagVisibility();

    void setNameTagVisibility(EnumVisibility visibility);

    EnumVisibility getDeathMessageVisibility();

    void setDeathMessageVisibility(EnumVisibility visibility);

    EnumCollisionRule getCollisionRule();

    void setCollisionRule(EnumCollisionRule collisionRule);

    Set<WSText> getMembers();

    void addMember(WSText member);

    boolean removeMember(WSText member);

    boolean hasMember (WSText member);

    Optional<WSScoreboard> getScoreboard();

    boolean unregister();

    Object getHandled();
}
