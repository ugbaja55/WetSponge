package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;

public abstract class WSObjectiveReplacement {

	private String key;

	public WSObjectiveReplacement(String key) {
		Validate.notNull(key, "Key cannot be null!");
		this.key = key;
	}


	public String getKey() {
		return key;
	}

	public abstract WSText getReplacement(WSPlayer player);

	public WSText apply(WSText text, WSPlayer player) {
		WSText replacement = getReplacement(player);
		if (replacement == null) return text;
		return WSText.getByFormattingText(text.toFormattingText().replace(key, replacement.toFormattingText()));
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		if (object == null || getClass() != object.getClass()) return false;

		WSObjectiveReplacement that = (WSObjectiveReplacement) object;

		return key.equals(that.key);
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}
}
