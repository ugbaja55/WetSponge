package com.degoos.wetsponge.console;


import com.degoos.wetsponge.command.SpigotCommandSource;
import org.bukkit.command.ConsoleCommandSender;

public class SpigotConsoleSource extends SpigotCommandSource implements WSConsoleSource {

	public SpigotConsoleSource (ConsoleCommandSender source) {
		super(source);
	}


	@Override
	public String getName () {
		return getHandled().getName();
	}


	@Override
	public ConsoleCommandSender getHandled () {
		return (ConsoleCommandSender) super.getHandled();
	}
}
