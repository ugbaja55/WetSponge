package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;

public class WSEnchantments {

    public static final WSEnchantment AQUA_AFFINITY = of("AQUA_AFFINITY", 6);
    public static final WSEnchantment BANE_OF_ARTHROPOD = of("BANE_OF_ARTHROPODS", 18);
    public static final WSEnchantment BINDING_CURSE = of("BINDING_CURSE", 10);
    public static final WSEnchantment BLAST_PROTECTION = of("BLAST_PROTECTION", 3);
    public static final WSEnchantment DEPTH_STRIDER = of("DEPTH_STRIDER", 8);
    public static final WSEnchantment EFFICIENCY = of("EFFICIENCY", 32);
    public static final WSEnchantment FEATHER_FALLING = of("FEATHER_FALLING", 2);
    public static final WSEnchantment FIRE_ASPECT = of("FIRE_ASPECT", 20);
    public static final WSEnchantment FIRE_PROTECTION = of("FIRE_PROTECTION", 1);
    public static final WSEnchantment FLAME = of("FLAME", 50);
    public static final WSEnchantment FORTUNE = of("FORTUNE", 35);
    public static final WSEnchantment FROST_WALKER = of("FROST_WALKER", 9);
    public static final WSEnchantment INFINITY = of("INFINITY", 51);
    public static final WSEnchantment KNOCKBACK = of("KNOCKBACK", 19);
    public static final WSEnchantment LOOTING = of("LOOTING", 21);
    public static final WSEnchantment LUCK_OF_THE_SEA = of("LUCK_OF_THE_SEA", 61);
    public static final WSEnchantment LURE = of("LURE", 62);
    public static final WSEnchantment MENDING = of("MENDING", 70);
    public static final WSEnchantment POWER = of("POWER", 48);
    public static final WSEnchantment PROJECTILE_PROTECTION = of("PROJECTILE_PROTECTION", 4);
    public static final WSEnchantment PROTECTION = of("PROTECTION", 0);
    public static final WSEnchantment PUNCH = of("PUNCH", 49);
    public static final WSEnchantment RESPIRATION = of("RESPIRATION", 5);
    public static final WSEnchantment SHARPNESS = of("SHARPNESS", 16);
    public static final WSEnchantment SILK_TOUCH = of("SILK_TOUCH", 33);
    public static final WSEnchantment SMITE = of("SMITE", 17);
    /**
     * Not implemented by Spigot.
     */
    public static final WSEnchantment SWEEPING = of("SWEEPING", 10000);
    public static final WSEnchantment THORNS = of("THORNS", 7);
    public static final WSEnchantment UNBREAKING = of("UNBREAKING", 34);
    public static final WSEnchantment VANISHING_CURSE = of("VANISHING_CURSE", 71);


    private static WSEnchantment of(String spongeId, int spigotId) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotEnchantment(spigotId);
            case SPONGE:
                return new SpongeEnchantment(spongeId);
            default:
                return null;
        }
    }


}
