package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class SpigotEnchantment implements WSEnchantment {

    private Enchantment enchantment;

    public SpigotEnchantment(Enchantment enchantment) {
        this.enchantment = enchantment;
    }

    public SpigotEnchantment(int spigotId) {
        enchantment = Enchantment.getById(spigotId);
        if (enchantment == null) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
    }

    public SpigotEnchantment(int id, String name, int minimumLevel, int maximumLevel, WSEnchantmentPrototype prototype) {
        enchantment = new Enchantment(id) {
            @Override
            public String getName() {
                return name;
            }

            @Override
            public int getMaxLevel() {
                return maximumLevel;
            }

            @Override
            public int getStartLevel() {
                return minimumLevel;
            }

            @Override
            public EnchantmentTarget getItemTarget() {
                return EnchantmentTarget.ALL;
            }

            @Override
            public boolean isTreasure() {
                return false;
            }

            @Override
            public boolean isCursed() {
                return false;
            }

            @Override
            public boolean conflictsWith(Enchantment enchantment) {
                return !prototype.isCompatibleWith(new SpigotEnchantment(enchantment));
            }

            @Override
            public boolean canEnchantItem(ItemStack itemStack) {
                return prototype.canBeAppliedToStack(new SpigotItemStack(itemStack));
            }
        };
    }

    @Override
    public WSTranslation getTranslation() {
        throw new IllegalAccessError("Not supported by Spigot");
    }

    @Override
    public String getName() {
        return enchantment.getName();
    }

    @Override
    public int getMaximumLevel() {
        return enchantment.getMaxLevel();
    }

    @Override
    public int getMinimumLevel() {
        return enchantment.getStartLevel();
    }

    @Override
    public boolean canBeAppliedToStack(WSItemStack itemStack) {
        return enchantment.canEnchantItem(((SpigotItemStack) itemStack).getHandled());
    }

    @Override
    public boolean isCompatibleWith(WSEnchantment enchantment) {
        return !this.enchantment.conflictsWith(((SpigotEnchantment) enchantment).getHandled());
    }

    @Override
    public Enchantment getHandled() {
        return enchantment;
    }
}
