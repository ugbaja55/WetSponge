package com.degoos.wetsponge.rcon;


import com.degoos.wetsponge.command.SpigotCommandSource;
import org.bukkit.command.RemoteConsoleCommandSender;

public class SpigotRconSource extends SpigotCommandSource implements WSRconSource {

	public SpigotRconSource (RemoteConsoleCommandSender source) {
		super(source);
	}


	@Override
	public String getName () {
		return getHandled().getName();
	}


	@Override
	public RemoteConsoleCommandSender getHandled () {
		return (RemoteConsoleCommandSender) super.getHandled();
	}
}
