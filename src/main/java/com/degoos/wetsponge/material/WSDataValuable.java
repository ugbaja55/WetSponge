package com.degoos.wetsponge.material;


import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.itemType.WSItemType;
import com.degoos.wetsponge.material.WSMaterial;

/**
 * This class represents a DataValuable object.
 * DataValues are sub-IDs used by Spigot.
 * It may be a {@link WSBlockType block type} or a {@link WSItemType item type}.
 */
public interface WSDataValuable {

    /**
     * This method returns the DataValue of the {@link WSMaterial}.
     * DataValues are used by Spigot.
     *
     * @return the DataValue.
     */
    short getDataValue();

    /**
     * This method sets the DataValue of the {@link WSMaterial}.
     * DataValues are used by Spigot.
     *
     * @param dataValue the DataValue.
     */
    void setDataValue(int dataValue);

}
