package com.degoos.wetsponge.material;


import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.material.itemType.WSItemType;
import com.degoos.wetsponge.material.itemType.WSItemTypes;
import com.degoos.wetsponge.text.translation.WSTranslatable;
import java.util.Optional;

public interface WSMaterial extends WSTranslatable {

	/**
	 * Returns a new instance of a {@link WSMaterial}. This instance can be a {@link WSBlockType} or a {@link WSItemType}, depending of the ID.
	 *
	 * @param id the {@link Integer} id of the {@link WSMaterial}.
	 * @param dataValue the DataValue of the {@link WSMaterial}. If the {@link WSMaterial} has not a DataValue, set it to 0.
	 *
	 * @return the {@link WSMaterial} requested, if present.
	 */
	public static Optional<? extends WSMaterial> of(int id, int dataValue) {
		Optional<? extends WSMaterial> optional = WSItemTypes.getType(id, dataValue);
		if (!optional.isPresent()) return WSBlockTypes.getType(id, dataValue);
		else return optional;
	}

	/**
	 * Returns a new instance of a {@link WSMaterial}. This instance can be a {@link WSBlockType} or a {@link WSItemType}, depending of the ID.
	 *
	 * @param id the {@link String} id of the {@link WSMaterial}.
	 *
	 * @return the {@link WSMaterial} requested, if present.
	 */
	public static Optional<? extends WSMaterial> of(String id) {
		Optional<? extends WSMaterial> optional = WSItemTypes.getType(id);
		if (!optional.isPresent()) return WSBlockTypes.getType(id);
		else return optional;
	}

	/**
	 * This method returns the {@link Integer} id of the material of this block. This ID is deprecated, but Spigot uses it as a base ID.
	 *
	 * @return the material id of the block.
	 */
	int getId();

	/**
	 * This method returns the {@link String} id of the material of this block. This ID is used by Sponge as a base ID.
	 *
	 * @return the material id of the block.
	 */
	String getStringId();

	/**
	 * @return returns a clone of this object.
	 */
	WSMaterial clone();

	/**
	 * @return the maximum amount of items a stack of this material can hold.
	 */
	int getMaxStackQuantity();
}
