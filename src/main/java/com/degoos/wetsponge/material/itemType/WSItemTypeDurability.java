package com.degoos.wetsponge.material.itemType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSItemTypeDurability extends WSItemType implements WSDataValuable {

    private int durability, maxDurability;


    public WSItemTypeDurability(int id, String stringId, int maxDurability) {
        super(id, stringId, 1);
        this.durability = 0;
        this.maxDurability = maxDurability;
    }


    /**
     * @return the durability of this item.
     */
    public int getDurability() {
        return durability;
    }


    /**
     * This method sets the durability of this item.
     *
     * @param durability the durability.
     * @return this {@link WSItemTypeDurability}.
     */
    public WSItemTypeDurability setDurability(int durability) {
        this.durability = durability;
        return this;
    }


    /**
     * @return the maximum durability of this item.
     */
    public int getMaxDurability() {
        return maxDurability;
    }

    @Override
    public WSItemTypeDurability clone() {
        return new WSItemTypeDurability(getId(), getStringId(), durability);
    }


    @Override
    public short getDataValue() {
        return (short) durability;
    }

    @Override
    public void setDataValue(int dataValue) {
        durability = dataValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSItemTypeDurability that = (WSItemTypeDurability) o;

        if (durability != that.durability) return false;
        return maxDurability == that.maxDurability;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + durability;
        result = 31 * result + maxDurability;
        return result;
    }
}
