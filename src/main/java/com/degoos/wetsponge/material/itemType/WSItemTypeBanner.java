package com.degoos.wetsponge.material.itemType;


import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.WSDataValuable;
import java.util.List;
import java.util.stream.Collectors;

public class WSItemTypeBanner extends WSItemType implements WSDataValuable {

	private EnumDyeColor baseColor;
	private List<WSBannerPattern> patterns;

	public WSItemTypeBanner(EnumDyeColor baseColor, List<WSBannerPattern> patterns) {
		super(425, "minecraft:banner", 64);
		this.baseColor = baseColor;
		this.patterns = patterns;
	}

	/**
	 * Returns the {@link EnumDyeColor dye color} of the {@link WSItemTypeBanner banner}.
	 *
	 * @return the {@link EnumDyeColor dye color}.
	 */
	public EnumDyeColor getBaseColor() {
		return baseColor;
	}

	/**
	 * Sets the {@link EnumDyeColor dye color} of the {@link WSItemTypeBanner banner}.
	 *
	 * @param baseColor the {@link EnumDyeColor dye color}.
	 *
	 * @return the {@link WSItemTypeBanner banner}.
	 */
	public WSItemTypeBanner setBaseColor(EnumDyeColor baseColor) {
		this.baseColor = baseColor;
		return this;
	}

	/**
	 * Returns a mutable list with all {@link WSBannerPattern banner pattern}s of the {@link WSItemTypeBanner banner}.
	 *
	 * @return a mutable list with all {@link WSBannerPattern banner pattern}s of the {@link WSItemTypeBanner banner}.
	 */
	public List<WSBannerPattern> getPatterns() {
		return patterns;
	}


	@Override
	public WSItemTypeBanner clone() {
		return new WSItemTypeBanner(baseColor, patterns.stream().map(pattern -> new WSBannerPattern(pattern.getShape(), pattern.getColor()))
			.collect(Collectors.toList()));
	}


	@Override
	public short getDataValue() {
		return (short) baseColor.getDyeData();
	}

	@Override
	public void setDataValue(int dataValue) {
		baseColor = EnumDyeColor.getByDyeData((byte) dataValue).orElse(EnumDyeColor.BLACK);
	}
}
