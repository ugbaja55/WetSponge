package com.degoos.wetsponge.material.itemType;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.enums.item.EnumBookGeneration;
import com.degoos.wetsponge.enums.item.EnumCoalType;
import com.degoos.wetsponge.enums.item.EnumGoldenAppleType;
import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.text.WSText;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public enum WSItemTypes {

	IRON_SHOVEL(new WSItemTypeDurability(256, "minecraft:iron_shovel", 250)),
	IRON_PICKAXE(new WSItemTypeDurability(257, "minecraft:iron_pickaxe", 250)),
	IRON_AXE(new WSItemTypeDurability(258, "minecraft:iron_axe", 250)),
	FLINT_AND_STEEL(new WSItemTypeDurability(259, "minecraft:flint_and_steel", 64)),
	APPLE(new WSItemType(260, "minecraft:apple", 64)),
	BOW(new WSItemTypeDurability(261, "minecraft:bow", 384)),
	ARROW(new WSItemType(262, "minecraft:arrow", 64)),
	COAL(new WSItemTypeCoal(EnumCoalType.COAL)),
	//id: 263
	DIAMOND(new WSItemType(264, "minecraft:diamond", 64)),
	IRON_INGOT(new WSItemType(265, "minecraft:iron_ingot", 64)),
	GOLD_INGOT(new WSItemType(266, "minecraft:gold_ingot", 64)),
	IRON_SWORD(new WSItemTypeDurability(267, "minecraft:iron_sword", 251)),
	WOODEN_SWORD(new WSItemTypeDurability(268, "minecraft:wooden_sword", 60)),
	WOODEN_SHOVEL(new WSItemTypeDurability(269, "minecraft:wooden_shovel", 60)),
	WOODEN_PICKAXE(new WSItemTypeDurability(270, "minecraft:wooden_pickaxe", 60)),
	WOODEN_AXE(new WSItemTypeDurability(271, "minecraft:wooden_axe", 60)),
	STONE_SWORD(new WSItemTypeDurability(272, "minecraft:stone_sword", 132)),
	STONE_SHOVEL(new WSItemTypeDurability(273, "minecraft:stone_shovel", 132)),
	STONE_PICKAXE(new WSItemTypeDurability(274, "minecraft:stone_pickaxe", 132)),
	STONE_AXE(new WSItemTypeDurability(275, "minecraft:stone_axe", 132)),
	DIAMOND_SWORD(new WSItemTypeDurability(276, "minecraft:diamond_sword", 1562)),
	DIAMOND_SHOVEL(new WSItemTypeDurability(277, "minecraft:diamond_shovel", 1562)),
	DIAMOND_PICKAXE(new WSItemTypeDurability(278, "minecraft:diamond_pickaxe", 1562)),
	DIAMOND_AXE(new WSItemTypeDurability(279, "minecraft:diamond_axe", 1562)),
	STICK(new WSItemType(280, "minecraft:stick", 64)),
	BOWL(new WSItemType(281, "minecraft:bowl", 64)),
	MUSHROOM_STEW(new WSItemType(282, "minecraft:mushroom_stew", 64)),
	GOLD_SWORD(new WSItemTypeDurability(283, "minecraft:gold_sword", 33)),
	GOLD_SHOVEL(new WSItemTypeDurability(284, "minecraft:gold_shovel", 33)),
	GOLD_PICKAXE(new WSItemTypeDurability(285, "minecraft:gold_pickaxe", 33)),
	GOLD_AXE(new WSItemTypeDurability(286, "minecraft:gold_axe", 33)),
	STRING(new WSItemType(287, "minecraft:string", 64)),
	FEATHER(new WSItemType(288, "minecraft:feather", 64)),
	GUNPOWDER(new WSItemType(289, "minecraft:gunpowder", 64)),
	WOODEN_HOE(new WSItemTypeDurability(290, "minecraft:wooden_hoe", 60)),
	STONE_HOE(new WSItemTypeDurability(291, "minecraft:stone_hoe", 132)),
	IRON_HOE(new WSItemTypeDurability(292, "minecraft:iron_hoe", 251)),
	DIAMOND_HOE(new WSItemTypeDurability(293, "minecraft:diamond_hoe", 1562)),
	GOLD_HOE(new WSItemTypeDurability(294, "minecraft:gold_hoe", 33)),
	WHEAT_SEEDS(new WSItemType(295, "minecraft:wheat_seeds", 64)),
	WHEAT(new WSItemType(296, "minecraft:wheat", 64)),
	BREAD(new WSItemType(297, "minecraft:bread", 64)),
	LEATHER_HELMET(new WSItemTypeLeatherArmor(298, "minecraft:leather_helmet", 56, null)),
	LEATHER_CHESTPLATE(new WSItemTypeLeatherArmor(299, "minecraft:leather_chestplate", 81, null)),
	LEATHER_LEGGINGS(new WSItemTypeLeatherArmor(300, "minecraft:leather_leggings", 76, null)),
	LEATHER_BOOTS(new WSItemTypeLeatherArmor(301, "minecraft:leather_boots", 66, null)),
	CHAINMAIL_HELMET(new WSItemTypeDurability(302, "minecraft:chainmail_helmet", 166)),
	CHAINMAIL_CHESTPLATE(new WSItemTypeDurability(303, "minecraft:chainmail_chestplate", 241)),
	CHAINMAIL_LEGGINGS(new WSItemTypeDurability(304, "minecraft:chainmail_leggings", 226)),
	CHAINMAIL_BOOTS(new WSItemTypeDurability(305, "minecraft:chainmail_boots", 196)),
	IRON_HELMET(new WSItemTypeDurability(306, "minecraft:iron_helmet", 166)),
	IRON_CHESTPLATE(new WSItemTypeDurability(307, "minecraft:iron_chestplate", 241)),
	IRON_LEGGINGS(new WSItemTypeDurability(308, "minecraft:iron_leggings", 226)),
	IRON_BOOTS(new WSItemTypeDurability(309, "minecraft:iron_boots", 196)),
	DIAMOND_HELMET(new WSItemTypeDurability(310, "minecraft:diamond_helmet", 364)),
	DIAMOND_CHESTPLATE(new WSItemTypeDurability(311, "minecraft:diamond_chestplate", 529)),
	DIAMOND_LEGGINGS(new WSItemTypeDurability(312, "minecraft:diamond_leggings", 496)),
	DIAMOND_BOOTS(new WSItemTypeDurability(313, "minecraft:diamond_boots", 430)),
	GOLD_HELMET(new WSItemTypeDurability(314, "minecraft:gold_helmet", 78)),
	GOLD_CHESTPLATE(new WSItemTypeDurability(315, "minecraft:gold_chestplate", 113)),
	GOLD_LEGGINGS(new WSItemTypeDurability(316, "minecraft:gold_leggings", 106)),
	GOLD_BOOTS(new WSItemTypeDurability(317, "minecraft:gold_boots", 92)),
	FLINT(new WSItemType(318, "minecraft:flint", 64)),
	RAW_PORKCHOP(new WSItemType(319, "minecraft:porkchop", 64)),
	COOKED_PORKCHOP(new WSItemType(320, "minecraft:cooked_porkchop", 64)),
	PAINTING(new WSItemType(321, "minecraft:painting", 64)),
	GOLDEN_APPLE(new WSItemTypeGoldenApple(EnumGoldenAppleType.GOLDEN_APPLE)),
	//id: 322
	SIGN(new WSItemType(323, "minecraft:sign", 64)),
	WOODEN_DOOR(new WSItemType(324, "minecraft:wooden_door", 64)),
	BUCKET(new WSItemType(325, "minecraft:bucket", 16)),
	WATER_BUCKET(new WSItemType(326, "minecraft:water_bucket", 1)),
	LAVA_BUCKET(new WSItemType(327, "minecraft:lava_bucket", 1)),
	MINECART(new WSItemType(328, "minecraft:minecart", 1)),
	SADDLE(new WSItemType(329, "minecraft:saddle", 1)),
	IRON_DOOR(new WSItemType(330, "minecraft:iron_door", 64)),
	REDSTONE(new WSItemType(331, "minecraft:redstone", 64)),
	SNOWBALL(new WSItemType(332, "minecraft:snowball", 16)),
	BOAT(new WSItemType(333, "minecraft:boat", 1)),
	LEATHER(new WSItemType(334, "minecraft:leather", 64)),
	MILK_BUCKET(new WSItemType(335, "minecraft:milk_bucket", 1)),
	BRICK(new WSItemType(336, "minecraft:brick", 64)),
	CLAY_BALL(new WSItemType(337, "minecraft:clay_ball", 64)),
	SUGAR_CANE(new WSItemType(338, "minecraft:reeds", 64)),
	PAPER(new WSItemType(339, "minecraft:paper", 64)),
	BOOK(new WSItemType(340, "minecraft:book", 64)),
	SLIME_BALL(new WSItemType(341, "minecraft:slime_ball", 64)),
	MINECART_WITH_CHEST(new WSItemType(342, "minecraft:chest_minecart", 1)),
	MINECART_WITH_FURNACE(new WSItemType(343, "minecart:furnace_minecart", 1)),
	EGG(new WSItemType(344, "minecraft:egg", 16)),
	COMPASS(new WSItemType(345, "minecraft:compass", 1)),
	FISHING_ROD(new WSItemTypeDurability(346, "minecraft:fishing_rod", 65)),
	CLOCK(new WSItemType(347, "minecraft:clock", 1)),
	GLOWSTONE_DUST(new WSItemType(348, "minecraft:glowstone_dust", 64)),
	//MORE ITEMS...
	BONE(new WSItemType(352, "minecraft:bone", 64)),
	SUGAR(new WSItemType(353, "minecraft:sugar", 64)),
	CAKE(new WSItemType(354, "minecraft:cake", 64)),
	BED(new WSItemType(355, "minecraft:bed", 1)),
	REDSTONE_REPEATER(new WSItemType(356, "minecraft:repeater", 64)),
	COOKIE(new WSItemType(357, "minecraft:cookie", 64)),
	FILLED_MAP(new WSItemTypeMap(0)),
	SHEARS(new WSItemType(359, "minecraft:shears", 1)),
	MELON(new WSItemType(360, "minecraft:melon", 64)),
	PUMPKIN_SEEDS(new WSItemType(361, "minecraft:pumpkin_seeds", 64)),
	MELON_SEEDS(new WSItemType(362, "minecraft:melon_seeds", 64)),
	RAW_BEEF(new WSItemType(363, "minecraft:beef", 64)),
	COOKED_BEEF(new WSItemType(364, "minecraft:cooked_beef", 64)),
	RAW_CHICKEN(new WSItemType(365, "minecraft:chicken", 64)),
	COOKED_CHICKEN(new WSItemType(366, "minecraft:cooked_chicken", 64)),
	ROTTEN_FLESH(new WSItemType(367, "minecraft:rotten_flesh", 64)),
	ENDER_PEARL(new WSItemType(368, "minecraft:ender_pearl", 16)),
	BLAZE_ROD(new WSItemType(369, "minecraft:blaze_rod", 64)),
	GHAST_TEAR(new WSItemType(370, "minecraft:ghast_tear", 64)),
	GOLD_NUGGET(new WSItemType(371, "minecraft:gold_nugget", 64)),
	NETHER_WARTS(new WSItemType(372, "minecraft:nether_wart", 64)),
	GLASS_BOTTLE(new WSItemType(374, "minecraft:glass_bottle", 64)),
	SPIDER_EYE(new WSItemType(375, "minecraft:spider_eye", 64)),
	FERMENTED_SPIDER_EYE(new WSItemType(376, "minecraft:fermented_spider_eye", 64)),
	BLAZE_POWDER(new WSItemType(377, "minecraft:blaze_powder", 64)),
	MAGMA_CREAM(new WSItemType(378, "minecraft:magma_cream", 64)),
	BREWING_STAND(new WSItemType(379, "minecraft:brewing_stand", 64)),
	CAULDRON(new WSItemType(380, "minecraft:cauldron", 64)),
	ENDER_EYE(new WSItemType(381, "minecraft:ender_eye", 64)),
	SPECKLED_MELON(new WSItemType(382, "minecraft:speckled_melon", 64)),
	SPAWN_EGG(new WSItemTypeSpawnEgg(EnumEntityType.PIG)),
	EXP_BOTTLE(new WSItemType(384, "minecraft:experience_bottle", 64)),
	//todo fireball in bukkit... bad name :P
	FIRE_CHARGE(new WSItemType(385, "minecraft:fire_charge", 64)),
	WRITABLE_BOOK(new WSItemTypeWrittenBook(386, "minecraft:writable_book", WSText.empty(), WSText.empty(), new ArrayList<>(), EnumBookGeneration.ORIGINAL)),
	WRITTEN_BOOK(new WSItemTypeWrittenBook(387, "minecraft:written_book", WSText.empty(), WSText.empty(), new ArrayList<>(), EnumBookGeneration.ORIGINAL)),
	EMERALD(new WSItemType(388, "minecraft:emerald", 64)),
	ITEM_FRAME(new WSItemType(389, "minecraft:item_frame", 64)),
	FLOWER_POT(new WSItemType(390, "minecraft:flower_pot", 64)),
	CARROT(new WSItemType(391, "minecraft:carrot", 64)),
	POTATO(new WSItemType(392, "minecraft:potato", 64)),
	BAKED_POTATO(new WSItemType(393, "minecraft:baked_potato", 64)),
	POISONOUS_POTATO(new WSItemType(394, "minecraft:poisonous_potato", 64)),
	EMPTY_MAP(new WSItemType(395, "minecraft:map", 64)),
	GOLDEN_CARROT(new WSItemType(396, "minecraft:golden_carrot", 64)),
	SKULL(new WSItemTypeSkull(EnumSkullType.SKELETON)),
	// id: 397
	CARROT_STICK(new WSItemType(398, "minecraft:carrot_on_a_stick", 64)),
	NETHER_STAR(new WSItemType(399, "minecraft:nether_star", 64)),
	PUMPKIN_PIE(new WSItemType(400, "minecraft:pumpkin_pie", 64)),
	FIREWORK(new WSItemTypeFirework()),
	FIREWORK_CHARGE(new WSItemTypeFireworkCharge()),
	ENCHANTED_BOOK(new WSItemType(403, "minecraft:enchanted_book", 1)),
	REDSTONE_COMPARATOR(new WSItemType(404, "minecraft:redstone_comparator", 64)),
	NETHER_BRICK_ITEM(new WSItemType(405, "minecraft:netherbrick", 64)),
	QUARTZ(new WSItemType(406, "minecraft:quartz", 64)),
	MINECART_WITH_TNT(new WSItemType(407, "minecraft:tnt_minecart", 64)),
	MINECART_WITH_HOPPER(new WSItemType(408, "minecraft:hopper_minecart", 64)),
	PRISMARINE_SHARD(new WSItemType(409, "minecraft:prismarine_shard", 64)),
	PRISMARINE_CRISTALS(new WSItemType(410, "minecraft:prismarine_cristals", 64)),
	RAW_RABBIT(new WSItemType(411, "minecraft:rabbit", 64)),
	COOKED_RABBIT(new WSItemType(412, "minecraft:cooked_rabbit", 64)),
	RABBIT_STEW(new WSItemType(413, "minecraft:rabbit_stew", 64)),
	RABBIT_FOOT(new WSItemType(414, "minecraft:rabbit_foot", 64)),
	RABBIT_HIDE(new WSItemType(415, "minecraft:rabbit_hide", 64)),
	ARMOR_STAND(new WSItemType(416, "minecraft:armor_stand", 64)),
	IRON_HORSE_ARMOR(new WSItemType(417, "minecraft:iron_horse_armor", 64)),
	GOLD_HORSE_ARMOR(new WSItemType(418, "minecraft:golden_horse_armor", 64)),
	DIAMOND_HORSE_ARMOR(new WSItemType(419, "minecraft:diamond_horse_armor", 64)),
	LEAD(new WSItemType(420, "minecraft:lead", 64)),
	NAME_TAG(new WSItemType(421, "minecraft:name_tag", 64)),
	MINECART_WITH_COMMAND_BLOCK(new WSItemType(422, "minecraft:command_block_minecart", 64)),
	RAW_MUTTON(new WSItemType(423, "minecraft:mutton", 64)),
	COOKED_MUTTON(new WSItemType(424, "minecraft:cooked_mutton", 64)),
	BANNER (new WSItemTypeBanner(EnumDyeColor.BLACK, new ArrayList<>())),
	END_CRYSTAL(new WSItemType(426, "minecraft:end_crystal", 64)),
	WOODEN_DOOR_SPRUCE(new WSItemType(427, "minecraft:spruce_door", 64)),
	WOODEN_DOOR_BIRCH(new WSItemType(428, "minecraft:birch_door", 64)),
	WOODEN_DOOR_JUNGLE(new WSItemType(429, "minecraft:jungle_door", 64)),
	WOODEN_DOOR_ACACIA(new WSItemType(430, "minecraft:acacia_door", 64)),
	WOODEN_DOOR_DARK_OAK(new WSItemType(431, "minecraft:dark_oak_door", 64)),
	CHORUS_FRUIT(new WSItemType(432, "minecraft:chorus_fruit", 64)),
	CHORUS_FRUIT_POPPED(new WSItemType(433, "minecraft:chorus_fruit_popped", 64)),
	BEETROOT(new WSItemType(434, "minecraft:beetroot", 64)),
	BEETROOT_SEEDS(new WSItemType(435, "minecraft:beetroot_seeds", 64)),
	BEETROOT_SOUP(new WSItemType(436, "minecraft:beetroot_soup", 64)),
	DRAGON_BREATH(new WSItemType(437, "minecraft:dragon_breath", 64)),
	// id: 438 - not used yet
	ARROW_SPECTRAL(new WSItemType(439, "minecraft:spectral_arrow", 64)),
	// id: 440 - not used yet
	// id: 441 - not used yet
	SHIELD(new WSItemType(442, "minecraft:", 64)),
	ELYTRA(new WSItemType(443, "minecraft:elytra", 64)),
	// id: 444 - not used yet
	// id: 445 - not used yet
	// id: 446 - not used yet
	// id: 447 - not used yet
	// id: 448 - not used yet
	TOTEM(new WSItemType(449, "minecraft:totem_of_undying", 64)),
	SHULKER_SHELL(new WSItemType(450, "minecraft:shulker_shell", 64)),
	// copypaste easifier -> (new WSItemType(, "minecraft:", 64)),
	RECORD_13(new WSItemType(2256, "minecraft:record_13", 64)),
	RECORD_CAT(new WSItemType(2257, "minecraft:record_cat", 64)),
	RECORD_BLOCKS(new WSItemType(2258, "minecraft:record_blocks", 64)),
	RECORD_CHIRP(new WSItemType(2259, "minecraft:record_chirp", 64)),
	RECORD_FAR(new WSItemType(2260, "minecraft:record_far", 64)),
	RECORD_MALL(new WSItemType(2261, "minecraft:record_mall", 64)),
	RECORD_MELLOHI(new WSItemType(2262, "minecraft:record_mellohi", 64)),
	RECORD_STAL(new WSItemType(2263, "minecraft:record_stal", 64)),
	RECORD_STRAD(new WSItemType(2264, "minecraft:record_strad", 64)),
	RECORD_WARD(new WSItemType(2265, "minecraft:record_ward", 64)),
	RECORD_11(new WSItemType(2266, "minecraft:record_11", 64)),
	RECORD_WAIT(new WSItemType(2267, "minecraft:record_wait", 64));


	private WSItemType defaultType;


	WSItemTypes(WSItemType defaultType) {
		this.defaultType = defaultType;
	}


	/**
	 * This method returns an optional of a {@link WSItemType}.
	 *
	 * @param id the {@link Integer} ID of the {@link WSItemType}.
	 * @param dataValue the DataValue of the {@link WSItemType}.
	 *
	 * @return the {@link WSItemType}, if present.
	 */
	public static Optional<WSItemType> getType(int id, int dataValue) {
		Optional<WSItemType> optional = Arrays.stream(values()).filter(type -> type.getId() == id).map(WSItemTypes::getDefaultType).findAny();
		if (!optional.isPresent()) return Optional.empty();
		WSItemType itemType = optional.get();
		setDataValue(itemType, dataValue);
		return Optional.of(itemType);
	}


	/**
	 * This method returns an optional of a {@link WSItemType}.
	 *
	 * @param id the {@link String} ID of the {@link WSItemType}.
	 *
	 * @return the {@link WSItemType}, if present.
	 */
	public static Optional<WSItemType> getType(String id) {
		Optional<WSItemType> optional = Arrays.stream(values()).filter(type -> type.getStringId().equals(id)).map(WSItemTypes::getDefaultType).findAny();
		if (!optional.isPresent()) return Optional.empty();
		WSItemType itemType = optional.get();
		return Optional.of(itemType);
	}


	private static void setDataValue(WSItemType itemType, int dataValue) {
		if (itemType instanceof WSDataValuable) ((WSDataValuable) itemType).setDataValue(dataValue);
	}


	/**
	 * @return a new instance of the default {@link WSItemType}.
	 */
	public WSItemType getDefaultType() {
		return defaultType.clone();
	}


	/**
	 * @return the {@link Integer} ID of the {@link WSItemType}.
	 */
	public int getId() {
		return defaultType.getId();
	}


	/**
	 * @return the {@link String} ID of the {@link WSItemType}.
	 */
	public String getStringId() {
		return defaultType.getStringId();
	}


	/**
	 * @param material the material.
	 *
	 * @return true if the material type is equals to this item type.
	 */
	public boolean equalsType(WSMaterial material) {
		return material.getId() == getId();
	}
}
