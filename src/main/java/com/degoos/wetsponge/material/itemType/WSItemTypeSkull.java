package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.resource.sponge.SpongeSkullBuilder;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;
import java.net.URL;
import java.util.Optional;

public class WSItemTypeSkull extends WSItemType implements WSDataValuable {

	private WSGameProfile profile;
	private EnumSkullType skullType;

	public WSItemTypeSkull(WSGameProfile profile, EnumSkullType skullType) {
		super(397, "minecraft:skull", 64);
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.profile = profile;
		this.skullType = skullType;
	}

	public WSItemTypeSkull(EnumSkullType skullType) {
		this(null, skullType);
	}

	public Optional<WSGameProfile> getProfile() {
		return Optional.ofNullable(profile);
	}

	public void setProfile(WSGameProfile profile) {
		this.profile = profile;
	}

	public void setTexture(String texture) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				SpigotSkullBuilder.setTexture(profile == null ? SpigotSkullBuilder.getEmptyGameProfile() : ((SpigotGameProfile) profile).getHandled(), texture);
				break;
			case SPONGE:
				SpongeSkullBuilder.setTexture(profile == null ? SpongeSkullBuilder.getEmptyGameProfile() : ((SpongeGameProfile) profile).getHandled(), texture);
			default:
				break;
		}
	}

	public void setTexture(URL texture) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				SpigotSkullBuilder.setTexture(
					profile == null ? SpigotSkullBuilder.getEmptyGameProfile() : ((SpigotGameProfile) profile).getHandled(), SpigotSkullBuilder.getTexture(texture));
				break;
			case SPONGE:
				SpongeSkullBuilder.setTexture(
					profile == null ? SpongeSkullBuilder.getEmptyGameProfile() : ((SpongeGameProfile) profile).getHandled(), SpigotSkullBuilder.getTexture(texture));
			default:
				break;
		}
	}

	public void setTextureByPlayerName(String name) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				profile = new SpigotGameProfile(SpigotSkullBuilder.getGameProfileByName(name));
				break;
			case SPONGE:
				profile = new SpongeGameProfile(SpongeSkullBuilder.getGameProfileByName(name));
			default:
				break;
		}
	}

	public void findFormatAndSetTexture(String texture) {
		if (texture.toLowerCase().startsWith("http:") || texture.toLowerCase().startsWith("https:")) setTexture(getTexture(texture));
		else if (texture.length() <= 16) setTextureByPlayerName(texture);
		else setTexture(texture);
	}

	public EnumSkullType getSkullType() {
		return skullType;
	}

	public void setSkullType(EnumSkullType skullType) {
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.skullType = skullType;
	}

	@Override
	public WSItemTypeSkull clone() {
		return new WSItemTypeSkull(profile, skullType);
	}

	@Override
	public short getDataValue() {
		return (short) skullType.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		skullType = EnumSkullType.getByValue(dataValue).orElseThrow(NullPointerException::new);
	}

	private String getTexture(String url) {
		if (url == null) return null;
		return String.format("{textures:{SKIN:{url:\"%s\"}}}", url);
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;
		if (object == null || getClass() != object.getClass()) return false;
		if (!super.equals(object)) return false;

		WSItemTypeSkull that = (WSItemTypeSkull) object;

		if (profile != null ? !profile.equals(that.profile) : that.profile != null) return false;
		return skullType == that.skullType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (profile != null ? profile.hashCode() : 0);
		result = 31 * result + skullType.hashCode();
		return result;
	}
}
