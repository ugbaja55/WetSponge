package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.firework.WSFireworkEffect;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WSItemTypeFirework extends WSItemType {

    private int power;
    private List<WSFireworkEffect> effects;

    public WSItemTypeFirework() {
        super(401, "minecraft:fireworks", 64);
        this.power = 1;
        this.effects = new ArrayList<>();
    }

    public WSItemTypeFirework(int power, List<WSFireworkEffect> effects) {
        this();
        this.power = power;
        this.effects = effects == null ? new ArrayList<>() : effects;
    }

    /**
     * Returns a mutable {@link List list} with all {@link WSFireworkEffect firework effect}s of the {@link WSItemTypeFirework firework}.
     *
     * @return the mutable {@link List list}.
     */
    public List<WSFireworkEffect> getEffects() {
        return effects;
    }

    /**
     * Sets the {@link List list} with {@link WSFireworkEffect firework effect}s of the {@link WSItemTypeFirework firework}.
     * Any change after use this method in the given {@link List list} will affect to the {@link WSItemTypeFirework firework}.
     *
     * @param effects the effects.
     */
    public void setEffects(List<WSFireworkEffect> effects) {
        this.effects = effects == null ? new ArrayList<>() : effects;
    }

    /**
     * Returns the power of the {@link WSItemTypeFirework firework}.
     * WARNING! Sponge doesn't support firework power!
     *
     * @return the power.
     */
    public int getPower() {
        return power;
    }

    /**
     * Sets the power of the {@link WSItemTypeFirework firework}.
     * WARNING! Sponge doesn't support firework power!
     *
     * @param power the power.
     */
    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public WSItemTypeFirework clone() {
        return new WSItemTypeFirework(power, effects.stream().map(WSFireworkEffect::clone).collect(Collectors.toList()));
    }
}
