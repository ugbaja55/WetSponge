package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.firework.WSFireworkEffect;

import java.util.Optional;

public class WSItemTypeFireworkCharge extends WSItemType {

    private WSFireworkEffect effect;

    public WSItemTypeFireworkCharge() {
        super(402, "minecraft:firework_charge", 64);
        this.effect = null;
    }

    public WSItemTypeFireworkCharge(WSFireworkEffect effect) {
        this();
        this.effect = effect;
    }

    /**
     * Returns the {@link WSFireworkEffect firework effect} of the {@link WSItemTypeFireworkCharge firework charge}, if present.
     *
     * @return the {@link WSFireworkEffect firework effect}, if present.
     */
    public Optional<WSFireworkEffect> getEffect() {
        return Optional.ofNullable(effect);
    }

    /**
     * Sets the {@link WSFireworkEffect firework effect} of the {@link WSItemTypeFireworkCharge firework charge}.
     * The {@link WSFireworkEffect firework effect} can be null.
     *
     * @param effect the {@link WSFireworkEffect firework effect}, or null.
     */
    public void setEffect(WSFireworkEffect effect) {
        this.effect = effect;
    }

    @Override
    public WSItemTypeFireworkCharge clone() {
        return new WSItemTypeFireworkCharge(effect == null ? null : effect.clone());
    }
}
