package com.degoos.wetsponge.material.itemType;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.util.Validate;

public class WSItemTypeSpawnEgg extends WSItemType implements WSDataValuable {

	private EnumEntityType entityType;


	public WSItemTypeSpawnEgg(EnumEntityType entityType) {
		super(383, "minecraft:spawn_egg", 64);
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}

	/**
	 * @return the {@link EnumEntityType} of the {@link WSItemTypeSpawnEgg}.
	 */
	public EnumEntityType getEntityType() {
		return entityType;
	}

	/**
	 * This method sets the {@link EnumEntityType} of the {@link WSItemTypeSpawnEgg}.
	 *
	 * @param entityType the {@link EnumEntityType}.
	 */
	public void setEntityType(EnumEntityType entityType) {
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}

	@Override
	public WSItemTypeSpawnEgg clone() {
		return new WSItemTypeSpawnEgg(entityType);
	}


	@Override
	public short getDataValue() {
		return WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 0 : entityType.getTypeId();
	}

	@Override
	public void setDataValue(int dataValue) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) && dataValue == 0) return;
		entityType = EnumEntityType.getByTypeId(dataValue).orElse(EnumEntityType.PIG);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSItemTypeSpawnEgg that = (WSItemTypeSpawnEgg) o;

		return entityType == that.entityType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + entityType.hashCode();
		return result;
	}
}
