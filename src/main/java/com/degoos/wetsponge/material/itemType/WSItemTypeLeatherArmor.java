package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.color.WSColor;

import javax.annotation.Nullable;
import java.util.Optional;

public class WSItemTypeLeatherArmor extends WSItemTypeDurability {

    private WSColor color;

    public WSItemTypeLeatherArmor(int id, String stringId, int maxDurability, WSColor color) {
        super(id, stringId, maxDurability);
        this.color = color;
    }

    /**
     * Returns the {@link WSColor color} of the {@link WSItemTypeLeatherArmor leather armor}, if present.
     * @return the {@link WSColor color}, if present.
     */
    public Optional<WSColor> getColor() {
        return Optional.ofNullable(color);
    }

    /**
     * Sets the {@link WSColor color} of the {@link WSItemTypeLeatherArmor leather armor}.
     * If null, the {@link WSColor color} will be cleared.
     * @param color the {@link WSColor color}, or null.
     */
    public void setColor(@Nullable WSColor color) {
        this.color = color;
    }

    @Override
    public WSItemTypeLeatherArmor clone() {
        return new WSItemTypeLeatherArmor(getId(), getStringId(), getMaxDurability(), color);
    }

    @Override
    public short getDataValue() {
        return super.getDataValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        super.setDataValue(dataValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSItemTypeLeatherArmor that = (WSItemTypeLeatherArmor) o;

        return color.equals(that.color);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }
}
