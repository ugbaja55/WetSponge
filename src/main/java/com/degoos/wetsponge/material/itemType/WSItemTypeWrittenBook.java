package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.enums.item.EnumBookGeneration;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public class WSItemTypeWrittenBook extends WSItemType {

    private WSText title;
    private WSText author;
    private List<WSText> pages;
    private EnumBookGeneration generation;


    public WSItemTypeWrittenBook(int id, String stringId, WSText title, WSText author, List<WSText> pages, EnumBookGeneration generation) {
        super(id, stringId, 1);

        this.title = title;
        this.author = author;
        this.pages = pages;
        this.generation = generation;
    }

    public WSText getAuthor() {
        return author;
    }

    public WSItemTypeWrittenBook setAuthor(WSText author) {
        this.author = author;
        return this;
    }

    public WSText getTitle() {
        return title;
    }


    public WSItemTypeWrittenBook setTitle(WSText title) {
        this.title = title;
        return this;
    }

    public List<WSText> getPages() {
        return pages;
    }


    public WSItemTypeWrittenBook addPage(WSText pageText) {
        pages.add(pageText);
        return this;
    }

    public WSItemTypeWrittenBook setPages(List<WSText> pages) {
        this.pages = pages;
        return this;
    }

    public EnumBookGeneration getGeneration() {
        return generation;
    }

    public void setGeneration(EnumBookGeneration generation) {
        this.generation = generation;
    }

    @Override
    public WSItemTypeWrittenBook clone() {
        return new WSItemTypeWrittenBook(getId(), getStringId(), title, author, new ArrayList<>(pages), generation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSItemTypeWrittenBook book = (WSItemTypeWrittenBook) o;

        if (!title.equals(book.title)) return false;
        if (!author.equals(book.author)) return false;
        if (!pages.equals(book.pages)) return false;
        return generation == book.generation;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + pages.hashCode();
        result = 31 * result + generation.hashCode();
        return result;
    }
}
