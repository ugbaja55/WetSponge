package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.material.WSDataValuable;

public class WSItemTypeMap extends WSItemType implements WSDataValuable {

	private int mapId;

	public WSItemTypeMap(int mapId) {
		super(358, "minecraft:filled_map", 1);
		this.mapId = mapId;
	}

	/**
	 * Returns the map id linked to the {@link WSItemTypeMap map}.
	 *
	 * @return the map id.
	 */
	public int getMapId() {
		return mapId;
	}

	/**
	 * Sets the map id linked to the {@link WSItemTypeMap map}.
	 *
	 * @param mapId the map id.
	 * @return the {@link WSItemTypeMap map}.
	 */
	public WSItemTypeMap setMapId(int mapId) {
		this.mapId = mapId;
		return this;
	}

	@Override
	public WSItemTypeMap clone() {
		return new WSItemTypeMap(mapId);
	}


	@Override
	public short getDataValue() {
		return (short) mapId;
	}

	@Override
	public void setDataValue(int dataValue) {
		this.mapId = dataValue;
	}
}
