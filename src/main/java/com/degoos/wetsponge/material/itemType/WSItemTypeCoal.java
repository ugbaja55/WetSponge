package com.degoos.wetsponge.material.itemType;


import com.degoos.wetsponge.enums.item.EnumCoalType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSItemTypeCoal extends WSItemType implements WSDataValuable {

    private EnumCoalType coalType;


    public WSItemTypeCoal(EnumCoalType coalType) {
        super(263, "minecraft:coal", 64);
        this.coalType = coalType;
    }

    /**
     * @return the {@link EnumCoalType} of the {@link WSItemTypeCoal}.
     */
    public EnumCoalType getCoalType() {
        return coalType;
    }

    /**
     * This method sets the {@link EnumCoalType} of the {@link WSItemTypeCoal}.
     *
     * @param coalType the {@link EnumCoalType}.
     */
    public void setCoalType(EnumCoalType coalType) {
        this.coalType = coalType;
    }

    @Override
    public WSItemTypeCoal clone() {
        return new WSItemTypeCoal(coalType);
    }


    @Override
    public short getDataValue() {
        return (short) coalType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        coalType = EnumCoalType.getByValue(dataValue).orElse(EnumCoalType.COAL);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSItemTypeCoal that = (WSItemTypeCoal) o;

        return coalType == that.coalType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + coalType.hashCode();
        return result;
    }
}
