package com.degoos.wetsponge.material.itemType;

import com.degoos.wetsponge.enums.item.EnumGoldenAppleType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSItemTypeGoldenApple extends WSItemType implements WSDataValuable {

    private EnumGoldenAppleType goldenAppleType;

    public WSItemTypeGoldenApple(EnumGoldenAppleType goldenAppleType) {
        super(322, "minecraft:golden_apple", 64);
        this.goldenAppleType = goldenAppleType;
    }

    /**
     * @return the {@link EnumGoldenAppleType} of the {@link WSItemTypeGoldenApple}.
     */
    public EnumGoldenAppleType getGoldenAppleType() {
        return goldenAppleType;
    }

    /**
     * This method sets the {@link EnumGoldenAppleType} of the {@link WSItemTypeGoldenApple}.
     *
     * @param goldenAppleType the {@link EnumGoldenAppleType}.
     */
    public void setGoldenAppleType(EnumGoldenAppleType goldenAppleType) {
        this.goldenAppleType = goldenAppleType;
    }

    @Override
    public WSItemTypeGoldenApple clone() {
        return new WSItemTypeGoldenApple(goldenAppleType);
    }

    @Override
    public short getDataValue() {
        return (short) goldenAppleType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        goldenAppleType = EnumGoldenAppleType.getByValue(dataValue).orElse(EnumGoldenAppleType.GOLDEN_APPLE);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSItemTypeGoldenApple that = (WSItemTypeGoldenApple) o;

        return goldenAppleType == that.goldenAppleType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + goldenAppleType.hashCode();
        return result;
    }
}
