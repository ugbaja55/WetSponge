package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumFlowerType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeFlower extends WSBlockType implements WSDataValuable {

    private EnumFlowerType flowerType;


    public WSBlockTypeFlower(EnumFlowerType flowerType) {
        super(38, "minecraft:red_flower", 64);
        this.flowerType = flowerType;
    }


    /**
     * @return the {@link EnumFlowerType} of this {@link WSBlockTypeFlower}.
     */
    public EnumFlowerType getFlowerType() {
        return flowerType;
    }


    /**
     * Sets the {@link EnumFlowerType} of this {@link WSBlockTypeFlower}.
     *
     * @param flowerType the {@link EnumFlowerType}
     * @return this {@link WSBlockTypeFlower}.
     */
    public WSBlockTypeFlower setFlowerType(EnumFlowerType flowerType) {
        this.flowerType = flowerType;
        return this;
    }


    @Override
    public WSBlockTypeFlower clone() {
        return new WSBlockTypeFlower(flowerType);
    }


    @Override
    public short getDataValue() {
        return (short) flowerType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setFlowerType(EnumFlowerType.getByValue(dataValue).orElse(EnumFlowerType.POPPY));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeFlower that = (WSBlockTypeFlower) o;

        return flowerType == that.flowerType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + flowerType.hashCode();
        return result;
    }
}
