package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeGrowable extends WSBlockType implements WSDataValuable {

    private int growthStage;


    public WSBlockTypeGrowable(int id, String stringId, int maxStackQuantity, int growthStage) {
        super(id, stringId, maxStackQuantity);
        this.growthStage = growthStage;
    }


    /**
     * @return the growthStage of this {@link WSBlockTypeGrowable}.
     */
    public int getGrowthStage() {
        return growthStage;
    }


    /**
     * Sets the growthStage of this {@link WSBlockTypeGrowable}.
     *
     * @param growthStage the growthStage.
     */
    public void setGrowthStage(int growthStage) {
        this.growthStage = growthStage;
    }


    @Override
    public WSBlockTypeGrowable clone() {
        return new WSBlockTypeGrowable(getId(), getStringId(), getMaxStackQuantity(), growthStage);
    }


    @Override
    public short getDataValue() {
        return (short) growthStage;
    }

    @Override
    public void setDataValue(int dataValue) {
        setGrowthStage(dataValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeGrowable that = (WSBlockTypeGrowable) o;

        return growthStage == that.growthStage;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + growthStage;
        return result;
    }
}
