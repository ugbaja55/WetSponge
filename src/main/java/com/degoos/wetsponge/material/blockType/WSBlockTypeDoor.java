package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumDoorDirection;
import com.degoos.wetsponge.material.WSDataValuable;
import java.util.Objects;

public class WSBlockTypeDoor extends WSBlockType implements WSDataValuable {

	private EnumDoorDirection direction;
	private boolean top, leftHinge, open, powered;


	public WSBlockTypeDoor(int id, String stringId, int maxStackQuantity, EnumDoorDirection direction, boolean top, boolean leftHinge, boolean open, boolean powered) {
		super(id, stringId, maxStackQuantity);
		this.direction = direction;
		this.top = top;
		this.leftHinge = leftHinge;
		this.open = open;
		this.powered = powered;
	}

	/**
	 * Returns the {@link EnumDoorDirection direction} of the {@link WSBlockTypeDoor door}.
	 *
	 * @return the {@link EnumDoorDirection direction}.
	 */
	public EnumDoorDirection getDirection() {
		return direction;
	}

	/**
	 * Sets the {@link EnumDoorDirection direction} of the {@link WSBlockTypeDoor door}.
	 *
	 * @param direction the {@link EnumDoorDirection direction}.
	 *
	 * @return the {@link WSBlockTypeDoor door}.
	 */
	public WSBlockTypeDoor setDirection(EnumDoorDirection direction) {
		this.direction = direction;
		return this;
	}

	/**
	 * Returns whether the {@link WSBlockTypeDoor door} is a top part.
	 *
	 * @return whether the {@link WSBlockTypeDoor door} is a top part.
	 */
	public boolean isTop() {
		return top;
	}

	/**
	 * Sets whether the {@link WSBlockTypeDoor door} is a top part.
	 *
	 * @param top whether the {@link WSBlockTypeDoor door} is a top part.
	 *
	 * @return the {@link WSBlockTypeDoor door}.
	 */
	public WSBlockTypeDoor setTop(boolean top) {
		this.top = top;
		return this;
	}

	/**
	 * Returns whether the {@link WSBlockTypeDoor door} has the hinge in the left.
	 *
	 * @return whether the {@link WSBlockTypeDoor door} has the hinge in the left.
	 */
	public boolean isLeftHinge() {
		return leftHinge;
	}

	/**
	 * Sets whether the {@link WSBlockTypeDoor door} has the hinge in the left.
	 *
	 * @param leftHinge whether the {@link WSBlockTypeDoor door} has the hinge in the left.
	 *
	 * @return the {@link WSBlockTypeDoor door}.
	 */
	public WSBlockTypeDoor setLeftHinge(boolean leftHinge) {
		this.leftHinge = leftHinge;
		return this;
	}

	/**
	 * Returns whether the {@link WSBlockTypeDoor door} is open.
	 *
	 * @return whether the {@link WSBlockTypeDoor door} is open.
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * Sets whether the {@link WSBlockTypeDoor door} is open.
	 *
	 * @param open whether the {@link WSBlockTypeDoor door} is open.
	 *
	 * @return the {@link WSBlockTypeDoor door}.
	 */
	public WSBlockTypeDoor setOpen(boolean open) {
		this.open = open;
		return this;
	}

	/**
	 * Returns whether the {@link WSBlockTypeDoor door} is powered.
	 *
	 * @return whether the {@link WSBlockTypeDoor door} is powered.
	 */
	public boolean isPowered() {
		return powered;
	}

	/**
	 * Sets whether the {@link WSBlockTypeDoor door} is powered.
	 *
	 * @param powered whether the {@link WSBlockTypeDoor door} is powered.
	 *
	 * @return the {@link WSBlockTypeDoor door}.
	 */
	public WSBlockTypeDoor setPowered(boolean powered) {
		this.powered = powered;
		return this;
	}

	@Override
	public WSBlockTypeDoor clone() {
		return new WSBlockTypeDoor(getId(), getStringId(), getMaxStackQuantity(), direction, top, leftHinge, open, powered);
	}


	@Override
	public short getDataValue() {
		if (top) return (short) (8 + (leftHinge ? 1 : 0) + (powered ? 2 : 0));
		return (short) ((open ? 4 : 0) + direction.getValue());
	}

	@Override
	public void setDataValue(int dataValue) {
		int realData = dataValue % 8;
		top = dataValue > 7;
		if (top) {
			leftHinge = (realData & 1) == 1;
			powered = (realData & 2) == 2;
		} else {
			open = (realData & 4) == 4;
			direction = EnumDoorDirection.getByValue(realData & 3).orElse(EnumDoorDirection.EAST);
		}
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		WSBlockTypeDoor that = (WSBlockTypeDoor) o;
		return top == that.top && leftHinge == that.leftHinge && open == that.open && powered == that.powered && direction == that.direction;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), direction, top, leftHinge, open, powered);
	}
}
