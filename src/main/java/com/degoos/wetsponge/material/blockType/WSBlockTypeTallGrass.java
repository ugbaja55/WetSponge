package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumTallGrassType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeTallGrass extends WSBlockType implements WSDataValuable {

    private EnumTallGrassType tallGrassType;


    public WSBlockTypeTallGrass(EnumTallGrassType tallGrassType) {
        super(31, "minecraft:tallgrass", 64);
        this.tallGrassType = tallGrassType;
    }


    /**
     * @return the {@link EnumTallGrassType} of this {@link WSBlockTypeTallGrass}.
     */
    public EnumTallGrassType getTallGrassType() {
        return tallGrassType;
    }


    /**
     * Sets the {@link EnumTallGrassType} of this {@link WSBlockTypeTallGrass}.
     *
     * @param tallGrassType the {@link EnumTallGrassType}
     * @return this {@link WSBlockTypeTallGrass}.
     */
    public WSBlockTypeTallGrass setTallGrassType(EnumTallGrassType tallGrassType) {
        this.tallGrassType = tallGrassType;
        return this;
    }


    @Override
    public WSBlockTypeTallGrass clone() {
        return new WSBlockTypeTallGrass(tallGrassType);
    }


    @Override
    public short getDataValue() {
        return (short) tallGrassType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setTallGrassType(EnumTallGrassType.getByValue(dataValue).orElse(EnumTallGrassType.DEAD_BUSH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeTallGrass that = (WSBlockTypeTallGrass) o;

        return tallGrassType == that.tallGrassType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + tallGrassType.hashCode();
        return result;
    }
}
