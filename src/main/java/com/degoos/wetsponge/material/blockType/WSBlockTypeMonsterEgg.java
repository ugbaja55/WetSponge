package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumDisguisedBlockType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeMonsterEgg extends WSBlockType implements WSDataValuable {

    private EnumDisguisedBlockType disguiseType;


    public WSBlockTypeMonsterEgg(EnumDisguisedBlockType disguiseType) {
        super(97, "minecraft:monster_egg", 64);
        this.disguiseType = disguiseType;
    }


    /**
     * @return the {@link EnumDisguisedBlockType} of this {@link WSBlockTypeMonsterEgg}.
     */
    public EnumDisguisedBlockType getDisguiseType() {
        return disguiseType;
    }


    /**
     * Sets the {@link EnumDisguisedBlockType} of this {@link WSBlockTypeMonsterEgg}.
     *
     * @param disguiseType the {@link EnumDisguisedBlockType}
     * @return this {@link WSBlockTypeMonsterEgg}.
     */
    public WSBlockTypeMonsterEgg setDisguiseType(EnumDisguisedBlockType disguiseType) {
        this.disguiseType = disguiseType;
        return this;
    }


    @Override
    public WSBlockTypeMonsterEgg clone() {
        return new WSBlockTypeMonsterEgg(disguiseType);
    }


    @Override
    public short getDataValue() {
        return (short) disguiseType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setDisguiseType(EnumDisguisedBlockType.getByValue(dataValue).orElse(EnumDisguisedBlockType.STONE));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeMonsterEgg that = (WSBlockTypeMonsterEgg) o;

        return disguiseType == that.disguiseType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + disguiseType.hashCode();
        return result;
    }
}
