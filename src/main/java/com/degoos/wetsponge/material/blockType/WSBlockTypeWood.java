package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeWood extends WSBlockType implements WSDataValuable {

    private EnumWoodType woodType;


    public WSBlockTypeWood(int id, String stringId, int maxStackQuantity, EnumWoodType woodType) {
        super(id, stringId, maxStackQuantity);
        this.woodType = woodType;
    }


    /**
     * @return the {@link EnumWoodType} of this {@link WSBlockTypeWood}.
     */
    public EnumWoodType getWoodType() {
        return woodType;
    }


    /**
     * Sets the {@link EnumWoodType} of this {@link WSBlockTypeWood}.
     *
     * @param woodType the {@link EnumWoodType}
     * @return this {@link WSBlockTypeWood}.
     */
    public WSBlockTypeWood setWoodType(EnumWoodType woodType) {
        this.woodType = woodType;
        return this;
    }


    @Override
    public WSBlockTypeWood clone() {
        return new WSBlockTypeWood(getId(), getStringId(), getMaxStackQuantity(), woodType);
    }


    @Override
    public short getDataValue() {
        return (short) woodType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setWoodType(EnumWoodType.getByValue(dataValue).orElse(EnumWoodType.OAK));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeWood that = (WSBlockTypeWood) o;

        return woodType == that.woodType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + woodType.hashCode();
        return result;
    }
}
