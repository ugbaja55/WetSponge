package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.enums.block.EnumBlockOrientation;

public class WSBlockTypeOrientation extends WSBlockType implements WSDataValuable {

    private EnumBlockOrientation orientation;


    public WSBlockTypeOrientation(int id, String stringId, int maxStackQuantity, EnumBlockOrientation orientation) {
        super(id, stringId, maxStackQuantity);
        this.orientation = orientation;
    }


    /**
     * @return the {@link EnumBlockOrientation} of this {@link WSBlockTypeOrientation}.
     */
    public EnumBlockOrientation getOrientation() {
        return orientation;
    }


    /**
     * Sets the {@link EnumBlockOrientation} of this {@link WSBlockTypeOrientation}.
     *
     * @param direction the {@link EnumBlockOrientation}
     * @return this {@link WSBlockTypeOrientation}.
     */
    public WSBlockTypeOrientation setOrientation(EnumBlockOrientation direction) {
        this.orientation = direction;
        return this;
    }


    @Override
    public WSBlockTypeOrientation clone() {
        return new WSBlockTypeOrientation(getId(), getStringId(), getMaxStackQuantity(), orientation);
    }


    @Override
    public short getDataValue() {
        return (short) orientation.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setOrientation(EnumBlockOrientation.getByValue(dataValue).orElse(EnumBlockOrientation.SOUTH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeOrientation that = (WSBlockTypeOrientation) o;

        return orientation == that.orientation;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + orientation.hashCode();
        return result;
    }
}
