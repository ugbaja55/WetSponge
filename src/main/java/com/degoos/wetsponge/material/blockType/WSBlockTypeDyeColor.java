package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeDyeColor extends WSBlockType implements WSDataValuable {

	private EnumDyeColor dyeColor;


	public WSBlockTypeDyeColor (int id, String stringId, int maxStackQuantity, EnumDyeColor dyeColor) {
		super(id, stringId, maxStackQuantity);
		this.dyeColor = dyeColor;
	}


	/**
	 * @return the {@link EnumDyeColor} of this {@link WSBlockTypeDyeColor}.
	 */
	public EnumDyeColor getDyeColor () {
		return dyeColor;
	}


	/**
	 * Sets the {@link EnumDyeColor} of this {@link WSBlockTypeDyeColor}.
	 *
	 * @param dyeColor
	 * 		the {@link EnumDyeColor}
	 *
	 * @return this {@link WSBlockTypeDyeColor}.
	 */
	public WSBlockTypeDyeColor setDyeColor (EnumDyeColor dyeColor) {
		this.dyeColor = dyeColor;
		return this;
	}


	@Override
	public WSBlockTypeDyeColor clone () {
		return new WSBlockTypeDyeColor(getId(), getStringId(), getMaxStackQuantity(), dyeColor);
	}


	@Override
	public short getDataValue () {
		return (short) dyeColor.getWoolData();
	}

	@Override
	public void setDataValue(int dataValue) {
		setDyeColor(EnumDyeColor.getByWoolData((byte) dataValue).orElse(EnumDyeColor.WHITE));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeDyeColor that = (WSBlockTypeDyeColor) o;

		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + dyeColor.hashCode();
		return result;
	}
}
