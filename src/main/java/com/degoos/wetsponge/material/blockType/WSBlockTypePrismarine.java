package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumPrismarineType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypePrismarine extends WSBlockType implements WSDataValuable {

    private EnumPrismarineType prismarineType;


    public WSBlockTypePrismarine(EnumPrismarineType prismarineType) {
        super(168, "minecraft:prismarine", 64);
        this.prismarineType = prismarineType;
    }


    /**
     * @return the {@link EnumPrismarineType} of this {@link WSBlockTypePrismarine}.
     */
    public EnumPrismarineType getPrismarineType() {
        return prismarineType;
    }


    /**
     * Sets the {@link EnumPrismarineType} of this {@link WSBlockTypePrismarine}.
     *
     * @param prismarineType the {@link EnumPrismarineType}
     * @return this {@link WSBlockTypePrismarine}.
     */
    public WSBlockTypePrismarine setPrismarineType(EnumPrismarineType prismarineType) {
        this.prismarineType = prismarineType;
        return this;
    }


    @Override
    public WSBlockTypePrismarine clone() {
        return new WSBlockTypePrismarine(prismarineType);
    }


    @Override
    public short getDataValue() {
        return (short) prismarineType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setPrismarineType(EnumPrismarineType.getByValue(dataValue).orElse(EnumPrismarineType.ROUGH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypePrismarine that = (WSBlockTypePrismarine) o;

        return prismarineType == that.prismarineType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + prismarineType.hashCode();
        return result;
    }
}
