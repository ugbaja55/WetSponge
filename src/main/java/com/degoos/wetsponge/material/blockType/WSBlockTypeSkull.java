package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.enums.block.EnumSkullFacingDirection;

public class WSBlockTypeSkull extends WSBlockType implements WSDataValuable {

    private EnumSkullFacingDirection facingDirection;


    public WSBlockTypeSkull(EnumSkullFacingDirection facingDirection) {
        super(144, "minecraft:skull", 64);
        this.facingDirection = facingDirection;
    }

    /**
     * @return the {@link EnumSkullFacingDirection} of the {@link WSBlockTypeSkull}.
     */
    public EnumSkullFacingDirection getFacingDirection() {
        return facingDirection;
    }

    /**
     * Sets the {@link EnumSkullFacingDirection} of the {@link WSBlockTypeSkull}.
     *
     * @param facingDirection the {@link EnumSkullFacingDirection}.
     * @return the {@link WSBlockTypeSkull}.
     */
    public WSBlockTypeSkull setFacingDirection(EnumSkullFacingDirection facingDirection) {
        this.facingDirection = facingDirection;
        return this;
    }

    @Override
    public WSBlockTypeSkull clone() {
        return new WSBlockTypeSkull(facingDirection);
    }


    @Override
    public short getDataValue() {
        return (short) facingDirection.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setFacingDirection(EnumSkullFacingDirection.getByValue(dataValue).orElse(EnumSkullFacingDirection.UP));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeSkull that = (WSBlockTypeSkull) o;

        return facingDirection == that.facingDirection;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + facingDirection.hashCode();
        return result;
    }
}
