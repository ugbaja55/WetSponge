package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.text.translation.SpigotTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.SpongeTranslationUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;

public class WSBlockType implements WSMaterial {

	private int id;
	private String stringId;
	private int maxStackQuantity;


	public WSBlockType(int id, String stringId, int maxStackQuantity) {
		this.id = id;
		this.stringId = stringId.toLowerCase();
		this.maxStackQuantity = maxStackQuantity;
	}

	@Override
	public int getId() {
		return id;
	}


	@Override
	public String getStringId() {
		return stringId;
	}


	@Override
	public WSBlockType clone() {
		return new WSBlockType(id, stringId, maxStackQuantity);
	}


	@Override
	public int getMaxStackQuantity() {
		return maxStackQuantity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WSBlockType type = (WSBlockType) o;

		if (id != type.id) return false;
		if (maxStackQuantity != type.maxStackQuantity) return false;
		return stringId.equals(type.stringId);
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + stringId.hashCode();
		result = 31 * result + maxStackQuantity;
		return result;
	}

	@Override
	public WSTranslation getTranslation() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					Class<?> itemClazz = NMSUtils.getNMSClass("Item");
					return new SpigotTranslation(itemClazz.getMethod("getName").invoke(itemClazz.getMethod("getById", int.class).invoke(null, id)) + ".name");
				} catch (Exception e) {
					return new SpigotTranslation("");
				}
			case SPONGE:
				return SpongeTranslationUtils.getMaterialTranslation(getStringId());
			default:
				return null;
		}
	}
}
