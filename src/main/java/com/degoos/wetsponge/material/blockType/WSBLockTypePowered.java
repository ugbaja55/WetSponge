package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSBLockTypePowered extends WSBlockType implements WSDataValuable {

    private boolean powered;


    public WSBLockTypePowered(int id, String stringId, int maxStackQuantity, boolean powered) {
        super(id, stringId, maxStackQuantity);
        this.powered = powered;
    }

    /**
     * @return true if this {@link WSBLockTypePowered} is powered.
     */
    public boolean isPowered() {
        return powered;
    }

    /**
     * Sets the this {@link WSBLockTypePowered} is powered.
     *
     * @param powered the boolean.
     */
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public WSBLockTypePowered clone() {
        return new WSBLockTypePowered(getId(), getStringId(), getMaxStackQuantity(), powered);
    }


    @Override
    public short getDataValue() {
        return (short) (powered ? 1 : 0);
    }

    @Override
    public void setDataValue(int dataValue) {
        setPowered(dataValue != 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBLockTypePowered that = (WSBLockTypePowered) o;

        return powered == that.powered;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (powered ? 1 : 0);
        return result;
    }
}
