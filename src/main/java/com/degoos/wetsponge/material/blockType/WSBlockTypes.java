package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumAnvilDamage;
import com.degoos.wetsponge.enums.block.EnumBlockAxis;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.enums.block.EnumBlockFacingDirection;
import com.degoos.wetsponge.enums.block.EnumBlockOrientation;
import com.degoos.wetsponge.enums.block.EnumDirtType;
import com.degoos.wetsponge.enums.block.EnumDisguisedBlockType;
import com.degoos.wetsponge.enums.block.EnumDoorDirection;
import com.degoos.wetsponge.enums.block.EnumFlowerType;
import com.degoos.wetsponge.enums.block.EnumLeverDirection;
import com.degoos.wetsponge.enums.block.EnumPrismarineType;
import com.degoos.wetsponge.enums.block.EnumPumpkinDirection;
import com.degoos.wetsponge.enums.block.EnumRepeaterDirection;
import com.degoos.wetsponge.enums.block.EnumSandType;
import com.degoos.wetsponge.enums.block.EnumSandstoneType;
import com.degoos.wetsponge.enums.block.EnumSkullFacingDirection;
import com.degoos.wetsponge.enums.block.EnumStairDirection;
import com.degoos.wetsponge.enums.block.EnumStoneBrickType;
import com.degoos.wetsponge.enums.block.EnumStoneType;
import com.degoos.wetsponge.enums.block.EnumTallGrassType;
import com.degoos.wetsponge.enums.block.EnumTorchDirection;
import com.degoos.wetsponge.enums.block.EnumVineDirection;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.WSMaterial;
import java.util.Arrays;
import java.util.Optional;

public enum WSBlockTypes {

	AIR(new WSBlockType(0, "minecraft:air", 64)),
	STONE(new WSBlockTypeStone(EnumStoneType.STONE)),
	GRASS(new WSBlockType(2, "minecraft:grass", 64)),
	DIRT(new WSBlockTypeDirt(EnumDirtType.DIRT)),
	COBBLESTONE(new WSBlockType(4, "minecraft:cobblestone", 64)),
	WOOD_PLANKS(new WSBlockTypeWood(5, "minecraft:planks", 64, EnumWoodType.OAK)),
	SAPLING(new WSBlockTypeWood(6, "minecraft:sapling", 64, EnumWoodType.OAK)),
	BEDROCK(new WSBlockType(7, "minecraft:bedrock", 64)),
	WATER(new WSBlockTypeFluid(8, "minecraft:flowing_water", 64, 0, false)),
	STATIONARY_WATER(new WSBlockTypeFluid(9, "minecraft:water", 64, 0, false)),
	LAVA(new WSBlockTypeFluid(10, "minecraft:flowing_lava", 64, 0, false)),
	STATIONARY_LAVA(new WSBlockTypeFluid(11, "minecraft:lava", 64, 0, false)),
	SAND(new WSBlockTypeSand(EnumSandType.NORMAL)),
	GRAVEL(new WSBlockType(13, "minecraft:gravel", 64)),
	GOLD_ORE(new WSBlockType(14, "minecraft:gold_ore", 64)),
	IRON_ORE(new WSBlockType(15, "minecraft:iron_ore", 64)),
	COAL_ORE(new WSBlockType(16, "minecraft:coal_ore", 64)),
	LOG(new WSBlockTypeLog(EnumWoodType.OAK, EnumBlockAxis.Y)),
	LEAVES(new WSBlockTypeLeaves(18, "minecraft:leaves", EnumWoodType.OAK, false)),
	SPONGE(new WSBlockTypeSponge(false)),
	GLASS(new WSBlockType(20, "minecraft:glass", 64)),
	LAPIS_LAZULI_ORE(new WSBlockType(21, "minecraft:lapis_ore", 64)),
	LAPIS_LAZULI_BLOCK(new WSBlockType(22, "minecraft:lapis_lazuli_block", 64)),
	DISPENSER(new WSBlockTypeDirectionPowered(23, "minecraft:dispenser", 64, EnumBlockDirection.DOWN, false)),
	SANDSTONE(new WSBlockTypeSandstone(24, "minecraft:sandstone", 64, EnumSandstoneType.DEFAULT)),
	NOTE_BLOCK(new WSBlockType(25, "minecraft:noteblock", 64)),
	BED(new WSBlockType(26, "minecraft:bed", 1)),
	POWERED_RAIL(new WSBlockType(27, "minecraft:golden_rail", 64)),
	DETECTOR_RAIL(new WSBlockType(28, "minecraft:detector_rail", 64)),
	STICKY_PISTON(new WSBlockTypeDirectionPowered(29, "minecraft:sticky_piston", 64, EnumBlockDirection.DOWN, false)),
	COBWEB(new WSBlockType(30, "minecraft:cobweb", 64)),
	TALL_GRASS(new WSBlockTypeTallGrass(EnumTallGrassType.DEAD_BUSH)),
	DEAD_BUSH(new WSBlockType(32, "minecraft:deadebush", 64)),
	PISTON(new WSBlockTypeDirectionPowered(33, "minecraft:piston", 64, EnumBlockDirection.DOWN, false)),
	PISTON_HEAD(new WSBlockType(34, "minecraft:piston_head", 64)),
	WOOL(new WSBlockTypeDyeColor(35, "minecraft:wool", 64, EnumDyeColor.WHITE)),
	PISTON_EXTENSION(new WSBlockType(36, "minecraft:piston_extension", 64)),
	DANDELION(new WSBlockType(37, "minecraft:yellow_flower", 64)),
	POPPY(new WSBlockTypeFlower(EnumFlowerType.POPPY)),
	BROWN_MUSHROOM(new WSBlockType(39, "minecraft:brown_mushroom", 64)),
	RED_MUSHROOM(new WSBlockType(40, "minecraft:red_mushroom", 64)),
	GOLD_BLOCK(new WSBlockType(41, "minecraft:gold_block", 64)),
	IRON_BLOCK(new WSBlockType(42, "minecraft:iron_block", 64)),
	DOUBLE_STONE_SLAB(new WSBlockType(43, " minecraft:double_stone_slab", 64)),
	STONE_SLAB(new WSBlockType(44, " minecraft:stone_slab", 64)),
	BRICK(new WSBlockType(45, "minecraft:brick_block", 64)),
	TNT(new WSBlockType(46, "minecraft:tnt", 64)),
	BOOKSHELF(new WSBlockType(47, "minecraft:bookshelf", 64)),
	MOSSY_STONE(new WSBlockType(48, "minecraft:mossy_cobblestone", 64)),
	OBSIDIAN(new WSBlockType(49, "minecraft:obsidian", 64)),
	TORCH(new WSBlockTypeTorch(50, "minecraft:torch", 64, EnumTorchDirection.UP)),
	FIRE(new WSBlockType(51, "minecraft:fire", 64)),
	MONSTER_SPAWNER(new WSBlockType(52, "minecraft:mob_spawner", 64)),
	OAK_STAIRS(new WSBlockTypeStair(53, "minecraft:oak_stairs", 64, EnumStairDirection.EAST, false)),
	CHEST(new WSBlockTypeDirection(54, "minecraft:chest", 64, EnumBlockDirection.NORTH)),
	REDSTONE_WIRE(new WSBlockTypeRedstoneWire(0)),
	DIAMOND_ORE(new WSBlockType(56, "minecraft:diamond_ore", 64)),
	DIAMOND_BLOCK(new WSBlockType(57, "minecraft:diamond_block", 64)),
	CRAFTING_TABLE(new WSBlockType(58, "minecraft:crafting_table", 64)),
	WHEAT(new WSBlockTypeGrowable(59, "minecraft:wheat", 64, 0)),
	FARMLAND(new WSBlockTypeFarmLand(0)),
	FURNACE(new WSBlockType(61, "minecraft:furnace", 64)),
	BURNING_FURNACE(new WSBlockType(62, "minecraft:lit_furnace", 64)),
	STANDING_SIGN(new WSBlockTypeOrientation(63, "minecraft:standing_sign", 64, EnumBlockOrientation.SOUTH)),
	OAK_DOOR(new WSBlockTypeDoor(64, "minecraft:wooden_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	LADDER(new WSBlockTypeDirection(65, "minecraft:ladder", 64, EnumBlockDirection.NORTH)),
	RAIL(new WSBlockType(66, "minecraft:rail", 64)),
	STONE_STAIRS(new WSBlockTypeStair(67, "minecraft:stone_stairs", 64, EnumStairDirection.EAST, false)),
	WALL_SIGN(new WSBlockTypeDirection(68, "minecraft:wall_sign", 64, EnumBlockDirection.NORTH)),
	LEVER(new WSBlockTypeLever(EnumLeverDirection.BOTTOM_EAST, false)),
	STONE_PRESSURE_PLATE(new WSBLockTypePowered(70, "minecraft:stone_pressure_plate", 64, false)),
	IRON_DOOR(new WSBlockType(71, "minecraft:iron_door", 64)),
	WOODEN_PRESSURE_PLATE(new WSBLockTypePowered(72, "minecraft:wooden_pressure_plate", 64, false)),
	REDSTONE_ORE(new WSBlockType(73, "minecraft:redstone_ore", 64)),
	GLOWING_REDSTONE_ORE(new WSBlockType(74, "minecraft:lit_redstone_ore", 64)),
	INACTIVE_REDSTONE_TORCH(new WSBlockTypeTorch(75, "minecraft:unlit_redstone_torch", 64, EnumTorchDirection.UP)),
	REDSTONE_TORCH(new WSBlockTypeTorch(76, "minecraft:redstone_torch", 64, EnumTorchDirection.UP)),
	STONE_BUTTON(new WSBlockTypeDirectionPowered(77, "minecraft:stone_button", 64, EnumBlockDirection.DOWN, false)),
	SNOW_LAYER(new WSBlockTypeGrowable(78, "minecraft:snow_layer", 64, 0)),
	ICE(new WSBlockType(79, "minecraft:ice", 64)),
	SNOW(new WSBlockType(80, "minecraft:snow", 64)),
	CACTUS(new WSBlockType(81, "minecraft:cactus", 64)),
	CLAY(new WSBlockType(82, "minecraft:clay", 64)),
	SUGAR_CANE(new WSBlockType(83, "minecraft:reeds", 64)),
	JUKEBOX(new WSBlockType(84, "minecraft:jukebox", 64)),
	OAK_FENCE(new WSBlockType(85, "minecraft:fence", 64)),
	PUMPKIN(new WSBlockTypePumpkin(86, "minecraft:pumpkin", 64, EnumPumpkinDirection.SOUTH)),
	NETHERRACK(new WSBlockType(87, "minecraft:netherrack", 64)),
	SOUL_SAND(new WSBlockType(88, "minecraft:soul_sand", 64)),
	GLOWSTONE(new WSBlockType(89, "minecraft:glowstone", 64)),
	NETHER_PORTAL(new WSBlockType(90, "minecraft:portal", 64)),
	JACK_O_LANTERN(new WSBlockType(91, "minecraft:lit_pumpkin", 64)),
	CAKE(new WSBlockTypeGrowable(92, "minecraft:cake", 64, 0)),
	UNPOWERED_REPEATER(new WSBlockTypeRepeater(93, "minecraft:unpowered_repeater", 64, EnumRepeaterDirection.NORTH, 0)),
	REPEATER(new WSBlockTypeRepeater(94, "minecraft:powered_repeater", 64, EnumRepeaterDirection.NORTH, 0)),
	STAINED_GLASS(new WSBlockTypeDyeColor(95, "minecraft:stained_glass", 64, EnumDyeColor.WHITE)),
	TRAPDOOR(new WSBlockType(96, "minecraft:trapdoor", 64)),
	MONSTER_EGG(new WSBlockTypeMonsterEgg(EnumDisguisedBlockType.STONE)),
	STONE_BRICK(new WSBlockTypeStoneBrick(EnumStoneBrickType.DEFAULT)),
	BROWN_MUSHROOM_BLOCK(new WSBlockType(99, "minecraft:brown_mushroom_block", 64)),
	RED_MUSHROOM_BLOCK(new WSBlockType(100, "minecraft:red_mushroom_block", 64)),
	IRON_BARS(new WSBlockType(101, "minecraft:iron_bars", 64)),
	GLASS_PANE(new WSBlockType(102, "minecraft:glass_pane", 64)),
	MELON(new WSBlockType(103, "minecraft:melon_block", 64)),
	PUMPKIN_STEM(new WSBlockTypeGrowable(104, "minecraft:pumpkin_stem", 64, 0)),
	MELON_STEM(new WSBlockTypeGrowable(105, "minecraft:melon_stem", 64, 0)),
	VINE(new WSBlockTypeVine(EnumVineDirection.SOUTH)),
	FENCE_GATE(new WSBlockTypeFacingPowered(107, "minecraft:fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	BRICK_STAIRS(new WSBlockTypeStair(108, "minecraft:brick_stairs", 64, EnumStairDirection.EAST, false)),
	STONE_BRICK_STAIRS(new WSBlockTypeStair(109, "minecraft:stone_brick_stairs", 64, EnumStairDirection.EAST, false)),
	MYCELIUM(new WSBlockType(110, "minecraft:mycelium", 64)),
	LILY_PAD(new WSBlockType(111, "minecraft:waterlily", 64)),
	NETHER_BRICK(new WSBlockType(112, "minecraft:nether_brick", 64)),
	NETHER_BRICK_FENCE(new WSBlockType(113, "minecraft:nether_brick_fence", 64)),
	NETHER_BRICK_STAIRS(new WSBlockTypeStair(114, "minecraft:nether_brick_stairs", 64, EnumStairDirection.EAST, false)),
	NETHER_WART(new WSBlockTypeGrowable(115, "minecraft:nether_wart", 64, 0)),
	ENCHANTING_TABLE(new WSBlockType(116, "minecraft:enchanting_table", 64)),
	BREWING_STAND(new WSBlockType(117, "minecraft:brewing_stand", 64)),
	CAULDRON(new WSBlockType(118, "minecraft:cauldron", 64)),
	END_PORTAL(new WSBlockType(119, "minecraft:end_portal", 64)),
	END_PORTAL_FRAME(new WSBlockTypeFacingPowered(120, "minecraft:end_portal_frame", 64, EnumBlockFacingDirection.SOUTH, false)),
	END_STONE(new WSBlockType(121, "minecraft:end_stone", 64)),
	DRAGON_EGG(new WSBlockType(122, "minecraft:dragon_egg", 64)),
	REDSTONE_LAMP(new WSBlockType(123, "minecraft:redstone_lamp", 64)),
	LIT_REDSTONE_LAMP(new WSBlockType(124, "minecraft:lit_redstone_lamp", 64)),
	DOUBLE_WOODEN_SLAB(new WSBlockType(125, "minecraft:double_wooden_slab", 64)),
	WOODEN_SLAB(new WSBlockType(126, "minecraft:wooden_slab", 64)),
	COCOA(new WSBlockType(127, "minecraft:cocoa", 64)),
	SANDSTONE_STAIRS(new WSBlockTypeStair(128, "minecraft:sandstone_stairs", 64, EnumStairDirection.EAST, false)),
	EMERALD_ORE(new WSBlockType(129, "minecraft:emerald_ore", 64)),
	ENDER_CHEST(new WSBlockTypeDirection(130, "minecraft:ender_chest", 64, EnumBlockDirection.NORTH)),
	TRIPWIRE_HOOK(new WSBlockType(131, "minecraft:tripwire_hook", 64)),
	TRIPWIRE(new WSBlockType(132, "minecraft:tripwire", 64)),
	EMERALD_BLOCK(new WSBlockType(133, "minecraft:emerald_block", 64)),
	SPRUCE_STAIRS(new WSBlockTypeStair(134, "minecraft:spruce_stairs", 64, EnumStairDirection.EAST, false)),
	BIRCH_STAIRS(new WSBlockTypeStair(135, "minecraft:birch_stairs", 64, EnumStairDirection.EAST, false)),
	JUNGLE_STAIRS(new WSBlockTypeStair(136, "minecraft:jungle_stairs", 64, EnumStairDirection.EAST, false)),
	COMMAND_BLOCK(new WSBlockType(137, "minecraft:command_block", 64)),
	BEACON(new WSBlockType(138, "minecraft:beacon", 64)),
	COBBLESTONE_WALL(new WSBlockTypeCobbleStoneWall(false)),
	FLOWER_POT(new WSBlockType(140, "minecraft:flower_pot", 64)),
	CARROT(new WSBlockTypeGrowable(141, "minecraft:carrots", 64, 0)),
	POTATOES(new WSBlockTypeGrowable(142, "minecraft:potatoes", 64, 0)),
	WOODEN_BUTTON(new WSBlockTypeDirectionPowered(143, "minecraft:wooden_button", 64, EnumBlockDirection.DOWN, false)),
	SKULL(new WSBlockTypeSkull(EnumSkullFacingDirection.UP)),
	ANVIL(new WSBlockTypeAnvil(EnumBlockFacingDirection.SOUTH, EnumAnvilDamage.NORMAL)),
	TRAPPED_CHEST(new WSBlockTypeDirection(146, "minecraft:trapped_chest", 64, EnumBlockDirection.NORTH)),
	LIGHT_WEIGHTED_PRESSURE_PLATE(new WSBLockTypePowered(147, "minecraft:light_weighted_pressure_plate", 64, false)),
	HEAVY_WEIGHTED_PRESSURE_PLATE(new WSBLockTypePowered(148, "minecraft:heavy_weighted_pressure_plate", 64, false)),
	COMPARATOR(new WSBlockType(149, "minecraft:unpowered_comparator", 64)),
	POWERED_COMPARATOR_OLD(new WSBlockType(150, "minecraft:powered_comparator", 64)),
	DAYLIGHT_SENSOR(new WSBlockType(151, "minecraft:daylight_sensor", 64)),
	REDSTONE_BLOCK(new WSBlockType(152, "minecraft:redstone_block", 64)),
	QUARTZ_ORE(new WSBlockType(153, "minecraft:quartz_ore", 64)),
	HOPPER(new WSBlockTypeDirectionPowered(154, "minecraft:hopper", 64, EnumBlockDirection.DOWN, false)),
	QUARTZ_BLOCK(new WSBlockType(155, "minecraft:quartz_block", 64)),
	QUARTZ_STAIRS(new WSBlockTypeStair(156, "minecraft:quartz_stairs", 64, EnumStairDirection.EAST, false)),
	ACTIVATOR_RAIL(new WSBlockType(157, "minecraft:activator_rail", 64)),
	DROPPER(new WSBlockTypeDirectionPowered(158, "minecraft:dropper", 64, EnumBlockDirection.DOWN, false)),
	STAINED_CLAY(new WSBlockTypeDyeColor(159, "minecraft:stained_hardened_clay", 64, EnumDyeColor.WHITE)),
	STAINED_GLASS_PANE(new WSBlockTypeDyeColor(160, "minecraft:stained_glass_pane", 64, EnumDyeColor.WHITE)),
	LEAVES_2(new WSBlockType(161, "minecraft:leaves2", 64)),
	LOG_2(new WSBlockType(162, "minecraft:log2", 64)),
	ACACIA_STAIRS(new WSBlockTypeStair(163, "minecraft:acacia_stairs", 64, EnumStairDirection.EAST, false)),
	DARK_OAK_STAIRS(new WSBlockTypeStair(164, "minecraft:dark_oak_stairs", 64, EnumStairDirection.EAST, false)),
	SLIME_BLOCK(new WSBlockType(165, "minecraft:slime", 64)),
	BARRIER(new WSBlockType(166, "minecraft:barrier", 64)),
	IRON_TRAPDOOR(new WSBlockType(167, "minecraft:iron_trapdoor", 64)),
	PRISMARINE(new WSBlockTypePrismarine(EnumPrismarineType.ROUGH)),
	SEA_LANTERN(new WSBlockType(169, "minecraft:sea_lantern", 64)),
	HAY_BALE(new WSBlockType(170, "minecraft:hay_block", 64)),
	CARPET(new WSBlockTypeDyeColor(171, "minecraft:carpet", 64, EnumDyeColor.WHITE)),
	HARDENED_CLAY(new WSBlockType(172, "minecraft:hardened_clay", 64)),
	COAL_BLOCK(new WSBlockType(173, "minecraft:coal_block", 64)),
	PACKED_ICE(new WSBlockType(174, "minecraft:packed_ice", 64)),
	LARGE_FLOWER(new WSBlockType(175, "minecraft:large_flower", 64)),
	BANNER(new WSBlockType(176, "minecraft:banner", 64)),
	WALL_BANNER(new WSBlockType(177, "minecraft:wall_banner", 64)),
	INVERTED_DAYLIGHT_SENSOR(new WSBlockType(178, "minecraft:daylight_detector_inverted", 64)),
	RED_SANDSTONE(new WSBlockTypeSandstone(179, "minecraft:red_sandstone", 64, EnumSandstoneType.DEFAULT)),
	RED_SANDSTONE_STAIRS(new WSBlockTypeStair(180, "minecraft:red_sandstone_stairs", 64, EnumStairDirection.EAST, false)),
	DOUBLE_RED_SANDSTONE_SLAB(new WSBlockType(181, "minecraft:double_stone_slab2", 64)),
	RED_SANDSTONE_SLAB(new WSBlockType(182, "minecraft:stone_slab2", 64)),
	SPRUCE_FENCE_GATE(new WSBlockTypeFacingPowered(183, "minecraft:spruce_fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	BIRCH_FENCE_GATE(new WSBlockTypeFacingPowered(184, "minecraft:birch_fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	JUNGLE_FENCE_GATE(new WSBlockTypeFacingPowered(185, "minecraft:jungle_fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	DARK_OAK_FENCE_GATE(new WSBlockTypeFacingPowered(186, "minecraft:dark_oak_fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	ACACIA_FENCE_GATE(new WSBlockTypeFacingPowered(187, "minecraft:acacia_fence_gate", 64, EnumBlockFacingDirection.SOUTH, false)),
	SPRUCE_FENCE(new WSBlockType(188, "minecraft:spruce_fence", 64)),
	BIRCH_FENCE(new WSBlockType(189, "minecraft:birch_fence", 64)),
	JUNGLE_FENCE(new WSBlockType(190, "minecraft:jungle_fence", 64)),
	DARK_OAK_FENCE(new WSBlockType(191, "minecraft:dark_oak_fence", 64)),
	ACACIA_FENCE(new WSBlockType(192, "minecraft:acacia_fence", 64)),
	SPRUCE_DOOR(new WSBlockTypeDoor(193, "minecraft:spruce_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	BIRCH_DOOR(new WSBlockTypeDoor(194, "minecraft:birch_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	JUNGLE_DOOR(new WSBlockTypeDoor(195, "minecraft:jungle_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	DARK_OAK_DOOR(new WSBlockTypeDoor(196, "minecraft:dark_oak_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	ACACIA_DOOR(new WSBlockTypeDoor(197, "minecraft:acacia_door", 64, EnumDoorDirection.EAST, false, false, false, false)),
	END_ROD(new WSBlockTypeDirection(198, "minecraft:end_rod", 64, EnumBlockDirection.DOWN)),
	CHORUS_PLANT(new WSBlockType(199, "minecraft:chorus_plant", 64)),
	CHORUS_FLOWER(new WSBlockType(200, "minecraft:chorus_flower", 64)),
	PURPUR_BLOCK(new WSBlockType(201, "minecraft:purpur_block", 64)),
	PURPUR_PILLAR(new WSBlockType(202, "minecraft:purpur_pillar", 64)),
	PURPUR_STAIRS(new WSBlockTypeStair(203, "minecraft:purpur_stairs", 64, EnumStairDirection.EAST, false)),
	PURPUR_DOUBLE_SLAB(new WSBlockType(204, "minecraft:purpur_double_slab", 64)),
	PURPUR_SLAB(new WSBlockType(205, "minecraft:purpur_slab", 64)),
	END_STONE_BRICKS(new WSBlockType(206, "minecraft:end_bricks", 64)),
	BEETROOTS(new WSBlockTypeGrowable(207, "minecraft:beetroots", 64, 0)),
	GRASS_PATH(new WSBlockType(208, "minecraft:grass_path", 64)),
	END_GATEWAY(new WSBlockType(209, "minecraft:end_gateway", 64)),
	REPEATING_COMMAND_BLOCK(new WSBlockType(210, "minecraft:repeating_command_block", 64)),
	CHAIN_COMMAND_BLOCK(new WSBlockType(211, "minecraft:chain_command_block", 64)),
	FROSTED_ICE(new WSBlockType(212, "minecraft:frosted_ice", 64)),
	MAGMA(new WSBlockType(213, "minecraft:magma", 64)),
	NETHER_WART_BLOCK(new WSBlockType(214, "minecraft:nether_wart_block", 64)),
	NETHER_WART_BRICK(new WSBlockType(215, "minecraft:nether_wart_brick", 64)),
	BONE_BLOCK(new WSBlockType(216, "minecraft:bone_block", 64)),
	STRUCTURE_VOID(new WSBlockType(217, "minecraft:structure_void", 64)),
	OBSERVER(new WSBlockType(218, "minecraft:observer", 64)),
	WHITE_SHULKER_BOX(new WSBlockType(219, "minecraft:white_shulker_box", 64)),
	ORANGE_SHULKER_BOX(new WSBlockType(220, "minecraft:orange_shulker_box", 64)),
	MAGENTA_SHULKER_BOX(new WSBlockType(221, "minecraft:magenta_shulker_box", 64)),
	LIGHT_BLUE_SHULKER_BOX(new WSBlockType(222, "minecraft:light_blue_shulker_box", 64)),
	YELLOW_SHULKER_BOX(new WSBlockType(223, "minecraft:yellow_shulker_box", 64)),
	LIME_SHULKER_BOX(new WSBlockType(224, "minecraft:lime_shulker_box", 64)),
	PINK_SHULKER_BOX(new WSBlockType(225, "minecraft:pink_shulker_box", 64)),
	GRAY_SHULKER_BOX(new WSBlockType(226, "minecraft:gray_shulker_box", 64)),
	SILVER_SHULKER_BOX(new WSBlockType(227, "minecraft:silver_shulker_box", 64)),
	CYAN_SHULKER_BOX(new WSBlockType(228, "minecraft:cyan_shulker_box", 64)),
	PURPLE_SHULKER_BOX(new WSBlockType(229, "minecraft:purple_shulker_box", 64)),
	BLUE_SHULKER_BOX(new WSBlockType(230, "minecraft:blue_shulker_box", 64)),
	BROWN_SHULKER_BOX(new WSBlockType(231, "minecraft:brown_shulker_box", 64)),
	GREEN_SHULKER_BOX(new WSBlockType(232, "minecraft:green_shulker_box", 64)),
	RED_SHULKER_BOX(new WSBlockType(233, "minecraft:red_shulker_box", 64)),
	BLACK_SHULKER_BOX(new WSBlockType(234, "minecraft:black_shulker_box", 64)),
	WHITE_GLAZED_TERRACOTTA(new WSBlockType(235, "minecraft:white_glazed_terracotta", 64)),
	ORANGE_GLAZED_TERRACOTTA(new WSBlockType(236, "minecraft:orange_glazed_terracotta", 64)),
	MAGENTA_GLAZED_TERRACOTTA(new WSBlockType(237, "minecraft:magenta_glazed_terracotta", 64)),
	LIGHT_BLUE_GLAZED_TERRACOTTA(new WSBlockType(238, "minecraft:light_blue_glazed_terracotta", 64)),
	YELLOW_GLAZED_TERRACOTTA(new WSBlockType(239, "minecraft:yellow_glazed_terracotta", 64)),
	LIME_GLAZED_TERRACOTTA(new WSBlockType(240, "minecraft:lime_glazed_terracotta", 64)),
	PINK_GLAZED_TERRACOTTA(new WSBlockType(241, "minecraft:pink_glazed_terracotta", 64)),
	GRAY_GLAZED_TERRACOTTA(new WSBlockType(242, "minecraft:gray_glazed_terracotta", 64)),
	SILVER_GLAZED_TERRACOTTA(new WSBlockType(243, "minecraft:silver_glazed_terracotta", 64)),
	CYAN_GLAZED_TERRACOTTA(new WSBlockType(244, "minecraft:cyan_glazed_terracotta", 64)),
	PURPLE_GLAZED_TERRACOTTA(new WSBlockType(245, "minecraft:purple_glazed_terracotta", 64)),
	BLUE_GLAZED_TERRACOTTA(new WSBlockType(246, "minecraft:blue_glazed_terracotta", 64)),
	BROWN_GLAZED_TERRACOTTA(new WSBlockType(247, "minecraft:brown_glazed_terracotta", 64)),
	GREEN_GLAZED_TERRACOTTA(new WSBlockType(248, "minecraft:green_glazed_terracotta", 64)),
	RED_GLAZED_TERRACOTTA(new WSBlockType(249, "minecraft:red_glazed_terracotta", 64)),
	BLACK_GLAZED_TERRACOTTA(new WSBlockType(250, "minecraft:black_glazed_terracotta", 64)),
	CONCRETE(new WSBlockTypeDyeColor(251, "minecraft:concrete", 64, EnumDyeColor.WHITE)),
	CONCRETE_POWDER(new WSBlockTypeDyeColor(252, "minecraft:concrete_powder", 64, EnumDyeColor.WHITE)),
	STRUCTURE_BLOCK(new WSBlockType(255, "minecraft:structure_block", 64));

	private WSBlockType defaultType;


	WSBlockTypes(WSBlockType defaultType) {
		this.defaultType = defaultType;
	}


	/**
	 * This method returns an optional of a {@link WSBlockType}.
	 *
	 * @param id the {@link Integer} ID of the {@link WSBlockType}.
	 * @param dataValue the DataValue of the {@link WSBlockType}.
	 *
	 * @return the {@link WSBlockType}, if present.
	 */
	public static Optional<WSBlockType> getType(int id, int dataValue) {
		Optional<WSBlockType> optional = Arrays.stream(values()).filter(type -> type.getId() == id).map(WSBlockTypes::getDefaultType).findAny();
		if (!optional.isPresent()) return Optional.empty();
		WSBlockType blockType = optional.get();
		setDataValue(blockType, dataValue);
		return Optional.of(blockType);
	}


	/**
	 * This method returns an optional of a {@link WSBlockType}.
	 *
	 * @param id the {@link String} ID of the {@link WSBlockType}.
	 *
	 * @return the {@link WSBlockType}, if present.
	 */
	public static Optional<WSBlockType> getType(String id) {
		Optional<WSBlockType> optional = Arrays.stream(values()).filter(type -> type.getStringId().equals(id.toLowerCase())).map(WSBlockTypes::getDefaultType).findAny();
		if (!optional.isPresent()) return Optional.empty();
		WSBlockType blockType = optional.get();
		return Optional.of(blockType);
	}

	private static void setDataValue(WSBlockType blockType, int dataValue) {
		if (blockType instanceof WSDataValuable) ((WSDataValuable) blockType).setDataValue(dataValue);
	}


	/**
	 * @return a new instance of the default {@link WSBlockType}.
	 */
	public WSBlockType getDefaultType() {
		return defaultType.clone();
	}


	/**
	 * @return the {@link Integer} ID of the {@link WSBlockType}.
	 */
	public int getId() {
		return defaultType.getId();
	}


	/**
	 * @return the {@link String} ID of the {@link WSBlockType}.
	 */
	public String getStringId() {
		return defaultType.getStringId();
	}


	/**
	 * @param material the material.
	 *
	 * @return true if the material type is equals to this block type.
	 */
	public boolean equalsType(WSMaterial material) {
		return material.getId() == getId();
	}

}
