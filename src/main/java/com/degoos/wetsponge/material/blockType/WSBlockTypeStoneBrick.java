package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumStoneBrickType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeStoneBrick extends WSBlockType implements WSDataValuable {

    private EnumStoneBrickType stoneBrickType;


    public WSBlockTypeStoneBrick(EnumStoneBrickType stoneBrickType) {
        super(98, "minecraft:stonebrick", 64);
        this.stoneBrickType = stoneBrickType;
    }


    /**
     * @return the {@link EnumStoneBrickType} of this {@link WSBlockTypeStoneBrick}.
     */
    public EnumStoneBrickType getStoneBrickType() {
        return stoneBrickType;
    }


    /**
     * Sets the {@link EnumStoneBrickType} of this {@link WSBlockTypeStoneBrick}.
     *
     * @param stoneBrickType the {@link EnumStoneBrickType}
     * @return this {@link WSBlockTypeStoneBrick}.
     */
    public WSBlockTypeStoneBrick setStoneBrickType(EnumStoneBrickType stoneBrickType) {
        this.stoneBrickType = stoneBrickType;
        return this;
    }


    @Override
    public WSBlockTypeStoneBrick clone() {
        return new WSBlockTypeStoneBrick(stoneBrickType);
    }


    @Override
    public short getDataValue() {
        return (short) stoneBrickType.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setStoneBrickType(EnumStoneBrickType.getByValue(dataValue).orElse(EnumStoneBrickType.DEFAULT));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeStoneBrick that = (WSBlockTypeStoneBrick) o;

        return stoneBrickType == that.stoneBrickType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + stoneBrickType.hashCode();
        return result;
    }
}
