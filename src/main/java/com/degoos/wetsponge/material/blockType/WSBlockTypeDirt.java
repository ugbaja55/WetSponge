package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumDirtType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeDirt extends WSBlockType implements WSDataValuable {

	private EnumDirtType dirtType;


	public WSBlockTypeDirt (EnumDirtType dirtType) {
		super(3, "minecraft:dirt", 64);
		this.dirtType = dirtType;
	}


	/**
	 * @return the {@link EnumDirtType} of this {@link WSBlockTypeDirt}.
	 */
	public EnumDirtType getDirtType () {
		return dirtType;
	}


	/**
	 * Sets the {@link EnumDirtType} of this {@link WSBlockTypeDirt}.
	 *
	 * @param dirtType
	 * 		the {@link EnumDirtType}
	 *
	 * @return this {@link WSBlockTypeDirt}.
	 */
	public WSBlockTypeDirt setDirtType (EnumDirtType dirtType) {
		this.dirtType = dirtType;
		return this;
	}


	@Override
	public WSBlockTypeDirt clone () {
		return new WSBlockTypeDirt(dirtType);
	}


	@Override
	public short getDataValue () {
		return (short) dirtType.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		setDirtType(EnumDirtType.getByValue(dataValue).orElse(EnumDirtType.DIRT));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeDirt that = (WSBlockTypeDirt) o;

		return dirtType == that.dirtType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + dirtType.hashCode();
		return result;
	}
}
