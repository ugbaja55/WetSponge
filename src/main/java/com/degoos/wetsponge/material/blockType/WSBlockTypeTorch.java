package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumTorchDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeTorch extends WSBlockType implements WSDataValuable {

    private EnumTorchDirection direction;


    public WSBlockTypeTorch(int id, String stringId, int maxStackQuantity, EnumTorchDirection direction) {
        super(id, stringId, maxStackQuantity);
        this.direction = direction;
    }


    /**
     * @return the {@link EnumTorchDirection} of this {@link WSBlockTypeTorch}.
     */
    public EnumTorchDirection getTorchDirection() {
        return direction;
    }


    /**
     * Sets the {@link EnumTorchDirection} of this {@link WSBlockTypeTorch}.
     *
     * @param direction the {@link EnumTorchDirection}
     * @return this {@link WSBlockTypeTorch}.
     */
    public WSBlockTypeTorch setTorchDirection(EnumTorchDirection direction) {
        this.direction = direction;
        return this;
    }


    @Override
    public WSBlockTypeTorch clone() {
        return new WSBlockTypeTorch(getId(), getStringId(), getMaxStackQuantity(), direction);
    }


    @Override
    public short getDataValue() {
        return (short) direction.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setTorchDirection(EnumTorchDirection.getByValue(dataValue).orElse(EnumTorchDirection.UP));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeTorch that = (WSBlockTypeTorch) o;

        return direction == that.direction;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }
}
