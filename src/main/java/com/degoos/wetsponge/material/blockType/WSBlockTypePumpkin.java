package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumPumpkinDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypePumpkin extends WSBlockType implements WSDataValuable {

    private EnumPumpkinDirection direction;


    public WSBlockTypePumpkin(int id, String stringId, int maxStackQuantity, EnumPumpkinDirection direction) {
        super(id, stringId, maxStackQuantity);
        this.direction = direction;
    }


    /**
     * @return the {@link EnumPumpkinDirection} of this {@link WSBlockTypePumpkin}.
     */
    public EnumPumpkinDirection getDirection() {
        return direction;
    }


    /**
     * Sets the {@link EnumPumpkinDirection} of this {@link WSBlockTypePumpkin}.
     *
     * @param direction the {@link EnumPumpkinDirection}
     * @return this {@link WSBlockTypePumpkin}.
     */
    public WSBlockTypePumpkin setDirection(EnumPumpkinDirection direction) {
        this.direction = direction;
        return this;
    }


    @Override
    public WSBlockTypePumpkin clone() {
        return new WSBlockTypePumpkin(getId(), getStringId(), getMaxStackQuantity(), direction);
    }


    @Override
    public short getDataValue() {
        return (short) direction.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setDirection(EnumPumpkinDirection.getByValue(dataValue).orElse(EnumPumpkinDirection.SOUTH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypePumpkin that = (WSBlockTypePumpkin) o;

        return direction == that.direction;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }
}
