package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumStoneType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeStone extends WSBlockType implements WSDataValuable {

	private EnumStoneType stoneType;


	public WSBlockTypeStone (EnumStoneType stoneType) {
		super(1, "minecraft:stone", 64);
		this.stoneType = stoneType;
	}


	/**
	 * @return the {@link EnumStoneType} of this {@link WSBlockTypeStone}.
	 */
	public EnumStoneType getStoneType () {
		return stoneType;
	}


	/**
	 * Sets the {@link EnumStoneType} of this {@link WSBlockTypeStone}.
	 *
	 * @param stoneType
	 * 		the {@link EnumStoneType}
	 *
	 * @return this {@link WSBlockTypeStone}.
	 */
	public WSBlockTypeStone setStoneType (EnumStoneType stoneType) {
		this.stoneType = stoneType;
		return this;
	}


	@Override
	public WSBlockTypeStone clone () {
		return new WSBlockTypeStone(stoneType);
	}


	@Override
	public short getDataValue () {
		return (short) stoneType.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		setStoneType(EnumStoneType.getByValue(dataValue).orElse(EnumStoneType.STONE));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeStone that = (WSBlockTypeStone) o;

		return stoneType == that.stoneType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + stoneType.hashCode();
		return result;
	}
}
