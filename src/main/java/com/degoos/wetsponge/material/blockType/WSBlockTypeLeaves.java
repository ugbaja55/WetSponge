package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.enums.block.EnumWoodType;

public class WSBlockTypeLeaves extends WSBlockTypeWood {

	private boolean decay;


	public WSBlockTypeLeaves (int id, String stringId, EnumWoodType woodType, boolean decay) {
		super(id, stringId, 64, woodType);
		this.decay = decay;
	}


	/**
	 * @return true if the leaves of the {@link WSBlock} can decay.
	 */
	public boolean isDecay () {
		return decay;
	}


	/**
	 * Sets if the leaves of the {@link WSBlock} can decay.
	 *
	 * @param decay
	 * 		true to let the leave decay
	 *
	 * @return this {@link WSBlockTypeLeaves}.
	 */
	public WSBlockTypeLeaves setDecay (boolean decay) {
		this.decay = decay;
		return this;
	}


	@Override
	public WSBlockTypeLeaves clone () {
		return new WSBlockTypeLeaves(getId(), getStringId(), getWoodType(), decay);
	}


	@Override
	public short getDataValue () {
		return (short) (getWoodType().getValue() + ((decay ? 0 : 1) * 4));
	}


	@Override
	public void setDataValue (int dataValue) {
		setDecay(dataValue / 4 > 1).setWoodType(EnumWoodType.getByValue(dataValue % 4).orElse(EnumWoodType.OAK));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeLeaves that = (WSBlockTypeLeaves) o;

		return decay == that.decay;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (decay ? 1 : 0);
		return result;
	}
}
