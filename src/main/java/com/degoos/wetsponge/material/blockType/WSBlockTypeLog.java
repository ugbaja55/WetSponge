package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.enums.block.EnumBlockAxis;

public class WSBlockTypeLog extends WSBlockTypeWood {

    private EnumBlockAxis logAxis;


    public WSBlockTypeLog(EnumWoodType woodType, EnumBlockAxis logAxis) {
        super(17, "minecraft:log", 64, woodType);
        this.logAxis = logAxis;
    }


    /**
     * This value is not supported by {@link WSItemStack}s and it will be ignored.
     *
     * @return the {@link EnumBlockAxis} of this {@link WSBlockTypeLog}.
     */
    public EnumBlockAxis getLogAxis() {
        return logAxis;
    }


    /**
     * Sets the {@link EnumBlockAxis} of this {@link WSBlockTypeLog}.
     * This value is not supported by {@link WSItemStack}s and it will be ignored.
     *
     * @param logAxis the {@link EnumBlockAxis}
     * @return this {@link WSBlockTypeLog}.
     */
    public WSBlockTypeLog setLogAxis(EnumBlockAxis logAxis) {
        this.logAxis = logAxis;
        return this;
    }


    @Override
    public WSBlockTypeLog clone() {
        return new WSBlockTypeLog(getWoodType(), logAxis);
    }


    @Override
    public short getDataValue() {
        return (short) (getWoodType().getValue() + (logAxis.getValue() * 4));
    }

    @Override
    public void setDataValue(int dataValue) {
        setLogAxis(EnumBlockAxis.getByValue(dataValue / 4).orElse(EnumBlockAxis.Y))
                .setWoodType(EnumWoodType.getByValue(dataValue % 4).orElse(EnumWoodType.OAK));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeLog that = (WSBlockTypeLog) o;

        return logAxis == that.logAxis;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + logAxis.hashCode();
        return result;
    }
}
