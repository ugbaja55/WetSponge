package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumStairDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeStair extends WSBlockType implements WSDataValuable {

    private EnumStairDirection direction;
    private boolean upside;


    public WSBlockTypeStair(int id, String stringId, int maxStackQuantity, EnumStairDirection direction, boolean upside) {
        super(id, stringId, maxStackQuantity);
        this.direction = direction;
        this.upside = upside;
    }


    /**
     * @return the {@link EnumStairDirection} of this {@link WSBlockTypeStair}.
     */
    public EnumStairDirection getStairDirection() {
        return direction;
    }


    /**
     * Sets the {@link EnumStairDirection} of this {@link WSBlockTypeStair}.
     *
     * @param direction the {@link EnumStairDirection}
     * @return this {@link WSBlockTypeStair}.
     */
    public WSBlockTypeStair setStairDirection(EnumStairDirection direction) {
        this.direction = direction;
        return this;
    }


    /**
     * @return true if the stair is upside.
     */
    public boolean isUpside() {
        return upside;
    }


    /**
     * Sets if the stair is upside.
     *
     * @param upside the boolean.
     * @return the {@link WSBlockTypeStair block type}.
     */
    public WSBlockTypeStair setUpside(boolean upside) {
        this.upside = upside;
        return this;
    }


    @Override
    public WSBlockTypeStair clone() {
        return new WSBlockTypeStair(getId(), getStringId(), getMaxStackQuantity(), direction, upside);
    }


    @Override
    public short getDataValue() {
        return (short) (direction.getValue() + (upside ? 1 : 0) * 4);
    }

    @Override
    public void setDataValue(int dataValue) {
        setStairDirection(EnumStairDirection.getByValue(dataValue % 4).orElse(EnumStairDirection.EAST)).setUpside(dataValue > 3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeStair that = (WSBlockTypeStair) o;

        if (upside != that.upside) return false;
        return direction == that.direction;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + direction.hashCode();
        result = 31 * result + (upside ? 1 : 0);
        return result;
    }
}
