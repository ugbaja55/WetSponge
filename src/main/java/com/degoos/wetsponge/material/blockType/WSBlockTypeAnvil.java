package com.degoos.wetsponge.material.blockType;

import com.degoos.wetsponge.enums.block.EnumAnvilDamage;
import com.degoos.wetsponge.enums.block.EnumBlockFacingDirection;

public class WSBlockTypeAnvil extends WSBlockTypeFacing {

    private EnumAnvilDamage anvilDamage;

    public WSBlockTypeAnvil(EnumBlockFacingDirection facingDirection, EnumAnvilDamage anvilDamage) {
        super(145, "minecraft:anvil", 64, facingDirection);
        this.anvilDamage = anvilDamage;
    }

    /**
     * @return the {@link EnumAnvilDamage modifier} of the {@link WSBlockTypeAnvil anvil}.
     */
    public EnumAnvilDamage getAnvilDamage() {
        return anvilDamage;
    }

    /**
     * Sets the {@link EnumAnvilDamage modifier} of the {@link WSBlockTypeAnvil anvil}.
     * @param anvilDamage the {@link EnumAnvilDamage modifier}.
     * @return the {@link WSBlockTypeAnvil anvil}.
     */
    public WSBlockTypeAnvil setAnvilDamage(EnumAnvilDamage anvilDamage) {
        this.anvilDamage = anvilDamage;
        return this;
    }

    @Override
    public WSBlockTypeAnvil clone() {
        return new WSBlockTypeAnvil(getFacingDirection(), anvilDamage);
    }


    @Override
    public short getDataValue() {
        return (short) (getFacingDirection().getValue() + anvilDamage.getValue() * 4);
    }

    @Override
    public void setDataValue(int dataValue) {
        setAnvilDamage(EnumAnvilDamage.getByValue(dataValue / 4).orElse(EnumAnvilDamage.NORMAL))
                .setFacingDirection(EnumBlockFacingDirection.getByValue(dataValue % 4).orElse(EnumBlockFacingDirection.SOUTH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeAnvil that = (WSBlockTypeAnvil) o;

        return anvilDamage == that.anvilDamage;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + anvilDamage.hashCode();
        return result;
    }
}
