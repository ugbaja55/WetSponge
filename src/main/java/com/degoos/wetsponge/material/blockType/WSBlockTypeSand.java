package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumSandType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeSand extends WSBlockType implements WSDataValuable {

	private EnumSandType sandType;


	public WSBlockTypeSand (EnumSandType sandType) {
		super(12, "minecraft:sand", 64);
		this.sandType = sandType;
	}


	/**
	 * @return the {@link EnumSandType} of this {@link WSBlockTypeSand}.
	 */
	public EnumSandType getSandType () {
		return sandType;
	}


	/**
	 * Sets the {@link EnumSandType} of this {@link WSBlockTypeSand}.
	 *
	 * @param sandType
	 * 		the {@link EnumSandType}
	 *
	 * @return this {@link WSBlockTypeSand}.
	 */
	public WSBlockTypeSand setSandType (EnumSandType sandType) {
		this.sandType = sandType;
		return this;
	}


	@Override
	public WSBlockTypeSand clone () {
		return new WSBlockTypeSand(sandType);
	}


	@Override
	public short getDataValue () {
		return (short) sandType.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		setSandType(EnumSandType.getByValue(dataValue).orElse(EnumSandType.NORMAL));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeSand that = (WSBlockTypeSand) o;

		return sandType == that.sandType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + sandType.hashCode();
		return result;
	}
}
