package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumBlockDirection;

public class WSBlockTypeDirectionPowered extends WSBlockTypeDirection {

    private boolean powered;


    public WSBlockTypeDirectionPowered(int id, String stringId, int maxStackQuantity, EnumBlockDirection direction, boolean powered) {
        super(id, stringId, maxStackQuantity, direction);
        this.powered = powered;
    }


    /**
     * @return true if the piston is powered.
     */
    public boolean isPowered() {
        return powered;
    }


    /**
     * Sets if the piston is powered.
     *
     * @param powered the boolean.
     * @return this {@link WSBlockTypeDirectionPowered}
     */
    public WSBlockTypeDirectionPowered setPowered(boolean powered) {
        this.powered = powered;
        return this;
    }


    @Override
    public WSBlockTypeDirectionPowered clone() {
        return new WSBlockTypeDirectionPowered(getId(), getStringId(), getMaxStackQuantity(), getDirection(), powered);
    }


    @Override
    public short getDataValue() {
        return (short) (getDirection().getValue() + ((powered ? 1 : 0) * 8));
    }


    @Override
    public void setDataValue(int dataValue) {
        setPowered(dataValue >= 8).setDirection(EnumBlockDirection.getByValue(dataValue < 8 ? dataValue % 6 : (dataValue - 8) % 6).orElse(EnumBlockDirection.DOWN));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeDirectionPowered that = (WSBlockTypeDirectionPowered) o;

        return powered == that.powered;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (powered ? 1 : 0);
        return result;
    }
}
