package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeSponge extends WSBlockType implements WSDataValuable {

    private boolean wet;


    public WSBlockTypeSponge(boolean wet) {
        super(19, "minecraft:sponge", 64);
        this.wet = wet;
    }


    /**
     * @return true if the sponge is wet.
     */
    public boolean isWet() {
        return wet;
    }


    /**
     * Sets if a sponge is wet.
     *
     * @param wet the boolean.
     */
    public void setWet(boolean wet) {
        this.wet = wet;
    }


    @Override
    public WSBlockTypeSponge clone() {
        return new WSBlockTypeSponge(wet);
    }


    @Override
    public short getDataValue() {
        return (short) (wet ? 1 : 0);
    }

    @Override
    public void setDataValue(int dataValue) {
        setWet(dataValue > 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeSponge that = (WSBlockTypeSponge) o;

        return wet == that.wet;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (wet ? 1 : 0);
        return result;
    }
}
