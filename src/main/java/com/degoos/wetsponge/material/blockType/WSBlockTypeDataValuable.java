package com.degoos.wetsponge.material.blockType;

import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeDataValuable extends WSBlockType implements WSDataValuable {

    private short dataValue;

    public WSBlockTypeDataValuable(int id, String stringId, int maxStackQuantity, int dataValue) {
        super(id, stringId, maxStackQuantity);
        this.dataValue = (short) dataValue;
    }

    @Override
    public short getDataValue() {
        return dataValue;
    }

    @Override
    public void setDataValue(int dataValue) {
        this.dataValue = (short) dataValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeDataValuable that = (WSBlockTypeDataValuable) o;

        return dataValue == that.dataValue;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) dataValue;
        return result;
    }
}
