package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeCobbleStoneWall extends WSBlockType implements WSDataValuable {

    private boolean mossy;


    public WSBlockTypeCobbleStoneWall(boolean mossy) {
        super(139, "minecraft:cobblestone_wall", 64);
        this.mossy = mossy;
    }


    /**
     * @return true if the {@link WSBlockTypeCobbleStoneWall} is mossy.
     */
    public boolean isMossy() {
        return mossy;
    }

    /**
     * Set if the {@link WSBlockTypeCobbleStoneWall} is mossy.
     *
     * @param mossy the boolean.
     */
    public void setMossy(boolean mossy) {
        this.mossy = mossy;
    }

    @Override
    public WSBlockTypeCobbleStoneWall clone() {
        return new WSBlockTypeCobbleStoneWall(mossy);
    }


    @Override
    public short getDataValue() {
        return (short) (mossy ? 1 : 0);
    }

    @Override
    public void setDataValue(int dataValue) {
        setMossy(dataValue > 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeCobbleStoneWall that = (WSBlockTypeCobbleStoneWall) o;

        return mossy == that.mossy;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mossy ? 1 : 0);
        return result;
    }
}
