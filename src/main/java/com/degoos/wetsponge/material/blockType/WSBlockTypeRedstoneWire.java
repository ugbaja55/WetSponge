package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;
import java.util.Objects;

public class WSBlockTypeRedstoneWire extends WSBlockType implements WSDataValuable {

	private int power;

	public WSBlockTypeRedstoneWire(int power) {
		super(55, "redstone_wire", 64);
		this.power = power;
	}

	/**
	 * Returns the power level of the {@link WSBlockTypeRedstoneWire}.
	 *
	 * @return the power level.
	 */
	public int getPower() {
		return power;
	}

	/**
	 * Sets the power level of the {@link WSBlockTypeRedstoneWire}.
	 *
	 * @param power the power level.
	 */
	public void setPower(int power) {
		this.power = power;
	}

	@Override
	public WSBlockTypeRedstoneWire clone() {
		return new WSBlockTypeRedstoneWire(power);
	}


	@Override
	public short getDataValue() {
		return (short) power;
	}

	@Override
	public void setDataValue(int dataValue) {
		this.power = dataValue;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		WSBlockTypeRedstoneWire that = (WSBlockTypeRedstoneWire) o;
		return power == that.power;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), power);
	}
}
