package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumVineDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeVine extends WSBlockType implements WSDataValuable {

    private EnumVineDirection vineDirection;


    public WSBlockTypeVine(EnumVineDirection vineDirection) {
        super(106, "minecraft:vine", 64);
        this.vineDirection = vineDirection;
    }


    /**
     * @return the {@link EnumVineDirection} of this {@link WSBlockTypeVine}.
     */
    public EnumVineDirection getVineDirection() {
        return vineDirection;
    }


    /**
     * Sets the {@link EnumVineDirection} of this {@link WSBlockTypeVine}.
     *
     * @param vineDirection the {@link EnumVineDirection}
     * @return this {@link WSBlockTypeVine}.
     */
    public WSBlockTypeVine setVineDirection(EnumVineDirection vineDirection) {
        this.vineDirection = vineDirection;
        return this;
    }


    @Override
    public WSBlockTypeVine clone() {
        return new WSBlockTypeVine(vineDirection);
    }


    @Override
    public short getDataValue() {
        return (short) vineDirection.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setVineDirection(EnumVineDirection.getByValue(dataValue).orElse(EnumVineDirection.SOUTH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeVine that = (WSBlockTypeVine) o;

        return vineDirection == that.vineDirection;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + vineDirection.hashCode();
        return result;
    }
}
