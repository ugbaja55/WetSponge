package com.degoos.wetsponge.material.blockType;

import com.degoos.wetsponge.enums.block.EnumRepeaterDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeRepeater extends WSBlockType implements WSDataValuable {

	private EnumRepeaterDirection direction;
	private int delay;

	public WSBlockTypeRepeater(int id, String stringId, int maxStackQuantity, EnumRepeaterDirection direction, int delay) {
		super(id, stringId, maxStackQuantity);
		this.delay = delay;
		this.direction = direction;
	}

	/**
	 * @return the facing {@link EnumRepeaterDirection} of this {@link WSBlockTypeRepeater}.
	 */
	public EnumRepeaterDirection getDirection() {
		return direction;
	}

	/**
	 * Sets the facing {@link EnumRepeaterDirection} of this {@link WSBlockTypeRepeater}.
	 *
	 * @param direction the {@link EnumRepeaterDirection}.
	 *
	 * @return this {@link WSBlockTypeRepeater}.
	 */
	public WSBlockTypeRepeater setDirection(EnumRepeaterDirection direction) {
		this.direction = direction;
		return this;
	}

	/**
	 * Returns a value between 0 and 3, depending of this {@link WSBlockTypeRepeater}'s delay.
	 *
	 * @return 0 if the repeater delay is of 1 tick,
	 * 1 if the delay is of 2 ticks,
	 * 2 if the delay is of 3 ticks
	 * or 3 if the delay is of 4 ticks.
	 */
	public int getDelay() {
		return delay;
	}

	/**
	 * Sets the delay value of this {@link WSBlockTypeRepeater}.
	 * Set it  to 0 to set the repeater delay to 1 tick,
	 * 1 to set the delay to 2 ticks,
	 * 2 to set the delay to 3 ticks
	 * or 3 to set the delay to 4 ticks.
	 *
	 * @param delay the delay value.
	 *
	 * @return this {@link WSBlockTypeRepeater}.
	 */
	public WSBlockTypeRepeater setDelay(int delay) {
		if (delay < 0 || delay > 3) return this;
		this.delay = delay;
		return this;
	}

	@Override
	public short getDataValue() {
		return (short) (direction.getValue() + (delay * 4));
	}

	@Override
		public void setDataValue(int dataValue) {
		setDirection(EnumRepeaterDirection.getByValue(dataValue % 4).orElse(EnumRepeaterDirection.NORTH)).setDelay(Math.floorDiv(dataValue, 4));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeRepeater that = (WSBlockTypeRepeater) o;

		if (delay != that.delay) return false;
		return direction == that.direction;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + direction.hashCode();
		result = 31 * result + delay;
		return result;
	}
}
