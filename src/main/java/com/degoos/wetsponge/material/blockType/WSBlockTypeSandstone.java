package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumSandstoneType;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeSandstone extends WSBlockType implements WSDataValuable {

	private EnumSandstoneType sandstoneType;


	public WSBlockTypeSandstone (int id, String stringId, int maxStackQuantity, EnumSandstoneType sandstoneType) {
		super(id, stringId, maxStackQuantity);
		this.sandstoneType = sandstoneType;
	}


	/**
	 * @return the {@link EnumSandstoneType} of this {@link WSBlockTypeSandstone}.
	 */
	public EnumSandstoneType getSandstoneType () {
		return sandstoneType;
	}


	/**
	 * Sets the {@link EnumSandstoneType} of this {@link WSBlockTypeSandstone}.
	 *
	 * @param sandstoneType
	 * 		the {@link EnumSandstoneType}
	 *
	 * @return this {@link WSBlockTypeSandstone}.
	 */
	public WSBlockTypeSandstone setSandstoneType (EnumSandstoneType sandstoneType) {
		this.sandstoneType = sandstoneType;
		return this;
	}


	@Override
	public WSBlockTypeSandstone clone () {
		return new WSBlockTypeSandstone(getId(), getStringId(), getMaxStackQuantity(), getSandstoneType());
	}


	@Override
	public short getDataValue () {
		return (short) sandstoneType.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		setSandstoneType(EnumSandstoneType.getByValue(dataValue).orElse(EnumSandstoneType.DEFAULT));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeSandstone that = (WSBlockTypeSandstone) o;

		return sandstoneType == that.sandstoneType;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + sandstoneType.hashCode();
		return result;
	}
}
