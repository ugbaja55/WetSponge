package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumBlockFacingDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeFacing extends WSBlockType implements WSDataValuable {

    private EnumBlockFacingDirection facingDirection;

    public WSBlockTypeFacing(int id, String stringId, int maxStackQuantity, EnumBlockFacingDirection facingDirection) {
        super(id, stringId, maxStackQuantity);
        this.facingDirection = facingDirection;
    }


    /**
     * @return the {@link EnumBlockFacingDirection} of this {@link WSBlockTypeVine}.
     */
    public EnumBlockFacingDirection getFacingDirection() {
        return facingDirection;
    }


    /**
     * Sets the {@link EnumBlockFacingDirection} of this {@link WSBlockTypeVine}.
     *
     * @param facingDirection the {@link EnumBlockFacingDirection}
     * @return this {@link WSBlockTypeVine}.
     */
    public WSBlockTypeFacing setFacingDirection(EnumBlockFacingDirection facingDirection) {
        this.facingDirection = facingDirection;
        return this;
    }

    @Override
    public WSBlockTypeFacing clone() {
        return new WSBlockTypeFacing(getId(), getStringId(), getMaxStackQuantity(), facingDirection);
    }


    @Override
    public short getDataValue() {
        return (short) facingDirection.getValue();
    }

    @Override
    public void setDataValue(int dataValue) {
        setFacingDirection(EnumBlockFacingDirection.getByValue(dataValue % 4).orElse(EnumBlockFacingDirection.SOUTH));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeFacing that = (WSBlockTypeFacing) o;

        return facingDirection == that.facingDirection;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + facingDirection.hashCode();
        return result;
    }
}
