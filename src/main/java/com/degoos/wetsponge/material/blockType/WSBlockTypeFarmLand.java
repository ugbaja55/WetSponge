package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeFarmLand extends WSBlockType implements WSDataValuable {

    private int moisture;


    public WSBlockTypeFarmLand(int moisture) {
        super(60, "minecraft:farmland", 64);
        this.moisture = moisture;
    }


    /**
     * @return the crop moisture.
     */
    public int getMoisture() {
        return moisture;
    }


    /**
     * Sets the crop moisture.
     *
     * @param moisture the moisture.
     */
    public void setMoisture(int moisture) {
        this.moisture = moisture;
    }


    @Override
    public WSBlockTypeFarmLand clone() {
        return new WSBlockTypeFarmLand(moisture);
    }


    @Override
    public short getDataValue() {
        return (short) moisture;
    }

    @Override
    public void setDataValue(int dataValue) {
        setMoisture(dataValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeFarmLand that = (WSBlockTypeFarmLand) o;

        return moisture == that.moisture;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + moisture;
        return result;
    }
}
