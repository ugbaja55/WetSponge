package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumLeverDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeLever extends WSBLockTypePowered implements WSDataValuable {

    private EnumLeverDirection direction;


    public WSBlockTypeLever(EnumLeverDirection direction, boolean powered) {
        super(69, "minecraft:lever", 64, powered);
        this.direction = direction;
    }


    /**
     * @return the {@link EnumLeverDirection} of this {@link WSBlockTypeLever}.
     */
    public EnumLeverDirection getDirection() {
        return direction;
    }


    /**
     * Sets the {@link EnumLeverDirection} of this {@link WSBlockTypeLever}.
     *
     * @param direction the {@link EnumLeverDirection}
     * @return this {@link WSBlockTypeLever}.
     */
    public WSBlockTypeLever setDirection(EnumLeverDirection direction) {
        this.direction = direction;
        return this;
    }

    @Override
    public WSBlockTypeLever clone() {
        return new WSBlockTypeLever(direction, isPowered());
    }


    @Override
    public short getDataValue() {
        return (short) (direction.getValue() + (isPowered() ? 0 : 1) * 8);
    }

    @Override
    public void setDataValue(int dataValue) {
        setDirection(EnumLeverDirection.getByValue(dataValue % 8).orElse(EnumLeverDirection.BOTTOM_EAST))
                .setPowered(dataValue > 7);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WSBlockTypeLever that = (WSBlockTypeLever) o;

        return direction == that.direction;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }
}
