package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.material.WSDataValuable;
import java.util.Objects;

public class WSBlockTypeFluid extends WSBlockType implements WSDataValuable {

	private int level;
	private boolean falling;


	public WSBlockTypeFluid(int id, String stringId, int maxStackQuantity, int level, boolean falling) {
		super(id, stringId, maxStackQuantity);
		this.level = level;
		this.falling = falling;
	}

	/**
	 * Returns the level value of the {@link WSBlockTypeFluid fluid}.
	 * The higher is the level, the lower is the fluid.
	 *
	 * @return the level value of the {@link WSBlockTypeFluid fluid}.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Sets the level value of the {@link WSBlockTypeFluid fluid}.
	 * The higher is the level, the lower is the fluid.
	 *
	 * @param level the level.
	 *
	 * @return the {@link WSBlockTypeFluid fluid}.
	 */
	public WSBlockTypeFluid setLevel(int level) {
		this.level = level;
		return this;
	}

	/**
	 * Returns whether the {@link WSBlockTypeFluid fluid} is falling.
	 *
	 * @return whether the {@link WSBlockTypeFluid fluid} is falling.
	 */
	public boolean isFalling() {
		return falling;
	}

	/**
	 * Sets whether the {@link WSBlockTypeFluid fluid} is falling.
	 *
	 * @param falling whether the {@link WSBlockTypeFluid fluid} is falling.
	 *
	 * @return the {@link WSBlockTypeFluid fluid}.
	 */
	public WSBlockTypeFluid setFalling(boolean falling) {
		this.falling = falling;
		return this;
	}

	@Override
	public WSBlockTypeFluid clone() {
		return new WSBlockTypeFluid(getId(), getStringId(), getMaxStackQuantity(), level, falling);
	}


	@Override
	public short getDataValue() {
		return (short) (level + (falling ? 8 : 0));
	}

	@Override
	public void setDataValue(int dataValue) {
		setFalling(dataValue > 7);
		setLevel(dataValue % 8);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		WSBlockTypeFluid that = (WSBlockTypeFluid) o;
		return level == that.level && falling == that.falling;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), level, falling);
	}
}
