package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.material.WSDataValuable;

public class WSBlockTypeDirection extends WSBlockType implements WSDataValuable {

	private EnumBlockDirection direction;


	public WSBlockTypeDirection (int id, String stringId, int maxStackQuantity, EnumBlockDirection direction) {
		super(id, stringId, maxStackQuantity);
		this.direction = direction;
	}


	/**
	 * @return the {@link EnumBlockDirection} of this {@link WSBlockTypeDirection}.
	 */
	public EnumBlockDirection getDirection () {
		return direction;
	}


	/**
	 * Sets the {@link EnumBlockDirection} of this {@link WSBlockTypeDirection}.
	 *
	 * @param direction
	 * 		the {@link EnumBlockDirection}
	 *
	 * @return this {@link WSBlockTypeDirection}.
	 */
	public WSBlockTypeDirection setDirection (EnumBlockDirection direction) {
		this.direction = direction;
		return this;
	}


	@Override
	public WSBlockTypeDirection clone () {
		return new WSBlockTypeDirection(getId(), getStringId(), getMaxStackQuantity(), direction);
	}


	@Override
	public short getDataValue () {
		return (short) direction.getValue();
	}

	@Override
	public void setDataValue(int dataValue) {
		setDirection(EnumBlockDirection.getByValue(dataValue).orElse(EnumBlockDirection.DOWN));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeDirection that = (WSBlockTypeDirection) o;

		return direction == that.direction;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + direction.hashCode();
		return result;
	}
}
