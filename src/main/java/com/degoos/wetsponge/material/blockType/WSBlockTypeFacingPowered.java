package com.degoos.wetsponge.material.blockType;


import com.degoos.wetsponge.enums.block.EnumBlockFacingDirection;

public class WSBlockTypeFacingPowered extends WSBlockTypeFacing {
	private boolean powered;


	public WSBlockTypeFacingPowered (int id, String stringId, int maxStackQuantity, EnumBlockFacingDirection facingDirection, boolean powered) {
		super(id, stringId, maxStackQuantity, facingDirection);
		this.powered = powered;
	}


	/**
	 * @return true if the fence gate is powered.
	 */
	public boolean isPowered () {
		return powered;
	}


	/**
	 * Sets if the fence gate is powered.
	 *
	 * @param powered
	 * 		the boolean.
	 *
	 * @return returns the current block
	 */
	public WSBlockTypeFacingPowered setPowered (boolean powered) {
		this.powered = powered;
		return this;
	}


	@Override
	public WSBlockTypeFacingPowered clone () {
		return new WSBlockTypeFacingPowered(getId(), getStringId(), getMaxStackQuantity(), getFacingDirection(), powered);
	}


	@Override
	public short getDataValue () {
		return (short) (getFacingDirection().getValue() * (powered ? 2 : 1));
	}


	@Override
	public void setDataValue (int dataValue) {
		setPowered(dataValue > 3).setFacingDirection(EnumBlockFacingDirection.getByValue(dataValue % 4).orElse(EnumBlockFacingDirection.SOUTH));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		WSBlockTypeFacingPowered that = (WSBlockTypeFacingPowered) o;

		return powered == that.powered;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (powered ? 1 : 0);
		return result;
	}
}
