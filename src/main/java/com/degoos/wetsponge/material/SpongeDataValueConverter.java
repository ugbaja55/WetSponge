package com.degoos.wetsponge.material;


import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;
import com.degoos.wetsponge.enums.block.EnumBlockAxis;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.enums.block.EnumBlockFacingDirection;
import com.degoos.wetsponge.enums.block.EnumBlockOrientation;
import com.degoos.wetsponge.enums.block.EnumDirtType;
import com.degoos.wetsponge.enums.block.EnumDisguisedBlockType;
import com.degoos.wetsponge.enums.block.EnumDoorDirection;
import com.degoos.wetsponge.enums.block.EnumFlowerType;
import com.degoos.wetsponge.enums.block.EnumLeverDirection;
import com.degoos.wetsponge.enums.block.EnumPrismarineType;
import com.degoos.wetsponge.enums.block.EnumPumpkinDirection;
import com.degoos.wetsponge.enums.block.EnumRepeaterDirection;
import com.degoos.wetsponge.enums.block.EnumSandType;
import com.degoos.wetsponge.enums.block.EnumSandstoneType;
import com.degoos.wetsponge.enums.block.EnumSkullFacingDirection;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.enums.block.EnumStairDirection;
import com.degoos.wetsponge.enums.block.EnumStoneBrickType;
import com.degoos.wetsponge.enums.block.EnumStoneType;
import com.degoos.wetsponge.enums.block.EnumTallGrassType;
import com.degoos.wetsponge.enums.block.EnumTorchDirection;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.enums.item.EnumBookGeneration;
import com.degoos.wetsponge.enums.item.EnumCoalType;
import com.degoos.wetsponge.enums.item.EnumGoldenAppleType;
import com.degoos.wetsponge.firework.SpongeFireworkEffect;
import com.degoos.wetsponge.material.blockType.WSBLockTypePowered;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypeCobbleStoneWall;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDirection;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDirectionPowered;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDirt;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDoor;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDyeColor;
import com.degoos.wetsponge.material.blockType.WSBlockTypeFacing;
import com.degoos.wetsponge.material.blockType.WSBlockTypeFacingPowered;
import com.degoos.wetsponge.material.blockType.WSBlockTypeFarmLand;
import com.degoos.wetsponge.material.blockType.WSBlockTypeFlower;
import com.degoos.wetsponge.material.blockType.WSBlockTypeGrowable;
import com.degoos.wetsponge.material.blockType.WSBlockTypeLeaves;
import com.degoos.wetsponge.material.blockType.WSBlockTypeLever;
import com.degoos.wetsponge.material.blockType.WSBlockTypeLog;
import com.degoos.wetsponge.material.blockType.WSBlockTypeMonsterEgg;
import com.degoos.wetsponge.material.blockType.WSBlockTypeOrientation;
import com.degoos.wetsponge.material.blockType.WSBlockTypePrismarine;
import com.degoos.wetsponge.material.blockType.WSBlockTypePumpkin;
import com.degoos.wetsponge.material.blockType.WSBlockTypeRedstoneWire;
import com.degoos.wetsponge.material.blockType.WSBlockTypeRepeater;
import com.degoos.wetsponge.material.blockType.WSBlockTypeSand;
import com.degoos.wetsponge.material.blockType.WSBlockTypeSandstone;
import com.degoos.wetsponge.material.blockType.WSBlockTypeSkull;
import com.degoos.wetsponge.material.blockType.WSBlockTypeSponge;
import com.degoos.wetsponge.material.blockType.WSBlockTypeStair;
import com.degoos.wetsponge.material.blockType.WSBlockTypeStone;
import com.degoos.wetsponge.material.blockType.WSBlockTypeStoneBrick;
import com.degoos.wetsponge.material.blockType.WSBlockTypeTallGrass;
import com.degoos.wetsponge.material.blockType.WSBlockTypeTorch;
import com.degoos.wetsponge.material.blockType.WSBlockTypeVine;
import com.degoos.wetsponge.material.blockType.WSBlockTypeWood;
import com.degoos.wetsponge.material.itemType.WSItemTypeBanner;
import com.degoos.wetsponge.material.itemType.WSItemTypeCoal;
import com.degoos.wetsponge.material.itemType.WSItemTypeDurability;
import com.degoos.wetsponge.material.itemType.WSItemTypeFirework;
import com.degoos.wetsponge.material.itemType.WSItemTypeFireworkCharge;
import com.degoos.wetsponge.material.itemType.WSItemTypeGoldenApple;
import com.degoos.wetsponge.material.itemType.WSItemTypeLeatherArmor;
import com.degoos.wetsponge.material.itemType.WSItemTypeMap;
import com.degoos.wetsponge.material.itemType.WSItemTypeSkull;
import com.degoos.wetsponge.material.itemType.WSItemTypeSpawnEgg;
import com.degoos.wetsponge.material.itemType.WSItemTypeWrittenBook;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.BrickType;
import org.spongepowered.api.data.type.BrickTypes;
import org.spongepowered.api.data.type.CoalType;
import org.spongepowered.api.data.type.CoalTypes;
import org.spongepowered.api.data.type.DirtType;
import org.spongepowered.api.data.type.DirtTypes;
import org.spongepowered.api.data.type.DisguisedBlockType;
import org.spongepowered.api.data.type.DisguisedBlockTypes;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.GoldenApple;
import org.spongepowered.api.data.type.GoldenApples;
import org.spongepowered.api.data.type.Hinges;
import org.spongepowered.api.data.type.LogAxes;
import org.spongepowered.api.data.type.LogAxis;
import org.spongepowered.api.data.type.PlantType;
import org.spongepowered.api.data.type.PlantTypes;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.data.type.PrismarineType;
import org.spongepowered.api.data.type.PrismarineTypes;
import org.spongepowered.api.data.type.SandType;
import org.spongepowered.api.data.type.SandTypes;
import org.spongepowered.api.data.type.SandstoneType;
import org.spongepowered.api.data.type.SandstoneTypes;
import org.spongepowered.api.data.type.ShrubType;
import org.spongepowered.api.data.type.ShrubTypes;
import org.spongepowered.api.data.type.SkullType;
import org.spongepowered.api.data.type.StoneType;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.data.type.WallTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.FireworkEffect;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Axis;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.util.Direction;

public class SpongeDataValueConverter {

	public static void refresh(WSMaterial material, ValueContainer<?> valueContainer) {
		if (material instanceof WSBlockTypeStone)
			((WSBlockTypeStone) material).setStoneType(EnumStoneType.getByName(valueContainer.get(Keys.STONE_TYPE).get().getName()).orElse(EnumStoneType.STONE));
		if (material instanceof WSBlockTypeDirt)
			((WSBlockTypeDirt) material).setDirtType(EnumDirtType.getByName(valueContainer.get(Keys.DIRT_TYPE).get().getName()).orElse(EnumDirtType.DIRT));
		if (material instanceof WSBlockTypeWood)
			((WSBlockTypeWood) material).setWoodType(EnumWoodType.getByName(valueContainer.get(Keys.TREE_TYPE).get().getName()).orElse(EnumWoodType.OAK));
		if (material instanceof WSBlockTypeSand)
			((WSBlockTypeSand) material).setSandType(EnumSandType.getByName(valueContainer.get(Keys.SAND_TYPE).get().getName()).orElse(EnumSandType.NORMAL));
		if (material instanceof WSBlockTypeLog && valueContainer instanceof BlockState)
			((WSBlockTypeLog) material).setLogAxis(EnumBlockAxis.getByName(valueContainer.get(Keys.LOG_AXIS).get().getName()).orElse(EnumBlockAxis.Y));
		if (material instanceof WSBlockTypeLeaves) ((WSBlockTypeLeaves) material).setDecay(valueContainer.get(Keys.DECAYABLE).orElse(false));
		if (material instanceof WSBlockTypeSponge) ((WSBlockTypeSponge) material).setWet(valueContainer.get(Keys.IS_WET).orElse(false));
		if (material instanceof WSBlockTypeSandstone) ((WSBlockTypeSandstone) material)
			.setSandstoneType(EnumSandstoneType.getByName(valueContainer.get(Keys.SANDSTONE_TYPE).get().getName()).orElse(EnumSandstoneType.DEFAULT));
		if (material instanceof WSBlockTypeDirection && valueContainer instanceof BlockState)
			((WSBlockTypeDirection) material).setDirection(EnumBlockDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumBlockDirection
				.DOWN));
		if (material instanceof WSBlockTypeDirectionPowered && valueContainer instanceof BlockState)
			((WSBlockTypeDirectionPowered) material).setPowered(valueContainer.get(Keys.EXTENDED).orElse(false));
		if (material instanceof WSBlockTypeTallGrass) ((WSBlockTypeTallGrass) material)
			.setTallGrassType(EnumTallGrassType.getByName(valueContainer.get(Keys.SHRUB_TYPE).get().getName()).orElse(EnumTallGrassType.DEAD_BUSH));
		if (material instanceof WSBlockTypeDyeColor) ((WSBlockTypeDyeColor) material)
			.setDyeColor(EnumDyeColor.getByName(valueContainer.get(Keys.DYE_COLOR).orElse(DyeColors.WHITE).getId()).orElse(EnumDyeColor.WHITE));
		if (material instanceof WSBlockTypeFlower)
			((WSBlockTypeFlower) material).setFlowerType(EnumFlowerType.getByName(valueContainer.get(Keys.PLANT_TYPE).get().getId()).orElse(EnumFlowerType.POPPY));
		if (material instanceof WSBlockTypeRedstoneWire) ((WSBlockTypeRedstoneWire) material).setPower(valueContainer.get(Keys.POWER).orElse(0));
		if (material instanceof WSBlockTypeTorch && valueContainer instanceof BlockState)
			((WSBlockTypeTorch) material).setTorchDirection(EnumTorchDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumTorchDirection.UP));
		if (material instanceof WSBlockTypeStair && valueContainer instanceof BlockState)
			((WSBlockTypeStair) material).setStairDirection(EnumStairDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumStairDirection
				.EAST))
				.setUpside(valueContainer.get(Keys.PORTION_TYPE).get().getName().equals("TOP"));
		if (material instanceof WSBlockTypeGrowable && valueContainer instanceof BlockState)
			((WSBlockTypeGrowable) material).setGrowthStage(valueContainer.get(Keys.GROWTH_STAGE).orElse(0));
		if (material instanceof WSBlockTypeFarmLand && valueContainer instanceof BlockState)
			((WSBlockTypeFarmLand) material).setMoisture(valueContainer.get(Keys.MOISTURE).get());
		if (material instanceof WSBlockTypeDoor) ((WSBlockTypeDoor) material).setOpen(valueContainer.get(Keys.OPEN).orElse(false))
			.setLeftHinge(valueContainer.get(Keys.HINGE_POSITION).map(h -> h == Hinges.LEFT ? true : false).orElse(false))
			.setPowered(valueContainer.get(Keys.POWERED).orElse(false))
			.setDirection(EnumDoorDirection.getByName(valueContainer.get(Keys.DIRECTION).orElse(Direction.EAST).name()).orElse(EnumDoorDirection.EAST));
		if (material instanceof WSBlockTypeOrientation && valueContainer instanceof BlockState) ((WSBlockTypeOrientation) material)
			.setOrientation(EnumBlockOrientation.getBySpongeName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumBlockOrientation.SOUTH));
		if (material instanceof WSBLockTypePowered && valueContainer instanceof BlockState)
			((WSBLockTypePowered) material).setPowered(valueContainer.get(Keys.POWERED).orElse(false));
		if (material instanceof WSBlockTypeLever && valueContainer instanceof BlockState) ((WSBlockTypeLever) material).setDirection(EnumLeverDirection
			.getByAxisAndDirection(EnumBlockAxis.getByName(valueContainer.get(Keys.AXIS).get().name()).orElse(EnumBlockAxis.Y), EnumBlockDirection
				.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumBlockDirection.DOWN)).orElse(EnumLeverDirection.BOTTOM_EAST));
		if (material instanceof WSBlockTypePumpkin && valueContainer instanceof BlockState) ((WSBlockTypePumpkin) material)
			.setDirection(EnumPumpkinDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumPumpkinDirection.SOUTH));
		if (material instanceof WSBlockTypeRepeater && valueContainer instanceof BlockState) ((WSBlockTypeRepeater) material)
			.setDirection(EnumRepeaterDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumRepeaterDirection.NORTH))
			.setDelay(valueContainer.get(Keys.DELAY).orElse(0));
		if (material instanceof WSBlockTypeMonsterEgg) ((WSBlockTypeMonsterEgg) material)
			.setDisguiseType(EnumDisguisedBlockType.getByName(valueContainer.get(Keys.DISGUISED_BLOCK_TYPE).get().getName()).orElse(EnumDisguisedBlockType.STONE));
		if (material instanceof WSBlockTypeStoneBrick) ((WSBlockTypeStoneBrick) material)
			.setStoneBrickType(EnumStoneBrickType.getByName(valueContainer.get(Keys.BRICK_TYPE).get().getName()).orElse(EnumStoneBrickType.DEFAULT));
		//if (material instanceof WSBlockTypeVine && valueContainer instanceof BlockState)
		//    ((WSBlockTypeVine) material).setVineDirection(EnumVineDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumVineDirection
		// .SOUTH));
		if (material instanceof WSBlockTypeFacing && valueContainer instanceof BlockState) ((WSBlockTypeFacing) material)
			.setFacingDirection(EnumBlockFacingDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumBlockFacingDirection.SOUTH));
		if (material instanceof WSBlockTypeFacingPowered && valueContainer instanceof BlockState)
			((WSBlockTypeFacingPowered) material).setPowered(valueContainer.get(Keys.OPEN).orElse(false));
		if (material instanceof WSBlockTypeCobbleStoneWall)
			((WSBlockTypeCobbleStoneWall) material).setMossy(valueContainer.get(Keys.WALL_TYPE).orElse(WallTypes.NORMAL).equals(WallTypes.MOSSY));
		if (material instanceof WSBlockTypeSkull && material instanceof BlockState) ((WSBlockTypeSkull) material)
			.setFacingDirection(EnumSkullFacingDirection.getByName(valueContainer.get(Keys.DIRECTION).get().name()).orElse(EnumSkullFacingDirection.UP));

		//ANVIL DAMAGE NO SUPPORTED BY SPONGE!!!

		if (material instanceof WSBlockTypePrismarine) ((WSBlockTypePrismarine) material)
			.setPrismarineType(EnumPrismarineType.getByName(valueContainer.get(Keys.PRISMARINE_TYPE).get().getName()).orElse(EnumPrismarineType.ROUGH));

		//ITEMS
		if (material instanceof WSItemTypeDurability)
			((WSItemTypeDurability) material).setDurability(valueContainer.get(Keys.ITEM_DURABILITY).orElse(((WSItemTypeDurability) material).getMaxDurability()));
		if (material instanceof WSItemTypeCoal) ((WSItemTypeCoal) material)
			.setCoalType(EnumCoalType.getByName(valueContainer.get(Keys.COAL_TYPE).orElse(CoalTypes.COAL).getName()).orElse(EnumCoalType.COAL));
		if (material instanceof WSItemTypeGoldenApple) ((WSItemTypeGoldenApple) material)
			.setGoldenAppleType(EnumGoldenAppleType.getByName(valueContainer.get(Keys.GOLDEN_APPLE_TYPE).orElse(GoldenApples.GOLDEN_APPLE).getName())
				.orElse(EnumGoldenAppleType.GOLDEN_APPLE));
		if (material instanceof WSItemTypeLeatherArmor) ((WSItemTypeLeatherArmor) material).setColor(WSColor.ofRGB(valueContainer.get(Keys.COLOR).orElse(null).getRgb
			()));
		if (material instanceof WSItemTypeSpawnEgg)
			((WSItemTypeSpawnEgg) material).setEntityType(SpongeEntityParser.getEntityType(valueContainer.get(Keys.SPAWNABLE_ENTITY_TYPE).orElse(null)));
		if (material instanceof WSItemTypeWrittenBook) {
			((WSItemTypeWrittenBook) material)
				.setGeneration(EnumBookGeneration.getByValue(valueContainer.get(Keys.GENERATION).orElse(0)).orElse(EnumBookGeneration.ORIGINAL));
			((WSItemTypeWrittenBook) material).setAuthor(SpongeText.of(valueContainer.get(Keys.BOOK_AUTHOR).orElse(null)));
			((WSItemTypeWrittenBook) material).setTitle(SpongeText.of(valueContainer.get(Keys.DISPLAY_NAME).orElse(null)));
			((WSItemTypeWrittenBook) material)
				.setPages(valueContainer.get(Keys.BOOK_PAGES).orElse(new ArrayList<>()).stream().map(SpongeText::of).collect(Collectors.toList()));
		}
		if (material instanceof WSItemTypeSkull) {
			((WSItemTypeSkull) material).setSkullType(EnumSkullType.getBySpongeName(valueContainer.get(Keys.SKULL_TYPE).orElseThrow(NullPointerException::new).getName())
				.orElseThrow(NullPointerException::new));
			valueContainer.get(Keys.REPRESENTED_PLAYER).ifPresent(profile -> ((WSItemTypeSkull) material).setProfile(new SpongeGameProfile(profile)));
		}
		if (material instanceof WSItemTypeFirework) {
			((WSItemTypeFirework) material)
				.setEffects(valueContainer.get(Keys.FIREWORK_EFFECTS).orElseThrow(NullPointerException::new).stream().map(SpongeFireworkEffect::new)
					.collect(Collectors.toList()));
			//SPONGE DOESN'T SUPPORT FIREWORK POWER!
		}
		if (material instanceof WSItemTypeFireworkCharge) {
			List<FireworkEffect> effects = valueContainer.get(Keys.FIREWORK_EFFECTS).orElseThrow(NullPointerException::new);
			((WSItemTypeFireworkCharge) material).setEffect(effects.isEmpty() ? null : new SpongeFireworkEffect(effects.get(0)));
		}
		if (material instanceof WSItemTypeMap) ((WSItemTypeMap) material).setMapId(((net.minecraft.item.ItemStack) (Object) valueContainer).getItemDamage());
		if (material instanceof WSItemTypeBanner) {
			((WSItemTypeBanner) material).getPatterns().clear();
			NBTTagCompound compound = ((net.minecraft.item.ItemStack) (Object) valueContainer).getTagCompound();
			if (compound == null) return;
			NBTBase base = compound.getTag("BlockEntityTag");
			if (base == null || !(base instanceof NBTTagCompound)) return;
			((WSItemTypeBanner) material)
				.setBaseColor(EnumDyeColor.getByDyeData((byte) ((net.minecraft.item.ItemStack) (Object) valueContainer).getItemDamage()).orElse(EnumDyeColor.BLACK));
			NBTBase patterns = ((NBTTagCompound) base).getTag("Patterns");
			if (patterns == null || !(patterns instanceof NBTTagList)) return;
			for (int i = 0; i < ((NBTTagList) patterns).tagCount(); i++) {
				NBTBase pattern = ((NBTTagList) patterns).get(i);
				if (pattern == null || !(pattern instanceof NBTTagCompound)) return;
				Optional<EnumBannerPatternShape> optional = EnumBannerPatternShape.getByCode(((NBTTagCompound) pattern).getString("Pattern"));
				if (!optional.isPresent()) continue;
				((WSItemTypeBanner) material).getPatterns()
					.add(new WSBannerPattern(optional.get(), EnumDyeColor.getByDyeData((byte) ((NBTTagCompound) pattern).getInteger("Color"))
						.orElse(EnumDyeColor.BLACK)));
			}
		}
	}


	public static BlockState update(WSBlockType material, BlockState blockState) {
		BlockState nbs = blockState;
		if (material instanceof WSBlockTypeStone)
			nbs = nbs.with(Keys.STONE_TYPE, Sponge.getRegistry().getType(StoneType.class, ((WSBlockTypeStone) material).getStoneType().name()).orElse(StoneTypes.STONE))
				.orElse(nbs);
		if (material instanceof WSBlockTypeDirt)
			nbs = nbs.with(Keys.DIRT_TYPE, Sponge.getRegistry().getType(DirtType.class, ((WSBlockTypeDirt) material).getDirtType().name()).orElse(DirtTypes.DIRT))
				.orElse(nbs);
		if (material instanceof WSBlockTypeWood)
			nbs = nbs.with(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, ((WSBlockTypeWood) material).getWoodType().name()).orElse(TreeTypes.OAK))
				.orElse(nbs);
		if (material instanceof WSBlockTypeSand)
			nbs = nbs.with(Keys.SAND_TYPE, Sponge.getRegistry().getType(SandType.class, ((WSBlockTypeSand) material).getSandType().name()).orElse(SandTypes.NORMAL))
				.orElse(nbs);
		if (material instanceof WSBlockTypeLog)
			nbs = nbs.with(Keys.LOG_AXIS, Sponge.getRegistry().getType(LogAxis.class, ((WSBlockTypeLog) material).getLogAxis().name()).orElse(LogAxes.Y)).orElse(nbs);
		if (material instanceof WSBlockTypeLeaves) nbs = nbs.with(Keys.DECAYABLE, ((WSBlockTypeLeaves) material).isDecay()).orElse(nbs);
		if (material instanceof WSBlockTypeSponge) nbs = nbs.with(Keys.IS_WET, ((WSBlockTypeSponge) material).isWet()).orElse(nbs);
		if (material instanceof WSBlockTypeSandstone) nbs = nbs
			.with(Keys.SANDSTONE_TYPE, Sponge.getRegistry().getType(SandstoneType.class, ((WSBlockTypeSandstone) material).getSandstoneType().name())
				.orElse(SandstoneTypes.DEFAULT)).orElse(nbs);
		if (material instanceof WSBlockTypeDirection)
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeDirection) material).getDirection().name())).orElse(nbs);
		if (material instanceof WSBlockTypeDirectionPowered) nbs = nbs.with(Keys.EXTENDED, ((WSBlockTypeDirectionPowered) material).isPowered()).orElse(nbs);
		if (material instanceof WSBlockTypeTallGrass) nbs = nbs
			.with(Keys.SHRUB_TYPE, Sponge.getRegistry().getType(ShrubType.class, ((WSBlockTypeTallGrass) material).getTallGrassType().name())
				.orElse(ShrubTypes.DEAD_BUSH)).orElse(nbs);
		if (material instanceof WSBlockTypeDyeColor) nbs = nbs
			.with(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, ((WSBlockTypeDyeColor) material).getDyeColor().name().toLowerCase())
				.orElse(DyeColors.WHITE)).orElse(nbs);
		if (material instanceof WSBlockTypeFlower)
			nbs = nbs.with(Keys.PLANT_TYPE, Sponge.getRegistry().getType(PlantType.class, ((WSBlockTypeFlower) material).getFlowerType().name()).orElse(PlantTypes
				.POPPY))
				.orElse(nbs);
		if (material instanceof WSBlockTypeRedstoneWire) nbs = nbs.with(Keys.POWER, ((WSBlockTypeRedstoneWire) material).getPower()).orElse(nbs);
		if (material instanceof WSBlockTypeTorch) nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeTorch) material).getTorchDirection().name())).orElse
			(nbs);
		if (material instanceof WSBlockTypeStair) nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeStair) material).getStairDirection().name())).orElse(nbs)
			.with(Keys.PORTION_TYPE, ((WSBlockTypeStair) material).isUpside() ? PortionTypes.TOP : PortionTypes.BOTTOM).orElse(nbs);
		if (material instanceof WSBlockTypeGrowable) nbs = nbs.with(Keys.GROWTH_STAGE, ((WSBlockTypeGrowable) material).getGrowthStage()).orElse(nbs);
		if (material instanceof WSBlockTypeFarmLand) nbs = nbs.with(Keys.MOISTURE, ((WSBlockTypeFarmLand) material).getMoisture()).orElse(nbs);

		if (material instanceof WSBlockTypeDoor) {
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeDoor) material).getDirection().name())).orElse(nbs);
			nbs = nbs.with(Keys.HINGE_POSITION, ((WSBlockTypeDoor) material).isLeftHinge() ? Hinges.LEFT : Hinges.RIGHT).orElse(nbs);
			nbs = nbs.with(Keys.OPEN, ((WSBlockTypeDoor) material).isOpen()).orElse(nbs);
			nbs = nbs.with(Keys.POWERED, ((WSBlockTypeDoor) material).isPowered()).orElse(nbs);
		}

		if (material instanceof WSBlockTypeOrientation)
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeOrientation) material).getOrientation().name())).orElse(nbs);
		if (material instanceof WSBLockTypePowered) nbs = nbs.with(Keys.POWERED, ((WSBlockTypeLever) material).isPowered()).orElse(nbs);
		if (material instanceof WSBlockTypeLever)
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeLever) material).getDirection().getDirection().name())).orElse(nbs)
				.with(Keys.AXIS, Axis.valueOf(((WSBlockTypeLever) material).getDirection().getAxis().name())).orElse(nbs);
		if (material instanceof WSBlockTypePumpkin) nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypePumpkin) material).getDirection().name())).orElse(nbs);
		if (material instanceof WSBlockTypeRepeater) nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeRepeater) material).getDirection().name())).orElse
			(nbs)
			.with(Keys.DELAY, ((WSBlockTypeRepeater) material).getDelay()).orElse(nbs);
		if (material instanceof WSBlockTypeMonsterEgg) nbs = nbs
			.with(Keys.DISGUISED_BLOCK_TYPE, Sponge.getRegistry().getType(DisguisedBlockType.class, ((WSBlockTypeMonsterEgg) material).getDisguiseType().name())
				.orElse(DisguisedBlockTypes.STONE)).orElse(nbs);
		if (material instanceof WSBlockTypeStoneBrick) nbs = nbs
			.with(Keys.BRICK_TYPE, Sponge.getRegistry().getType(BrickType.class, ((WSBlockTypeStoneBrick) material).getStoneBrickType().name())
				.orElse(BrickTypes.DEFAULT)).orElse(nbs);
		if (material instanceof WSBlockTypeVine) nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeVine) material).getVineDirection().name())).orElse(nbs);
		if (material instanceof WSBlockTypeFacing)
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeFacing) material).getFacingDirection().name())).orElse(nbs);
		if (material instanceof WSBlockTypeFacingPowered) nbs = nbs.with(Keys.OPEN, ((WSBlockTypeFacingPowered) material).isPowered()).orElse(nbs);
		if (material instanceof WSBlockTypeCobbleStoneWall)
			nbs = nbs.with(Keys.WALL_TYPE, ((WSBlockTypeCobbleStoneWall) material).isMossy() ? WallTypes.MOSSY : WallTypes.NORMAL).orElse(nbs);
		if (material instanceof WSBlockTypeSkull)
			nbs = nbs.with(Keys.DIRECTION, Direction.valueOf(((WSBlockTypeSkull) material).getFacingDirection().name())).orElse(nbs);

		//ANVIL DAMAGE NO SUPPORTED BY SPONGE!!!

		if (material instanceof WSBlockTypePrismarine) nbs = nbs
			.with(Keys.PRISMARINE_TYPE, Sponge.getRegistry().getType(PrismarineType.class, ((WSBlockTypePrismarine) material).getPrismarineType().name())
				.orElse(PrismarineTypes.ROUGH)).orElse(nbs);

		return nbs;
	}


	public static void update(WSMaterial material, ItemStack itemStack) {
		if (material instanceof WSBlockTypeStone)
			itemStack.offer(Keys.STONE_TYPE, Sponge.getRegistry().getType(StoneType.class, ((WSBlockTypeStone) material).getStoneType().name()).orElse(StoneTypes
				.STONE));
		if (material instanceof WSBlockTypeDirt)
			itemStack.offer(Keys.DIRT_TYPE, Sponge.getRegistry().getType(DirtType.class, ((WSBlockTypeDirt) material).getDirtType().name()).orElse(DirtTypes.DIRT));
		if (material instanceof WSBlockTypeWood)
			itemStack.offer(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, ((WSBlockTypeWood) material).getWoodType().name()).orElse(TreeTypes.OAK));
		if (material instanceof WSBlockTypeSand)
			itemStack.offer(Keys.SAND_TYPE, Sponge.getRegistry().getType(SandType.class, ((WSBlockTypeSand) material).getSandType().name()).orElse(SandTypes.NORMAL));
		//LOG AXIS not supported.
		if (material instanceof WSBlockTypeLeaves) itemStack.offer(Keys.DECAYABLE, ((WSBlockTypeLeaves) material).isDecay());
		if (material instanceof WSBlockTypeSponge) itemStack.offer(Keys.IS_WET, ((WSBlockTypeSponge) material).isWet());
		if (material instanceof WSBlockTypeSandstone) itemStack
			.offer(Keys.SANDSTONE_TYPE, Sponge.getRegistry().getType(SandstoneType.class, ((WSBlockTypeSandstone) material).getSandstoneType().name())
				.orElse(SandstoneTypes.DEFAULT));
		//DIRECTION and PISTON not supported.
		if (material instanceof WSBlockTypeTallGrass) itemStack
			.offer(Keys.SHRUB_TYPE, Sponge.getRegistry().getType(ShrubType.class, ((WSBlockTypeTallGrass) material).getTallGrassType().name())
				.orElse(ShrubTypes.DEAD_BUSH));
		if (material instanceof WSBlockTypeDyeColor) itemStack
			.offer(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, ((WSBlockTypeDyeColor) material).getDyeColor().name().toLowerCase())
				.orElse(DyeColors.WHITE));
		if (material instanceof WSBlockTypeFlower) itemStack
			.offer(Keys.PLANT_TYPE, Sponge.getRegistry().getType(PlantType.class, ((WSBlockTypeFlower) material).getFlowerType().name()).orElse(PlantTypes.POPPY));
		//TORCH DIRECTION, STAIR DIRECTION, GROWTH LEVEL, MOISTURE, ORIENTATION, LEVER DIRECTION, POWERED, PUMPKIN DIRECTION and LEVER DIRECTION not supported.
		if (material instanceof WSBlockTypeMonsterEgg) itemStack
			.offer(Keys.DISGUISED_BLOCK_TYPE, Sponge.getRegistry().getType(DisguisedBlockType.class, ((WSBlockTypeMonsterEgg) material).getDisguiseType().name())
				.orElse(DisguisedBlockTypes.STONE));
		if (material instanceof WSBlockTypeStoneBrick) itemStack
			.offer(Keys.BRICK_TYPE, Sponge.getRegistry().getType(BrickType.class, ((WSBlockTypeStoneBrick) material).getStoneBrickType().name())
				.orElse(BrickTypes.DEFAULT));
		//VINE DIRECTION and FENCE GATE DIRECTION not supported.
		if (material instanceof WSBlockTypeCobbleStoneWall)
			itemStack.offer(Keys.WALL_TYPE, ((WSBlockTypeCobbleStoneWall) material).isMossy() ? WallTypes.MOSSY : WallTypes.NORMAL);
		//SKULL DIRECTION not supported.
		//ANVIL DAMAGE NO SUPPORTED BY SPONGE!!!
		if (material instanceof WSBlockTypePrismarine) itemStack
			.offer(Keys.PRISMARINE_TYPE, Sponge.getRegistry().getType(PrismarineType.class, ((WSBlockTypePrismarine) material).getPrismarineType().name())
				.orElse(PrismarineTypes.ROUGH));

		//ITEMS
		if (material instanceof WSItemTypeDurability) itemStack.offer(Keys.ITEM_DURABILITY, ((WSItemTypeDurability) material).getDurability());
		if (material instanceof WSItemTypeCoal)
			itemStack.offer(Keys.COAL_TYPE, Sponge.getRegistry().getType(CoalType.class, ((WSItemTypeCoal) material).getCoalType().name()).orElse(CoalTypes.COAL));
		if (material instanceof WSItemTypeGoldenApple) itemStack
			.offer(Keys.GOLDEN_APPLE_TYPE, Sponge.getRegistry().getType(GoldenApple.class, ((WSItemTypeGoldenApple) material).getGoldenAppleType().name())
				.orElse(GoldenApples.GOLDEN_APPLE));
		if (material instanceof WSItemTypeLeatherArmor) {
			if (((WSItemTypeLeatherArmor) material).getColor().isPresent())
				itemStack.offer(Keys.COLOR, Color.ofRgb(((WSItemTypeLeatherArmor) material).getColor().get().toRGB()));
			else itemStack.remove(Keys.COLOR);
		}
		if (material instanceof WSItemTypeSpawnEgg)
			itemStack.offer(Keys.SPAWNABLE_ENTITY_TYPE, SpongeEntityParser.getSuperEntityType(((WSItemTypeSpawnEgg) material).getEntityType()));
		if (material instanceof WSItemTypeWrittenBook) {
			itemStack.offer(Keys.BOOK_AUTHOR, ((SpongeText) ((WSItemTypeWrittenBook) material).getAuthor()).getHandled());
			itemStack.offer(Keys.BOOK_PAGES, ((WSItemTypeWrittenBook) material).getPages().stream().map(text -> ((SpongeText) text).getHandled())
				.collect(Collectors.toList()));
			itemStack.offer(Keys.DISPLAY_NAME, ((SpongeText) ((WSItemTypeWrittenBook) material).getTitle()).getHandled());
			itemStack.offer(Keys.GENERATION, ((WSItemTypeWrittenBook) material).getGeneration().getValue());
		}
		if (material instanceof WSItemTypeSkull) {
			itemStack.offer(Keys.SKULL_TYPE, Sponge.getRegistry()
				.getType(SkullType.class, ((WSItemTypeSkull) material).getSkullType().getSpongeName()).<NullPointerException>orElseThrow(NullPointerException::new));
			WSGameProfile profile = ((WSItemTypeSkull) material).getProfile().orElse(null);
			itemStack.offer(Keys.REPRESENTED_PLAYER, profile == null ? null : ((SpongeGameProfile) profile).getHandled());
		}
		if (material instanceof WSItemTypeFirework) {
			itemStack.offer(Keys.FIREWORK_EFFECTS, ((WSItemTypeFirework) material).getEffects().stream().map(effect -> ((SpongeFireworkEffect) effect).getHandled())
				.collect(Collectors.toList()));
			//SPONGE DOESN'T SUPPORT FIREWORK POWER!
		}
		if (material instanceof WSItemTypeFireworkCharge) itemStack.offer(Keys.FIREWORK_EFFECTS, ((WSItemTypeFireworkCharge) material).getEffect()
			.map(effect -> Collections.singletonList(((SpongeFireworkEffect) effect).getHandled())).orElse(new ArrayList<>()));
		if (material instanceof WSItemTypeMap) ((net.minecraft.item.ItemStack) (Object) itemStack).setItemDamage(((WSItemTypeMap) material).getMapId());
		if (material instanceof WSItemTypeBanner) {
			((net.minecraft.item.ItemStack) (Object) itemStack).setItemDamage(((WSItemTypeBanner) material).getBaseColor().getDyeData());
			NBTTagCompound compound = ((net.minecraft.item.ItemStack) (Object) itemStack).getTagCompound();
			if (compound == null) compound = new NBTTagCompound();
			NBTTagCompound banner = new NBTTagCompound();
			banner.setInteger("Base", ((WSItemTypeBanner) material).getBaseColor().getDyeData());

			NBTTagList patterns = new NBTTagList();

			((WSItemTypeBanner) material).getPatterns().forEach(pattern -> {
				NBTTagCompound tag = new NBTTagCompound();
				tag.setInteger("Color", pattern.getColor().getDyeData());
				tag.setString("Pattern", pattern.getShape().getCode());
				patterns.appendTag(tag);
			});

			banner.setTag("Patterns", patterns);

			compound.setTag("BlockEntityTag", banner);

			((net.minecraft.item.ItemStack) (Object) itemStack).setTagCompound(compound);
		}
	}

}
