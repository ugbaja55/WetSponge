package com.degoos.wetsponge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;
import com.degoos.wetsponge.enums.item.EnumBookGeneration;
import com.degoos.wetsponge.firework.SpigotFireworkEffect;
import com.degoos.wetsponge.material.itemType.WSItemTypeBanner;
import com.degoos.wetsponge.material.itemType.WSItemTypeFirework;
import com.degoos.wetsponge.material.itemType.WSItemTypeFireworkCharge;
import com.degoos.wetsponge.material.itemType.WSItemTypeLeatherArmor;
import com.degoos.wetsponge.material.itemType.WSItemTypeSkull;
import com.degoos.wetsponge.material.itemType.WSItemTypeSpawnEgg;
import com.degoos.wetsponge.material.itemType.WSItemTypeWrittenBook;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.mojang.authlib.GameProfile;
import java.util.Optional;
import java.util.stream.Collectors;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SpigotItemMetaConverter {

	public static void refresh(ItemMeta itemMeta, WSMaterial material) {
		if (itemMeta instanceof LeatherArmorMeta && material instanceof WSItemTypeLeatherArmor) {
			Color color = ((LeatherArmorMeta) itemMeta).getColor();
			((WSItemTypeLeatherArmor) material).setColor(color == null ? null : new WSColor(color.getRed(), color.getGreen(), color.getBlue()));
		}
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) && itemMeta instanceof org.bukkit.inventory.meta.SpawnEggMeta &&
			material instanceof WSItemTypeSpawnEgg)
			((org.bukkit.inventory.meta.SpawnEggMeta) itemMeta).setSpawnedType(SpigotEntityParser.getSuperEntityType(((WSItemTypeSpawnEgg) material).getEntityType()));
		if (itemMeta instanceof BookMeta && material instanceof WSItemTypeWrittenBook) {
			((WSItemTypeWrittenBook) material).setAuthor(WSText.getByFormattingText(((BookMeta) itemMeta).getAuthor()));
			((WSItemTypeWrittenBook) material).setTitle(WSText.getByFormattingText(((BookMeta) itemMeta).getTitle()));
			((WSItemTypeWrittenBook) material).setPages(((BookMeta) itemMeta).getPages().stream().map(WSText::getByFormattingText).collect(Collectors.toList()));
			((WSItemTypeWrittenBook) material)
				.setGeneration(EnumBookGeneration.getByName(((BookMeta) itemMeta).getGeneration().name()).orElse(EnumBookGeneration.ORIGINAL));
		}
		if (itemMeta instanceof SkullMeta && material instanceof WSItemTypeSkull) {
			GameProfile profile = SpigotSkullBuilder.getGameProfile(itemMeta);
			((WSItemTypeSkull) material).setProfile(profile == null ? null : new SpigotGameProfile(profile));
		}
		if (itemMeta instanceof FireworkMeta && material instanceof WSItemTypeFirework) {
			((WSItemTypeFirework) material).setEffects(((FireworkMeta) itemMeta).getEffects().stream().map(SpigotFireworkEffect::new).collect(Collectors.toList()));
			((WSItemTypeFirework) material).setPower(((FireworkMeta) itemMeta).getPower());
		}
		if (itemMeta instanceof FireworkEffectMeta && material instanceof WSItemTypeFireworkCharge) ((WSItemTypeFireworkCharge) material)
			.setEffect(((FireworkEffectMeta) itemMeta).getEffect() == null ? null : new SpigotFireworkEffect(((FireworkEffectMeta) itemMeta).getEffect()));
		if (itemMeta instanceof BannerMeta && material instanceof WSItemTypeBanner) {
			((WSItemTypeBanner) material).getPatterns().clear();
			((WSItemTypeBanner) material).getPatterns().addAll(((BannerMeta) itemMeta).getPatterns().stream()
				.map(pattern -> new WSBannerPattern(EnumBannerPatternShape.getByName(pattern.getPattern().name()).orElse(EnumBannerPatternShape.BASE), EnumDyeColor
					.getByWoolData(pattern.getColor().getWoolData()).orElse(EnumDyeColor.BLACK))).collect(Collectors.toSet()));
		}
	}

	public static void update(ItemMeta itemMeta, WSMaterial material) {
		if (itemMeta instanceof LeatherArmorMeta && material instanceof WSItemTypeLeatherArmor) ((LeatherArmorMeta) itemMeta)
			.setColor(((WSItemTypeLeatherArmor) material).getColor().map(color -> Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue())).orElse(null));
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) && itemMeta instanceof org.bukkit.inventory.meta.SpawnEggMeta &&
			material instanceof WSItemTypeSpawnEgg)
			((WSItemTypeSpawnEgg) material).setEntityType(SpigotEntityParser.getEntityType(((org.bukkit.inventory.meta.SpawnEggMeta) itemMeta).getSpawnedType()));
		if (itemMeta instanceof BookMeta && material instanceof WSItemTypeWrittenBook) {
			((BookMeta) itemMeta).setAuthor(((WSItemTypeWrittenBook) material).getAuthor().toFormattingText());
			((BookMeta) itemMeta).setTitle(((WSItemTypeWrittenBook) material).getTitle().toFormattingText());
			((BookMeta) itemMeta).setPages(((WSItemTypeWrittenBook) material).getPages().stream().map(WSText::toFormattingText).collect(Collectors.toList()));
			((BookMeta) itemMeta).setGeneration(BookMeta.Generation.valueOf(((WSItemTypeWrittenBook) material).getGeneration().name()));
		}
		if (itemMeta instanceof SkullMeta && material instanceof WSItemTypeSkull) {
			Optional<WSGameProfile> profile = ((WSItemTypeSkull) material).getProfile();
			SpigotSkullBuilder.injectGameProfile(itemMeta, profile.isPresent() ? ((SpigotGameProfile) profile.get()).getHandled() : null);
		}
		if (itemMeta instanceof FireworkMeta && material instanceof WSItemTypeFirework) {
			((FireworkMeta) itemMeta).setPower(((WSItemTypeFirework) material).getPower());
			((FireworkMeta) itemMeta).clearEffects();
			((FireworkMeta) itemMeta).addEffects(((WSItemTypeFirework) material).getEffects().stream().map(effect -> ((SpigotFireworkEffect) effect).getHandled())
				.collect(Collectors.toList()));
		}
		if (itemMeta instanceof FireworkEffectMeta && material instanceof WSItemTypeFireworkCharge) ((FireworkEffectMeta) itemMeta)
			.setEffect(((WSItemTypeFireworkCharge) material).getEffect().map(effect -> ((SpigotFireworkEffect) effect).getHandled()).orElse(null));
		if (itemMeta instanceof BannerMeta && material instanceof WSItemTypeBanner) {
			((BannerMeta) itemMeta).setBaseColor(DyeColor.valueOf(((WSItemTypeBanner) material).getBaseColor().name()));
			while (((BannerMeta) itemMeta).numberOfPatterns() > 0) ((BannerMeta) itemMeta).removePattern(0);
			((WSItemTypeBanner) material).getPatterns().forEach(pattern -> ((BannerMeta) itemMeta)
				.addPattern(new Pattern(DyeColor.valueOf(pattern.getColor().name()), PatternType.valueOf(pattern.getShape().name()))));
		}
	}

}
