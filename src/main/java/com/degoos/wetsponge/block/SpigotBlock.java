package com.degoos.wetsponge.block;


import com.degoos.wetsponge.block.tileentity.SpigotTileEntity;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityBrewingStand;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityChest;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityCommandBlock;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityDispenser;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityFurnace;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityJukebox;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityMonsterSpawner;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityNameable;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityNameableInventory;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntityNoteblock;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntitySign;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntitySkull;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.MapUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import java.util.Optional;
import org.bukkit.block.Block;

public class SpigotBlock implements WSBlock {

	private Block block;


	public SpigotBlock(Block block) {
		this.block = block;
	}


	@Override
	public WSLocation getLocation() {
		return new SpigotLocation(block.getLocation());
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(block.getWorld().getName(), block.getWorld());
	}


	@Override
	public int getId() {
		return block.getTypeId();
	}


	@Override
	public String getStringId() {
		Optional<WSBlockType> optional = WSBlockTypes.getType(block.getType().getId(), 0);
		return optional.map(WSBlockType::getStringId).orElse("minecraft:air");
	}


	@Override
	public WSBlockState createState() {
		return new SpigotBlockState(this);
	}


	@Override
	public SpigotTileEntity getTileEntity() {
		switch (getId()) {
			case 23:
				return new SpigotTileEntityDispenser(this);
			case 25:
				return new SpigotTileEntityNoteblock(this);
			case 52:
				return new SpigotTileEntityMonsterSpawner(this);
			case 54:
			case 146:
				return new SpigotTileEntityChest(this);
			case 61:
			case 62:
				return new SpigotTileEntityFurnace(this);
			case 63:
			case 68:
				return new SpigotTileEntitySign(this);
			case 84:
				return new SpigotTileEntityJukebox(this);
			case 116:
				return new SpigotTileEntityNameable(this);
			case 117:
				return new SpigotTileEntityBrewingStand(this);
			case 137:
				return new SpigotTileEntityCommandBlock(this);
			case 144:
				return new SpigotTileEntitySkull(this);
			case 154:
				return new SpigotTileEntityNameableInventory(this);
			default:
				return new SpigotTileEntity(this);
		}
	}

	@Override
	public WSBlockType getBlockType() {
		return WSBlockTypes.getType(getId(), getHandled().getData()).orElse(WSBlockTypes.AIR.getDefaultType());
	}

	@Override
	public WSBlock getRelative(EnumBlockDirection direction) {
		return getLocation().add(direction.getRelative().toDouble()).getBlock();
	}

	@Override
	public EnumMapBaseColor getMapBaseColor() {
		Object world = HandledUtils.getWorldHandle(block.getWorld());
		Object blockPosition = HandledUtils.getBlockPosition(block.getLocation());
		try {
			Object iBlockData = ReflectionUtils.invokeMethod(world, "getType", blockPosition);
			return MapUtils.getMapBaseColor(iBlockData, world, blockPosition, getId());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the map base color of the block " + getStringId() + "!");
			return EnumMapBaseColor.AIR;
		}
	}

	@Override
	public Block getHandled() {
		return block;
	}

}
