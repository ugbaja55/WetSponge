package com.degoos.wetsponge.block;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.packet.play.server.WSSPacketBlockChange;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.event.cause.Cause;

public class SpongeBlockState implements WSBlockState {

    private WSLocation location;
    private WSBlockType blockType;
    private SpongeBlock block;


    public SpongeBlockState(SpongeBlock block) {
        this.location = block.getLocation();
        this.block = block;
        refresh();
    }

    public WSLocation getLocation() {
        return location.clone();
    }


    @Override
    public WSWorld getWorld() {
        return location.getWorld();
    }


    public WSBlock getBlock() {
        return block;
    }


    public WSBlockType getBlockType() {
        return blockType;
    }


    public SpongeBlockState setBlockType(WSBlockType blockType) {
        this.blockType = blockType;
        return this;
    }


    public void update() {
        update(true);
    }


    public void update(boolean applyPhysics) {
        block.getHandled()
                .setBlock(SpongeDataValueConverter.update(blockType,
                        Sponge.getRegistry().getType(BlockType.class, blockType.getStringId()).get().getDefaultState()));
        getWorld().getPlayers().forEach(player -> player.sendPacket(WSSPacketBlockChange.of(blockType, location.toVector3i())));
    }


    @Override
    public void refresh() {
        this.blockType = WSBlockTypes.getType(block.getStringId()).orElse(WSBlockTypes.AIR.getDefaultType());
        SpongeDataValueConverter.refresh(blockType, block.getHandled().getBlock());
    }

}
