package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.blockType.WSBlockTypeDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import org.bukkit.block.Block;

public class SpigotBlockSnapshot extends WSBlockSnapshot {


    public SpigotBlockSnapshot(Block block) {
        super(WSBlockTypes.getType(block.getTypeId(), block.getData()).orElse(
                new WSBlockTypeDataValuable(block.getTypeId(), "", block.getType().getMaxStackSize(), block.getData())));
    }
}
