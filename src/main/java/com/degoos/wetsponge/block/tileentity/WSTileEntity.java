package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.WSBlock;

public interface WSTileEntity {

	WSBlock getBlock();

	Object getHandled();

	void setNBTTag(String nbtTag);

	String getNBTTag();
}
