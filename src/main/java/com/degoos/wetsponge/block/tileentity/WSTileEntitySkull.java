package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.enums.block.EnumBlockOrientation;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.user.WSGameProfile;

import java.net.URL;

public interface WSTileEntitySkull extends WSTileEntity {

    WSGameProfile getGameProfile();

    void setGameProfile(WSGameProfile gameProfile);

    EnumBlockOrientation getOrientation();

    void setOrientation(EnumBlockOrientation orientation);

    EnumSkullType getSkullType();

    void setSkullType(EnumSkullType skullType);

    void setTexture(String texture);

    void setTexture(URL texture);

    void setTextureByPlayerName(String name);

    void findFormatAndSetTexture(String texture);

}
