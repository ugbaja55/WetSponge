package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.text.WSText;
import org.bukkit.Nameable;

public class SpigotTileEntityNameable extends SpigotTileEntity implements WSTileEntityNameable {


    public SpigotTileEntityNameable(SpigotBlock block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return WSText.getByFormattingText(getHandled().getCustomName());
    }

    @Override
    public void setCustomName(WSText customName) {
        if (customName == null) getHandled().setCustomName(null);
        else getHandled().setCustomName(customName.toFormattingText());
        update();
    }

    @Override
    public Nameable getHandled() {
        return (Nameable) super.getHandled();
    }

}
