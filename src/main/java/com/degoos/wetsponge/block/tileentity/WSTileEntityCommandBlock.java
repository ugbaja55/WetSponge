package com.degoos.wetsponge.block.tileentity;


public interface WSTileEntityCommandBlock extends WSTileEntityNameable {

    String getCommand();

    void setCommand(String command);

}
