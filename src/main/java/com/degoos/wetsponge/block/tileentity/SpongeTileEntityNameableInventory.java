package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.SpongeText;
import org.spongepowered.api.block.tileentity.carrier.TileEntityCarrier;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.text.Text;

public class SpongeTileEntityNameableInventory extends SpongeTileEntityInventory implements WSTileEntityNameableInventory {

    public SpongeTileEntityNameableInventory(SpongeBlock block) {
        super(block);
    }

    @Override
    public WSText getCustomName() {
        return SpongeText.of(getHandled().get(Keys.DISPLAY_NAME).orElse(Text.EMPTY));
    }

    @Override
    public void setCustomName(WSText customName) {
        getHandled().offer(Keys.DISPLAY_NAME, ((SpongeText) customName).getHandled());
    }

    @Override
    public TileEntityCarrier getHandled() {
        return super.getHandled();
    }

}
