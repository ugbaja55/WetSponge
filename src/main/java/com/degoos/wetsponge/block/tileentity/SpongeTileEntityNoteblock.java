package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.enums.EnumNotePitch;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.Note;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.NotePitch;

public class SpongeTileEntityNoteblock extends SpongeTileEntity implements WSTileEntityNoteblock {

	public SpongeTileEntityNoteblock (SpongeBlock block) {
		super(block);
	}


	@Override
	public void playNote () {
		getHandled().playNote();
	}


	@Override
	public EnumNotePitch getNote () {
		return EnumNotePitch.getByName(getHandled().note().get().getId()).orElse(EnumNotePitch.F_SHARP0);
	}


	@Override
	public void setNote (EnumNotePitch note) {
		Validate.notNull(note, "Note cannot be null!");
		Sponge.getRegistry().getType(NotePitch.class, note.name()).ifPresent(notePitch -> getHandled().offer(Keys.NOTE_PITCH, notePitch));
	}


	@Override
	public Note getHandled () {
		return (Note) super.getHandled();
	}
}
