package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.tileentity.MobSpawner;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;

public class SpongeTileEntityMonsterSpawner extends SpongeTileEntity implements WSTileEntityMonsterSpawner {

    public SpongeTileEntityMonsterSpawner(SpongeBlock block) {
        super(block);
    }

    @Override
    public EnumEntityType getEntity() {
        return SpongeEntityParser.getEntityType(getHandled().get(Keys.SPAWNABLE_ENTITY_TYPE).orElse(EntityTypes.PIG));
    }

    @Override
    public void setEntity(EnumEntityType type) {
        getHandled().offer(Keys.SPAWNABLE_ENTITY_TYPE, Sponge.getRegistry().getType(EntityType.class, type.getName()).orElse(EntityTypes.PIG));
    }

    @Override
    public MobSpawner getHandled() {
        return (MobSpawner) super.getHandled();
    }

}
