package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.inventory.WSInventory;
import java.util.Optional;
import java.util.Set;

public interface WSTileEntityChest extends WSTileEntityInventory {

	WSInventory getSoloInventory();

	Optional<WSInventory> getDoubleChestInventory();

	Set<WSTileEntityChest> getConnectedChests();

}
