package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.CommandBlock;

public class SpigotTileEntityCommandBlock extends SpigotTileEntity implements WSTileEntityCommandBlock {

    public SpigotTileEntityCommandBlock(SpigotBlock block) {
        super(block);
    }

    @Override
    public String getCommand() {
        return getHandled().getCommand();
    }

    @Override
    public void setCommand(String command) {
        Validate.notNull(command, "Command cannot be null!");
        getHandled().setCommand(command);
        update();
    }

    @Override
    public WSText getCustomName() {
        return WSText.of(getHandled().getName());
    }

    @Override
    public void setCustomName(WSText customName) {
        if (customName == null) getHandled().setName(null);
        else getHandled().setName(customName.toFormattingText());
        update();
    }

    @Override
    public CommandBlock getHandled() {
        return (CommandBlock) super.getHandled();
    }

}
