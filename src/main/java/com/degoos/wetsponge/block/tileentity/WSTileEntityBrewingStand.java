package com.degoos.wetsponge.block.tileentity;

public interface WSTileEntityBrewingStand extends WSTileEntityNameableInventory {

    int getFuelLevel();

    void setFuelLevel(int fuelLevel);

    int getBrewingTime();

    void setBrewingTime(int brewingTime);

}
