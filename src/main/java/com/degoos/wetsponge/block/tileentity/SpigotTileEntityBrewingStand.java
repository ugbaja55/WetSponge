package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpigotBlock;
import org.bukkit.block.BrewingStand;

public class SpigotTileEntityBrewingStand extends SpigotTileEntityNameableInventory implements WSTileEntityBrewingStand {


    public SpigotTileEntityBrewingStand(SpigotBlock block) {
        super(block);
    }

    @Override
    public int getFuelLevel() {
        return getHandled().getFuelLevel();
    }

    @Override
    public void setFuelLevel(int fuelLevel) {
        getHandled().setFuelLevel(fuelLevel);
        update();
    }

    @Override
    public int getBrewingTime() {
        return getHandled().getBrewingTime();
    }

    @Override
    public void setBrewingTime(int brewingTime) {
        getHandled().setBrewingTime(brewingTime);
        update();
    }


    @Override
    public BrewingStand getHandled() {
        return (BrewingStand) super.getHandled();
    }
}
