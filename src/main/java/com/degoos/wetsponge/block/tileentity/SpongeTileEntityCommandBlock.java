package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.tileentity.CommandBlock;
import org.spongepowered.api.data.key.Keys;

public class SpongeTileEntityCommandBlock extends SpongeTileEntityNameable implements WSTileEntityCommandBlock {


    public SpongeTileEntityCommandBlock(SpongeBlock block) {
        super(block);
    }

    @Override
    public String getCommand() {
        return getHandled().get(Keys.COMMAND).orElse("");
    }

    @Override
    public void setCommand(String command) {
        Validate.notNull(command, "Command cannot be null!");
        getHandled().offer(Keys.COMMAND, command);
    }


    @Override
    public CommandBlock getHandled() {
        return (CommandBlock) super.getHandled();
    }
}
