package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.enums.block.EnumBlockOrientation;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.SkullType;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;

import java.net.URL;

public class SpigotTileEntitySkull extends SpigotTileEntity implements WSTileEntitySkull {


    public SpigotTileEntitySkull(SpigotBlock block) {
        super(block);
    }

    @Override
    public WSGameProfile getGameProfile() {
        Skull skull = getHandled();
        return WSGameProfile.of(skull.getOwningPlayer().getUniqueId(), skull.getOwningPlayer().getName());
    }

    @Override
    public void setGameProfile(WSGameProfile gameProfile) {
        GameProfile handledProfile = ((SpigotGameProfile) gameProfile).getHandled();
        getHandled().setOwningPlayer(Bukkit.getOfflinePlayer(handledProfile.getId()));
        SpigotSkullBuilder.injectGameProfile(getHandled(), ((SpigotGameProfile) gameProfile).getHandled());
        update();
    }

    @Override
    public EnumBlockOrientation getOrientation() {
        return EnumBlockOrientation.getBySpigotName(getHandled().getRotation().name()).orElse(EnumBlockOrientation.SOUTH);
    }

    @Override
    public void setOrientation(EnumBlockOrientation orientation) {
        getHandled().setRotation(BlockFace.valueOf(orientation.getSpigotName()));
        update();
    }

    @Override
    public EnumSkullType getSkullType() {
        return EnumSkullType.getBySpigotName(getHandled().getSkullType().name()).orElse(EnumSkullType.SKELETON);
    }

    @Override
    public void setSkullType(EnumSkullType skullType) {
        getHandled().setSkullType(SkullType.valueOf(skullType.getSpigotName()));
        update();
    }

    @Override
    public void setTexture(String texture) {
        SpigotSkullBuilder.updateSkullByTexture(getHandled(), texture);
        update();
    }

    @Override
    public void setTexture(URL texture) {
        SpigotSkullBuilder.updateSkullByURL(getHandled(), texture);
        update();
    }

    @Override
    public void setTextureByPlayerName(String name) {
        SpigotSkullBuilder.updateSkullByPlayerName(getHandled(), name);
        update();
    }

    @Override
    public void findFormatAndSetTexture(String texture) {
        SpigotSkullBuilder.updateSkullByUnknownFormat(getHandled(), texture);
        update();
    }

    @Override
    public Skull getHandled() {
        return (Skull) super.getHandled();
    }
}
