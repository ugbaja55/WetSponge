package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.enums.EnumEntityType;

public interface WSTileEntityMonsterSpawner extends WSTileEntity {

	EnumEntityType getEntity ();

	void setEntity (EnumEntityType type);

}
