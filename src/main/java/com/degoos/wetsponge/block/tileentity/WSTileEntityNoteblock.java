package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.enums.EnumNotePitch;

public interface WSTileEntityNoteblock extends WSTileEntity {

    void playNote();

    EnumNotePitch getNote();

    void setNote(EnumNotePitch note);

}
