package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpigotBlock;
import org.bukkit.block.Furnace;

public class SpigotTileEntityFurnace extends SpigotTileEntityInventory implements WSTileEntityFurnace {

    public SpigotTileEntityFurnace(SpigotBlock block) {
        super(block);
    }


    @Override
    public int getBurnTime() {
        return getHandled().getBurnTime();
    }

    @Override
    public void setBurnTime(int burnTime) {
        getHandled().setBurnTime((short) burnTime);
        update();
    }

    @Override
    public int getCookTime() {
        return getHandled().getCookTime();
    }

    @Override
    public void setCookTime(int cookTime) {
        getHandled().setCookTime((short) cookTime);
        update();
    }

    @Override
    public Furnace getHandled() {
        return (Furnace) super.getHandled();
    }

}
