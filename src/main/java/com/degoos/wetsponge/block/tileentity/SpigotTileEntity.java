package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NBTTagUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.block.BlockState;

public class SpigotTileEntity implements WSTileEntity {

	private SpigotBlock block;
	private BlockState blockState;


	public SpigotTileEntity(SpigotBlock block) {
		this.block = block;
		this.blockState = block.getHandled().getState();
	}

	public SpigotTileEntity(BlockState block) {
		this.block = new SpigotBlock(block.getBlock());
		this.blockState = block;
	}

	public void update() {
		blockState.update(true);
	}

	@Override
	public SpigotBlock getBlock() {
		return block;
	}

	@Override
	public void setNBTTag(String nbtTag) {
		try {
			Object nbtTagCompound = NBTTagUtils.parseNBTTag(nbtTag);
			Object tileEntity = ReflectionUtils.invokeMethod(blockState, "getTileEntity");
			ReflectionUtils.invokeMethod(tileEntity, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "load" : "a", nbtTagCompound);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the NBTTag of a tile entity!");
		}
	}

	@Override
	public String getNBTTag() {
		try {
			Object nbtTagCompound = NBTTagUtils.newNBTTagCompound();
			Object tileEntity = ReflectionUtils.invokeMethod(blockState, "getTileEntity");
			ReflectionUtils.invokeMethod(tileEntity, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "save" : "b", nbtTagCompound);
			return nbtTagCompound.toString();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the NBTTag of a tile entity!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return blockState;
	}
}
