package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import com.flowpowered.math.vector.Vector3d;
import com.degoos.wetsponge.entity.projectile.WSProjectile;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import org.spongepowered.api.block.tileentity.carrier.Dispenser;
import org.spongepowered.api.entity.projectile.Projectile;

import java.util.Optional;

public class SpongeTileEntityDispenser extends SpongeTileEntityNameableInventory implements WSTileEntityDispenser {


    public SpongeTileEntityDispenser(SpongeBlock block) {
        super(block);
    }


    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile) {
        return launchProjectile(projectile, new Vector3d(0, 0, 0));
    }

    @Override
    public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity) {
        EnumEntityType type = EnumEntityType.getByClass(projectile).orElse(EnumEntityType.UNKNOWN);
        if (type == EnumEntityType.UNKNOWN) return Optional.empty();
        try {
            Class<? extends Projectile> spongeClass =  (Class<? extends Projectile>) SpongeEntityParser.getEntityData(type).getEntityClass();
            return getHandled().launchProjectile(spongeClass).map(entity -> (T) SpongeEntityParser.getWSEntity(entity));
        } catch (Throwable ex) {
            ex.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Dispenser getHandled() {
        return (Dispenser) super.getHandled();
    }
}
