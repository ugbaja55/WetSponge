package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.util.InternalLogger;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.api.block.tileentity.TileEntity;

public class SpongeTileEntity implements WSTileEntity {

	private SpongeBlock block ;
	private TileEntity tileEntity;


	public SpongeTileEntity(SpongeBlock block) {
		this.block = block;
		this.tileEntity = block.getHandled().getTileEntity().orElse(null);
	}

	public SpongeTileEntity(TileEntity tileEntity) {
		this.tileEntity = tileEntity;
		this.block = new SpongeBlock(tileEntity.getLocation());
	}

	@Override
	public void setNBTTag(String nbtTag) {
		try {
			NBTTagCompound nbtTagCompound = JsonToNBT.getTagFromJson(nbtTag);
			((net.minecraft.tileentity.TileEntity) this.getHandled()).readFromNBT(nbtTagCompound);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the NBTTag of a tile entity!");
		}
	}

	@Override
	public String getNBTTag() {
		try {
			NBTTagCompound nbtTagCompound = new NBTTagCompound();
			((net.minecraft.tileentity.TileEntity) this.getHandled()).writeToNBT(nbtTagCompound);

			return nbtTagCompound.toString();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the NBTTag of a tile entity!");
			return null;
		}
	}


	@Override
	public SpongeBlock getBlock() {
		return block;
	}


	@Override
	public Object getHandled() {
		return tileEntity;
	}
}
