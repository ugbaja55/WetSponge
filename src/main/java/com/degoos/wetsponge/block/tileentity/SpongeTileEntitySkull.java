package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.enums.block.EnumBlockOrientation;
import com.degoos.wetsponge.enums.block.EnumSkullType;
import com.degoos.wetsponge.resource.sponge.SpongeSkullBuilder;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.tileentity.Skull;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullType;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.util.Direction;

import java.net.URL;

public class SpongeTileEntitySkull extends SpongeTileEntity implements WSTileEntitySkull {


    public SpongeTileEntitySkull(SpongeBlock block) {
        super(block);
    }

    @Override
    public WSGameProfile getGameProfile() {
        return new SpongeGameProfile(getHandled().getBlock().get(Keys.REPRESENTED_PLAYER).orElse(null));
    }

    @Override
    public void setGameProfile(WSGameProfile gameProfile) {
        BlockState state = getHandled().getBlock();
        getHandled().getLocation().setBlock(state.with(Keys.REPRESENTED_PLAYER, ((SpongeGameProfile) gameProfile).getHandled()).orElse(state));
    }

    @Override
    public EnumBlockOrientation getOrientation() {
        return EnumBlockOrientation.getBySpongeName(getHandled().get(Keys.DIRECTION).orElse(Direction.DOWN).name()).orElse(EnumBlockOrientation.SOUTH);
    }

    @Override
    public void setOrientation(EnumBlockOrientation orientation) {
        getHandled().offer(Keys.DIRECTION, Direction.valueOf(orientation.name()));
    }

    @Override
    public EnumSkullType getSkullType() {
        return EnumSkullType.getBySpongeName(getHandled().skullType().get().getName()).orElse(EnumSkullType.SKELETON);
    }

    @Override
    public void setSkullType(EnumSkullType skullType) {
        getHandled().offer(Keys.SKULL_TYPE, Sponge.getRegistry().getType(SkullType.class, skullType.getSpongeName()).orElse(SkullTypes.SKELETON));
    }

    @Override
    public void setTexture(String texture) {
        getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByTexture(getHandled().getBlock(), texture));
    }

    @Override
    public void setTexture(URL texture) {
        getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByURL(getHandled().getBlock(), texture));
    }

    @Override
    public void setTextureByPlayerName(String name) {
        getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByPlayerName(getHandled().getBlock(), name));
    }

    @Override
    public void findFormatAndSetTexture(String texture) {
        getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByUnknownFormat(getHandled().getBlock(), texture));
    }

    @Override
    public Skull getHandled() {
        return (Skull) super.getHandled();
    }
}
