package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.block.SpigotBlock;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;

public class SpigotTileEntityMonsterSpawner extends SpigotTileEntity implements WSTileEntityMonsterSpawner {

    public SpigotTileEntityMonsterSpawner(SpigotBlock block) {
        super(block);
    }

    @Override
    public EnumEntityType getEntity() {
        return SpigotEntityParser.getEntityType(getHandled().getSpawnedType());
    }

    @Override
    public void setEntity(EnumEntityType type) {
        CreatureSpawner spawner = getHandled();
        spawner.setSpawnedType(EntityType.fromName(type.getName()));
        update();
    }

    @Override
    public CreatureSpawner getHandled() {
        return (CreatureSpawner) super.getHandled();
    }

}
