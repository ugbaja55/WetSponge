package com.degoos.wetsponge.block;


import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.packet.play.server.WSSPacketBlockChange;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

public class SpigotBlockState implements WSBlockState {

    private WSLocation location;
    private WSBlockType blockType;
    private SpigotBlock block;


    public SpigotBlockState(SpigotBlock block) {
        this.location = block.getLocation();
        this.block = block;
        refresh();
    }


    public WSLocation getLocation() {
        return location;
    }


    @Override
    public WSWorld getWorld() {
        return location.getWorld();
    }


    public WSBlock getBlock() {
        return block;
    }


    public WSBlockType getBlockType() {
        return blockType;
    }


    public SpigotBlockState setBlockType(WSBlockType blockType) {
        this.blockType = blockType;
        return this;
    }


    public void update() {
        update(true);
    }


    public void update(boolean applyPhysics) {
        Block spigotBlock = block.getHandled();
        spigotBlock.setType(Material.getMaterial(blockType.getId()));
        if (blockType instanceof WSDataValuable) spigotBlock.setData((byte) ((WSDataValuable) blockType).getDataValue());
        else spigotBlock.setData((byte) 0);
        BlockState state = spigotBlock.getState();
        state.update(true, applyPhysics);
        getWorld().getPlayers().forEach(player -> player.sendPacket(WSSPacketBlockChange.of(blockType, location.toVector3i())));
    }


    @Override
    public void refresh() {
        this.blockType = WSBlockTypes.getType(block.getId(), block.getHandled().getData()).orElse(WSBlockTypes.AIR.getDefaultType());
    }

}
