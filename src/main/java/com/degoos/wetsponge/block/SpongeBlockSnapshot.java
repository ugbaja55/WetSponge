package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import org.spongepowered.api.block.BlockSnapshot;

public class SpongeBlockSnapshot extends WSBlockSnapshot {

    private BlockSnapshot snapshot;

    public SpongeBlockSnapshot(BlockSnapshot snapshot) {
        super(WSBlockTypes.getType(snapshot.getState().getType().getId())
                .orElse(new WSBlockTypeDataValuable(0, snapshot.getState().getType().getId(), 64, 0)));
        this.snapshot = snapshot;
        SpongeDataValueConverter.refresh(original, snapshot.getState());
    }
}
