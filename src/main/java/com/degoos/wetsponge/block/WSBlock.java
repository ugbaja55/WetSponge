package com.degoos.wetsponge.block;


import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.world.WSLocatable;

public interface WSBlock extends WSLocatable {

    /**
     * This method returns the {@link Integer} id of the material
     * of this block. This ID is deprecated, but Spigot uses it as
     * a base ID.
     *
     * @return the material id of the block.
     */
    int getId();

    /**
     * This method returns the {@link String} id of the material
     * of this block. This ID is used by Sponge as a base ID.
     *
     * @return the material id of the block.
     */
    String getStringId();

    /**
     * This method creates a new {@link WSBlockState} from this {@link WSBlock}.
     * With a {@link WSBlockState} you can edit everything you want of a block.
     *
     * @return the {@link WSBlockState}.
     */
    WSBlockState createState();

    /**
     * This method returns the tile entity of this block. Tile entities updates automatically,
     * so you needn't use any update method.
     *
     * @return the tile entity of this block.
     */
    WSTileEntity getTileEntity();

    /**
     * Returns the {@link WSBlockType block type} of the {@link WSBlock}.
     * Any changes to the {@link WSBlockType block type} won't affect to the {@link WSBlock}.
     * For a mutable {@link WSBlockType block type}, use {@link WSBlock#createState()}.
     *
     * @return the {@link WSBlockType block type}.
     */
    WSBlockType getBlockType();

    /**
     * Gets the relative {@link WSBlock block} by the given {@link EnumBlockDirection block direction}.
     * @param direction the {@link EnumBlockDirection block direction}.
     * @return the relative {@link WSBlock block}.
     */
    WSBlock getRelative (EnumBlockDirection direction);

    /**
     * Returns the {@link EnumMapBaseColor map base color} associated to the {@link WSBlock block}.
     * @return the {@link EnumMapBaseColor map base color}.
     */
    EnumMapBaseColor getMapBaseColor ();

    /**
     * @return the handled object.
     */
    Object getHandled();
}
