package com.degoos.wetsponge.block;


import com.degoos.wetsponge.block.tileentity.SpongeTileEntity;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityBrewingStand;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityChest;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityCommandBlock;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityDispenser;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityFurnace;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityJukebox;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityMonsterSpawner;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityNameable;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityNameableInventory;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityNoteblock;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntitySign;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntitySkull;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import java.util.Optional;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class SpongeBlock implements WSBlock {

	private Location<World> location;


	public SpongeBlock(Location<World> location) {
		this.location = location;
	}


	@Override
	public WSLocation getLocation() {
		return new SpongeLocation(location);
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(location.getExtent().getName(), location.getExtent());
	}


	@Override
	public int getId() {
		Optional<WSBlockType> optional = WSBlockTypes.getType(location.getBlock().getType().getId());
		return optional.map(WSBlockType::getId).orElse(0);
	}


	@Override
	public String getStringId() {
		return location.getBlock().getType().getId();
	}


	@Override
	public WSBlockState createState() {
		return new SpongeBlockState(this);
	}


	@Override
	public SpongeTileEntity getTileEntity() {
		switch (getId()) {
			case 23:
				return new SpongeTileEntityDispenser(this);
			case 25:
				return new SpongeTileEntityNoteblock(this);
			case 52:
				return new SpongeTileEntityMonsterSpawner(this);
			case 54:
			case 146:
				return new SpongeTileEntityChest(this);
			case 61:
			case 62:
				return new SpongeTileEntityFurnace(this);
			case 63:
			case 68:
				return new SpongeTileEntitySign(this);
			case 84:
				return new SpongeTileEntityJukebox(this);
			case 116:
				return new SpongeTileEntityNameable(this);
			case 117:
				return new SpongeTileEntityBrewingStand(this);
			case 137:
				return new SpongeTileEntityCommandBlock(this);
			case 144:
				return new SpongeTileEntitySkull(this);
			case 154:
				return new SpongeTileEntityNameableInventory(this);
			default:
				return new SpongeTileEntity(this);
		}
	}

	@Override
	public WSBlockType getBlockType() {
		WSBlockType type = WSBlockTypes.getType(getStringId()).orElse(WSBlockTypes.AIR.getDefaultType());
		SpongeDataValueConverter.refresh(type, getHandled().getBlock());
		return type;
	}

	@Override
	public WSBlock getRelative(EnumBlockDirection direction) {
		return getLocation().add(direction.getRelative().toDouble()).getBlock();
	}

	@Override
	public EnumMapBaseColor getMapBaseColor() {
		BlockPos pos = new BlockPos(location.getX(), location.getY(), location.getZ());
		return EnumMapBaseColor.getById(((net.minecraft.world.World) location.getExtent()).getBlockState(pos)
			.getMapColor((net.minecraft.world.World) getWorld().getHandled(), pos).colorIndex).orElse(EnumMapBaseColor.AIR);
	}

	@Override
	public Location<World> getHandled() {
		return location;
	}

}
