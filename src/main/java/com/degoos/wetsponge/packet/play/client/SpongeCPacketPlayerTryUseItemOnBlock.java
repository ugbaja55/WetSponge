package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.block.EnumBlockDirection;
import com.degoos.wetsponge.packet.SpongePacket;
import com.flowpowered.math.vector.Vector3i;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketPlayerTryUseItemOnBlock;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeCPacketPlayerTryUseItemOnBlock extends SpongePacket implements WSCPacketPlayerTryUseItemOnBlock {

    private Vector3i position;
    private EnumBlockDirection placedBlockDirection;
    private boolean mainHand;
    private float facingX, facingY, facingZ;
    private boolean changed;

    public SpongeCPacketPlayerTryUseItemOnBlock(Vector3i position, EnumBlockDirection placedBlockDirection, boolean mainHand, float facingX, float
            facingY, float facingZ) {
        super(new CPacketPlayerTryUseItemOnBlock());
        this.position = position;
        this.placedBlockDirection = placedBlockDirection;
        this.mainHand = mainHand;
        this.facingX = facingX;
        this.facingY = facingY;
        this.facingZ = facingZ;
        update();
    }

    public SpongeCPacketPlayerTryUseItemOnBlock(Packet<?> packet) {
        super(packet);
        refresh();
    }


    @Override
    public Vector3i getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3i position) {
        this.position = position;
        changed = true;
    }

    @Override
    public EnumBlockDirection getPlacedBlockDirection() {
        return placedBlockDirection;
    }

    @Override
    public void setPlacedBlockDirection(EnumBlockDirection direction) {
        this.placedBlockDirection = direction;
        changed = true;
    }

    @Override
    public boolean isMainHand() {
        return mainHand;
    }

    @Override
    public void setMainHand(boolean mainHand) {
        this.mainHand = mainHand;
        changed = true;
    }

    @Override
    public float getFacingX() {
        return facingX;
    }

    @Override
    public void setFacingX(float facingX) {
        this.facingX = facingX;
        changed = true;
    }

    @Override
    public float getFacingY() {
        return facingY;
    }

    @Override
    public void setFacingY(float facingY) {
        this.facingY = facingY;
        changed = true;
    }

    @Override
    public float getFacingZ() {
        return facingZ;
    }

    @Override
    public void setFacingZ(float facingZ) {
        this.facingZ = facingZ;
        changed = true;
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].set(getHandler(), new BlockPos(position.getX(), position.getY(), position.getZ()));
            fields[1].set(getHandler(), getEnumFacing(placedBlockDirection));
            fields[2].set(getHandler(), mainHand ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
            fields[3].setFloat(getHandler(), facingX);
            fields[4].setFloat(getHandler(), facingY);
            fields[5].setFloat(getHandler(), facingZ);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            BlockPos blockPos = (BlockPos) fields[0].get(getHandler());
            position = new Vector3i(blockPos.getX(), blockPos.getY(), blockPos.getZ());
            placedBlockDirection = getEnumBlockDirection((EnumFacing) fields[1].get(getHandler()));
            mainHand = fields[2].get(getHandler()) == EnumHand.MAIN_HAND;
            facingX = fields[3].getFloat(getHandler());
            facingY = fields[4].getFloat(getHandler());
            facingZ = fields[5].getFloat(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }


    private EnumFacing getEnumFacing(EnumBlockDirection direction) {
        switch (direction) {
            case DOWN:
                return EnumFacing.DOWN;
            case EAST:
                return EnumFacing.EAST;
            case NORTH:
                return EnumFacing.NORTH;
            case SOUTH:
                return EnumFacing.SOUTH;
            case UP:
                return EnumFacing.UP;
            case WEST:
                return EnumFacing.WEST;
            default:
                return EnumFacing.DOWN;
        }
    }

    private EnumBlockDirection getEnumBlockDirection(EnumFacing direction) {
        switch (direction) {
            case DOWN:
                return EnumBlockDirection.DOWN;
            case EAST:
                return EnumBlockDirection.EAST;
            case NORTH:
                return EnumBlockDirection.NORTH;
            case SOUTH:
                return EnumBlockDirection.SOUTH;
            case UP:
                return EnumBlockDirection.UP;
            case WEST:
                return EnumBlockDirection.WEST;
            default:
                return EnumBlockDirection.DOWN;
        }
    }
}
