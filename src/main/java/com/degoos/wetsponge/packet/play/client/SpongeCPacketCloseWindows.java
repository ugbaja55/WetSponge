package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.packet.SpongePacket;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketCloseWindow;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeCPacketCloseWindows extends SpongePacket implements WSCPacketCloseWindows {

    private int windowsId;
    private boolean changed;

    public SpongeCPacketCloseWindows(int windowsId) {
        super(new CPacketCloseWindow());
        this.windowsId = windowsId;
        update();
    }

    public SpongeCPacketCloseWindows(Packet<?> packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].setInt(getHandler(), windowsId);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            windowsId = fields[0].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public int getWindowsId() {
        return windowsId;
    }

    @Override
    public void setWindowsId(int windowsId) {
        this.windowsId = windowsId;
        changed = true;
    }
}
