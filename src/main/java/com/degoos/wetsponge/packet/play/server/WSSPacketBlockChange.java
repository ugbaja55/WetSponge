package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketBlockChange extends WSPacket {

    public static WSSPacketBlockChange of(WSBlockType material, Vector3i position) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketBlockChange(material, position);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketBlockChange(material, position);
            default:
                return null;
        }
    }

    public Vector3i getBlockPosition();

    public void setBlockPosition(Vector3i position);

    public WSBlockType getMaterial();

    public void setMaterial(WSBlockType material);

}
