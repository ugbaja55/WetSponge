package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumUseEntityType;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector3d;
import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotCPacketUseEntity extends SpigotPacket implements WSCPacketUseEntity {

	private int target;
	private EnumUseEntityType type;
	private Vector3d interactAtPosition;
	private boolean mainHand, changed;

	public SpigotCPacketUseEntity(int target, EnumUseEntityType type, Vector3d interactAtPosition, boolean mainHand)
		throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayInUseEntity").newInstance());
		this.target = target;
		this.type = type;
		this.interactAtPosition = interactAtPosition;
		this.mainHand = mainHand;
		update();
	}

	public SpigotCPacketUseEntity(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getTarget() {
		return target;
	}

	@Override
	public void setTarget(int target) {
		if (this.target != target) {
			this.target = target;
			changed = true;
		}
	}

	@Override
	public EnumUseEntityType getType() {
		return type;
	}

	@Override
	public void setType(EnumUseEntityType type) {
		Validate.notNull(type, "Type cannot be null!");
		if (this.type != type) {
			this.type = type;
			changed = true;
		}
	}

	@Override
	public Vector3d getInteractAtPosition() {
		return interactAtPosition == null ? new Vector3d() : interactAtPosition;
	}

	@Override
	public void setInteractAtPosition(Vector3d interactAtPosition) {
		if (!this.interactAtPosition.equals(interactAtPosition)) {
			this.interactAtPosition = interactAtPosition;
			changed = true;
		}
	}

	@Override
	public boolean isMainHand() {
		return mainHand;
	}

	@Override
	public void setMainHand(boolean mainHand) {
		if (mainHand != this.mainHand) {
			this.mainHand = mainHand;
			changed = true;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			Class<? extends Enum> typeClass = (Class<? extends Enum>) NMSUtils.getNMSClass("PacketPlayInUseEntity$EnumEntityUseAction");
			Class<? extends Enum> handClass = (Class<? extends Enum>) NMSUtils.getNMSClass("EnumHand");

			fields[0].setInt(getHandler(), target);
			fields[1].set(getHandler(), Enum.valueOf(typeClass, type.name()));
			if (interactAtPosition != null) fields[2].set(getHandler(), NMSUtils.getNMSClass("Vec3D").getConstructor(double.class, double.class, double.class)
				.newInstance(interactAtPosition.getX(), interactAtPosition.getY(), interactAtPosition.getZ()));

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				fields[3].set(getHandler(), Enum.valueOf(handClass, mainHand ? "MAIN_HAND" : "OFF_HAND"));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			target = fields[0].getInt(getHandler());
			type = EnumUseEntityType.valueOf(((Enum) fields[1].get(getHandler())).name());

			Object vec3d = fields[2].get(getHandler());
			if (vec3d != null)
				interactAtPosition = new Vector3d(NMSUtils.getNMSClass("Vec3D").getField("x").getDouble(vec3d), vec3d.getClass().getField("y").getDouble(vec3d), vec3d
					.getClass().getField("z").getDouble(vec3d));

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Object object = fields[3].get(getHandler());
				mainHand = object == null || ((Enum) object).name().equals("MAIN_HAND");
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
