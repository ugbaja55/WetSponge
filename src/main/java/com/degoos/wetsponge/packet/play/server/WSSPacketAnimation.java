package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketAnimation extends WSPacket {

    public static WSSPacketAnimation of(WSEntity entity, int animationType) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketAnimation(entity, animationType);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketAnimation(entity, animationType);
            default:
                return null;
        }
    }

    public static WSSPacketAnimation of(int entityId, int animationType) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketAnimation(entityId, animationType);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketAnimation(entityId, animationType);
            default:
                return null;
        }
    }

    public int getEntityId();

    public void setEntityId(int entityId);

    public int getAnimationType();

    public void setAnimationType(int animationType);
}
