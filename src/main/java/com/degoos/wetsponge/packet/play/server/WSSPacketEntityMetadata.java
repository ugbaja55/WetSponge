package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketEntityMetadata extends WSPacket {

	public static WSSPacketEntityMetadata of(WSEntity entity) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					return new SpigotSPacketEntityMetadata(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityMetadata(entity);
			default:
				return null;
		}
	}


	int getEntityId();

	void setEntityId(int entityId);

	void setMetadataOf(WSEntity entity);
}
