package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntity;

public class SpongeSPacketEntityLookMove extends SpongeSPacketEntity implements WSSPacketEntityLookMove {

    public SpongeSPacketEntityLookMove(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
        this(entity.getEntityId(), position, rotation, onGround);
    }

    public SpongeSPacketEntityLookMove(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
        super(new SPacketEntity.S17PacketEntityLookMove(entity, position.getX(), position.getY(), position.getZ(),
                (byte) rotation.getY(), (byte) rotation.getX(), onGround));
    }

    public SpongeSPacketEntityLookMove(Packet<?> packet) {
        super(packet);
    }
}
