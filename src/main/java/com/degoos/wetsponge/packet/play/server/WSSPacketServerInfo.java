package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;

public interface WSSPacketServerInfo extends WSPacket {

	public static WSSPacketServerInfo of(WSServerStatusResponse response) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketServerInfo(response);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketServerInfo(response);
			default:
				return null;
		}
	}

	WSServerStatusResponse getResponse();

	void setResponse(WSServerStatusResponse response);
}
