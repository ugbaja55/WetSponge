package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;

public interface WSSPacketMaps extends WSPacket {

	public static WSSPacketMaps of(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketMaps(mapId, origin, size, mapView);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketMaps(mapId, origin, size, mapView);
			default:
				return null;
		}
	}

	int getMapId();

	void setMapId(int id);

	Vector2i getOrigin();

	void setOrigin(Vector2i origin);

	Vector2i setSize();

	void setSize(Vector2i size);

	WSMapView getMapView();

	void setMapView(WSMapView mapView);
}
