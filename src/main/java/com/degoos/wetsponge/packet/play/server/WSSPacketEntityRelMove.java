package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketEntityRelMove extends WSSPacketEntity {

    public static WSSPacketEntityRelMove of(WSEntity entity, Vector3i position, boolean onGround) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotSPacketEntityRelMove(entity, position, onGround);
            case SPONGE:
                return new SpongeSPacketEntityRelMove(entity, position, onGround);
            default:
                return null;
        }
    }

    public static WSSPacketEntityRelMove of(int entity, Vector3i position, boolean onGround) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotSPacketEntityRelMove(entity, position, onGround);
            case SPONGE:
                return new SpongeSPacketEntityRelMove(entity, position, onGround);
            default:
                return null;
        }
    }

}
