package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector3i;

public class SpigotSPacketEntityRelMove extends SpigotSPacketEntity implements WSSPacketEntityRelMove {

    public SpigotSPacketEntityRelMove(WSEntity entity, Vector3i position, boolean onGround) {
        this(entity.getEntityId(), position, onGround);
    }

    public SpigotSPacketEntityRelMove(int entity, Vector3i position, boolean onGround) {
        super(getPacket(entity, position, onGround));
    }

    public SpigotSPacketEntityRelMove(Object packet) {
        super(packet);
    }

    public static Object getPacket(int entity, Vector3i position, boolean onGround) {
        try {
            Class<?> clazz = NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMove");
            if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
                return clazz.getConstructor(int.class, long.class, long.class, long.class, boolean.class)
                        .newInstance(entity, (long) position.getX(), (long) position.getY(), (long) position.getZ(), onGround);
            else return clazz.getConstructor(int.class, byte.class, byte.class, byte.class, boolean.class)
                    .newInstance(entity, (byte) position.getX(), (byte) position.getY(), (byte) position.getZ(), onGround);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
