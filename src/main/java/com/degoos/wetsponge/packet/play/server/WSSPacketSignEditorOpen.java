package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketSignEditorOpen extends WSPacket {

    public static WSSPacketSignEditorOpen of(Vector3d position) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSignEditorOpen(position);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSignEditorOpen(position);
            default:
                return null;
        }
    }

    Vector3d getPosition();

    void setPosition(Vector3d position);
}
