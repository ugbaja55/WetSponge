package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketAnimation;

import java.lang.reflect.Field;

public class SpongeSPacketAnimation extends SpongePacket implements WSSPacketAnimation {

    private int entityId;
    private int animationType;
    private boolean changed;

    public SpongeSPacketAnimation(WSEntity entity, int animationType) {
        super(new SPacketAnimation((Entity) ((SpongeEntity) entity).getHandled(), animationType));
        this.entityId = ((Entity) ((SpongeEntity) entity).getHandled()).getEntityId();
        this.animationType = animationType;
    }

    public SpongeSPacketAnimation(int entity, int animationType) {
        super(new SPacketAnimation());
        this.entityId = entity;
        this.animationType = animationType;
        update();
    }

    public SpongeSPacketAnimation(Packet<?> packet) {
        super(packet);
        refresh();
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        changed = true;
        this.entityId = entityId;
    }

    public int getAnimationType() {
        return animationType;
    }

    public void setAnimationType(int animationType) {
        changed = true;
        this.animationType = animationType;
    }

    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            for (Field field : fields) field.setAccessible(true);
            entityId = fields[0].getInt(getHandler());
            animationType = fields[1].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            for (Field field : fields) field.setAccessible(true);
            fields[0].setInt(getHandler(), entityId);
            fields[1].setInt(getHandler(), animationType);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }
}
