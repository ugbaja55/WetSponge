package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.enums.EnumMapDecorationType;
import com.degoos.wetsponge.enums.EnumMapScale;
import com.degoos.wetsponge.map.WSMapDecoration;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2i;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketMaps;
import net.minecraft.world.storage.MapDecoration;

public class SpongeSPacketMaps extends SpongePacket implements WSSPacketMaps {

	private int mapId;
	private Vector2i origin, size;
	private WSMapView mapView;
	private boolean changed;

	public SpongeSPacketMaps(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) {
		super(new SPacketMaps());
		Validate.notNull(origin, "Origin cannot be null!");
		Validate.notNull(size, "Size cannot be null!");
		Validate.notNull(mapView, "MapView cannot be null!");
		this.mapId = mapId;
		this.origin = origin;
		this.size = size;
		this.mapView = mapView;
		update();
	}

	public SpongeSPacketMaps(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));

			fields[0].setInt(getHandler(), mapId);
			fields[1].setByte(getHandler(), mapView.getMapScale().getId());
			fields[2].setBoolean(getHandler(), mapView.isTrackingPositions());

			fields[3].set(getHandler(), mapView.getAllDecorations().stream()
				.map(decoration -> new MapDecoration(MapDecoration.Type.byIcon(decoration.getType().getId()), (byte) decoration.getPosition().getX(), (byte) decoration
					.getPosition().getY(), (byte) decoration.getRotation())).toArray(MapDecoration[]::new));

			fields[4].setInt(getHandler(), origin.getX());
			fields[5].setInt(getHandler(), origin.getY());
			fields[6].setInt(getHandler(), size.getX());
			fields[7].setInt(getHandler(), size.getY());
			fields[8].set(getHandler(), mapView.getColors());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			mapId = fields[0].getInt(getHandler());

			origin = new Vector2i(fields[4].getInt(getHandler()), fields[5].getInt(getHandler()));
			size = new Vector2i(fields[6].getInt(getHandler()), fields[7].getInt(getHandler()));

			byte[] colors = (byte[]) fields[8].get(getHandler());
			if (colors.length == 16384) mapView = new WSMapView((byte[]) fields[8].get(getHandler()), Arrays.stream((MapDecoration[]) fields[3].get(getHandler()))
				.map(mapDecoration -> new WSMapDecoration(new Vector2i(mapDecoration.getX(), mapDecoration.getY()), EnumMapDecorationType
					.getById(mapDecoration.getType().getIcon()).orElse(EnumMapDecorationType.PLAYER), mapDecoration.getRotation())).collect(Collectors.toSet()),
				fields[2]
				.getBoolean(getHandler()), EnumMapScale.getById(fields[1].getByte(getHandler())).orElse(EnumMapScale.CLOSEST));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getMapId() {
		return mapId;
	}

	@Override
	public void setMapId(int id) {
		this.mapId = id;
		changed = true;
	}

	@Override
	public Vector2i getOrigin() {
		return origin;
	}

	@Override
	public void setOrigin(Vector2i origin) {
		this.origin = origin;
		changed = true;
	}

	@Override
	public Vector2i setSize() {
		return size;
	}

	@Override
	public void setSize(Vector2i size) {
		this.size = size;
		changed = true;
	}

	@Override
	public WSMapView getMapView() {
		changed = true;
		return mapView;
	}

	@Override
	public void setMapView(WSMapView mapView) {
		this.mapView = mapView;
		changed = true;
	}
}
