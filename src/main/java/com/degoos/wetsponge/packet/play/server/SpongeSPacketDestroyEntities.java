package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketDestroyEntities;

import java.util.ArrayList;
import java.util.List;

public class SpongeSPacketDestroyEntities extends SpongePacket implements WSSPacketDestroyEntities {

    private List<Integer> entityIds;
    private boolean changed;

    public SpongeSPacketDestroyEntities(int... entities) {
        super(new SPacketDestroyEntities(entities));
        entityIds = new ArrayList<>();
        update();
    }

    public SpongeSPacketDestroyEntities(Packet<?> packet) {
        super(packet);
        entityIds = new ArrayList<>();
        refresh();
    }

    @Override
    public void update() {
        try {
            int[] ids = new int[entityIds.size()];
            for (int i = 0; i < entityIds.size(); i++) ids[i] = entityIds.get(i);
            ReflectionUtils.setFirstObject(getHandler().getClass(), int[].class, getHandler(), ids);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            entityIds.clear();
            int[] ids = (int[]) ReflectionUtils.getFirstObject(getHandler().getClass(), int[].class, getHandler());
            for (int i : ids) entityIds.add(i);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Integer> getEntityIds() {
        changed = true;
        return entityIds;
    }

    @Override
    public void setEntityIds(List<Integer> entityIds) {
        changed = true;
        this.entityIds = entityIds;
    }

    @Override
    public void addEntityId(int entityId) {
        changed = true;
        entityIds.add(entityId);
    }

    @Override
    public void removeEntityId(int entityId) {
        changed = true;
        entityIds.add(entityId);
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }
}
