package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.WSPacket;
import java.util.List;

public interface WSSPacketWindowItems extends WSPacket {

	public static WSSPacketWindowItems of(int windowsId, List<WSItemStack> itemStacks) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketWindowItems(windowsId, itemStacks);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketWindowItems(windowsId, itemStacks);
			default:
				return null;
		}
	}

	int getWindowId();

	void setWindowId(int windowsId);

	List<WSItemStack> getItemStacks();
}
