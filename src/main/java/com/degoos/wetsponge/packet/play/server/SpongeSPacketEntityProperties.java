package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntityProperties;
import net.minecraft.network.play.server.SPacketEntityProperties.Snapshot;

public class SpongeSPacketEntityProperties extends SpongePacket implements WSSPacketEntityProperties {

	private int entityId;
	private List<SPacketEntityProperties.Snapshot> entries = new ArrayList<>();
	private boolean changed;

	public SpongeSPacketEntityProperties(WSLivingEntity entity) {
		super(new SPacketEntityProperties(entity.getEntityId(), ((EntityLivingBase) entity.getHandled()).getAttributeMap().getAllAttributes()));
		setPropertiesOf(entity);
		refresh();
	}

	public SpongeSPacketEntityProperties(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setPropertiesOf(WSLivingEntity entity) {
		SPacketEntityProperties packet = ((SPacketEntityProperties) getHandler());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries.clear();
		((EntityLivingBase) entity.getHandled()).getAttributeMap().getAllAttributes()
			.forEach(attribute -> entries.add(packet.new Snapshot(attribute.getAttribute().getName(), attribute.getBaseValue(), attribute.getModifiers())));
		changed = true;
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			Collection<SPacketEntityProperties.Snapshot> snapshots = (Collection<Snapshot>) fields[1].get(getHandler());
			snapshots.clear();
			snapshots.addAll(entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			Collection<SPacketEntityProperties.Snapshot> snapshots = (Collection<Snapshot>) fields[1].get(getHandler());
			entries.clear();
			entries.addAll(snapshots);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
