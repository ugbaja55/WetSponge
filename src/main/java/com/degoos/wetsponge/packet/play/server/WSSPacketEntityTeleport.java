package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketEntityTeleport extends WSPacket {

	public static WSSPacketEntityTeleport of(WSEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityTeleport(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entity);
			default:
				return null;
		}
	}

	public static WSSPacketEntityTeleport of(int entityIds, Vector3d position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityTeleport(entityIds, position, rotation, onGround);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entityIds, position, rotation, onGround);
			default:
				return null;
		}
	}

	int getEntityId();

	void setEntityId(int entityId);

	Vector3d getPosition();

	void setPosition(Vector3d position);

	Vector2i getRotation();

	void setRotation(Vector2i rotation);

	boolean isOnGround();

	void setOnGround(boolean onGround);

}
