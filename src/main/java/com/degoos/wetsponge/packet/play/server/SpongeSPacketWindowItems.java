package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.SpongePacket;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketWindowItems;
import org.spongepowered.api.item.inventory.ItemStack;

public class SpongeSPacketWindowItems extends SpongePacket implements WSSPacketWindowItems {

	private int windowId;
	private List<WSItemStack> itemStacks;
	private boolean changed;

	public SpongeSPacketWindowItems(int windowsId, List<WSItemStack> itemStacks) {
		super(new SPacketWindowItems());
		this.windowId = windowsId;
		this.itemStacks = itemStacks;
		update();
	}

	public SpongeSPacketWindowItems(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), windowId);
			fields[1].set(getHandler(), itemStacks.stream().map(item -> item == null ? null : ((SpongeItemStack) item).getHandled()).collect(Collectors.toList()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			windowId = fields[0].getInt(getHandler());
			itemStacks = ((List<? extends ItemStack>) fields[1].get(getHandler())).stream().map(item -> item == null ? null : new SpongeItemStack(item).clone())
				.collect(Collectors.toList());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowsId) {
		this.windowId = windowsId;
		changed = true;
	}

	@Override
	public List<WSItemStack> getItemStacks() {
		changed = true;
		return itemStacks;
	}
}
