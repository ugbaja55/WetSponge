package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketEntityLookMove extends WSSPacketEntity {

	public static WSSPacketEntityLookMove of(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityLookMove of(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}
	}

}
