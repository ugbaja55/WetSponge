package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntityMetadata;

public class SpongeSPacketEntityMetadata extends SpongePacket implements WSSPacketEntityMetadata {

	private int entityId;
	private List<?> entries;
	private boolean changed;

	public SpongeSPacketEntityMetadata(WSEntity entity) {
		super(new SPacketEntityMetadata());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entityId = entity.getEntityId();
		this.entries = ((Entity) ((SpongeEntity) entity).getHandled()).getDataManager().getDirty();
		this.changed = false;
		update();
	}

	public SpongeSPacketEntityMetadata(Packet<?> packet) {
		super(packet);
		this.changed = false;
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setMetadataOf(WSEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries = ((Entity) ((SpongeEntity) entity).getHandled()).getDataManager().getDirty();
		changed = true;
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			this.entries = (List) fields[1].get(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
