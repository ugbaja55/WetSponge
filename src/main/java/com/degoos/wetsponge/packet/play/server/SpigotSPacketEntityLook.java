package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;

public class SpigotSPacketEntityLook extends SpigotSPacketEntity implements WSSPacketEntityLook {

    public SpigotSPacketEntityLook(WSEntity entity, Vector2i rotation, boolean onGround) {
        this(entity.getEntityId(), rotation, onGround);
    }

    public SpigotSPacketEntityLook(int entity, Vector2i rotation, boolean onGround) {
        super(getPacket(entity, rotation, onGround));
    }

    public SpigotSPacketEntityLook(Object packet) {
        super(packet);
    }

    public static Object getPacket(int entity, Vector2i rotation, boolean onGround) {
        try {
            return NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutEntityLook").getConstructor(int.class, byte.class, byte.class, boolean.class)
                    .newInstance(entity, (byte) rotation.getX(), (byte) rotation.getY(),
                            onGround);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
