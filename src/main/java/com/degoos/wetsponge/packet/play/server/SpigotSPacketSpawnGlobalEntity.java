package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector3d;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotSPacketSpawnGlobalEntity extends SpigotPacket implements WSSPacketSpawnGlobalEntity {

    private Vector3d position;
    private int entityId, type;
    private boolean changed;

    public SpigotSPacketSpawnGlobalEntity(Vector3d position, int entityId, int type) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutSpawnEntityWeather").newInstance());
        this.position = position;
        this.entityId = entityId;
        this.type = type;
        update();
    }

    public SpigotSPacketSpawnGlobalEntity(Object packet) {
        super(packet);
        refresh();
    }

    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        changed = true;
        this.position = position;
    }

    @Override
    public int getEntityId() {
        return entityId;
    }

    @Override
    public void setEntityId(int entityId) {
        changed = true;
        this.entityId = entityId;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public void setType(int type) {
        changed = true;
        this.type = type;
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].set(getHandler(), entityId);
            fields[1].set(getHandler(), position.getX());
            fields[2].set(getHandler(), position.getY());
            fields[3].set(getHandler(), position.getZ());
            fields[4].set(getHandler(), type);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            entityId = fields[0].getInt(getHandler());
            position = new Vector3d(fields[1].getDouble(getHandler()), fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()));
            type = fields[4].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

}
