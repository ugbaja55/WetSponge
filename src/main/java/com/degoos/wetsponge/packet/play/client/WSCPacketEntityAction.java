package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityAction;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSCPacketEntityAction extends WSPacket {

	public static WSCPacketEntityAction of(int entityId, EnumEntityAction entityAction, int jumpBoost) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotCPacketEntityAction(entityId, entityAction, jumpBoost);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeCPacketEntityAction(entityId, entityAction, jumpBoost);
			default:
				return null;
		}
	}

	int getEntityId();

	void setEntityId(int entityId);

	EnumEntityAction getAction();

	void setAction(EnumEntityAction entityAction);

	int getJumpBoost();

	void setJumpBoost(int jumpBoost);
}
