package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.EnumUseEntityType;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSCPacketUseEntity extends WSPacket {

	int getTarget();

	void setTarget(int target);

	EnumUseEntityType getType();

	void setType(EnumUseEntityType type);

	Vector3d getInteractAtPosition();

	void setInteractAtPosition(Vector3d interactAtPosition);

	boolean isMainHand();

	void setMainHand(boolean mainHand);

}
