package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketSetSlot extends WSPacket {

	public static WSSPacketSetSlot of(int windowsId, int slot, WSItemStack itemStack) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSetSlot(windowsId, slot, itemStack);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSetSlot(windowsId, slot, itemStack);
			default:
				return null;
		}
	}

	int getWindowId();

	void setWindowId(int windowsId);

	int getSlot();

	void setSlot(int slot);

	WSItemStack getItemStack();

	void setItemStack(WSItemStack itemStack);
}
