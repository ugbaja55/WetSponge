package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.util.InternalLogger;

public interface WSSPacketEntityHeadLook extends WSPacket {

	public static WSSPacketEntityHeadLook of(int entityId, byte yaw) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entityId, yaw);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityHeadLook(entityId, yaw);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}
	}

	public static WSSPacketEntityHeadLook of(WSLivingEntity entity, byte yaw) {
		return of(entity.getEntityId(), yaw);
	}

	public static WSSPacketEntityHeadLook of(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entity);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityHeadLook(entity);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}
	}


	int getEntityId();

	void setEntityId(int entityId);

	byte getYaw();

	void setYaw(byte yaw);

}
