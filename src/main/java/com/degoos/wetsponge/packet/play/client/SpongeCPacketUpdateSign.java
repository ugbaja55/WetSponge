package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector3d;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketUpdateSign;
import net.minecraft.util.math.BlockPos;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeCPacketUpdateSign extends SpongePacket implements WSCPacketUpdateSign {

    private Vector3d position;
    private String[] lines;
    private boolean changed;

    public SpongeCPacketUpdateSign(Vector3d position, String[] lines) {
        super(new CPacketUpdateSign());
        Validate.notNull(position, "Position cannot be null!");
        Validate.notNull(lines, "Lines cannot be null!");
        Validate.isTrue(lines.length > 3, "Lines must have 4 instances!");
        this.position = position;
        this.lines = lines.clone();
        update();
    }

    public SpongeCPacketUpdateSign(Packet<?> packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].set(getHandler(), new BlockPos(position.getX(), position.getY(), position.getZ()));
            fields[1].set(getHandler(), lines);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            BlockPos blockPos = (BlockPos) fields[0].get(getHandler());
            position = new Vector3d(blockPos.getX(), blockPos.getY(), blockPos.getZ());
            lines = (String[]) fields[1].get(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }


    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        Validate.notNull(position, "Position cannot be null!");
        this.position = position;
        changed = true;
    }

    @Override
    public String[] getLines() {
        return lines.clone();
    }

    @Override
    public void setLines(String[] lines) {
        Validate.notNull(lines, "Lines cannot be null!");
        Validate.isTrue(lines.length > 3, "Lines must have 4 instances!");
        this.lines = lines.clone();
        changed = true;
    }
}
