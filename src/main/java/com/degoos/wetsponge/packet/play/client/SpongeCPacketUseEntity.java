package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.EnumUseEntityType;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector3d;
import java.lang.reflect.Field;
import java.util.Arrays;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketUseEntity;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.Vec3d;

public class SpongeCPacketUseEntity extends SpongePacket implements WSCPacketUseEntity {

	private int target;
	private EnumUseEntityType type;
	private Vector3d interactAtPosition;
	private boolean mainHand, changed;

	public SpongeCPacketUseEntity(int target, EnumUseEntityType type, Vector3d interactAtPosition, boolean mainHand) {
		super(new CPacketUseEntity());
		this.target = target;
		this.type = type;
		this.interactAtPosition = interactAtPosition;
		this.mainHand = mainHand;
		update();
	}

	public SpongeCPacketUseEntity(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getTarget() {
		return target;
	}

	@Override
	public void setTarget(int target) {
		if (this.target != target) {
			this.target = target;
			changed = true;
		}
	}

	@Override
	public EnumUseEntityType getType() {
		return type;
	}

	@Override
	public void setType(EnumUseEntityType type) {
		Validate.notNull(type, "Type cannot be null!");
		if (this.type != type) {
			this.type = type;
			changed = true;
		}
	}

	@Override
	public Vector3d getInteractAtPosition() {
		return interactAtPosition == null ? new Vector3d() : interactAtPosition;
	}

	@Override
	public void setInteractAtPosition(Vector3d interactAtPosition) {
		if (!this.interactAtPosition.equals(interactAtPosition)) {
			this.interactAtPosition = interactAtPosition;
			changed = true;
		}
	}

	@Override
	public boolean isMainHand() {
		return mainHand;
	}

	@Override
	public void setMainHand(boolean mainHand) {
		if (mainHand != this.mainHand) {
			this.mainHand = mainHand;
			changed = true;
		}
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), target);
			fields[1].set(getHandler(), CPacketUseEntity.Action.valueOf(type.name()));
			if (interactAtPosition != null) fields[2].set(getHandler(), new Vec3d(interactAtPosition.getX(), interactAtPosition.getY(), interactAtPosition.getZ()));
			fields[3].set(getHandler(), mainHand ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			target = fields[0].getInt(getHandler());
			type = EnumUseEntityType.valueOf(((CPacketUseEntity.Action) fields[1].get(getHandler())).name());

			Vec3d vec3d = (Vec3d) fields[2].get(getHandler());
			if (vec3d != null) interactAtPosition = new Vector3d(vec3d.x, vec3d.y, vec3d.z);

			mainHand = EnumHand.MAIN_HAND.equals(fields[3].get(getHandler()));

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
