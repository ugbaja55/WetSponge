package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;

import java.util.UUID;

public interface WSSPacketSpawnPlayer extends WSPacket {

    public static WSSPacketSpawnPlayer of(WSPlayer entity) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSpawnPlayer(entity);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSpawnPlayer(entity);
            default:
                return null;
        }
    }

    public static WSSPacketSpawnPlayer of(WSPlayer entity, Vector3d position, Vector2d rotation) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSpawnPlayer(entity, position, rotation);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSpawnPlayer(entity, position, rotation);
            default:
                return null;
        }
    }

    void setEntity(WSPlayer entity);

    int getEntityId();

    void setEntityId(int entityId);

    UUID getUniqueId();

    void setUniqueId(UUID uniqueId);

    Vector3d getPosition();

    void setPosition(Vector3d position);

    Vector2d getRotation();

    void setRotation(Vector2d rotation);


}
