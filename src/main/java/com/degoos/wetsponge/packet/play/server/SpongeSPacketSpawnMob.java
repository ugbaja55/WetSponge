package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketSpawnMob;

public class SpongeSPacketSpawnMob extends SpongePacket implements WSSPacketSpawnMob {

	private Optional<WSLivingEntity> entity;
	private int entityId;
	private UUID uniqueId;
	private int type;
	private Vector3d position, velocity;
	private Vector2d rotation;
	private double headPitch;
	private boolean changed;
	private WSLivingEntity disguise;

	public SpongeSPacketSpawnMob(WSLivingEntity entity) {
		super(new SPacketSpawnMob((EntityLivingBase) ((SpongeEntity) entity).getHandled()));
		this.entity = Optional.ofNullable(entity);
		this.disguise = null;
		refresh();
	}

	public SpongeSPacketSpawnMob(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, double headPitch) {
		super(new SPacketSpawnMob());
		updateEntity(entity);
		this.position = position;
		this.velocity = velocity;
		this.rotation = rotation;
		this.headPitch = headPitch;
		this.disguise = null;
		update();
	}

	public SpongeSPacketSpawnMob(Packet<?> packet) {
		super(packet);
		this.entity = Optional.empty();
		refresh();
	}

	public Optional<WSLivingEntity> getEntity() {
		return entity;
	}

	public void setEntity(WSLivingEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		updateEntity(entity);
	}

	@Override
	public Optional<WSLivingEntity> getDisguise() {
		return Optional.ofNullable(disguise);
	}

	public void setDisguise(WSLivingEntity disguise) {
		Validate.notNull(disguise, "Disguise cannot be null!");
		changed = true;
		this.disguise = disguise;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		changed = true;
		this.entityId = entityId;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		changed = true;
		this.uniqueId = uniqueId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		changed = true;
		this.type = type;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		changed = true;
		this.position = position;
	}

	public Vector3d getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector3d velocity) {
		changed = true;
		this.velocity = velocity;
	}

	public Vector2d getRotation() {
		return rotation;
	}

	public void setRotation(Vector2d rotation) {
		changed = true;
		this.rotation = rotation;
	}

	public double getHeadPitch() {
		return headPitch;
	}

	public void setHeadPitch(double headPitch) {
		changed = true;
		this.headPitch = headPitch;
	}

	private void updateEntity(WSLivingEntity entity) {
		EntityLivingBase livingBase = (EntityLivingBase) ((SpongeEntity) entity).getHandled();
		this.entity = Optional.ofNullable(entity);
		this.entityId = livingBase.getEntityId();
		this.uniqueId = livingBase.getUniqueID();
		this.type = EntityList.REGISTRY.getIDForObject(livingBase.getClass());
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), uniqueId);
			fields[2].setInt(getHandler(), disguise == null ? type : disguise.getEntityType().getTypeId());

			fields[3].setDouble(getHandler(), position.getX());
			fields[4].setDouble(getHandler(), position.getY());
			fields[5].setDouble(getHandler(), position.getZ());

			fields[6].setInt(getHandler(), (int) (velocity.getX() * 8000.0D));
			fields[7].setInt(getHandler(), (int) (velocity.getY() * 8000.0D));
			fields[8].setInt(getHandler(), (int) (velocity.getZ() * 8000.0D));

			fields[9].setByte(getHandler(), (byte) ((int) (rotation.getY() * 256.0F / 360.0F)));
			fields[10].setByte(getHandler(), (byte) ((int) (rotation.getX() * 256.0F / 360.0F)));
			fields[11].setByte(getHandler(), (byte) ((int) (headPitch * 256.0F / 360.0F)));

			if (disguise != null) fields[12].set(getHandler(), ((EntityLivingBase) ((SpongeEntity) disguise).getHandled()).getDataManager());
			else if (entity.isPresent()) fields[12].set(getHandler(), ((EntityLivingBase) ((SpongeEntity) entity.get()).getHandled()).getDataManager());

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			uniqueId = (UUID) fields[1].get(getHandler());
			type = fields[2].getInt(getHandler());

			position = new Vector3d(fields[3].getDouble(getHandler()), fields[4].getDouble(getHandler()), fields[5].getDouble(getHandler()));

			velocity = new Vector3d(fields[6].getInt(getHandler()) / 8000.0D, fields[7].getInt(getHandler()) / 8000.0D, fields[8].getInt(getHandler()) / 8000.0D);

			rotation = new Vector2d(fields[10].getByte(getHandler()) * 360.0D / 256.0D, fields[9].getByte(getHandler()) * 360.0D / 256.0D);
			headPitch = fields[11].getByte(getHandler()) * 360.0D / 256.0D;

			entity = Optional.empty();
			disguise = null;
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

}
