package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.SpongePacket;
import java.lang.reflect.Field;
import java.util.Arrays;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketSetSlot;
import org.spongepowered.api.item.inventory.ItemStack;

public class SpongeSPacketSetSlot extends SpongePacket implements WSSPacketSetSlot {

	private int windowId, slot;
	private WSItemStack itemStack;
	private boolean changed;

	public SpongeSPacketSetSlot(int windowsId, int slot, WSItemStack itemStack) {
		super(new SPacketSetSlot());
		this.windowId = windowsId;
		this.slot = slot;
		this.itemStack = itemStack;
		update();
	}

	public SpongeSPacketSetSlot(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), windowId);
			fields[1].setInt(getHandler(), slot);
			fields[2].set(getHandler(), itemStack.getHandled());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			windowId = fields[0].getInt(getHandler());
			slot = fields[1].getInt(getHandler());
			itemStack = new SpongeItemStack((ItemStack) fields[2].get(getHandler())).clone();
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowsId) {
		this.windowId = windowsId;
		changed = true;
	}

	@Override
	public int getSlot() {
		return slot;
	}

	@Override
	public void setSlot(int slot) {
		this.slot = slot;
		changed = true;
	}

	@Override
	public WSItemStack getItemStack() {
		changed = true;
		return itemStack;
	}

	@Override
	public void setItemStack(WSItemStack itemStack) {
		this.itemStack = itemStack;
		changed = true;
	}
}
