package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSCPacketCloseWindows extends WSPacket {

    public static WSCPacketCloseWindows of(int windowsId) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotCPacketCloseWindows(windowsId);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeCPacketCloseWindows(windowsId);
            default:
                return null;
        }
    }

    int getWindowsId();

    void setWindowsId(int windowsId);
}
