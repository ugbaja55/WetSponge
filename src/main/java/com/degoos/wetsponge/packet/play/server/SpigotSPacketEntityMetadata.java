package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import org.bukkit.entity.Entity;

public class SpigotSPacketEntityMetadata extends SpigotPacket implements WSSPacketEntityMetadata {

	private int entityId;
	private List<?> entries;
	private boolean changed;

	public SpigotSPacketEntityMetadata(WSEntity entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityMetadata").newInstance());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entityId = entity.getEntityId();
		this.entries = getEntityMetadata(((SpigotEntity) entity).getHandled());
		this.changed = false;
		update();
	}

	public SpigotSPacketEntityMetadata(Object packet) {
		super(packet);
		this.changed = false;
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setMetadataOf(WSEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries = getEntityMetadata(((SpigotEntity) entity).getHandled());
		changed = true;
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			this.entries = (List) fields[1].get(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}


	private List<?> getEntityMetadata(Entity entity) {
		try {
			Object handle = HandledUtils.getHandle(entity);
			Object watcher = NMSUtils.getNMSClass("Entity").getMethod("getDataWatcher").invoke(handle);
			return (List) NMSUtils.getNMSClass("DataWatcher").getMethod("b").invoke(watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
