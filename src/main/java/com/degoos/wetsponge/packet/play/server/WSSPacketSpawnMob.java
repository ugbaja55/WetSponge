package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import java.util.Optional;
import java.util.UUID;

public interface WSSPacketSpawnMob extends WSPacket {

	public static WSSPacketSpawnMob of(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnMob(entity);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnMob of(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, float headPitch) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
			default:
				return null;
		}
	}

	Optional<WSLivingEntity> getEntity();

	void setEntity(WSLivingEntity entity);

	Optional<WSLivingEntity> getDisguise();

	void setDisguise(WSLivingEntity disguise);

	int getEntityId();

	void setEntityId(int entityId);

	UUID getUniqueId();

	void setUniqueId(UUID uniqueId);

	int getType();

	void setType(int type);

	Vector3d getPosition();

	void setPosition(Vector3d position);

	Vector3d getVelocity();

	void setVelocity(Vector3d velocity);

	Vector2d getRotation();

	void setRotation(Vector2d rotation);

	double getHeadPitch();

	void setHeadPitch(double headPitch);

}
