package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketSpawnGlobalEntity extends WSPacket {

    public static WSSPacketSpawnGlobalEntity of(Vector3d position, int entityId, int type) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSpawnGlobalEntity(position, entityId, type);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSpawnGlobalEntity(position, entityId, type);
            default:
                return null;
        }
    }

    Vector3d getPosition();

    void setPosition(Vector3d position);

    int getEntityId();

    void setEntityId(int entityId);

    int getType();

    void setType(int type);
}
