package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketEntityProperties extends WSPacket {

	public static WSSPacketEntityProperties of(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					return new SpigotSPacketEntityProperties(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityProperties(entity);
			default:
				return null;
		}
	}

	int getEntityId();

	void setEntityId(int entityId);

	void setPropertiesOf(WSLivingEntity entity);

}
