package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketHeldItemChange extends WSPacket {

    public static WSSPacketHeldItemChange of(int slot) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketHeldItemChange(slot);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketHeldItemChange(slot);
            default:
                return null;
        }
    }

    int getSlot();

    void setSlot(int slot);

}
