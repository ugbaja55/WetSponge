package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.parser.packet.SpongePacketParser;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketMultiBlockChange;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;

public class SpongeSPacketMultiBlockChange extends SpongePacket implements WSSPacketMultiBlockChange {

	private Vector2i chunkPosition;
	private Map<Vector3i, WSBlockType> materials;
	private int index;
	private boolean changed;

	public SpongeSPacketMultiBlockChange(Vector2i chunkPosition, Map<Vector3i, WSBlockType> materials) {
		super(new SPacketMultiBlockChange());
		this.chunkPosition = chunkPosition;
		this.materials = materials;
		update();
	}

	public SpongeSPacketMultiBlockChange(Packet<?> packet) {
		super(packet);
		refresh();
	}

	public Vector2i getChunkPosition() {
		return chunkPosition;
	}

	public void setChunkPosition(Vector2i chunkPosition) {
		changed = true;
		this.chunkPosition = chunkPosition;
	}

	public Map<Vector3i, WSBlockType> getMaterials() {
		changed = true;
		return materials;
	}

	public void setMaterials(Map<Vector3i, WSBlockType> materials) {
		changed = true;
		this.materials = materials;
	}

	public void removeMaterial(Vector3i position) {
		changed = true;
		materials.remove(position);
	}

	public void addMaterial(Vector3i position, WSBlockType type) {
		changed = true;
		materials.put(position, type);
	}

	@Override
	public void update() {
		index = 0;
		try {
			ReflectionUtils.setFirstObject(SPacketMultiBlockChange.class, ChunkPos.class, getHandler(), new ChunkPos(chunkPosition.getX(), chunkPosition.getY()));
			SPacketMultiBlockChange.BlockUpdateData[] data = new SPacketMultiBlockChange.BlockUpdateData[materials.size()];

			materials.forEach((vector3i, type) -> {
				try {
					short offset = (short) (((vector3i.getX() << 12) & 61440) | (vector3i.getY() & 255) | ((vector3i.getZ() << 8) & 3840));
					IBlockState state = SpongePacketParser.getBlockState(type);
					data[index] = ((SPacketMultiBlockChange) getHandler()).new BlockUpdateData(offset, state);
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
				index++;
			});

			ReflectionUtils.setAccessible(SPacketMultiBlockChange.class.getDeclaredFields()[1]).set(getHandler(), data);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			SPacketMultiBlockChange.BlockUpdateData[] blocks = ReflectionUtils.
				getFirstObject(SPacketMultiBlockChange.class, SPacketMultiBlockChange.BlockUpdateData[].class, getHandler());
			ChunkPos pos = ReflectionUtils.getFirstObject(SPacketMultiBlockChange.class, ChunkPos.class, getHandler());

			chunkPosition = new Vector2i(pos.x, pos.z);
			materials = new HashMap<>();

			for (SPacketMultiBlockChange.BlockUpdateData data : blocks) {
				materials.put(new Vector3i(data.getOffset() >> 12 & 15, data.getOffset() & 255, data.getOffset() >> 8 & 15), SpongePacketParser
					.getMaterial(data.getBlockState()));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
