package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;

import java.util.List;

public interface WSSPacketDestroyEntities extends WSPacket {

    public static WSSPacketDestroyEntities of(int... entityIds) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketDestroyEntities(entityIds);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketDestroyEntities(entityIds);
            default:
                return null;
        }
    }

    List<Integer> getEntityIds();

    void setEntityIds(List<Integer> entityIds);

    void addEntityId(int entityId);

    void removeEntityId(int entityId);
}
