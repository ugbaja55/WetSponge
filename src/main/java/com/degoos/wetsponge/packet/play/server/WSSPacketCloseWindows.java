package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketCloseWindows extends WSPacket {

    public static WSSPacketCloseWindows of(int windowsId) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketCloseWindows(windowsId);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketCloseWindows(windowsId);
            default:
                return null;
        }
    }

    int getWindowsId();

    void setWindowsId(int windowsId);

}
