package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSCPacketUpdateSign extends WSPacket {

    public static WSCPacketUpdateSign of(Vector3d position, String[] lines) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotCPacketUpdateSign(position, lines);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeCPacketUpdateSign(position, lines);
            default:
                return null;
        }
    }

    Vector3d getPosition();

    void setPosition(Vector3d position);

    String[] getLines();

    void setLines(String[] lines);

}
