package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;

import java.util.UUID;

public interface WSSPacketSpawnObject extends WSPacket {

    public static WSSPacketSpawnObject of(WSEntity entity, int type, int data) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSpawnObject(entity, type, data);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSpawnObject(entity, type, data);
            default:
                return null;
        }
    }

    public static WSSPacketSpawnObject of(WSEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, int type, int data) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                try {
                    return new SpigotSPacketSpawnObject(entity, position, velocity, rotation, type, data);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            case SPONGE:
                return new SpongeSPacketSpawnObject(entity, position, velocity, rotation, type, data);
            default:
                return null;
        }
    }

    void setEntity(WSLivingEntity entity);

    int getEntityId();

    void setEntityId(int entityId);

    UUID getUniqueId();

    void setUniqueId(UUID uniqueId);

    int getType();

    void setType(int type);

    Vector3d getPosition();

    void setPosition(Vector3d position);

    Vector3d getVelocity();

    void setVelocity(Vector3d velocity);

    Vector2d getRotation();

    void setRotation(Vector2d rotation);

    int getData();

    void setData(int data);

}
