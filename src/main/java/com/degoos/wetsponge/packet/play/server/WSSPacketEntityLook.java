package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;

public interface WSSPacketEntityLook extends WSSPacketEntity {

    public static WSSPacketEntityLook of(WSEntity entity, Vector2i rotation, boolean onGround) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotSPacketEntityLook(entity, rotation, onGround);
            case SPONGE:
                return new SpongeSPacketEntityLook(entity, rotation, onGround);
            default:
                return null;
        }
    }

    public static WSSPacketEntityLook of(int entity, Vector2i rotation, boolean onGround) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotSPacketEntityLook(entity, rotation, onGround);
            case SPONGE:
                return new SpongeSPacketEntityLook(entity, rotation, onGround);
            default:
                return null;
        }
    }

}
