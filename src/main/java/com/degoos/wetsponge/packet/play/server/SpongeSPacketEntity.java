package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;
import java.lang.reflect.Field;
import java.util.Arrays;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntity;

public class SpongeSPacketEntity extends SpongePacket implements WSSPacketEntity {

	private int entityId;
	private Vector3i position;
	private Vector2i rotation;
	private boolean onGround, rotating, changed;

	public SpongeSPacketEntity(Packet<?> packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	public Vector3i getPosition() {
		return position;
	}

	public void setPosition(Vector3i position) {
		this.position = position;
		changed = true;
	}

	public Vector2i getRotation() {
		return rotation;
	}

	public void setRotation(Vector2i rotation) {
		this.rotation = rotation;
		changed = true;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
		changed = true;
	}

	public boolean isRotating() {
		return rotating;
	}

	public void setRotating(boolean rotating) {
		this.rotating = rotating;
		changed = true;
	}

	@Override
	public void update() {
		try {
			Field[] fields = SPacketEntity.class.getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].setInt(getHandler(), position.getX());
			fields[2].setInt(getHandler(), position.getY());
			fields[3].setInt(getHandler(), position.getZ());
			fields[4].setByte(getHandler(), (byte) (rotation.getY() * 256D / 360D));
			fields[5].setByte(getHandler(), (byte) (rotation.getX() * 256D / 360D));
			fields[6].setBoolean(getHandler(), onGround);
			fields[7].setBoolean(getHandler(), rotating);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = SPacketEntity.class.getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			position = new Vector3i(fields[1].getInt(getHandler()), fields[2].getInt(getHandler()), fields[3].getInt(getHandler()));
			rotation = new Vector2i(fields[5].getByte(getHandler()) * 360D / 256D, fields[4].getByte(getHandler()) * 360D / 256D);
			onGround = fields[6].getBoolean(getHandler());
			rotating = fields[7].getBoolean(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
