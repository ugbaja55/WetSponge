package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector3d;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class SpigotCPacketUpdateSign extends SpigotPacket implements WSCPacketUpdateSign {

	private Vector3d position;
	private String[] lines;
	private boolean changed;

	public SpigotCPacketUpdateSign(Vector3d position, String[] lines) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayInUpdateSign").newInstance());
		Validate.notNull(position, "Position cannot be null!");
		Validate.notNull(lines, "Lines cannot be null!");
		Validate.isTrue(lines.length > 3, "Lines must have 4 instances!");
		this.position = position;
		this.lines = lines.clone();
		update();
	}

	public SpigotCPacketUpdateSign(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].set(getHandler(), HandledUtils.getBlockPosition(position));
			Object array = Array.newInstance(NMSUtils.getNMSClass("IChatBaseComponent"), 4);
			for (int i = 0; i < 4; i++)
				Array.set(array, i, NMSUtils.getNMSClass("TextComponent").getConstructor(String.class).newInstance(lines[i]));
			fields[1].set(getHandler(), array);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			Object blockPos = fields[0].get(getHandler());
			position = new Vector3d((int) blockPos.getClass().getMethod("getX").invoke(blockPos), (int) blockPos.getClass().getMethod("getY")
				.invoke(blockPos), (int) blockPos.getClass().getMethod("getZ").invoke(blockPos));
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) lines = (String[]) fields[1].get(getHandler());
			else {
				lines = new String[4];
				Method textMethod = NMSUtils.getNMSClass("IChatBaseComponent").getMethod("getText");
				Object[] array = (Object[]) fields[1].get(getHandler());
				for (int i = 0; i < 4; i++) lines[i] = (String) textMethod.invoke(array[i]);
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}


	@Override
	public Vector3d getPosition() {
		return position;
	}

	@Override
	public void setPosition(Vector3d position) {
		Validate.notNull(position, "Position cannot be null!");
		this.position = position;
		changed = true;
	}

	@Override
	public String[] getLines() {
		return lines.clone();
	}

	@Override
	public void setLines(String[] lines) {
		Validate.notNull(lines, "Lines cannot be null!");
		Validate.isTrue(lines.length > 3, "Lines must have 4 instances!");
		this.lines = lines.clone();
		changed = true;
	}
}
