package com.degoos.wetsponge.util;

import java.util.Arrays;

public class SpigotEventUtils {

	public static boolean shouldBeExecuted() {
		return Arrays.stream(Thread.currentThread().getStackTrace()).noneMatch(trace -> trace.getClassName().contains("citizensnpcs"));
	}

}
