package com.degoos.wetsponge.util;


import com.degoos.wetsponge.plugin.WSPlugin;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONValue;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class PluginUpdater {

	private WSPlugin instance = null;
	private int resourceID = 0;
	private boolean autoUpdate = false;


	public PluginUpdater (WSPlugin instance, int resourceID, boolean autoUpdate) {
		this.instance = instance;
		this.resourceID = resourceID;
		this.autoUpdate = autoUpdate;
	}


	public WSPlugin getInstance () {
		return instance;
	}


	public void setInstance (WSPlugin instance) {
		this.instance = instance;
	}

	public int getResourceID () {
		return resourceID;
	}


	public void setResourceID (int resourceID) {
		this.resourceID = resourceID;
	}


	public boolean isAutoUpdate () {
		return autoUpdate;
	}


	public void setAutoUpdate (boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}


	public URL getVersionsUrl () throws MalformedURLException {
		return new URL(String.valueOf("https://api.spiget.org/v2/resources/" + this.resourceID + "/versions?size=" + Integer.MAX_VALUE + "&spiget__ua=SpigetDocs"));
	}


	public URL getUpdatesUrl () throws MalformedURLException {
		return new URL(String.valueOf("https://api.spiget.org/v2/resources/" + this.resourceID + "/updates?size=" + Integer.MAX_VALUE + "&spiget__ua=SpigetDocs"));
	}


	public Object[] getLastUpdate () {
		try {
			Scanner   versionsScanner = new Scanner(this.getVersionsUrl().openStream(), "UTF-8").useDelimiter("\\A");
			String    versionsResult  = versionsScanner.hasNext() ? versionsScanner.next() : "";
			JSONArray versionsArray   = (JSONArray) JSONValue.parseWithException(versionsResult);

			String lastVersion = ((JSONObject) versionsArray.get(versionsArray.size() - 1)).get("name").toString();

			//todo: get the resource "plugin.yml - version"
			if (lastVersion.compareToIgnoreCase(this.instance.getPluginDescription().getVersion()) > 0) {
				Scanner   updatesScanner = new Scanner(this.getUpdatesUrl().openStream(), "UTF-8").useDelimiter("\\A");
				String    updatesResult  = versionsScanner.hasNext() ? versionsScanner.next() : "";
				JSONArray updatesArray   = (JSONArray) JSONValue.parseWithException(versionsResult);

				String    updateName   = ((JSONObject) updatesArray.get(updatesArray.size() - 1)).get("title").toString();

				return new Object[] {lastVersion, updateName};
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return new String[0];
	}
}