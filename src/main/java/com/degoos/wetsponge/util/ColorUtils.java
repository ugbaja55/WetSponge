package com.degoos.wetsponge.util;

import java.awt.Color;

public class ColorUtils {

	public static double getDistance(Color c1, Color c2) {
		final int deltaR = c1.getRed() - c2.getRed();
		final int deltaG = c1.getGreen() - c2.getGreen();
		final int deltaB = c1.getBlue() - c2.getBlue();
		return (deltaR * deltaR) + (deltaG * deltaG) + (deltaB * deltaB);
	}


}
