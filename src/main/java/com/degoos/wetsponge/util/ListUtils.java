package com.degoos.wetsponge.util;

import java.lang.reflect.Array;
import java.util.*;

public class ListUtils {

    public static <T> List<T> toList(T... array) {
        List<T> list = new ArrayList<>();
        for (T object : array) list.add(object);
        return list;
    }

    public static <T> Set<T> toSet(T... array) {
        Set<T> set = new HashSet<>();
        for (T object : array) set.add(object);
        return set;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(Class<T> clazz, Collection<T> list) {
        T[] array = (T[]) Array.newInstance(clazz, list.size());
        int i = 0;
        for (T object : list) {
            array[i] = object;
            i++;
        }
        return array;
    }

}
