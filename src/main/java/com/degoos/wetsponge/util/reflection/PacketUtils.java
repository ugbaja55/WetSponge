package com.degoos.wetsponge.util.reflection;

import org.bukkit.entity.Player;

public class PacketUtils {

    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle           = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", NMSUtils.getNMSClass("Packet")).invoke(playerConnection, packet);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
