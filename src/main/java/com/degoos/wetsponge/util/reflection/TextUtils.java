package com.degoos.wetsponge.util.reflection;


public class TextUtils {

	private static Class<?> clazz = NMSUtils.getNMSClass("ChatBaseComponent$ChatSerializer");

	public static String toJSON(Object iChatBaseComponent) {
		try {
			return (String) ReflectionUtils.invokeMethod(null, clazz, "a", iChatBaseComponent);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static Object toIChatBaseComponent(String json) {
		try {
			return ReflectionUtils.invokeMethod(null, clazz, "a", json);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
