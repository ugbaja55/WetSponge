package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MapUtils {

	private static Field field;
	private static Method method;

	static {
		try {
			method = NMSUtils.getNMSClass("IBlockData").getMethod("a", NMSUtils.getNMSClass("IBlockAccess"), NMSUtils.getNMSClass("BlockPosition"));
			field = ReflectionUtils.setAccessible(NMSUtils.getNMSClass("MaterialMapColor").getField("ad"));
			AccessibleObject.setAccessible(new AccessibleObject[]{field}, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static EnumMapBaseColor getMapBaseColor(Object iBlockData, Object iBlockAccess, Object blockPosition, int id) {
		try {
			if (id == 0) return EnumMapBaseColor.AIR;
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				return EnumMapBaseColor.getById(field.getInt(method.invoke(iBlockData, iBlockAccess, blockPosition))).orElse(EnumMapBaseColor.AIR);
			} else {
				return EnumMapBaseColor.getById((int) ReflectionUtils
					.getObject(ReflectionUtils.invokeMethod(ReflectionUtils.invokeMethod(null, NMSUtils.getNMSClass("Block"), "getById", id), "g", iBlockData), "L"))
					.orElse(EnumMapBaseColor.AIR);
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the map base color of the block " + id + "!");
			return EnumMapBaseColor.AIR;
		}
	}

}
