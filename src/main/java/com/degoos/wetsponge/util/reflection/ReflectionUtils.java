package com.degoos.wetsponge.util.reflection;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import sun.reflect.ConstructorAccessor;
import sun.reflect.FieldAccessor;
import sun.reflect.ReflectionFactory;

/**
 * <b>ReflectionUtils</b>
 * <p>
 * This class provides useful methods which makes dealing with reflection much easier, especially when working with Bukkit
 * <p>
 * You are welcome to use it, modify it and redistribute it under the following conditions: <ul> <li>Don't claim this class as your own <li>Don't remove this
 * disclaimer </ul>
 * <p>
 * <i>It would be nice if you provide credit to me if you use this class in a published project</i>
 *
 * @author DarkBlade12
 * @version 1.1
 */
public final class ReflectionUtils {

	// Prevent accidental construction
	private ReflectionUtils() {
	}

	/**
	 * Returns the constructor of a given class with the given parameter types
	 *
	 * @param clazz Target class
	 * @param parameterTypes Parameter types of the desired constructor
	 *
	 * @return The constructor of the target class with the specified parameter types
	 *
	 * @throws NoSuchMethodException If the desired constructor with the specified parameter types cannot be found
	 * @see DataType
	 * @see DataType#getPrimitive(Class[])
	 * @see DataType#compare(Class[], Class[])
	 */
	public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes) throws NoSuchMethodException {
		Class<?>[] primitiveTypes = DataType.getPrimitive(parameterTypes);
		for (Constructor<?> constructor : clazz.getConstructors()) {
			if (DataType.compare(DataType.getPrimitive(constructor.getParameterTypes()), primitiveTypes)) {
				continue;
			}
			return constructor;
		}
		throw new NoSuchMethodException("There is no such constructor in this class with the specified parameter types");
	}

	/**
	 * Returns the constructor of a desired class with the given parameter types
	 *
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param parameterTypes Parameter types of the desired constructor
	 *
	 * @return The constructor of the desired target class with the specified parameter types
	 *
	 * @throws NoSuchMethodException If the desired constructor with the specified parameter types cannot be found
	 * @throws ClassNotFoundException ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getClass() (String, PackageType)
	 * @see #getConstructor(Class, Class...)
	 */
	public static Constructor<?> getConstructor(String className, PackageType packageType, Class<?>... parameterTypes)
		throws NoSuchMethodException, ClassNotFoundException {
		return getConstructor(packageType.getClass(className), parameterTypes);
	}

	/**
	 * Returns a field of the target class with the given name
	 *
	 * @param clazz Target class
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 *
	 * @return The field of the target class with the specified name
	 *
	 * @throws NoSuchFieldException If the desired field of the given class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 */
	public static Field getField(Class<?> clazz, boolean declared, String fieldName) throws NoSuchFieldException, SecurityException {
		Field field = declared ? clazz.getDeclaredField(fieldName) : clazz.getField(fieldName);
		field.setAccessible(true);
		return field;
	}


	/**
	 * Returns a field of the target class with the given name
	 *
	 * @param clazz Target class
	 * @param fieldName Name of the desired field
	 *
	 * @return The field of the target class with the specified name
	 *
	 * @throws NoSuchFieldException If the desired field of the given class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 */
	public static Field getField(Class<?> clazz, String fieldName) throws NoSuchFieldException, SecurityException {
		Field field;
		try {
			field = clazz.getField(fieldName);

		} catch (NoSuchFieldException ex) {
			field = clazz.getDeclaredField(fieldName);
		}
		field.setAccessible(true);
		return field;
	}

	/**
	 * Returns a field of a desired class with the given name
	 *
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 *
	 * @return The field of the desired target class with the specified name
	 *
	 * @throws NoSuchFieldException If the desired field of the desired class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getField(Class, boolean, String)
	 */
	public static Field getField(String className, PackageType packageType, boolean declared, String fieldName)
		throws NoSuchFieldException, SecurityException, ClassNotFoundException {
		return getField(packageType.getClass(className), declared, fieldName);
	}

	/**
	 * Returns a method of a class with the given parameter types
	 *
	 * @param clazz Target class
	 * @param methodName Name of the desired method
	 * @param parameterTypes Parameter types of the desired method
	 *
	 * @return The method of the target class with the specified name and parameter types
	 *
	 * @throws NoSuchMethodException If the desired method of the target class with the specified name and parameter types cannot be found
	 * @see DataType#getPrimitive(Class[])
	 * @see DataType#compare(Class[], Class[])
	 */
	public static Method getMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) throws NoSuchMethodException {
		Class<?>[] primitiveTypes = DataType.getPrimitive(parameterTypes);
		for (Method method : clazz.getMethods()) {
			if (!method.getName().equals(methodName) || DataType.compare(DataType.getPrimitive(method.getParameterTypes()), primitiveTypes)) {
				continue;
			}
			return method;
		}
		throw new NoSuchMethodException("There is no such method in this class with the specified name and parameter types " + Arrays.toString(parameterTypes));
	}

	public static Method getMethodByName(Class<?> clazz, String methodName) throws NoSuchMethodException {
		for (Method method : clazz.getMethods())
			if (method.getName().equals(methodName)) return method;
		for (Method method : clazz.getDeclaredMethods())
			if (method.getName().equals(methodName)) return method;
		throw new NoSuchMethodException("There is no such method in this class with the specified name and parameter types");
	}

	public static Object invokeConstructor(Class<?> playOutRespawn, Class<?>[] classes, Object... params)
		throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		return playOutRespawn.getConstructor(classes).newInstance(params);
	}

	/**
	 * Returns a method of a desired class with the given parameter types
	 *
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param methodName Name of the desired method
	 * @param parameterTypes Parameter types of the desired method
	 *
	 * @return The method of the desired target class with the specified name and parameter types
	 *
	 * @throws NoSuchMethodException If the desired method of the desired target class with the specified name and parameter types cannot be found
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getClass() (String, PackageType)
	 * @see #getMethod(Class, String, Class...)
	 */
	public static Method getMethod(String className, PackageType packageType, String methodName, Class<?>... parameterTypes)
		throws NoSuchMethodException, ClassNotFoundException {
		return getMethod(packageType.getClass(className), methodName, parameterTypes);
	}

	public static Optional<Method> getFirstMethod(Class<?> clazz, Class<?>... parameters) {
		Optional<Method> optional = Arrays.stream(clazz.getMethods()).filter(method -> Arrays.equals(method.getParameterTypes(), parameters)).findAny();
		if (!optional.isPresent()) optional = Arrays.stream(clazz.getDeclaredMethods()).filter(method -> Arrays.equals(method.getParameterTypes(), parameters))
			.findAny();
		return optional;
	}

	public static Optional<Method> getFirstMethodWithReturn(Class<?> clazz, Class<?> returns, Class<?>... parameters) {
		Optional<Method> optional = Arrays.stream(clazz.getMethods())
			.filter(method -> method.getReturnType().equals(returns) && Arrays.equals(method.getParameterTypes(), parameters)).findAny();
		if (!optional.isPresent()) optional = Arrays.stream(clazz.getDeclaredMethods())
			.filter(method -> method.getReturnType().equals(returns) && Arrays.equals(method.getParameterTypes(), parameters)).findAny();
		return optional;
	}

	/**
	 * Returns the value of a field with the given name of an object
	 *
	 * @param instance Target object
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 *
	 * @return The value of field of the target object
	 *
	 * @throws IllegalArgumentException If the target object does not feature the desired field (should not occur since it searches for a field with the given name in
	 * the
	 * class of the object)
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the target object cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @see #getValue(Object, Class, boolean, String)
	 */
	public static Object getValue(Object instance, boolean declared, String fieldName)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		return getValue(instance, instance.getClass(), declared, fieldName);
	}

	/**
	 * Returns the value of a field of the given class of an object
	 *
	 * @param instance Target object
	 * @param clazz Target class
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 *
	 * @return The value of field of the target object
	 *
	 * @throws IllegalArgumentException If the target object does not feature the desired field
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the target class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @see #getField(Class, boolean, String)
	 */
	public static Object getValue(Object instance, Class<?> clazz, boolean declared, String fieldName)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		return getField(clazz, declared, fieldName).get(instance);
	}

	/**
	 * Returns the value of a field of a desired class of an object
	 *
	 * @param instance Target object
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 *
	 * @return The value of field of the target object
	 *
	 * @throws IllegalArgumentException If the target object does not feature the desired field
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the desired class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getValue(Object, Class, boolean, String)
	 */
	public static Object getValue(Object instance, String className, PackageType packageType, boolean declared, String fieldName)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, ClassNotFoundException {
		return getValue(instance, packageType.getClass(className), declared, fieldName);
	}

	/**
	 * Returns an instance of a class with the given arguments
	 *
	 * @param clazz Target class
	 * @param arguments Arguments which are used to construct an object of the target class
	 *
	 * @return The instance of the target class with the specified arguments
	 *
	 * @throws InstantiationException If you cannot create an instance of the target class due to certain circumstances
	 * @throws IllegalAccessException If the desired constructor cannot be accessed due to certain circumstances
	 * @throws IllegalArgumentException If the types of the arguments do not match the parameter types of the constructor (this should not occur since it searches for a
	 * constructor with the types of the arguments)
	 * @throws InvocationTargetException If the desired constructor cannot be invoked
	 * @throws NoSuchMethodException If the desired constructor with the specified arguments cannot be found
	 */
	public static Object instantiateObject(Class<?> clazz, Object... arguments)
		throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		return getConstructor(clazz, DataType.getPrimitive(arguments)).newInstance(arguments);
	}

	/**
	 * Returns an instance of a desired class with the given arguments
	 *
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param arguments Arguments which are used to construct an object of the desired target class
	 *
	 * @return The instance of the desired target class with the specified arguments
	 *
	 * @throws InstantiationException If you cannot create an instance of the desired target class due to certain circumstances
	 * @throws IllegalAccessException If the desired constructor cannot be accessed due to certain circumstances
	 * @throws IllegalArgumentException If the types of the arguments do not match the parameter types of the constructor (this should not occur since it searches for a
	 * constructor with the types of the arguments)
	 * @throws InvocationTargetException If the desired constructor cannot be invoked
	 * @throws NoSuchMethodException If the desired constructor with the specified arguments cannot be found
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getClass() (String, PackageType)
	 * @see #instantiateObject(Class, Object...)
	 */
	public static Object instantiateObject(String className, PackageType packageType, Object... arguments)
		throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
		return instantiateObject(packageType.getClass(className), arguments);
	}

	/**
	 * Invokes a method of the target class on an object with the given arguments
	 *
	 * @param instance Target object
	 * @param clazz Target class
	 * @param methodName Name of the desired method
	 * @param arguments Arguments which are used to invoke the desired method
	 *
	 * @return The result of invoking the desired method on the target object
	 *
	 * @throws IllegalAccessException If the desired method cannot be accessed due to certain circumstances
	 * @throws IllegalArgumentException If the types of the arguments do not match the parameter types of the method (this should not occur since it searches for a
	 * method
	 * with the types of the arguments)
	 * @throws InvocationTargetException If the desired method cannot be invoked on the target object
	 * @throws NoSuchMethodException If the desired method of the target class with the specified name and arguments cannot be found
	 * @see #getMethod(Class, String, Class...)
	 * @see DataType#getPrimitive(Object[])
	 */
	public static Object invokeMethod(Object instance, Class<?> clazz, String methodName, Object... arguments)
		throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		return getMethod(clazz, methodName, DataType.getPrimitive(arguments)).invoke(instance, arguments);
	}

	/**
	 * Invokes a method on an object with the given arguments
	 *
	 * @param instance Target object
	 * @param methodName Name of the desired method
	 * @param arguments Arguments which are used to invoke the desired method
	 *
	 * @return The result of invoking the desired method on the target object
	 *
	 * @throws IllegalAccessException If the desired method cannot be accessed due to certain circumstances
	 * @throws IllegalArgumentException If the types of the arguments do not match the parameter types of the method (this should not occur since it searches for a
	 * method
	 * with the types of the arguments)
	 * @throws InvocationTargetException If the desired method cannot be invoked on the target object
	 * @throws NoSuchMethodException If the desired method of the class of the target object with the specified name and arguments cannot be found
	 * @see #getMethod(Class, String, Class...)
	 * @see DataType#getPrimitive(Object[])
	 */
	public static Object invokeMethod(Object instance, String methodName, Object... arguments)
		throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		Method method = getMethod(instance.getClass(), methodName, DataType.getPrimitive(arguments));
		method.setAccessible(true);
		return method.invoke(instance, arguments);
	}

	/**
	 * Invokes a method of a desired class on an object with the given arguments
	 *
	 * @param instance Target object
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param methodName Name of the desired method
	 * @param arguments Arguments which are used to invoke the desired method
	 *
	 * @return The result of invoking the desired method on the target object
	 *
	 * @throws IllegalAccessException If the desired method cannot be accessed due to certain circumstances
	 * @throws IllegalArgumentException If the types of the arguments do not match the parameter types of the method (this should not occur since it searches for a
	 * method
	 * with the types of the arguments)
	 * @throws InvocationTargetException If the desired method cannot be invoked on the target object
	 * @throws NoSuchMethodException If the desired method of the desired target class with the specified name and arguments cannot be found
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #getClass() (String, PackageType)
	 * @see #invokeMethod(Object, Class, String, Object...)
	 */
	public static Object invokeMethod(Object instance, String className, PackageType packageType, String methodName, Object... arguments)
		throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
		return invokeMethod(instance, packageType.getClass(className), methodName, arguments);
	}

	/**
	 * Sets the value of a field with the given name of an object
	 *
	 * @param instance Target object
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 * @param value New value
	 *
	 * @throws IllegalArgumentException If the type of the value does not match the type of the desired field
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the target object cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @see #setValue(Object, Class, boolean, String, Object)
	 */
	public static void setValue(Object instance, boolean declared, String fieldName, Object value)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		setValue(instance, instance.getClass(), declared, fieldName, value);
	}

	/**
	 * Sets the value of a field of the given class of an object
	 *
	 * @param instance Target object
	 * @param clazz Target class
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 * @param value New value
	 *
	 * @throws IllegalArgumentException If the type of the value does not match the type of the desired field
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the target class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @see #getField(Class, boolean, String)
	 */
	public static void setValue(Object instance, Class<?> clazz, boolean declared, String fieldName, Object value)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		getField(clazz, declared, fieldName).set(instance, value);
	}

	/**
	 * Sets the value of a field of a desired class of an object
	 *
	 * @param instance Target object
	 * @param className Name of the desired target class
	 * @param packageType Package where the desired target class is located
	 * @param declared Whether the desired field is declared or not
	 * @param fieldName Name of the desired field
	 * @param value New value
	 *
	 * @throws IllegalArgumentException If the type of the value does not match the type of the desired field
	 * @throws IllegalAccessException If the desired field cannot be accessed
	 * @throws NoSuchFieldException If the desired field of the desired class cannot be found
	 * @throws SecurityException If the desired field cannot be made accessible
	 * @throws ClassNotFoundException If the desired target class with the specified name and package cannot be found
	 * @see #setValue(Object, Class, boolean, String, Object)
	 */
	public static void setValue(Object instance, String className, PackageType packageType, boolean declared, String fieldName, Object value)
		throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, ClassNotFoundException {
		setValue(instance, packageType.getClass(className), declared, fieldName, value);
	}

	public static Object getObject(Object object, String fieldName) throws IllegalAccessException, NoSuchFieldException {
		Field field;
		try {
			field = object.getClass().getDeclaredField(fieldName);
		} catch (Exception ex) {
			field = object.getClass().getField(fieldName);
		}
		boolean accessible = field.isAccessible();
		if (!accessible) field.setAccessible(true);
		Object obj = field.get(object);
		if (!accessible) field.setAccessible(false);
		return obj;
	}

	public static <T> T getFirstObject(Class<?> clazz, Class<T> objClass, Object instance) throws Exception {
		Field f = null;
		for (Field fi : clazz.getDeclaredFields()) {
			if (fi.getType().equals(objClass)) {
				f = fi;
				break;
			}
		}

		if (f == null) {
			for (Field fi : clazz.getFields()) {
				if (fi.getType().equals(objClass)) {
					f = fi;
					break;
				}
			}
		}

		f.setAccessible(true);
		return (T) f.get(instance);
	}

	public static Field getFirstField(Class<?> clazz, Class<?> objClass) throws Exception {
		Field f = null;
		for (Field fi : clazz.getDeclaredFields()) {
			if (fi.getType().equals(objClass)) {
				f = fi;
				break;
			}
		}

		if (f == null) {
			for (Field fi : clazz.getFields()) {
				if (fi.getType().equals(objClass)) {
					f = fi;
					break;
				}
			}
		}

		f.setAccessible(true);
		return f;
	}

	public static Object getStaticObject(Class<?> clazz, String fieldName) throws IllegalAccessException, NoSuchFieldException {
		Field field = clazz.getDeclaredField(fieldName);
		boolean accessible = field.isAccessible();
		if (!accessible) field.setAccessible(true);
		Object obj = field.get(null);
		if (!accessible) field.setAccessible(false);
		return obj;
	}

	public static <T> T getFirstObjectOrElse(Class<?> clazz, Class<T> objClass, Object instance, T orElse) {
		try {
			Field f = null;
			for (Field fi : clazz.getDeclaredFields()) {
				if (fi.getType().equals(objClass)) {
					f = fi;
					break;
				}
			}

			if (f == null) {
				for (Field fi : clazz.getFields()) {
					if (fi.getType().equals(objClass)) {
						f = fi;
						break;
					}
				}
			}

			if (f == null) return orElse;

			f.setAccessible(true);
			Object object = f.get(instance);
			return object == null ? orElse : (T) object;
		} catch (Throwable ex) {
			return orElse;
		}
	}

	public static void setFirstObject(Class<?> clazz, Class<?> objClass, Object instance, Object value) throws Exception {
		Field f = null;
		for (Field fi : clazz.getDeclaredFields()) {
			if (fi.getType().equals(objClass)) {
				f = fi;
				break;
			}
		}

		if (f == null) {
			for (Field fi : clazz.getFields()) {
				if (fi.getType().equals(objClass)) {
					f = fi;
					break;
				}
			}
		}

		f.setAccessible(true);
		f.set(instance, value);
	}

	public static Enum<?> getEnum(Class<?> clazz, String enumname, String constant) throws Exception {
		Class<?> c = Class.forName(clazz.getName() + "$" + enumname);
		Enum<?>[] econstants = (Enum<?>[]) c.getEnumConstants();
		for (Enum<?> e : econstants) {
			if (e.name().equalsIgnoreCase(constant)) return e;
		}
		throw new Exception("Enum constant not found " + constant);
	}

	public static Enum<?> getEnum(Class<?> clazz, String constant) throws Exception {
		Class<?> c = Class.forName(clazz.getName());
		Enum<?>[] econstants = (Enum<?>[]) c.getEnumConstants();
		for (Enum<?> e : econstants) {
			if (e.name().equalsIgnoreCase(constant)) return e;
		}
		throw new Exception("Enum constant not found " + constant);
	}

	public static Field setAccessible(Field field) {
		return setAccessible(field, true);
	}

	public static Field setAccessible(Field field, boolean accessible) {
		try {
			field.setAccessible(accessible);
			if (accessible) {
				Field modifiersField = Field.class.getDeclaredField("modifiers");
				modifiersField.setAccessible(true);
				modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
				modifiersField.setAccessible(false);
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return field;
	}

	public static void transferFieldData(Field field, Object from, Object to) {
		try {
			//todo check if next line works as intended
			if (field.getType().equals(Byte.class) || field.getType().equals(byte.class)) field.setByte(to, field.getByte(from));
			else if (field.getType().equals(Short.class) || field.getType().equals(short.class)) field.setShort(to, field.getShort(from));
			else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) field.setInt(to, field.getInt(from));
			else if (field.getType().equals(Float.class) || field.getType().equals(float.class)) field.setFloat(to, field.getFloat(from));
			else if (field.getType().equals(Long.class) || field.getType().equals(long.class)) field.setLong(to, field.getLong(from));
			else if (field.getType().equals(Double.class) || field.getType().equals(double.class)) field.setDouble(to, field.getDouble(from));
			else if (field.getType().equals(Character.class) || field.getType().equals(char.class)) field.setChar(to, field.getChar(from));
			else if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class)) field.setBoolean(to, field.getBoolean(from));
			else field.set(to, field.get(from));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Represents an enumeration of Java data types with corresponding classes
	 * <p>
	 * This class is part of the <b>ReflectionUtils</b> and follows the same usage conditions
	 *
	 * @author DarkBlade12
	 * @since 1.0
	 */
	public enum DataType {
		BYTE(byte.class, Byte.class),
		SHORT(short.class, Short.class),
		INTEGER(int.class, Integer.class),
		LONG(long.class, Long.class),
		CHARACTER(char.class, Character.class),
		FLOAT(float.class, Float.class),
		DOUBLE(double.class, Double.class),
		BOOLEAN(boolean.class, Boolean.class);

		private static final Map<Class<?>, DataType> CLASS_MAP = new HashMap<>();

		// Initialize map for quick class lookup
		static {
			for (DataType type : values()) {
				CLASS_MAP.put(type.primitive, type);
				CLASS_MAP.put(type.reference, type);
			}
		}

		private final Class<?> primitive;

		private final Class<?> reference;


		/**
		 * Construct a new data type
		 *
		 * @param primitive Primitive class of this data type
		 * @param reference Reference class of this data type
		 */
		DataType(Class<?> primitive, Class<?> reference) {
			this.primitive = primitive;
			this.reference = reference;
		}


		/**
		 * Compares TWO class arrays on equivalence
		 *
		 * @param primary Primary class array
		 * @param secondary Class array which is compared to the primary array
		 *
		 * @return Whether these arrays are equal or not
		 */
		public static boolean compare(Class<?>[] primary, Class<?>[] secondary) {
			if (primary == null || secondary == null || primary.length != secondary.length) {
				return true;
			}
			for (int index = 0; index < primary.length; index++) {
				Class<?> primaryClass = primary[index];
				Class<?> secondaryClass = secondary[index];
				if (primaryClass.equals(secondaryClass) || primaryClass.isAssignableFrom(secondaryClass)) {
					continue;
				}
				return true;
			}
			return false;
		}


		/**
		 * Returns the data type with the given primitive/reference class
		 *
		 * @param clazz Primitive/Reference class of the data type
		 *
		 * @return The data type
		 */
		public static DataType fromClass(Class<?> clazz) {
			return CLASS_MAP.get(clazz);
		}


		/**
		 * Returns the primitive class of the data type with the given reference class
		 *
		 * @param clazz Reference class of the data type
		 *
		 * @return The primitive class
		 */
		public static Class<?> getPrimitive(Class<?> clazz) {
			DataType type = fromClass(clazz);
			return type == null ? clazz : type.getPrimitive();
		}


		/**
		 * Returns the primitive class array of the given class array
		 *
		 * @param classes Given class array
		 *
		 * @return The primitive class array
		 */
		public static Class<?>[] getPrimitive(Class<?>[] classes) {
			int length = classes == null ? 0 : classes.length;
			Class<?>[] types = new Class<?>[length];
			for (int index = 0; index < length; index++) {
				types[index] = getPrimitive(classes[index]);
			}
			return types;
		}


		/**
		 * Returns the primitive class array of the given object array
		 *
		 * @param objects Given object array
		 *
		 * @return The primitive class array
		 */
		public static Class<?>[] getPrimitive(Object[] objects) {
			int length = objects == null ? 0 : objects.length;
			Class<?>[] types = new Class<?>[length];
			for (int index = 0; index < length; index++) {
				types[index] = getPrimitive(objects[index].getClass());
			}
			return types;
		}


		/**
		 * Returns the reference class of the data type with the given primitive class
		 *
		 * @param clazz Primitive class of the data type
		 *
		 * @return The reference class
		 */
		public static Class<?> getReference(Class<?> clazz) {
			DataType type = fromClass(clazz);
			return type == null ? clazz : type.getReference();
		}


		/**
		 * Returns the reference class array of the given class array
		 *
		 * @param classes Given class array
		 *
		 * @return The reference class array
		 */
		public static Class<?>[] getReference(Class<?>[] classes) {
			int length = classes == null ? 0 : classes.length;
			Class<?>[] types = new Class<?>[length];
			for (int index = 0; index < length; index++) {
				types[index] = getReference(classes[index]);
			}
			return types;
		}


		/**
		 * Returns the reference class array of the given object array
		 *
		 * @param objects Given object array
		 *
		 * @return The reference class array
		 */
		public static Class<?>[] getReference(Object[] objects) {
			int length = objects == null ? 0 : objects.length;
			Class<?>[] types = new Class<?>[length];
			for (int index = 0; index < length; index++) {
				types[index] = getReference(objects[index].getClass());
			}
			return types;
		}


		/**
		 * Returns the primitive class of this data type
		 *
		 * @return The primitive class
		 */
		public Class<?> getPrimitive() {
			return primitive;
		}


		/**
		 * Returns the reference class of this data type
		 *
		 * @return The reference class
		 */
		public Class<?> getReference() {
			return reference;
		}
	}

	/**
	 * Represents an enumeration of dynamic packages of NMS and CraftBukkit
	 * <p>
	 * This class is part of the <b>ReflectionUtils</b> and follows the same usage conditions
	 *
	 * @author DarkBlade12
	 * @since 1.0
	 */
	public enum PackageType {
		MINECRAFT_SERVER("net.minecraft.server." + getServerVersion()),
		CRAFTBUKKIT("org.bukkit.craftbukkit." + getServerVersion()),
		CRAFTBUKKIT_BLOCK(CRAFTBUKKIT, "block"),
		CRAFTBUKKIT_CHUNKIO(CRAFTBUKKIT, "chunkio"),
		CRAFTBUKKIT_COMMAND(CRAFTBUKKIT, "command"),
		CRAFTBUKKIT_CONVERSATIONS(CRAFTBUKKIT, "conversations"),
		CRAFTBUKKIT_ENCHANTMENS(CRAFTBUKKIT, "enchantments"),
		CRAFTBUKKIT_ENTITY(CRAFTBUKKIT, "entity"),
		CRAFTBUKKIT_EVENT(CRAFTBUKKIT, "event"),
		CRAFTBUKKIT_GENERATOR(CRAFTBUKKIT, "generator"),
		CRAFTBUKKIT_HELP(CRAFTBUKKIT, "help"),
		CRAFTBUKKIT_INVENTORY(CRAFTBUKKIT, "inventory"),
		CRAFTBUKKIT_MAP(CRAFTBUKKIT, "map"),
		CRAFTBUKKIT_METADATA(CRAFTBUKKIT, "metadata"),
		CRAFTBUKKIT_POTION(CRAFTBUKKIT, "potion"),
		CRAFTBUKKIT_PROJECTILES(CRAFTBUKKIT, "projectiles"),
		CRAFTBUKKIT_SCHEDULER(CRAFTBUKKIT, "scheduler"),
		CRAFTBUKKIT_SCOREBOARD(CRAFTBUKKIT, "scoreboard"),
		CRAFTBUKKIT_UPDATER(CRAFTBUKKIT, "updater"),
		CRAFTBUKKIT_UTIL(CRAFTBUKKIT, "wetspongeutils");

		private final String path;


		/**
		 * Construct a new package type
		 *
		 * @param parent Parent package of the package
		 * @param path Path of the package
		 */
		PackageType(PackageType parent, String path) {
			this(parent + "." + path);
		}


		/**
		 * Construct a new package type
		 *
		 * @param path Path of the package
		 */
		PackageType(String path) {
			this.path = path;
		}


		/**
		 * Returns the version of your server
		 *
		 * @return The server version
		 */
		public static String getServerVersion() {
			return org.bukkit.Bukkit.getServer().getClass().getPackage().getName().substring(23);
		}


		/**
		 * Returns the class with the given name
		 *
		 * @param className Name of the desired class
		 *
		 * @return The class with the specified name
		 *
		 * @throws ClassNotFoundException If the desired class with the specified name and package cannot be found
		 */
		public Class<?> getClass(String className) throws ClassNotFoundException {
			return Class.forName(this + "." + className);
		}


		/**
		 * Returns the path of this package type
		 *
		 * @return The path
		 */
		public String getPath() {
			return path;
		}


		// Override for convenience
		@Override
		public String toString() {
			return path;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T extends Enum<?>> T addEnum(Class<T> enumType, String enumName) {

		// 0. Sanity checks
		if (!Enum.class.isAssignableFrom(enumType)) {
			throw new RuntimeException("class " + enumType + " is not an instance of Enum");
		}
		// 1. Lookup "$VALUES" holder in enum class and get previous enum instances
		Field valuesField = null;
		Field[] fields = enumType.getDeclaredFields();
		for (Field field : fields) {
			if (field.getName().contains("$VALUES")) {
				valuesField = field;
				break;
			}
		}
		AccessibleObject.setAccessible(new Field[]{valuesField}, true);

		try {

			// 2. Copy it
			T[] previousValues = (T[]) valuesField.get(enumType);
			List values = new ArrayList(Arrays.asList(previousValues));

			// 3. build new enum
			T newValue = (T) makeEnum(enumType, // The target enum class
				enumName, // THE NEW ENUM INSTANCE TO BE DYNAMICALLY ADDED
				values.size(), new Class<?>[]{}, // can be used to pass values to the enum constuctor
				new Object[]{}); // can be used to pass values to the enum constuctor

			// 4. add new value
			values.add(newValue);

			// 5. Set new values field
			setFailsafeFieldValue(valuesField, null, values.toArray((T[]) Array.newInstance(enumType, 0)));

			// 6. Clean enum cache
			cleanEnumCache(enumType);
			return newValue;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private static Object makeEnum(Class<?> enumClass, String value, int ordinal, Class<?>[] additionalTypes, Object[] additionalValues) throws Exception {
		Object[] parms = new Object[additionalValues.length + 2];
		parms[0] = value;
		parms[1] = Integer.valueOf(ordinal);
		System.arraycopy(additionalValues, 0, parms, 2, additionalValues.length);
		return enumClass.cast(getConstructorAccessor(enumClass, additionalTypes).newInstance(parms));
	}

	private static ConstructorAccessor getConstructorAccessor(Class<?> enumClass, Class<?>[] additionalParameterTypes) throws NoSuchMethodException {
		Class<?>[] parameterTypes = new Class[additionalParameterTypes.length + 2];
		parameterTypes[0] = String.class;
		parameterTypes[1] = int.class;
		System.arraycopy(additionalParameterTypes, 0, parameterTypes, 2, additionalParameterTypes.length);
		return ReflectionFactory.getReflectionFactory().newConstructorAccessor(enumClass.getDeclaredConstructor(parameterTypes));
	}

	public static void setFailsafeFieldValue(Field field, Object target, Object value) throws NoSuchFieldException, IllegalAccessException {

		// let's make the field accessible
		field.setAccessible(true);

		// next we change the modifier in the Field instance to
		// not be final anymore, thus tricking reflection into
		// letting us modify the static final field
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		int modifiers = modifiersField.getInt(field);

		// blank out the final bit in the modifiers int
		modifiers &= ~Modifier.FINAL;
		modifiersField.setInt(field, modifiers);

		FieldAccessor fa = ReflectionFactory.getReflectionFactory().newFieldAccessor(field, false);
		fa.set(target, value);
	}

	private static void blankField(Class<?> enumClass, String fieldName) throws NoSuchFieldException, IllegalAccessException {
		for (Field field : Class.class.getDeclaredFields()) {
			if (field.getName().contains(fieldName)) {
				AccessibleObject.setAccessible(new Field[]{field}, true);
				setFailsafeFieldValue(field, enumClass, null);
				break;
			}
		}
	}

	private static void cleanEnumCache(Class<?> enumClass) throws NoSuchFieldException, IllegalAccessException {
		blankField(enumClass, "enumConstantDirectory"); // Sun (Oracle?!?) JDK 1.5/6
		blankField(enumClass, "enumConstants"); // IBM JDK
	}
}