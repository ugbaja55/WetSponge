package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.item.SpigotNBTTagUpdater;
import com.degoos.wetsponge.util.InternalLogger;

public class NBTTagUtils {

	public static Object newNBTTagCompound() {
		try {
			return NMSUtils.getNMSClass("NBTTagCompound").newInstance();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a new NBTTagCompound!");
			return null;
		}
	}

	public static Object parseNBTTag(String nbtTag) {
		try {
			return NMSUtils.getNMSClass("MojangsonParser").getMethod("parse", String.class).invoke(null, SpigotNBTTagUpdater.update(nbtTag));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing a NBTTagCompound!");
			return null;
		}
	}

}
