package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumEnvironment {

    /**
     * The default minecraft overworld world.
     */
    OVERWORLD(0),
    /**
     * The default minecraft nether world.
     */
    THE_NETHER(-1),
    /**
     * The default minecraft the end world.
     */
    THE_END(1),
    /**
     * The default minecraft the void world. Only compatible on {@link EnumServerType#SPONGE Sponge}.
     * If you use this on {@link EnumServerType#SPIGOT Spigot}, {@link EnumEnvironment#OVERWORLD} will be used.
     */
    THE_VOID(0),
    /**
     * A {@link EnumServerType#SPONGE Sponge} easter egg. Only compatible on {@link EnumServerType#SPONGE Sponge}.
     * If you use this on {@link EnumServerType#SPIGOT Spigot}, {@link EnumEnvironment#OVERWORLD} will be used.
     */
    THE_SKYLANDS(0);

    private int value;

    EnumEnvironment(int value) {
        this.value = value;
    }

    public static Optional<EnumEnvironment> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public int getValue() {
        return value;
    }

}
