package com.degoos.wetsponge.enums;


public enum EnumEquipType {

	HELMET, CHESTPLATE, LEGGINGS, BOOTS, MAIN_HAND, OFF_HAND

}
