package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumCriteria {

    HEALTH("health", "HEALTH"),
    PLAYER_KILLS("playerKillCount", "PLAYER_KILLS"),
    TOTAL_KILLS("totalKillCount", "TOTAL_KILLS"),
    DEATHS("deathCount", "DEATHS"),
    DUMMY("dummy", "DUMMY"),
    TRIGGER("trigger", "TRIGGER");

    private String spigotName, spongeName;

    EnumCriteria(String spigotName, String spongeName) {
        this.spigotName = spigotName;
        this.spongeName = spongeName;
    }

    public static Optional<EnumCriteria> getBySpigotName(String name) {
        if (name == null) return Optional.of(DUMMY);
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
    }

    public static Optional<EnumCriteria> getBySpongeName(String name) {
        if (name == null) return Optional.of(DUMMY);
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return spongeName;
    }
}
