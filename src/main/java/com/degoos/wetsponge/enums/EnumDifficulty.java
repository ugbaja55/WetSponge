package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumDifficulty {

    PEACEFUL(0), EASY(1), NORMAL(2), HARD(3);

    private int value;

    EnumDifficulty(int value) {
        this.value = value;
    }

    public static Optional<EnumDifficulty> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public int getValue() {
        return value;
    }

}
