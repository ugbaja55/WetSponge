package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumHealthModifierType {

    ABSORPTION,
    ARMOR,
    DEFENSIVE_POTION_EFFECT,
    DIFFICULTY,
    MAGIC,
    OFFENSIVE_POTION_EFFECT,
    WEAPON_ENCHANTMENT;

    public static Optional<EnumHealthModifierType> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }
}
