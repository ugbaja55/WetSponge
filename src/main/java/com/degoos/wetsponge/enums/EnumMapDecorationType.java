package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumMapDecorationType {

	PLAYER(false),
	FRAME(true),
	RED_MARKER(false),
	BLUE_MARKER(false),
	TARGET_X(true),
	TARGET_POINT(true),
	PLAYER_OFF_MAP(false),
	PLAYER_OFF_LIMITS(false),
	MANSION(true, 5393476),
	MONUMENT(true, 3830373);

	private byte id;
	private boolean renderedOnFrame;
	private int mapColor;

	EnumMapDecorationType(boolean renderedOnFrame) {
		this(renderedOnFrame, -1);
	}

	EnumMapDecorationType(boolean renderedOnFrame, int mapColor) {
		this.id = (byte) this.ordinal();
		this.renderedOnFrame = renderedOnFrame;
		this.mapColor = mapColor;
	}

	public byte getId() {
		return id;
	}

	public boolean isRenderedOnFrame() {
		return renderedOnFrame;
	}

	public int getMapColor() {
		return mapColor;
	}

	public static Optional<EnumMapDecorationType> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.id == id).findAny();
	}
}
