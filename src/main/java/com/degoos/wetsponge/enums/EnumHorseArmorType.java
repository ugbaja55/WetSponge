package com.degoos.wetsponge.enums;

import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.material.itemType.WSItemTypes;
import java.util.Arrays;
import java.util.Optional;

public enum EnumHorseArmorType {

	NONE(WSBlockTypes.AIR.getDefaultType()),
	IRON(WSItemTypes.IRON_HORSE_ARMOR.getDefaultType()),
	GOLD(WSItemTypes.GOLD_HORSE_ARMOR.getDefaultType()),
	DIAMOND(WSItemTypes.DIAMOND_HORSE_ARMOR.getDefaultType());

	private WSMaterial itemType;

	EnumHorseArmorType(WSMaterial itemType) {
		this.itemType = itemType;
	}

	public WSItemStack getArmorItemStack() {
		return WSItemStack.of(itemType);
	}

	public static EnumHorseArmorType getByMaterial(int id) {
		switch (id) {
			case 417:
				return IRON;
			case 418:
				return GOLD;
			case 419:
				return DIAMOND;
			default:
				return NONE;
		}
	}

	public static Optional<EnumHorseArmorType> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}
}
