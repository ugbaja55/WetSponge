package com.degoos.wetsponge.enums;

public enum EnumTristate {

    TRUE(true) {
        @Override
        public EnumTristate and(EnumTristate other) {
            return other == TRUE || other == UNDEFINED ? TRUE : FALSE;
        }

        @Override
        public EnumTristate or(EnumTristate other) {
            return TRUE;
        }
    },
    FALSE(false) {
        @Override
        public EnumTristate and(EnumTristate other) {
            return FALSE;
        }

        @Override
        public EnumTristate or(EnumTristate other) {
            return other == TRUE ? TRUE : FALSE;
        }
    },
    UNDEFINED(false) {
        @Override
        public EnumTristate and(EnumTristate other) {
            return other;
        }

        @Override
        public EnumTristate or(EnumTristate other) {
            return other;
        }
    };

    private final boolean val;

    EnumTristate(boolean val) {
        this.val = val;
    }


    public static EnumTristate fromBoolean(boolean val) {
        return val ? TRUE : FALSE;
    }

    public abstract EnumTristate and(EnumTristate other);

    public abstract EnumTristate or(EnumTristate other);

    public boolean toBoolean() {
        return this.val;
    }

}
