package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumResourcePackStatus {

	/**
	 * The client is attempting to download the pack.
	 */
	ACCEPTED("ACCEPTED"),

	/**
	 * The client declined to download the pack.
	 */
	DECLINED("DECLINED"),

	/**
	 * The client failed to download the resource pack.
	 *
	 * In some client versions, such as 1.8.0, this may only be sent when
	 * the resource pack URL does not end in <tt>.zip</tt>. Otherwise,
	 * {@link #SUCCESSFULLY_LOADED} will be sent.
	 */
	FAILED("FAILED_DOWNLOAD"),

	/**
	 * The pack URI was successfully loaded. This does not mean that pack
	 * was loaded, as the vanilla client sends this even when encountering a
	 * 404 or similar.
	 */
	SUCCESSFULLY_LOADED("SUCCESSFULLY_LOADED");

	private String spigotName;


	EnumResourcePackStatus (String spigotName) {
		this.spigotName = spigotName;
	}


	public String getSpigotName () {
		return spigotName;
	}


	public static Optional<EnumResourcePackStatus> getBySpigotName (String spigotName) {
		return Arrays.stream(values()).filter(target -> target.getSpigotName().equals(spigotName)).findAny();
	}


	public static Optional<EnumResourcePackStatus> getBySpongeName (String spongeName) {
		return Arrays.stream(values()).filter(target -> target.name().equals(spongeName)).findAny();
	}
}
