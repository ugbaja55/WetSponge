package com.degoos.wetsponge.enums;


public enum EnumInventoryRows {

	ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6);

	private int rows;


	EnumInventoryRows (int rows) {
		this.rows = rows;
	}


	public static EnumInventoryRows getBySize (int size) {
		return getByRows(size / 9);
	}


	public static EnumInventoryRows getByRows (int rows) {
		switch (rows / 9) {
			case 1:
				return ONE;
			case 2:
				return TWO;
			case 3:
				return THREE;
			case 4:
				return FOUR;
			case 5:
				return FIVE;
			case 6:
				return SIX;
		}
		return ONE;
	}


	public int getRows () {
		return rows;
	}
}
