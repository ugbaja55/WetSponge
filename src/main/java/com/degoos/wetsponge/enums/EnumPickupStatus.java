package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumPickupStatus {

	DISALLOWED, ALLOWED, CREATIVE_ONLY;

	public static Optional<EnumPickupStatus> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}
}
