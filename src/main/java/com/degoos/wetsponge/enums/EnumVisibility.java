package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumVisibility {

    ALWAYS("ALWAYS", "ALWAYS"),
    HIDE_FOR_OTHER_TEAMS("FOR_OWN_TEAM", "HIDE_FOR_OTHER_TEAMS"),
    HIDE_FOR_OWN_TEAM("FOR_OTHER_TEAMS", "HIDE_FOR_OWN_TEAM"),
    NEVER("NEVER", "NEVER");

    private String spigotName, spongeName;

    EnumVisibility(String spigotName, String spongeName) {
        this.spigotName = spigotName;
        this.spongeName = spongeName;
    }

    public static Optional<EnumVisibility> getBySpigotName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
    }

    public static Optional<EnumVisibility> getBySpongeName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return spongeName;
    }
}
