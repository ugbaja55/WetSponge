package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumLlamaType {

    CREAMY, WHITE, BROWN, GRAY;

    public static Optional<EnumLlamaType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

}
