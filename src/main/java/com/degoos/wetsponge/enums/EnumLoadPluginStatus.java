package com.degoos.wetsponge.enums;

public enum  EnumLoadPluginStatus {

    LOADED, ALREADY_LOADED, ERROR

}
