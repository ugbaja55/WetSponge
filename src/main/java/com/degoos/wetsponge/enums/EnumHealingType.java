package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumHealingType {

    FOOD("EATING", "SATIATED"),
    GENERIC("REGEN", "WITHER_SPAWN", "WITHER", "CUSTOM"),
    MAGIC("MAGIC", "MAGIC_REGEN", "ENDER_CRYSTAL");

    private String mainSpigotName;
    private String[] spigotNames;

    EnumHealingType(String... spigotNames) {
        mainSpigotName = spigotNames[0];
        this.spigotNames = spigotNames;
    }

    public String getMainSpigotName() {
        return mainSpigotName;
    }

    public String[] getSpigotNames() {
        return spigotNames.clone();
    }

    public static Optional<EnumHealingType> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equals(name)).findAny();
    }

    public static Optional<EnumHealingType> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> Arrays.stream(target.spigotNames).anyMatch(spigotName -> spigotName.equals(name))).findAny();
    }

}
