package com.degoos.wetsponge.enums;

import com.degoos.wetsponge.util.reflection.NMSUtils;

public enum EnumSoundCategory {


    MASTER("MASTER"),
    BLOCKS("BLOCK"),
    HOSTILE("HOSTILE"),
    AMBIENT("AMBIENT"),
    MUSIC("MUSIC"),
    NEUTRAL("NEUTRAL"),
    PLAYERS("PLAYER"),
    RECORDS("RECORD"),
    VOICE("VOICE"),
    WEATHER("WEATHER");

    private String spongeName;

    EnumSoundCategory(String spongeName) {
        this.spongeName = spongeName;
    }

    public String getSpongeName() {
        return spongeName;
    }


    public String getSpigotName() {
        return name();
    }


    public Object getSpigotHandle() {
        try {
            return NMSUtils.getNMSClass("SoundCategory").getMethod("valueOf", String.class).invoke(null, name());
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }


}
