package com.degoos.wetsponge.enums;

import java.awt.Color;
import java.util.Arrays;
import java.util.Optional;

public enum EnumMapIllumination {

	DARKEST(3, 135), DARKER(0, 180), DARK(1, 220), NORMAL(2, 255);

	private byte id;
	private double multiply;

	EnumMapIllumination(int id, int mult) {
		this.multiply = mult / 255.0D;
		this.id = (byte) id;
	}

	public byte getId() {
		return id;
	}

	public Color maskColor(Color color) {
		return new Color((int) (color.getRed() * multiply), (int) (color.getGreen() * multiply), (int) (color.getBlue() * multiply));
	}

	public static Optional<EnumMapIllumination> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.id == id).findAny();
	}
}
