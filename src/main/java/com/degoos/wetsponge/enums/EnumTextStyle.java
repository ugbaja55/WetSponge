package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumTextStyle {

    OBFUSCATED('k'), BOLD('l'), STRIKETHROUGH('m'), UNDERLINE('n'), ITALIC('o'), RESET('r');

    private char id;


    EnumTextStyle(char id) {
        this.id = id;
    }


    public static Optional<EnumTextStyle> getByChar(char c) {
        return Arrays.stream(values()).filter(enumTextStyle -> enumTextStyle.getId() == c).findAny();
    }


    public static Optional<EnumTextStyle> getByName(String name) {
        return Arrays.stream(values()).filter(enumTextStyle -> enumTextStyle.name().equalsIgnoreCase(name)).findAny();
    }


    public char getId() {
        return id;
    }

    @Override
    public String toString() {
        return "\u00A7" + id;
    }
}
