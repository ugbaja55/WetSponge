package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumDirtType {

	DIRT(0), COARSE_DIRT(1), PODZOL(2);

	private int value;


	EnumDirtType (int value) {
		this.value = value;
	}


	public static Optional<EnumDirtType> getByValue (int value) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.getValue() == value).findAny();
	}

	public static Optional<EnumDirtType> getByName (String name) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}
