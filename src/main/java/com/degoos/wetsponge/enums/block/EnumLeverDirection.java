package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumLeverDirection {

    BOTTOM_EAST(0, EnumBlockAxis.X, EnumBlockDirection.DOWN),
    EAST(1, EnumBlockAxis.Y, EnumBlockDirection.EAST),
    WEST(2, EnumBlockAxis.Y, EnumBlockDirection.WEST),
    SOUTH(3, EnumBlockAxis.Y, EnumBlockDirection.SOUTH),
    NORTH(4, EnumBlockAxis.Y, EnumBlockDirection.NORTH),
    TOP_SOUTH(5, EnumBlockAxis.Z, EnumBlockDirection.UP),
    TOP_EAST(6, EnumBlockAxis.X, EnumBlockDirection.UP),
    BOTTOM_SOUTH(7, EnumBlockAxis.Z, EnumBlockDirection.DOWN);

    private int value;
    private EnumBlockAxis axis;
    private EnumBlockDirection direction;


    EnumLeverDirection(int value, EnumBlockAxis axis, EnumBlockDirection direction) {
        this.value = value;
        this.axis = axis;
        this.direction = direction;
    }

    public static Optional<EnumLeverDirection> getByAxisAndDirection(EnumBlockAxis axis, EnumBlockDirection direction) {
        return Arrays.stream(values()).filter(target -> target.getAxis() == axis && target.getDirection() == direction).findAny();
    }

    public static Optional<EnumLeverDirection> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public static Optional<EnumLeverDirection> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public int getValue() {
        return value;
    }

    public EnumBlockAxis getAxis() {
        return axis;
    }

    public EnumBlockDirection getDirection() {
        return direction;
    }


}
