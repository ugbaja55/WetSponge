package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockOrientation {

    SOUTH(0, "SOUTH"),
    SOUTH_SOUTHWEST(1, "SOUTH_SOUTH_WEST"),
    SOUTHWEST(2, "SOUTH_WEST"),
    WEST_SOUTHWEST(3, "WEST_SOUTH_WEST"),
    WEST(4, "WEST"),
    WEST_NORTHWEST(5, "WEST_NORTH_WEST"),
    NORTHWEST(6, "NORTH_WEST"),
    NORTH_NORTHWEST(7, "NORTH_NORTH_WEST"),
    NORTH(8, "NORTH"),
    NORTH_NORTHEAST(9, "NORTH_NORTH_EAST"),
    NORTHEAST(10, "NORTH_EAST"),
    EAST_NORTHEAST(11, "EAST_NORTH_EAST"),
    EAST(12, "EAST"),
    EAST_SOUTHEAST(13, "EAST_SOUTH_EAST"),
    SOUTHEAST(14, "SOUTH_EAST"),
    SOUTH_SOUTHEAST(15, "SOUTH_SOUTH_EAST");

    private int value;
    private String spigotName;

    EnumBlockOrientation(int value, String spigotName) {
        this.value = value;
        this.spigotName = spigotName;
    }


    public int getValue() {
        return value;
    }

    public String getSpigotName() {
        return spigotName;
    }

    public static Optional<EnumBlockOrientation> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public static Optional<EnumBlockOrientation> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumBlockOrientation> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }
}
