package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumStoneBrickType {

    DEFAULT(0), MOSSY(1), CRACKED(2), CHISELED(3);

    private int value;

    EnumStoneBrickType(int value) {
        this.value = value;
    }


    public static Optional<EnumStoneBrickType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumStoneBrickType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
