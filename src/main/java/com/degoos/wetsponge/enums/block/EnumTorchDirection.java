package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumTorchDirection {

	EAST(1), WEST(2), SOUTH(3), NORTH(4), UP(5);

	private int value;


	EnumTorchDirection(int value) {
		this.value = value;
	}


	public static Optional<EnumTorchDirection> getByValue(int value) {
		return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
	}


	public static Optional<EnumTorchDirection> getByName(String name) {
		return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}

}
