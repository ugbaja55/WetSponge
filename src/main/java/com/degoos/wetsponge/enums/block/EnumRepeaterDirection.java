package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumRepeaterDirection {

    NORTH(0), EAST(1), SOUTH(2), WEST(3);

    private int value;


    EnumRepeaterDirection(int value) {
        this.value = value;
    }


    public static Optional<EnumRepeaterDirection> getByValue(int value) {
        return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
    }


    public static Optional<EnumRepeaterDirection> getByName(String name) {
        return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
