package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumDisguisedBlockType {

    STONE(0), COBBLESTONE(1), STONEBRICK(2), MOSSY_STONEBRICK(3), CRACKED_STONEBRICK(4), CHISELED_STONEBRICK(5);

    private int value;

    EnumDisguisedBlockType(int value) {
        this.value = value;
    }


    public static Optional<EnumDisguisedBlockType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumDisguisedBlockType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
