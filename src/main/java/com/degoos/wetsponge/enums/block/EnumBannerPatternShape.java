package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBannerPatternShape {

	BASE("b"),
	SQUARE_BOTTOM_LEFT("bl"),
	SQUARE_BOTTOM_RIGHT("br"),
	SQUARE_TOP_LEFT("tl"),
	SQUARE_TOP_RIGHT("tr"),
	STRIPE_BOTTOM("bs"),
	STRIPE_TOP("ts"),
	STRIPE_LEFT("ls"),
	STRIPE_RIGHT("rs"),
	STRIPE_CENTER("cs"),
	STRIPE_MIDDLE("ms"),
	STRIPE_DOWNRIGHT("drs"),
	STRIPE_DOWNLEFT("dls"),
	STRIPE_SMALL("ss"),
	CROSS("cr"),
	STRAIGHT_CROSS("sc"),
	TRIANGLE_BOTTOM("bt"),
	TRIANGLE_TOP("tt"),
	TRIANGLES_BOTTOM("bts"),
	TRIANGLES_TOP("tts"),
	DIAGONAL_LEFT("ld"),
	DIAGONAL_RIGHT("rd"),
	DIAGONAL_LEFT_MIRROR("lud"),
	DIAGONAL_RIGHT_MIRROR("rud"),
	CIRCLE_MIDDLE("mc"),
	RHOMBUS_MIDDLE("mr"),
	HALF_VERTICAL("vh"),
	HALF_HORIZONTAL("hh"),
	HALF_VERTICAL_MIRROR("vhr"),
	HALF_HORIZONTAL_MIRROR("hhb"),
	BORDER("bo"),
	CURLY_BORDER("cbo"),
	CREEPER("cre"),
	GRADIENT("gra"),
	GRADIENT_UP("gru"),
	BRICKS("bri"),
	SKULL("sku"),
	FLOWER("flo"),
	MOJANG("moj");

	private String code;

	EnumBannerPatternShape(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public static Optional<EnumBannerPatternShape> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumBannerPatternShape> getByCode(String code) {
		return Arrays.stream(values()).filter(target -> target.getCode().equalsIgnoreCase(code)).findAny();
	}
}
