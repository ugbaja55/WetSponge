package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockAxis {

    NONE(3), X(1), Y(0), Z(2);

    private int value;


    EnumBlockAxis(int value) {
        this.value = value;
    }


    public static Optional<EnumBlockAxis> getByValue(int value) {
        return Arrays.stream(values()).filter(logAxis -> logAxis.getValue() == value).findAny();
    }

    public static Optional<EnumBlockAxis> getByName(String name) {
        return Arrays.stream(values()).filter(logAxis -> logAxis.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
