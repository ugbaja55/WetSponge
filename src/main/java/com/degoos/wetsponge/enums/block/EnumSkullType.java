package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumSkullType {

    CREEPER("CREEPER", "CREEPER", 4),
    ENDER_DRAGON("ENDER_DRAGON", "DRAGON", 5),
    PLAYER("PLAYER", "PLAYER", 3),
    SKELETON("SKELETON", "SKELETON", 0),
    WITHER_SKELETON("WITHER_SKELETON", "WITHER", 1),
    ZOMBIE("ZOMBIE", "ZOMBIE", 2);

    private String spigotName, spongeName;
    private int value;

    EnumSkullType(String spigotName, String spongeName, int value) {
        this.spigotName = spigotName;
        this.spongeName = spongeName;
        this.value = value;
    }

    public static Optional<EnumSkullType> getBySpigotName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
    }

    public static Optional<EnumSkullType> getBySpongeName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
    }

    public static Optional<EnumSkullType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> value == target.getValue()).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return spongeName;
    }

    public int getValue() {
        return value;
    }
}
