package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumTallGrassType {

	DEAD_BUSH(0), TALL_GRASS(1), FERN(2);

	private int value;


	EnumTallGrassType (int value) {
		this.value = value;
	}


	public static Optional<EnumTallGrassType> getByValue (int value) {
		return Arrays.stream(values()).filter(tallGrassType -> tallGrassType.getValue() == value).findAny();
	}


	public static Optional<EnumTallGrassType> getByName (String name) {
		return Arrays.stream(values()).filter(tallGrassType -> tallGrassType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}
