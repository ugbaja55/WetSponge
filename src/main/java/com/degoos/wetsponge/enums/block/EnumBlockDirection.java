package com.degoos.wetsponge.enums.block;


import com.flowpowered.math.vector.Vector3i;
import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockDirection {

	DOWN(0, new Vector3i(0, -1, 0)),
	UP(1, new Vector3i(0, 1, 0)),
	NORTH(2, new Vector3i(0, 0, -1)),
	SOUTH(3, new Vector3i(0, 0, 1)),
	WEST(4, new Vector3i(-1, 0, 0)),
	EAST(5, new Vector3i(1, 0, 0)),
	NONE(6, new Vector3i(0, 0, 0));

	private int value;
	private Vector3i relative;


	EnumBlockDirection(int value, Vector3i relative) {
		this.value = value;
		this.relative = relative;
	}


	public static Optional<EnumBlockDirection> getByValue(int value) {
		return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
	}


	public static Optional<EnumBlockDirection> getByName(String name) {
		return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue() {
		return value;
	}

	public Vector3i getRelative() {
		return relative;
	}
}
