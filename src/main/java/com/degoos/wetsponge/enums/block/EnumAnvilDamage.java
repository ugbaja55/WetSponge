package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumAnvilDamage {

    NORMAL(0), DAMAGED(1), VERY_DAMAGED(2);

    private int value;

    EnumAnvilDamage(int value) {
        this.value = value;
    }

    public static Optional<EnumAnvilDamage> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public static Optional<EnumAnvilDamage> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public int getValue() {
        return value;
    }


}
