package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumVineDirection {

    SOUTH(1), WEST(2), NORTH(4), EAST(8);

    private int value;


    EnumVineDirection(int value) {
        this.value = value;
    }


    public static Optional<EnumVineDirection> getByValue(int value) {
        return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
    }


    public static Optional<EnumVineDirection> getByName(String name) {
        return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
