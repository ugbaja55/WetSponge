package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumStairDirection {

	EAST(0), WEST(1), SOUTH(2), NORTH(3);

	private int value;


	EnumStairDirection(int value) {
		this.value = value;
	}


	public static Optional<EnumStairDirection> getByValue(int value) {
		return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
	}


	public static Optional<EnumStairDirection> getByName(String name) {
		return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}

}
