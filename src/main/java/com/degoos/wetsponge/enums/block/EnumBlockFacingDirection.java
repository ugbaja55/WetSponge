package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockFacingDirection {

    SOUTH(0), WEST(1), NORTH(2), EAST(3);

    private int value;


    EnumBlockFacingDirection(int value) {
        this.value = value;
    }


    public static Optional<EnumBlockFacingDirection> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumBlockFacingDirection> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
