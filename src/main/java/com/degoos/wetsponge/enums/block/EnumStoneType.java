package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumStoneType {

	STONE(0), GRANITE(1), SMOOTH_GRANITE(2), DIORITE(3), SMOOTH_DIORITE(4), ANDESITE(5), SMOOTH_ANDESITE(6);

	private int value;


	EnumStoneType (int value) {
		this.value = value;
	}


	public static Optional<EnumStoneType> getByValue (int value) {
		return Arrays.stream(values()).filter(stoneType -> stoneType.getValue() == value).findAny();
	}

	public static Optional<EnumStoneType> getByName (String name) {
		return Arrays.stream(values()).filter(stoneType -> stoneType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}
