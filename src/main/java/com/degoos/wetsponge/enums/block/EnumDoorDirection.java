package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumDoorDirection {

	EAST(0), NORTH(3), SOUTH(1), WEST(2);

	private int value;


	EnumDoorDirection(int value) {
		this.value = value;
	}


	public static Optional<EnumDoorDirection> getByValue(int value) {
		return Arrays.stream(values()).filter(facing -> facing.getValue() == value).findAny();
	}


	public static Optional<EnumDoorDirection> getByName(String name) {
		return Arrays.stream(values()).filter(facing -> facing.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue() {
		return value;
	}
}
