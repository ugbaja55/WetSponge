package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumRabbitType {

	BROWN, WHITE, BLACK, BLACK_AND_WHITE, GOLD, SALT_AND_PEPPER, THE_KILLER_BUNNY("KILLER");

	private String spigotName, spongeName;


	EnumRabbitType () {
		this.spigotName = name();
		this.spongeName = name();
	}


	EnumRabbitType (String spongeName) {
		this.spigotName = name();
		this.spongeName = spongeName;
	}


	public static Optional<EnumRabbitType> getRabbitType (String name) {
		if (name.equalsIgnoreCase("KILLER")) return Optional.of(THE_KILLER_BUNNY);
		else return Arrays.stream(values()).filter(enumRabbitType -> enumRabbitType.name().equalsIgnoreCase(name)).findAny();
	}


	public String getSpigotName () {
		return spigotName;
	}


	public String getSpongeName () {
		return spongeName;
	}
}
