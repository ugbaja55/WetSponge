package com.degoos.wetsponge.enums;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import java.util.Arrays;
import java.util.Optional;

public enum EnumEntityType {

	AREA_EFFECT_CLOUD("area_effect_cloud", 3, "other.AreaEffectCloud"),
	ARMOR_STAND("armor_stand", 30, "living.ArmorStand"),
	ARROW("arrow", 10, "projectile.Arrow"),
	BAT("bat", 65, "living.ambient.Bat"),
	BLAZE("blaze", 61, "living.monster.Blaze"),
	BOAT("boat", 41, "vehicle.Boat"),
	CAVE_SPIDER("cave_spider", 59, "living.monster.CaveSpider"),
	CHESTED_MINECART("chest_minecart", 43, "MinecartChest"),
	CHICKEN("chicken", 93, "living.animal.Chicken"),
	COMMANDBLOCK_MINECART("commandblock_minecart", 40, "MinecartCommand"),
	COMPLEX_PART(null, -1, false, "living.complex.ComplexLivingPart"),
	COW("cow", 92, "living.animal.Cow"),
	CREEPER("creeper", 50, "living.monster.Creeper"),
	DONKEY("donkey", 31, "living.animal.Donkey"),
	DRAGON_FIREBALL("dragon_fireball", 26, "SmallFireball"),
	EGG("egg", 7, "projectile.Egg"),
	ELDER_GUARDIAN("elder_guardian", 4, "ElderGuardian"),
	ENDERMAN("enderman", 58, "living.monster.Enderman"),
	ENDERMITE("endermite", 67, "Endermite"),
	ENDER_CRYSTAL("ender_crystal", 200, "EnderCrystal"),
	ENDER_DRAGON("ender_dragon", 63, "living.complex.EnderDragon"),
	ENDER_PEARL("ender_pearl", 14, "EnderPearl"),
	EVOCATION_FANGS("evocation_fangs", 33, "EvocationFangs"),
	EVOCATION_ILLAGER("evocation_illager", 34, "EvocationIllager"),
	EXPERIENCE_ORB("xp_orb", 2, "ExperienceOrb"),
	EYE_OF_ENDER("eye_of_ender_signal", 15, "EnderSignal"),
	FALLING_BLOCK("falling_block", 21, false, "other.FallingBlock"),
	FIREBALL("fireball", 12, "projectile.Fireball"),
	FIREWORK("fireworks_rocket", 22, false, "other.Firework"),
	FISHING_HOOK(null, -1, false, "FishingHook"),
	FURNACE_MINECART("chest_minecart", 44, "MinecartFurnace"),
	GHAST("ghast", 56, "living.aerial.Ghast"),
	GIANT("giant", 53, "living.monster.Giant"),
	GUARDIAN("guardian", 68, "living.monster.Guardian"),
	HOPPER_MINECART("hopper_minecart", 46, "MinecartHopper"),
	HORSE("horse", 100, "living.animal.Horse"),
	HUSK("husk", 23, "Husk"),
	IRON_GOLEM("villager_golem", 99, "living.golem.IronGolem"),
	ITEM("item", 1, false, "other.Item"),
	ITEM_FRAME("item_frame", 18, "hanging.ItemFrame"),
	LEASH_HITCH("leash_knot", 8, "LeashHitch"),
	LIGHTNING(null, -1, false, "Lightning"),
	LINGERING_POTION(null, -1, false, "LingeringPotion"),
	LLAMA("llama", 103, "living.animal.Llama"),
	LLAMA_SPIT("llama_spit", 104, "LlamaSplit"),
	MAGMA_CUBE("magma_cube", 62, "MagmaCube"),
	MOB_SPAWNER_MINECART("spawner_minecart", 47, "MinecartMobSpawner"),
	MULE("mule", 32, "Mule"),
	MUSHROOM_COW("mooshroom", 96, "living.animal.Mooshroom"),
	OCELOT("ocelot", 98, "living.animal.Ocelot"),
	PAINTING("painting", 9, "hanging.Painting"),
	PIG("pig", 90, "living.animal.Pig"),
	PIG_ZOMBIE("zombie_pigman", 57, "ZombiePigman"),
	PLAYER(null, -1, false, "living.player.Player"),
	POLAR_BEAR("polar_bear", 102, "living.animal.PolarBear"),
	PRIMED_TNT("tnt", 20, "explosive.PrimedTNT"),
	RABBIT("rabbit", 101, "living.animal.Rabbit"),
	RIDEABLE_MINECART("minecart", 42, "Boat"),
	SHEEP("sheep", 91, "living.animal.Sheep"),
	SHULKER("shulker", 69, "Shulker"),
	SHULKER_BULLET("shulker_bullet", 25, "ShulkerBullet"),
	SILVERFISH("silverfish", 60, "Silverfish"),
	SKELETON("skeleton", 51, "living.monster.Skeleton"),
	SKELETON_HORSE("skeleton_horse", 28, "SkeletonHorse"),
	SLIME("slime", 55, "living.monster.Slime"),
	SMALL_FIREBALL("small_fireball", 13, "SmallFireball"),
	SNOWBALL("snowball", 11, "projectile.Snowball"),
	SNOWMAN("snowman", 97, "living.golem.SnowGolem"),
	SPECTRAL_ARROW("spectral_arrow", 24, "SpectralArrow"),
	SPIDER("spider", 52, "living.monster.Spider"),
	SPLASH_POTION("potion", 16, false, "SplashPotion"),
	SQUID("squid", 94, "living.aquatic.Squid"),
	STRAY("stray", 6, "Stray"),
	THROWN_EXP_BOTTLE("xp_bottle", 17, "ThrownExpBottle"),
	TIPPED_ARROW("TippedArrow", -1, "projectile.TippedArrow"),
	TNT_MINECART("tnt_minecart", 45, "MinecartTnT"),
	UNKNOWN(null, -1, false, null),
	VEX("vex", 35, "living.monster.Vex"),
	VILLAGER("villager", 120, "living.merchant.Villager"),
	VINDICATION_ILLAGER("vindication_illager", 36, "living.monster.Vindicator"),
	WEATHER(null, -1, false, "Weather"),
	WITCH("witch", 66, "living.monster.Witch"),
	WITHER("wither", 64, "Wither"),
	WITHER_SKELETON("wither_skeleton", 5, "WitherSkeleton"),
	WITHER_SKULL("wither_skull", 19, "WitherSkull"),
	WOLF("wolf", 95, "living.animal.Wolf"),
	ZOMBIE("zombie", 54, "living.monster.Zombie"),
	ZOMBIE_HORSE("zombie_horse", 29, "ZombieHorse"),
	ZOMBIE_VILLAGER("zombie_villager", 27, "ZombieVillager");

	private String name;
	private Class<? extends WSEntity> clazz, serverClass;
	private short typeId;
	private boolean independent, loaded;
	private String className;


	EnumEntityType(String name, int typeId, String className) {
		this(name, typeId, true, className);
	}


	EnumEntityType(String name, int typeId, boolean independent, String className) {
		this.name = name;
		this.typeId = (short) typeId;
		this.independent = independent;
		this.className = className;
	}

	public void load() {
		if (loaded) return;
		String[] array = ("com.degoos.wetsponge.entity." + className).replace(".", ";").split(";");
		array[array.length - 1] = "<REPLACE>" + array[array.length - 1];
		StringBuilder builder = new StringBuilder();

		String serverString = null;
		boolean spigot = false;
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				serverString = "Spigot";
				spigot = true;
				break;
			case SPONGE:
				serverString = "Sponge";
				spigot = false;
				break;
		}

		for (String string : array) builder.append(".").append(string);
		//boolean error = false;

		try {
			clazz = (Class<? extends WSEntity>) Class.forName(builder.toString().replaceFirst(".", "").replace("<REPLACE>", "WS"));
		} catch (Throwable e) {
			//error = true;
			clazz = null;
		}
		if (spigot ? SpigotEntityParser.containsValue(this) : SpongeEntityParser.containsValue(this)) try {
			serverClass = (Class<? extends WSEntity>) Class.forName(builder.toString().replaceFirst(".", "").replace("<REPLACE>", serverString));
		} catch (Throwable e) {
			//error = true;
			serverClass = null;
		}

		//if (error) InternalLogger.sendWarning("Class for entity " + this + " not found!");
		loaded = true;
	}


	public static Optional<EnumEntityType> getByName(String name) {
		return Arrays.stream(values()).filter(type -> type.getName() != null && type.getName().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumEntityType> getByTypeId(int typeId) {
		return Arrays.stream(values()).filter(type -> type.getTypeId() == typeId).findAny();
	}

	public static Optional<EnumEntityType> getByClass(Class<? extends WSEntity> clazz) {
		return Arrays.stream(values()).filter(type -> clazz.equals(type.getEntityClass())).findAny();
	}

	public static Optional<EnumEntityType> fromId(int id) {
		return Arrays.stream(values()).filter(type -> type.getTypeId() == id).findAny();
	}


	public String getName() {
		return this.name;
	}


	public Class<? extends WSEntity> getEntityClass() {
		return this.clazz;
	}


	public Class<? extends WSEntity> getServerClass() {
		return serverClass;
	}


	public short getTypeId() {
		return this.typeId;
	}


	public boolean isSpawnable() {
		return this.independent;
	}

}
