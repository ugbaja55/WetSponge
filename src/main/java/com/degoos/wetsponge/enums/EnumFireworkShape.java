package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumFireworkShape {

    BALL("BALL"), BURST("BURST"), CREEPER("CREEPER"), LARGE_BALL("BALL_LARGE"), STAR("STAR");

    private String spigotName;

    EnumFireworkShape(String spigotName) {
        this.spigotName = spigotName;
    }

    public String getSpigotName() {
        return spigotName;
    }

    public static Optional<EnumFireworkShape> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumFireworkShape> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }
}
