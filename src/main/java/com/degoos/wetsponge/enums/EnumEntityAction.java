package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumEntityAction {

	START_SNEAKING, STOP_SNEAKING, STOP_SLEEPING, START_SPRINTING, STOP_SPRINTING, START_RIDING_JUMP("RIDING_JUMP"), STOP_RIDING_JUMP, OPEN_INVENTORY, START_FALL_FLYING;

	private String oldSpigotName;

	EnumEntityAction() {}

	EnumEntityAction(String oldSpigotName) {
		this.oldSpigotName = oldSpigotName;
	}

	public String getOldSpigotName() {
		return oldSpigotName;
	}

	public static Optional<EnumEntityAction> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.ordinal() == id).findAny();
	}

}
