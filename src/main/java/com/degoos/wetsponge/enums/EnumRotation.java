package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumRotation {

    BOTTOM("FLIPPED"),
    BOTTOM_LEFT("FLIPPED_45"),
    BOTTOM_RIGHT("CLOCKWISE_135"),
    LEFT("COUNTER_CLOCKWISE"),
    RIGHT("CLOCKWISE"),
    TOP("NONE"),
    TOP_LEFT("COUNTER_CLOCKWISE_45"),
    TOP_RIGHT("CLOCKWISE_45");

    private String spigotName;

    EnumRotation(String spigotName) {
        this.spigotName = spigotName;
    }

    public static Optional<EnumRotation> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpongeName().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumRotation> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return name();
    }
}
