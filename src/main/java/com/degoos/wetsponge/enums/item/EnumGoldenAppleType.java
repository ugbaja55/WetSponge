package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumGoldenAppleType {

    GOLDEN_APPLE(0), ENCHANTED_GOLDEN_APPLE(1);

    private int value;

    EnumGoldenAppleType(int value) {
        this.value = value;
    }

    public static Optional<EnumGoldenAppleType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumGoldenAppleType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }
}
