package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBookGeneration {

    ORIGINAL(0), COPY_OF_ORIGINAL(1), COPY_OF_COPY(2), TATTERED(3);

    private int value;

    EnumBookGeneration(int value) {
        this.value = value;
    }

    public static Optional<EnumBookGeneration> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumBookGeneration> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }
}
