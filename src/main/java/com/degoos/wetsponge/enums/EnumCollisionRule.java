package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumCollisionRule {

    ALWAYS("ALWAYS", "ALWAYS"),
    PUSH_OTHER_TEAMS("FOR_OTHER_TEAMS", "PUSH_OTHER_TEAMS"),
    PUSH_OWN_TEAM("FOR_OWN_TEAM", "PUSH_OWN_TEAM"),
    NEVER("NEVER", "NEVER");

    private String spigotName, spongeName;

    EnumCollisionRule(String spigotName, String spongeName) {
        this.spigotName = spigotName;
        this.spongeName = spongeName;
    }

    public static Optional<EnumCollisionRule> getBySpigotName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
    }

    public static Optional<EnumCollisionRule> getBySpongeName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return spongeName;
    }
}
