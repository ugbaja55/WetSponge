package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumDyeColor {

	WHITE(0, 15, "WHITE"),
	ORANGE(1, 14, "ORANGE"),
	MAGENTA(2, 13, "MAGENTA"),
	LIGHT_BLUE(3, 12, "LIGHT_BLUE"),
	YELLOW(4, 11, "YELLOW"),
	LIME(5, 10, "LIME"),
	PINK(6, 9, "PINK"),
	GRAY(7, 8, "GRAY"),
	SILVER(8, 7, "SILVER"),
	CYAN(9, 6, "CYAN"),
	PURPLE(10, 5, "PURPLE"),
	BLUE(11, 4, "BLUE"),
	BROWN(12, 3, "BROWN"),
	GREEN(13, 2, "GREEN"),
	RED(14, 1, "RED"),
	BLACK(15, 0, "BLACK");

	private final byte woolData;
	private final byte dyeData;
	private final String name;


	EnumDyeColor(int woolData, int dyeData, String name) {
		this.woolData = (byte) woolData;
		this.dyeData = (byte) dyeData;
		this.name = name;
	}


	public static Optional<EnumDyeColor> getByWoolData(byte data) {
		return Arrays.stream(values()).filter(dyeColor -> dyeColor.getWoolData() == data).findAny();
	}


	public static Optional<EnumDyeColor> getByDyeData(byte data) {
		return Arrays.stream(values()).filter(dyeColor -> dyeColor.getDyeData() == data).findAny();
	}


	public static Optional<EnumDyeColor> getByName(String name) {
		return Arrays.stream(values()).filter(dyeColor -> dyeColor.getName().equalsIgnoreCase(name)).findAny();
	}


	public byte getWoolData() {
		return this.woolData;
	}


	public byte getDyeData() {
		return this.dyeData;
	}


	public String getName() {
		return name;
	}

	public EnumTextColor toTextColor() {
		switch (this) {
			case WHITE:
				return EnumTextColor.WHITE;
			case ORANGE:
				return EnumTextColor.GOLD;
			case MAGENTA:
				return EnumTextColor.DARK_PURPLE;
			case LIGHT_BLUE:
				return EnumTextColor.AQUA;
			case YELLOW:
				return EnumTextColor.YELLOW;
			case LIME:
				return EnumTextColor.GREEN;
			case PINK:
				return EnumTextColor.LIGHT_PURPLE;
			case GRAY:
				return EnumTextColor.DARK_GRAY;
			case SILVER:
				return EnumTextColor.GRAY;
			case CYAN:
				return EnumTextColor.DARK_AQUA;
			case PURPLE:
				return EnumTextColor.LIGHT_PURPLE;
			case BLUE:
				return EnumTextColor.BLUE;
			case BROWN:
				return EnumTextColor.RED;
			case GREEN:
				return EnumTextColor.DARK_GREEN;
			case RED:
				return EnumTextColor.DARK_RED;
			case BLACK:
				return EnumTextColor.BLACK;
			default:
				return EnumTextColor.WHITE;
		}
	}

}
