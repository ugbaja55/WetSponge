package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumNotePitch {

	A1(1, 'A', false),
	A2(2, 'A', false),
	A_SHARP1(1, 'A', true),
	A_SHARP2(2, 'A', true),
	B1(1, 'B', false),
	B2(2, 'B', false),
	C1(1, 'C', false),
	C2(2, 'C', false),
	C_SHARP1(1, 'C', true),
	C_SHARP2(2, 'C', true),
	D1(1, 'D', false),
	D2(2, 'D', false),
	D_SHARP1(1, 'D', true),
	D_SHARP2(2, 'D', true),
	E1(1, 'E', false),
	E2(2, 'E', false),
	F1(1, 'F', false),
	F2(2, 'F', false),
	F_SHARP0(0, 'F', true),
	F_SHARP1(1, 'F', true),
	F_SHARP2(2, 'F', true),
	G0(0, 'G', false),
	G1(1, 'G', false),
	G_SHARP0(0, 'G', true),
	G_SHARP1(1, 'G', true);

	private int     octave;
	private char    tone;
	private boolean sharped;


	EnumNotePitch (int octave, char tone, boolean sharped) {
		this.octave = octave;
		this.tone = tone;
		this.sharped = sharped;
	}


	public static Optional<EnumNotePitch> getByName (String name) {
		return Arrays.stream(values()).filter(enumNotePitch -> enumNotePitch.name().equalsIgnoreCase(name)).findAny();
	}


	public static Optional<EnumNotePitch> getByArguments (int octave, char tone, boolean sharped) {
		return Arrays.stream(values()).filter(notePitch -> notePitch.getOctave() == octave && notePitch.getTone() == tone && notePitch.sharped == sharped).findAny();
	}


	public int getOctave () {
		return octave;
	}


	public char getTone () {
		return tone;
	}


	public boolean isSharped () {
		return sharped;
	}

}
