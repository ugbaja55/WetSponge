package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;

public class SpigotInjector {

	public static void inject() {
		if (WetSponge.getServerType() == EnumServerType.SPIGOT || WetSponge.getServerType() == EnumServerType.PAPER_SPIGOT) {
			AsyncCatcherRemover.load();
			WSChunkProviderAssist.load();
			SpigotTimingsAssist.load();
		}
	}

}
