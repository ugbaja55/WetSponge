package com.degoos.wetsponge.mixin.spigot;

import co.aikar.timings.WSPaperSpigotTimings;
import co.aikar.timings.WSSpigotTimings;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.Field;

public class SpigotTimingsAssist {

	public static void load() {
		try {
			if (WetSponge.getServerType() == EnumServerType.PAPER_SPIGOT) {
				//TODO IllegalAccessError.
				/*Field field = co.aikar.timings.TimingsManager.class.getField("FULL_SERVER_TICK");
				ReflectionUtils.setAccessible(field);
				field.set(null, new WSPaperSpigotTimings());
				ReflectionUtils.setAccessible(field, false);*/
			} else {
				Field field = NMSUtils.getOBCClass("SpigotTimings").getField("serverTickTimer");
				ReflectionUtils.setAccessible(field);
				field.set(null, new WSSpigotTimings("** Full Server Tick"));
				ReflectionUtils.setAccessible(field, false);
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was injecting code!");
		}
	}

}
