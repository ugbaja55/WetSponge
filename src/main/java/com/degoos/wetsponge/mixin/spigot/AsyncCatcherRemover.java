package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.util.InternalLogger;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.AsyncCatcher;

public class AsyncCatcherRemover {


	public static void load() {
		try {
			AsyncCatcher.enabled = false;
			new BukkitRunnable() {
				@Override
				public void run() {
					if(AsyncCatcher.enabled) AsyncCatcher.enabled = false;
				}
			}.runTaskTimer(SpigotWetSponge.getInstance(), 20, 20);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was injecting code!");
		}
	}

}
