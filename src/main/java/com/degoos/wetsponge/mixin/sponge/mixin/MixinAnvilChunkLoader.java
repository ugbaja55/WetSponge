package com.degoos.wetsponge.mixin.sponge.mixin;

import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunk;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.SpongeWorld;
import com.degoos.wetsponge.world.WSChunkLoaderSavable;
import com.degoos.wetsponge.world.WSWorld;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.datafix.DataFixer;
import net.minecraft.util.datafix.FixTypes;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.MinecraftException;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraft.world.chunk.storage.RegionFileCache;
import net.minecraft.world.gen.ChunkProviderServer;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(value = AnvilChunkLoader.class, priority = Integer.MAX_VALUE)
public abstract class MixinAnvilChunkLoader implements WSChunkLoaderSavable {

	private Set<World> worlds = new HashSet<>();

	@Shadow
	@Final
	private DataFixer fixer;

	@Shadow
	@Final
	private File chunkSaveLocation;

	@Shadow
	@Final
	private Map<ChunkPos, NBTTagCompound> chunksToSave;

	@Shadow
	private void writeChunkToNBT(Chunk chunkIn, World worldIn, NBTTagCompound nbtTagCompound) {}

	@Shadow
	protected abstract void addChunkToPending(ChunkPos pos, NBTTagCompound compound);

	@Shadow
	public abstract boolean writeNextIO();

	@Shadow
	protected abstract Chunk checkedReadChunkFromNBT(World world, int chunkX, int chunkZ, NBTTagCompound nbt);

	@Overwrite
	public void saveChunk(World worldIn, Chunk chunkIn) throws MinecraftException, IOException {
		if (((WSMixinChunk) chunkIn).isPreparedToCancelUnload()) return;
		if (((WSMixinChunk) chunkIn).canBeSaved() && !worlds.contains(worldIn)) {
			worldIn.checkSessionLock();
			try {
				NBTTagCompound exception = new NBTTagCompound();
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				exception.setTag("Level", nbttagcompound1);
				exception.setInteger("DataVersion", 922);
				this.writeChunkToNBT(chunkIn, worldIn, nbttagcompound1);
				this.addChunkToPending(chunkIn.getPos(), exception);
			} catch (Throwable var5) {
				InternalLogger.sendError("Minecraft -> Failed to save chunk");
				var5.printStackTrace();
			}
		}
	}

	@Overwrite
	public void saveExtraChunkData(World worldIn, Chunk chunkIn) {

	}


	public boolean canSaveChunks(WSWorld world) {
		return !worlds.contains(world.getHandled());
	}

	public void setSaveChunks(boolean saveChunks, WSWorld world) {
		if (saveChunks) worlds.remove(world.getHandled());
		else worlds.add((World) ((SpongeWorld) world).getHandled());
	}

	@Override
	public void saveAllChunks(WSWorld wsWorld) {
		Validate.notNull(wsWorld, "World cannot be null!");
		World world = (World) wsWorld.getHandled();
		((ChunkProviderServer) world.getChunkProvider()).getLoadedChunks().forEach(chunk -> {
			try {
				saveChunk(world, chunk);
			} catch (Exception e) {
				InternalLogger.printException(e, "An error has occurred while WetSponge was trying to save the chunks of the world" + wsWorld.getName());
			}
		});
		while (writeNextIO()) {}
	}

	@Override
	public Chunk loadVanillaChunk(World world, int chunkX, int chunkZ) throws IOException {
		ChunkPos lvt_4_1_ = new ChunkPos(chunkX, chunkZ);
		NBTTagCompound lvt_5_1_ = this.chunksToSave.get(lvt_4_1_);
		if (lvt_5_1_ == null) {
			DataInputStream lvt_6_1_ = RegionFileCache.getChunkInputStream(this.chunkSaveLocation, chunkX, chunkZ);
			if (lvt_6_1_ == null) {
				return null;
			}

			lvt_5_1_ = this.fixer.process(FixTypes.CHUNK, CompressedStreamTools.read(lvt_6_1_));
		}
		return checkedReadChunkFromNBT(world, chunkX, chunkZ, lvt_5_1_);
	}
}
