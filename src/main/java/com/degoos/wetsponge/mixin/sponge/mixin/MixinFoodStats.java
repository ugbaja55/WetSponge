package com.degoos.wetsponge.mixin.sponge.mixin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.entity.player.WSPlayerFoodLevelChangeEvent;
import com.degoos.wetsponge.parser.player.PlayerParser;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.FoodStats;
import net.minecraft.world.EnumDifficulty;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(FoodStats.class)
public class MixinFoodStats {

	@Shadow
	private int foodLevel;
	@Shadow
	private float foodSaturationLevel;
	@Shadow
	private float foodExhaustionLevel;
	@Shadow
	private int foodTimer;
	@Shadow
	private int prevFoodLevel;

	@Shadow
	public void addExhaustion(float ex) {}

	private EntityPlayer player = null;

	@Overwrite
	public void addStats(int foodStat, float saturationLevel) {
		sendFoodLevelEvent(Math.min(foodStat + this.foodLevel, 20));
		this.foodSaturationLevel = Math.min(this.foodSaturationLevel + (float) foodStat * saturationLevel * 2.0F, (float) this.foodLevel);
	}

	@Overwrite
	public void addStats(ItemFood itemFood, ItemStack itemStack) {
		this.addStats(itemFood.getHealAmount(itemStack), itemFood.getSaturationModifier(itemStack));
	}

	@Overwrite
	public void onUpdate(EntityPlayer entityPlayer) {
		this.player = entityPlayer;
		EnumDifficulty lvt_2_1_ = entityPlayer.world.getDifficulty();
		this.prevFoodLevel = this.foodLevel;
		if (this.foodExhaustionLevel > 4.0F) {
			this.foodExhaustionLevel -= 4.0F;
			if (this.foodSaturationLevel > 0.0F) {
				this.foodSaturationLevel = Math.max(this.foodSaturationLevel - 1.0F, 0.0F);
			} else if (lvt_2_1_ != EnumDifficulty.PEACEFUL) {
				sendFoodLevelEvent(Math.max(this.foodLevel - 1, 0));
			}
		}

		boolean lvt_3_1_ = entityPlayer.world.getGameRules().getBoolean("naturalRegeneration");
		if (lvt_3_1_ && this.foodSaturationLevel > 0.0F && entityPlayer.shouldHeal() && this.foodLevel >= 20) {
			++this.foodTimer;
			if (this.foodTimer >= 10) {
				float lvt_4_1_ = Math.min(this.foodSaturationLevel, 6.0F);
				entityPlayer.heal(lvt_4_1_ / 6.0F);
				this.addExhaustion(lvt_4_1_);
				this.foodTimer = 0;
			}
		} else if (lvt_3_1_ && this.foodLevel >= 18 && entityPlayer.shouldHeal()) {
			++this.foodTimer;
			if (this.foodTimer >= 80) {
				entityPlayer.heal(1.0F);
				this.addExhaustion(6.0F);
				this.foodTimer = 0;
			}
		} else if (this.foodLevel <= 0) {
			++this.foodTimer;
			if (this.foodTimer >= 80) {
				if (entityPlayer.getHealth() > 10.0F || lvt_2_1_ == EnumDifficulty.HARD || entityPlayer.getHealth() > 1.0F && lvt_2_1_ == EnumDifficulty.NORMAL) {
					entityPlayer.attackEntityFrom(DamageSource.STARVE, 1.0F);
				}

				this.foodTimer = 0;
			}
		} else {
			this.foodTimer = 0;
		}

	}

	@Overwrite
	public void readNBT(NBTTagCompound nbt) {
		if (nbt.hasKey("foodLevel", 99)) {
			sendFoodLevelEvent(nbt.getInteger("foodLevel"));
			this.foodTimer = nbt.getInteger("foodTickTimer");
			this.foodSaturationLevel = nbt.getFloat("foodSaturationLevel");
			this.foodExhaustionLevel = nbt.getFloat("foodExhaustionLevel");
		}

	}

	@Overwrite
	public void setFoodLevel(int p_setFoodLevel_1_) {
		sendFoodLevelEvent(p_setFoodLevel_1_);
	}

	public void sendFoodLevelEvent(int foodLevel) {
		if (player == null) return;
		WSPlayerFoodLevelChangeEvent event = new WSPlayerFoodLevelChangeEvent(PlayerParser.getOrCreatePlayer(player, player.getUniqueID()), foodLevel, foodLevel);
		WetSponge.getEventManager().callEvent(event);
		if (!event.isCancelled()) this.foodLevel = event.getFoodLevel();

	}

}
