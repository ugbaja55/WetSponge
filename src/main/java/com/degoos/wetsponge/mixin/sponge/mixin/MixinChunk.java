package com.degoos.wetsponge.mixin.sponge.mixin;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.world.WSChunkUnloadEvent;
import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunk;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpongeChunk;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = Chunk.class, priority = Integer.MAX_VALUE)
public class MixinChunk implements WSMixinChunk {

	@Shadow
	@Final
	private World world;
	private boolean canBeUnloaded          = true;
	private boolean canBeSaved             = true;
	private boolean preparedToCancelUnload = false;

	@Override
	public boolean canBeUnloaded() {
		return canBeUnloaded;
	}

	@Override
	public void setCanBeUnloaded(boolean canBeUnloaded) {
		this.canBeUnloaded = canBeUnloaded;
	}

	@Override
	public boolean canBeSaved() {
		return canBeSaved;
	}

	@Override
	public void setCanBeSaved(boolean canBeSaved) {
		this.canBeSaved = canBeSaved;
	}

	@Override
	public boolean isPreparedToCancelUnload() {
		return preparedToCancelUnload;
	}

	@Override
	public void setPreparedToCancelUnload(boolean preparedToCancelUnload) {
		this.preparedToCancelUnload = preparedToCancelUnload;
	}

	@Inject(method = "onUnload", at = @At("HEAD"), cancellable = true)
	public void onOnUnload(CallbackInfo ci) {
		WSChunkUnloadEvent event = new WSChunkUnloadEvent(WorldParser.getOrCreateWorld(((org.spongepowered.api.world.World) world).getName(), world),
			new SpongeChunk((org.spongepowered.api.world.Chunk) this), !canBeUnloaded);
		WetSponge.getEventManager().callEvent(event);
		if (event.isCancelled()) {
			ci.cancel();
			preparedToCancelUnload = true;
		}
	}
}
