package com.degoos.wetsponge.mixin.sponge.mixin;

import co.aikar.wetspongetimings.TimingsManager;
import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = MinecraftServer.class, priority = Integer.MAX_VALUE)
public class MixinMinecraftServer {

	@Inject(method = "stopServer", at = @At("HEAD"))
	public void onStopServer(CallbackInfo ci) {
		((SpongeWetSponge) WetSponge.getLoader()).unload();
	}


	@Inject(method = "tick", at = @At(value = "HEAD"))
	public void onServerTickStart(CallbackInfo ci) {
		TimingsManager.FULL_SERVER_TICK.startTiming();
	}

	@Inject(method = "tick", at = @At(value = "RETURN"))
	public void onServerTickEnd(CallbackInfo ci) {
		TimingsManager.FULL_SERVER_TICK.stopTiming();
	}
}
