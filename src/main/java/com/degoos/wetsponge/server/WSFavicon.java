package com.degoos.wetsponge.server;

import com.degoos.wetsponge.WetSponge;
import java.awt.image.BufferedImage;

/**
 * This class represents the server icon.
 */
public interface WSFavicon {

	public static WSFavicon ofEncoded(String encoded) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				return new SpigotFavicon(encoded);
			case SPONGE:
				return new SpongeFavicon(encoded);
			default:
				return null;
		}
	}

	/**
	 * Returns the {@link BufferedImage image} of the {@link WSFavicon favicon}.
	 *
	 * @return the {@link BufferedImage image}.
	 */
	BufferedImage getImage();

	/**
	 * Returns the Base64 value of the {@link WSFavicon favicon}.
	 * WARNING! The string must be "data:image/png;base64,"+ encoded image!
	 *
	 * @return the Base64 value.
	 */
	String toBase64();

}
