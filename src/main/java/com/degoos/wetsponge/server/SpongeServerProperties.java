package com.degoos.wetsponge.server;

import com.degoos.wetsponge.text.WSText;
import net.minecraft.server.dedicated.DedicatedServer;
import org.spongepowered.api.Server;

public class SpongeServerProperties implements WSServerProperties {

	private DedicatedServer server;

	public SpongeServerProperties(Server server) {
		this.server = (DedicatedServer) server;
	}

	@Override
	public boolean isFlightAllowed() {
		return server.isFlightAllowed();
	}

	@Override
	public WSServerProperties setFlightAllowed(boolean flightAllowed) {
		server.setAllowFlight(flightAllowed);
		return this;
	}

	@Override
	public boolean isPVPEnabled() {
		return server.isPVPEnabled();
	}

	@Override
	public WSServerProperties setPVPEnabled(boolean pvpEnabled) {
		server.setAllowPvp(pvpEnabled);
		return this;
	}

	@Override
	public boolean canSpawnNPCs() {
		return server.getCanSpawnNPCs();
	}

	@Override
	public WSServerProperties setCanSpawnNPCs(boolean canSpawnNPCs) {
		server.setCanSpawnNPCs(canSpawnNPCs);
		return this;
	}

	@Override
	public boolean canSpawnAnimals() {
		return server.getCanSpawnAnimals();
	}

	@Override
	public WSServerProperties setCanSpawnAnimals(boolean canSpawnAnimals) {
		server.setCanSpawnAnimals(canSpawnAnimals);
		return this;
	}

	@Override
	public boolean preventProxyConnections() {
		return server.getPreventProxyConnections();
	}

	@Override
	public WSServerProperties setPreventProxyConnections(boolean preventProxyConnections) {
		server.setPreventProxyConnections(preventProxyConnections);
		return this;
	}

	@Override
	public boolean isServerInOnlineMode() {
		return server.isServerInOnlineMode();
	}

	@Override
	public WSServerProperties setOnlineMode(boolean onlineMode) {
		server.setOnlineMode(onlineMode);
		return this;
	}

	@Override
	public WSText getMOTD() {
		return server.getMotd() == null ? WSText.empty() : WSText.getByFormattingText(server.getMotd());
	}

	@Override
	public WSServerProperties setMOTD(WSText motd) {
		server.setMOTD(motd == null ? null : motd.toFormattingText());
		return this;
	}

	@Override
	public int getBuildLimit() {
		return server.getBuildLimit();
	}

	@Override
	public WSServerProperties setBuildLimit(int buildLimit) {
		server.setBuildLimit(buildLimit);
		return this;
	}
}
