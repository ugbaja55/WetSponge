package com.degoos.wetsponge.server;

import com.degoos.wetsponge.util.InternalLogger;
import java.awt.image.BufferedImage;
import java.io.IOException;
import org.spongepowered.api.network.status.Favicon;

public class SpongeFavicon implements WSFavicon {

	private Favicon favicon;

	public SpongeFavicon(String encoded) {
		try {
			favicon = new org.spongepowered.common.network.status.SpongeFavicon(encoded);
		} catch (IOException e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was loading a favicon!");
		}
	}

	public SpongeFavicon(Favicon favicon) {
		this.favicon = favicon;
	}

	@Override
	public BufferedImage getImage() {
		return favicon.getImage();
	}

	@Override
	public String toBase64() {
		return ((org.spongepowered.common.network.status.SpongeFavicon) favicon).getEncoded();
	}

	public Favicon getHandled() {
		return favicon;
	}
}
