package com.degoos.wetsponge.server.response;

import com.degoos.wetsponge.server.WSFavicon;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;

public class WSServerStatusResponse {

	private WSText motd;
	private WSFavicon favicon;
	private String faviconString;
	private WSStatusPlayers players;
	private WSStatusVersion version;
	private boolean changed;

	public WSServerStatusResponse(WSText motd, String faviconString, WSStatusPlayers players, WSStatusVersion version) {
		Validate.notNull(motd, "MOTD cannot be null!");
		this.motd = motd;
		this.faviconString = faviconString;
		this.favicon = null;
		this.players = players;
		this.version = version;
		this.changed = false;
	}

	public WSServerStatusResponse(WSText motd, WSFavicon favicon, WSStatusPlayers players, WSStatusVersion version) {
		Validate.notNull(motd, "MOTD cannot be null!");
		this.motd = motd;
		this.favicon = favicon;
		this.faviconString = favicon.toBase64();
		this.players = players;
		this.version = version;
	}

	public WSText getMOTD() {
		return motd;
	}

	public void setMOTD(WSText motd) {
		Validate.notNull(motd, "MOTD cannot be null!");
		this.motd = motd;
		this.changed = true;
	}

	public WSFavicon getFavicon() {
		if (faviconString == null && favicon == null) return null;
		if (favicon == null) favicon = WSFavicon.ofEncoded(faviconString);
		return favicon;
	}

	public void setFavicon(WSFavicon favicon) {
		this.favicon = favicon;
		if (favicon == null) this.faviconString = null;
		else this.faviconString = favicon.toBase64();
		this.changed = true;
	}

	public String getFaviconString() {
		return faviconString;
	}

	public WSStatusPlayers getPlayers() {
		return players;
	}

	public void setPlayers(WSStatusPlayers players) {
		Validate.notNull(players, "Players cannot be null!");
		this.players = players;
		this.changed = true;
	}

	public WSStatusVersion getVersion() {
		return version;
	}

	public void setVersion(WSStatusVersion version) {
		Validate.notNull(version, "Version cannot be null!");
		this.version = version;
		this.changed = true;
	}

	public boolean hasChanged() {
		return changed || version.hasChanged() || players.hasChanged();
	}
}
