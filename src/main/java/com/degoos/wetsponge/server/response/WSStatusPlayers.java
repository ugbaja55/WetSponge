package com.degoos.wetsponge.server.response;

import com.degoos.wetsponge.user.WSGameProfile;
import com.google.common.collect.ImmutableSet;
import java.util.HashSet;
import java.util.Set;

public class WSStatusPlayers {

	private int maxPlayers;
	private int playerCount;
	private Set<WSGameProfile> profiles;
	private boolean changed;

	public WSStatusPlayers(int maxPlayers, int playerCount, Set<WSGameProfile> profiles) {
		this.maxPlayers = maxPlayers;
		this.playerCount = playerCount;
		this.profiles = profiles;
		this.changed = false;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
		changed = true;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
		changed = true;
	}

	public Set<WSGameProfile> getProfiles() {
		return ImmutableSet.copyOf(profiles);
	}

	public void setProfiles(Set<WSGameProfile> profiles) {
		if (profiles == null) this.profiles = new HashSet<>();
		else this.profiles = new HashSet<>(profiles);
		changed = true;
	}

	public boolean hasChanged() {
		return changed;
	}
}
