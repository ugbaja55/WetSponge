package com.degoos.wetsponge.server;


import co.aikar.wetspongetimings.TimingsManager;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.SpongeConsoleSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEnvironment;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.scoreboard.SpongeScoreboard;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.world.SpongeWorld;
import com.degoos.wetsponge.world.SpongeWorldProperties;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.WSWorldProperties;
import com.degoos.wetsponge.world.generation.SpongeBlockVolume;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.spongepowered.api.Server;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.WorldArchetype;
import org.spongepowered.api.world.WorldArchetypes;
import org.spongepowered.api.world.storage.WorldProperties;

public class SpongeServer implements WSServer {

	private Server server;
	private WSServerInfo serverInfo;
	private WSServerProperties properties;


	public SpongeServer(Server server) {
		this.server = server;
		this.serverInfo = new SpongeServerInfo(server);
		this.properties = new SpongeServerProperties(server);
		TimingsManager.FULL_SERVER_TICK.startTiming();
	}


	@Override
	public Optional<WSWorld> getWorld(String name) {
		return server.getWorld(name).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> getWorld(UUID uuid) {
		return server.getWorld(uuid).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> loadWorld(String name) {
		return server.loadWorld(name).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> loadWorld(UUID uuid) {
		return server.loadWorld(uuid).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public boolean unloadWorld(WSWorld world) {
		return server.unloadWorld(((SpongeWorld) world).getHandled());
	}


	@Override
	public Set<WSWorld> getWorlds() {
		return server.getWorlds().stream().map(world -> WorldParser.getOrCreateWorld(world.getName(), world)).collect(Collectors.toSet());
	}


	@Override
	public Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment) {
		try {
			properties.setEnabled(true);
			WorldProperties worldProperties = Sponge.getServer()
				.createWorldProperties(properties.getWorldName(), Sponge.getRegistry().getType(WorldArchetype.class, environment.name())
					.orElse(WorldArchetypes.OVERWORLD));
			new SpongeWorldProperties(worldProperties).apply(properties);
			Optional<World> optional = Sponge.getServer().loadWorld(worldProperties);
			return optional.map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}


	@Override
	public Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment, WSGenerationPopulator populator) {
		try {
			properties.setEnabled(true);
			WorldProperties worldProperties = Sponge.getServer()
				.createWorldProperties(properties.getWorldName(), Sponge.getRegistry().getType(WorldArchetype.class, environment.name())
					.orElse(WorldArchetypes.OVERWORLD));
			new SpongeWorldProperties(worldProperties).apply(properties);
			Optional<World> optional = Sponge.getServer().loadWorld(worldProperties);
			optional.ifPresent(world -> world.getWorldGenerator()
				.setBaseGenerationPopulator((w, buffer, biomes) -> populator.populate(WorldParser.getOrCreateWorld(w.getName(), w), new SpongeBlockVolume(buffer))));
			return optional.map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
		} catch (IOException e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}


	@Override
	public boolean deleteWorld(WSWorld world) {
		if (this.unloadWorld(world)) try {
			return server.deleteWorld(server.getWorldProperties(world.getName()).orElseGet(null)).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public Optional<WSPlayer> getPlayer(String name) {
		return PlayerParser.getPlayer(name);
	}


	@Override
	public Optional<WSPlayer> getPlayer(UUID uuid) {
		return PlayerParser.getPlayer(uuid);
	}


	@Override
	public Set<WSPlayer> getOnlinePlayers() {
		return server.getOnlinePlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}


	@Override
	public WSScoreboard getMainScoreboard() {
		return new SpongeScoreboard(server.getServerScoreboard().orElse(Scoreboard.builder().build()));
	}


	@Override
	public WSScoreboard createScoreboard() {
		return new SpongeScoreboard(Scoreboard.builder().build());
	}


	@Override
	public WSCommandSource getConsole() {
		return new SpongeConsoleSource(Sponge.getServer().getConsole());
	}


	@Override
	public WSServerInfo getServerInfo() {
		return serverInfo;
	}

	@Override
	public WSServerProperties getProperties() {
		return properties;
	}


	@Override
	public void shutdown() {
		TimingsManager.FULL_SERVER_TICK.stopTiming();
		server.shutdown();
	}

}
