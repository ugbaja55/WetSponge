package com.degoos.wetsponge.server;


import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEnvironment;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.user.WSUser;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.WSWorldProperties;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * The WSServer class represent the Minecraft Server which WetSponge is running on.
 */
public interface WSServer {

	/**
	 * Returns the target {@link WSWorld world} by its {@link String name}, if present.
	 *
	 * @param name the {@link String name} of the target world.
	 *
	 * @return the target {@link WSWorld world}, if present.
	 */
	Optional<WSWorld> getWorld(String name);

	/**
	 * Returns the target {@link WSWorld world} by its {@link UUID}, if present.
	 *
	 * @param uuid the {@link UUID} of the target world.
	 *
	 * @return the target {@link WSWorld world}, if present.
	 */
	Optional<WSWorld> getWorld(UUID uuid);

	/**
	 * Loads the target {@link WSWorld world} by its {@link String name},
	 * if the {@link java.io.File folder} of the {@link WSWorld world}
	 * is present in the {@link WSServer server} {@link java.io.File folder}.
	 * If the target {@link WSWorld world} is already loaded, it just will return
	 * the {@link WSWorld world}.
	 *
	 * @param name the {@link String name} of the target {@link WSWorld world}.
	 *
	 * @return the target {@link WSWorld world}, if present.
	 */
	Optional<WSWorld> loadWorld(String name);

	/**
	 * Loads the target {@link WSWorld world} by its {@link UUID},
	 * if the {@link java.io.File folder} of the {@link WSWorld world}
	 * is present in the {@link WSServer server} {@link java.io.File folder}.
	 * If the target {@link WSWorld world} is already loaded, it just will return
	 * the {@link WSWorld world}.
	 *
	 * @param uuid the {@link UUID} of the target {@link WSWorld world}.
	 *
	 * @return the target {@link WSWorld world}, if present.
	 */
	Optional<WSWorld> loadWorld(UUID uuid);

	/**
	 * Unloads a {@link WSWorld world}.
	 * <p>It's recommended to remove every {@link WSPlayer player}
	 * of the world before unloading it!</p>
	 *
	 * @param world the {@link WSWorld world} to unload.
	 *
	 * @return whether the world has been successfully unloaded.
	 */
	boolean unloadWorld(WSWorld world);

	/**
	 * Gets all loaded {@link WSWorld}s of the {@link WSServer}.
	 *
	 * @return all loaded {@link WSWorld}s of the {@link WSServer}.
	 */
	Set<WSWorld> getWorlds();

	/**
	 * Creates a new {@link WSWorld world} from the {@link WSWorldProperties properties}
	 * and a {@link EnumEnvironment environment}.
	 * If the {@link WSWorld world} already exist, it will be loaded,
	 * without changing its properties.
	 * If the {@link WSWorld world} is already loaded, it just will return
	 * the {@link WSWorld world}.
	 *
	 * @param properties the new {@link WSWorld world} {@link WSWorldProperties properties}.
	 * @param environment the new {@link WSWorld world} {@link EnumEnvironment environment}.
	 *
	 * @return the new {@link WSWorld}, if it didn't exists and if it were successfully create.
	 */
	Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment);

	/**
	 * Creates a new {@link WSWorld world} from the {@link WSWorldProperties properties},
	 * a {@link EnumEnvironment environment} and the {@link WSGenerationPopulator populator}.
	 * If the {@link WSWorld world} already exist, it will be loaded,
	 * without changing its properties.
	 * If the {@link WSWorld world} is already loaded, it just will return
	 * the {@link WSWorld world}.
	 * <p>
	 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge} the Spawn Chunks can be
	 * generated with the vanilla generator.
	 *
	 * @param properties the new {@link WSWorld world} {@link WSWorldProperties properties}.
	 * @param environment the new {@link WSWorld world} {@link EnumEnvironment environment}.
	 * @param populator the {@link WSWorld world} {@link WSGenerationPopulator populator}.
	 *
	 * @return the new {@link WSWorld}, if it didn't exists and if it were successfully create.
	 */
	Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment, WSGenerationPopulator populator);

	/**
	 * Unloads and deletes a {@link WSWorld world}.
	 * <p>It's recommended to remove every {@link WSPlayer player}
	 * of the world before deleting it!</p>
	 *
	 * @param world the {@link WSWorld world} to unload.
	 *
	 * @return whether the world has been successfully deleted.
	 */
	boolean deleteWorld(WSWorld world);


	/**
	 * Returns the target {@link WSPlayer online player} by their {@link String name}, if present.
	 *
	 * @param name the {@link WSPlayer}'s {@link String name}.
	 *
	 * @return the target {@link WSPlayer player}, if present.
	 */
	Optional<WSPlayer> getPlayer(String name);

	/**
	 * Returns the target {@link WSPlayer online player} by their {@link UUID}, if present.
	 *
	 * @param uuid the {@link WSPlayer}'s {@link UUID}.
	 *
	 * @return the target {@link WSPlayer player}, if present.
	 */
	Optional<WSPlayer> getPlayer(UUID uuid);

	/**
	 * Gets all {@link WSPlayer online player}s of the {@link WSServer server}.
	 *
	 * @return all {@link WSPlayer online player}s of the {@link WSServer server}.
	 */
	Set<WSPlayer> getOnlinePlayers();

	/**
	 * Gets the target {@link WSUser user} by their {@link UUID}.
	 * <p>the is equivalent to the OfflinePlayer of {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT}</p>
	 *
	 * @param uuid the {@link WSUser user}'s {@link UUID}.
	 *
	 * @return the target {@link WSUser}.
	 */
	default WSUser getUser(UUID uuid) {
		return WSUser.of(uuid);
	}

	/**
	 * Gets the target {@link WSUser user} by their {@link UUID} and their {@link String name}.
	 * <p>the is equivalent to the OfflinePlayer of {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT}</p>
	 *
	 * @param uuid the {@link WSUser user}'s {@link UUID}.
	 * @param name the {@link WSUser user}'s {@link String name}.
	 *
	 * @return the target {@link WSUser}.
	 */
	default WSUser getUser(UUID uuid, String name) {
		return WSUser.of(uuid, name);
	}

	/**
	 * @return the main {@link WSScoreboard scoreboard} of the {@link WSServer}.
	 */
	WSScoreboard getMainScoreboard();

	/**
	 * Creates a new {@link WSScoreboard scoreboard}.
	 *
	 * @return the created {@link WSScoreboard scoreboard}.
	 *
	 * @see WSPlayer#setScoreboard(WSScoreboard)
	 */
	WSScoreboard createScoreboard();

	/**
	 * @return the {@link WSCommandSource console} of the {@link WSServer server}.
	 */
	WSCommandSource getConsole();

	/**
	 * Gets the {@link WSServerInfo server info} of the {@link WSServer server}.
	 * The {@link WSServerInfo server info} storages the information about the {@link WSServer server}.
	 *
	 * @return the {@link WSServerInfo server info} of the {@link WSServer server}.
	 */
	WSServerInfo getServerInfo();

	/**
	 * Returns the {@link WSServerProperties server properties} of the Server.
	 * You can modify them, but they won't be saved in the file server.properties.
	 *
	 * @return the {@link WSServerProperties server properties}.
	 */
	WSServerProperties getProperties();

	/**
	 * Shutdowns the server.
	 */
	void shutdown();

}
