package com.degoos.wetsponge.data;


public class WSTransaction<T> {

	private T newData, oldData;
	private boolean hasChanged;


	public WSTransaction(T oldData, T newData) {
		this.oldData = oldData;
		this.newData = newData;
		hasChanged = false;
	}

	public T getNewData() {
		return newData;
	}

	public void setNewData(T newData) {
		this.newData = newData;
		hasChanged = true;
	}

	public T getOldData() {
		return oldData;
	}

	public boolean hasChanged() {
		return hasChanged;
	}
}
