package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.block.SpigotBlockSnapshot;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.block.WSBlockBreakEvent;
import com.degoos.wetsponge.event.block.WSBlockChangeEvent;
import com.degoos.wetsponge.event.block.WSBlockModifyEvent;
import com.degoos.wetsponge.event.block.WSBlockPlaceEvent;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import java.util.Optional;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class SpigotBlockListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlock()), new WSBlockSnapshot(WSBlockTypes.AIR
				.getDefaultType()));

			WSBlockBreakEvent wetSpongeEvent = new WSBlockBreakEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional
				.ofNullable(event.getPlayer())
				.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());

			if (wetSpongeEvent.getTransaction().getNewData().getMaterial().getId() != 0) {
				wetSpongeEvent.setCancelled(true);
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getBlock()));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockBreakEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockPlaced(BlockPlaceEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlockAgainst()), new SpigotBlockSnapshot(event
				.getBlockPlaced()));

			WSBlockPlaceEvent wetSpongeEvent = new WSBlockPlaceEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional
				.ofNullable(event.getPlayer())
				.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getBlockPlaced()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockPlaceEvent!");
		}
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new SpigotBlockSnapshot(event.getBlock()), new SpigotBlockSnapshot(event.getToBlock()));

			WSBlockChangeEvent wetSpongeEvent;
			if (transaction.getNewData().getMaterial().getId() == transaction.getOldData().getMaterial().getId())
				wetSpongeEvent = new WSBlockModifyEvent(transaction, new SpigotLocation(event.getBlock().getLocation()), Optional.empty());
			else wetSpongeEvent = new WSBlockChangeEvent(transaction, new SpigotLocation(event.getBlock().getLocation()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new SpigotBlock(event.getToBlock()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockFromToEvent!");
		}
	}

}
