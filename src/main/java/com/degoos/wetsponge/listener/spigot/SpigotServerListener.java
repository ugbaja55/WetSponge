package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.server.WSServerListPingEvent;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.util.CachedServerIcon;

public class SpigotServerListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onServerListPing(ServerListPingEvent event) {
		try {
			WSServerListPingEvent wetSpongeEvent = new WSServerListPingEvent(WSText.getByFormattingText(event.getMotd()), WetSponge.getServer().getServerInfo()
				.getServerIcon(), event.getMaxPlayers(), event.getAddress());
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setMotd(wetSpongeEvent.getDescription().toFormattingText());
			event.setMaxPlayers(wetSpongeEvent.getMaxPlayers());
			try {
				if (wetSpongeEvent.getFavicon().isPresent()) event.setServerIcon((CachedServerIcon) ReflectionUtils
					.instantiateObject(NMSUtils.getOBCClass("util.CraftIconCache"), wetSpongeEvent.getFavicon().get().toBase64()));
				else event.setServerIcon(null);
			} catch (Exception ignore) {}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-ServerListPingEvent!");
		}
	}

}
