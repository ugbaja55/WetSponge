package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.entity.player.WSPlayerSwapHandItemsEvent;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.SpigotEventUtils;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;

public class SpigotNewVersionListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onSwapItems(PlayerSwapHandItemsEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		WSItemStack main = new SpigotItemStack(event.getOffHandItem());
		WSItemStack off = new SpigotItemStack(event.getMainHandItem());
		WSPlayerSwapHandItemsEvent wetSpongeEvent = new WSPlayerSwapHandItemsEvent(WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId())
			.orElse(null), new WSTransaction<>(main, off), new WSTransaction<>(off, main));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		main = wetSpongeEvent.getMainHand().getNewData();
		off = wetSpongeEvent.getOffHand().getNewData();
		event.setMainHandItem(main == null ? new ItemStack(Material.AIR) : ((SpigotItemStack) main).getHandled());
		event.setOffHandItem(off == null ? new ItemStack(Material.AIR) : ((SpigotItemStack) off).getHandled());
		event.setCancelled(wetSpongeEvent.isCancelled());
	}
}
