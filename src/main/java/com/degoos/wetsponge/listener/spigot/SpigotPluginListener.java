package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.plugin.WSBasePluginDisableEvent;
import com.degoos.wetsponge.event.plugin.WSBasePluginEnableEvent;
import com.degoos.wetsponge.plugin.SpigotBasePlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;

public class SpigotPluginListener implements Listener {


	@EventHandler(priority = EventPriority.LOWEST)
	public void onPluginEnable(PluginEnableEvent event) {
		WetSponge.getHookManager().enableHook(event.getPlugin().getName());
		WetSponge.getEventManager().callEvent(new WSBasePluginEnableEvent(new SpigotBasePlugin(event.getPlugin()), event.getPlugin().getName()));
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPluginDisable(PluginDisableEvent event) {
		WetSponge.getEventManager().callEvent(new WSBasePluginDisableEvent(new SpigotBasePlugin(event.getPlugin()), event.getPlugin().getName()));
		WetSponge.getHookManager().disableHook(event.getPlugin().getName());
	}

}
