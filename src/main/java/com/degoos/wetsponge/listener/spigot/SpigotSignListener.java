package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.event.block.WSSignChangeEvent;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SpigotSignListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onSignChange(SignChangeEvent event) {
		try {
			WSSignChangeEvent wetSpongeEvent = new WSSignChangeEvent(new SpigotBlock(event.getBlock()), WetSponge.getServer().getPlayer(event.getPlayer().getName())
				.orElse(null), Arrays.stream(event.getLines()).map(WSText::getByFormattingText).collect(Collectors.toList()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			for (int i = 0; i < 4; i++) event.setLine(i, wetSpongeEvent.getLine(i).toFormattingText());
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-SignChangeEvent!");
		}
	}

}
