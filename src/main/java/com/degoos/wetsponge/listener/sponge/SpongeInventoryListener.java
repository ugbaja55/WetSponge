package com.degoos.wetsponge.listener.sponge;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.inventory.WSInventoryClickEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryCloseEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryOpenEvent;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.inventory.WSSlotPos;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryListener;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.util.Optional;
import net.minecraft.inventory.Slot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent.Drop;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.common.item.inventory.adapter.impl.slots.SlotAdapter;
import org.spongepowered.common.item.inventory.custom.CustomContainer;
import org.spongepowered.common.item.inventory.custom.CustomInventory;

public class SpongeInventoryListener {

	@Listener(order = Order.FIRST)
	public void onClick(ClickInventoryEvent event, @First Player player) {
		try {
			if (event instanceof Drop) return;
			event.getTransactions().forEach(slotTransaction -> {
				int size = event.getTargetInventory().capacity() - 36;
				int slot = slotTransaction.getSlot() instanceof SlotAdapter ? ((SlotAdapter) slotTransaction.getSlot()).getOrdinal()
				                                                            : ((Slot) slotTransaction.getSlot()).slotNumber;
				WSInventory inventory;
				if (slot >= size) {
					slot -= size;
					if (slot >= 27) slot -= 27;
					else slot += 9;
					inventory = new SpongeInventory(player.getInventory());
				} else {
					if (event.getTargetInventory() instanceof CustomContainer) {
						try {
							inventory = new SpongeInventory((Inventory) ReflectionUtils
								.getFirstObject(CustomContainer.class, CustomInventory.class, event.getTargetInventory()));
						} catch (Throwable e) {
							e.printStackTrace();
							inventory = new SpongeInventory(event.getTargetInventory());
						}

					} else inventory = new SpongeInventory(event.getTargetInventory());
				}
				WSSlotPos slotPos = WSSlotPos.getBySlot(9, slot);

				Optional<WSItemStack> from = slotTransaction.getOriginal().getType().equals(ItemTypes.AIR) ? Optional.empty() : Optional
					.of(new SpongeItemStack(slotTransaction.getOriginal().createStack()));
				Optional<WSItemStack> to = slotTransaction.getFinal().getType().equals(ItemTypes.AIR) ? Optional.empty() : Optional
					.of(new SpongeItemStack(slotTransaction.getFinal().createStack()));

				WSInventoryClickEvent wetSpongeEvent = new WSInventoryClickEvent(slotPos, inventory, PlayerParser.getPlayer(player.getUniqueId())
					.orElse(null), new WSTransaction<>(from, to));
				MultiInventoryListener.clickInv(wetSpongeEvent);
				if (wetSpongeEvent.getTransaction().hasChanged()) {
					if (wetSpongeEvent.getTransaction().getNewData().isPresent())
						slotTransaction.setCustom(((SpongeItemStack) wetSpongeEvent.getTransaction().getNewData().get()).getHandled());
					else slotTransaction.setCustom(ItemStack.of(ItemTypes.AIR, 0));
				}
				if (!wetSpongeEvent.isCancelled()) WetSponge.getEventManager().callEvent(wetSpongeEvent);
				slotTransaction.setValid(!wetSpongeEvent.isCancelled());
			});
			if (event.getTransactions().stream().noneMatch(Transaction::isValid)) event.setCancelled(true);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ClickInventoryEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onClose(InteractInventoryEvent.Close event, @First Player player) {
		try {
			WSInventoryCloseEvent wetSpongeEvent = new WSInventoryCloseEvent(new SpongeInventory(event.getTargetInventory()), PlayerParser.getPlayer(player
				.getUniqueId())
				.orElse(null));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (!wetSpongeEvent.isCancelled()) MultiInventoryListener.closeInv(wetSpongeEvent.getPlayer());
			if (wetSpongeEvent.isCancelled()) wetSpongeEvent.setCancelled(true);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-InteractInventoryEvent.Close!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onOpen(InteractInventoryEvent.Open event, @First Player player) {
		try {
			WSInventoryOpenEvent wetSpongeEvent = new WSInventoryOpenEvent(new SpongeInventory(event.getTargetInventory()), PlayerParser.getPlayer(player.getUniqueId())
				.orElse(null));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-InteractInventoryEvent.Open!");
		}
	}
}
