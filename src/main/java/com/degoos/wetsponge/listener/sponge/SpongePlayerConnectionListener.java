package com.degoos.wetsponge.listener.sponge;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.wetspongecommand.WetSpongeSubcommandErrors;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerLoginEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryListener;
import com.degoos.wetsponge.packet.play.server.WSSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnMob;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.user.SpongeUser;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.Optional;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.scheduler.Task;

public class SpongePlayerConnectionListener {

	@Listener(order = Order.FIRST)
	public void onLogin(ClientConnectionEvent.Login event) {
		try {
			WSPlayerLoginEvent wetSpongeEvent = new WSPlayerLoginEvent(new SpongeUser(event.getTargetUser()), SpongeText
				.of(event.getFormatter().getHeader().toText()), SpongeText.of(event.getFormatter().getBody().toText()), SpongeText
				.of(event.getFormatter().getFooter().toText()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setMessage(((SpongeText) wetSpongeEvent.getCancelledMessageHeader()).getHandled(), ((SpongeText) wetSpongeEvent.getCancelledMessage())
				.getHandled(), ((SpongeText) wetSpongeEvent.getCancelledMessageFooter()).getHandled());
			if (event.isCancelled()) event.setCancelled(true);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ClientConnectionEvent.Login!");
		}
	}


	@Listener(order = Order.FIRST)
	public void onJoin(ClientConnectionEvent.Join event) {
		try {
			SpongePlayer player = new SpongePlayer(event.getTargetEntity());
			PlayerParser.addPlayer(player);
			SpongePacketListener.inject(player);
			WSPlayerJoinEvent wetSpongeEvent = new WSPlayerJoinEvent(player, SpongeText.of(event.getMessage()), SpongeText.of(event.getOriginalMessage()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (wetSpongeEvent.getMessage() == null) event.setMessageCancelled(true);
			else event.setMessage(((SpongeText) wetSpongeEvent.getMessage()).getHandled());
			Task.builder().execute(() -> player.getWorld().getPlayers().stream().filter(WSLivingEntity::hasDisguise).forEach(target -> {
				player.sendPacket(WSSPacketDestroyEntities.of(target.getEntityId()));
				player.sendPacket(WSSPacketSpawnMob.of(target));
			})).delayTicks(10).submit(SpongeWetSponge.getInstance());
			if (player.hasPermission("wetsponge.admin") && InternalLogger.getLastStackTrace() != null) WetSpongeSubcommandErrors.sendErrors(player);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ClientConnectionEvent.Join!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onQuit(ClientConnectionEvent.Disconnect event) {
		try {
			Optional<WSPlayer> player = WetSponge.getServer().getPlayer(event.getTargetEntity().getUniqueId());
			if (!player.isPresent()) return;
			WSPlayerQuitEvent wetSpongeEvent = new WSPlayerQuitEvent(player.get(), SpongeText.of(event.getMessage()), SpongeText.of(event.getOriginalMessage()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			MultiInventoryListener.leave(wetSpongeEvent);
			if (wetSpongeEvent.getMessage() == null) event.setMessageCancelled(true);
			else event.setMessage(((SpongeText) wetSpongeEvent.getMessage()).getHandled());
			SpongePacketListener.uninject(player.get());
			PlayerParser.removePlayer(player.get());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ClientConnectionEvent.Disconnect!");
		}
	}

}
