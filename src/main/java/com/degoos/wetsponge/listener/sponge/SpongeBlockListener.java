package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpongeBlockSnapshot;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.block.WSBlockBreakEvent;
import com.degoos.wetsponge.event.block.WSBlockChangeEvent;
import com.degoos.wetsponge.event.block.WSBlockModifyEvent;
import com.degoos.wetsponge.event.block.WSBlockPlaceEvent;
import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import java.util.Optional;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;

public class SpongeBlockListener {

	@Listener(order = Order.FIRST)
	public void onBlockChange(ChangeBlockEvent event) {
		try {
			Optional<WSPlayer> player = event.getCause().first(Player.class)
				.map(target -> WetSponge.getServer().getPlayer(target.getUniqueId()).<NullPointerException>orElseThrow(NullPointerException::new));

			for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {

				WSBlockChangeEvent wetSpongeEvent;
				WSTransaction<WSBlockSnapshot> blockTransaction = new WSTransaction<>(new SpongeBlockSnapshot(transaction
					.getOriginal()), new SpongeBlockSnapshot(transaction.getFinal()));
				WSLocation location = new SpongeLocation(transaction.getOriginal().getLocation().orElse(null));

				if (event instanceof ChangeBlockEvent.Break) wetSpongeEvent = new WSBlockBreakEvent(blockTransaction, location, player);
				else if (event instanceof ChangeBlockEvent.Place) wetSpongeEvent = new WSBlockPlaceEvent(blockTransaction, location, player);
				else if (event instanceof ChangeBlockEvent.Modify) wetSpongeEvent = new WSBlockModifyEvent(blockTransaction, location, player);
				else wetSpongeEvent = new WSBlockChangeEvent(blockTransaction, location);

				WetSponge.getEventManager().callEvent(wetSpongeEvent);

				transaction.setValid(!wetSpongeEvent.isCancelled());
				if (blockTransaction.getNewData().hasChanged()) transaction.setCustom(transaction.getFinal().withState(SpongeDataValueConverter
					.update(blockTransaction.getNewData().getMaterial(), Sponge.getRegistry()
						.getType(BlockType.class, blockTransaction.getNewData().getMaterial().getStringId()).get().getDefaultState())));
			}

			if (event.getTransactions().stream().noneMatch(Transaction::isValid)) event.setCancelled(true);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-ChangeBlockEvent!");
		}
	}

}
