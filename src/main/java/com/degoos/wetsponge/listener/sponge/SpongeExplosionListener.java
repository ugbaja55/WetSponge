package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.world.WSExplosionEvent;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Detonate;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Pre;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpongeExplosion;
import com.degoos.wetsponge.world.SpongeLocation;
import java.util.stream.Collectors;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.world.ExplosionEvent;

public class SpongeExplosionListener {

	@Listener(order = Order.FIRST)
	public void onPreExplosion(ExplosionEvent.Pre event) {
		WSExplosionEvent.Pre wetspongeEvent = new Pre(WorldParser.getOrCreateWorld(event.getTargetWorld().getName(), event.getTargetWorld()), new SpongeExplosion(event
			.getExplosion()));
		WetSponge.getEventManager().callEvent(wetspongeEvent);
		event.setExplosion(((SpongeExplosion) wetspongeEvent.getExplosion()).getHandler());
		event.setCancelled(wetspongeEvent.isCancelled());
	}


	@Listener(order = Order.FIRST)
	public void onDetonate(ExplosionEvent.Detonate event) {
		WSExplosionEvent.Detonate wetSpongeEvent = new Detonate(WorldParser
			.getOrCreateWorld(event.getTargetWorld().getName(), event.getTargetWorld()), new SpongeExplosion(event.getExplosion()), event.getAffectedLocations().stream()
			.map(SpongeLocation::new).collect(Collectors.toList()));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.getAffectedLocations().clear();
		event.getAffectedLocations()
			.addAll(wetSpongeEvent.getAffectedLocations().stream().map(l -> ((SpongeLocation) l).getLocation().getLocation()).collect(Collectors.toList()));
	}

}
