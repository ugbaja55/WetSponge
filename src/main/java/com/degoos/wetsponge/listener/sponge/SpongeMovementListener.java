package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.movement.WSEntityMoveEvent;
import com.degoos.wetsponge.event.entity.movement.WSEntityTeleportEvent;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerChangeWorldEvent;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerMoveEvent;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerTeleportEvent;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.world.World;

public class SpongeMovementListener {

	@Listener(order = Order.FIRST)
	public void onMove(MoveEntityEvent event) {
		try {
			WSLocation to = new SpongeLocation(event.getToTransform().getLocation());
			to.setPitch((float) event.getToTransform().getPitch());
			to.setYaw((float) event.getToTransform().getYaw());
			WSLocation from = new SpongeLocation(event.getFromTransform().getLocation());
			from.setPitch((float) event.getFromTransform().getPitch());
			from.setYaw((float) event.getFromTransform().getYaw());
			WSEntity entity = SpongeEntityParser.getWSEntity(event.getTargetEntity());

			to = callEntityMoveEvent(event, from, to, entity);
			if (event.isCancelled()) return;
			event.setToTransform(getTransform(to));

			if (event instanceof MoveEntityEvent.Teleport) {
				to = callEntityTeleportEvent(event, from, to, entity);
				if (event.isCancelled()) return;
				event.setToTransform(getTransform(to));
			}

			if (!(event.getTargetEntity() instanceof Player)) return;

			WSPlayer player = WetSponge.getServer().getPlayer(entity.getUniqueId()).orElseThrow(NullPointerException::new);

			to = callPlayerMoveEvent(event, from, to, player);
			if (event.isCancelled()) return;
			event.setToTransform(getTransform(to));

			boolean refreshed = false;

			if (event instanceof MoveEntityEvent.Teleport) {
				to = callPlayerTeleportEvent(event, from, to, player);
				if (event.isCancelled()) return;
				Task.builder().delayTicks(5).execute(() -> player.getFakeBlocks().forEach((location, type) -> player.refreshFakeBlock(location)))
					.submit(SpongeWetSponge.getInstance());
				refreshed = true;
				event.setToTransform(getTransform(to));
			}

			WSLocation toFinal = to.clone();
			if (!refreshed) player.getFakeBlocks().forEach((location, type) -> {
				if (location.distance(from) > 100 && location.distance(toFinal) <= 100) player.refreshFakeBlock(location);
			});

			if (event.getFromTransform().getExtent().equals(event.getToTransform().getExtent())) return;
			WetSponge.getEventManager().callEvent(new WSPlayerChangeWorldEvent(PlayerParser.getPlayer(player.getUniqueId()).orElse(null), WorldParser
				.getOrCreateWorld(event.getFromTransform().getExtent().getName(), event.getFromTransform().getExtent()), WorldParser
				.getOrCreateWorld(event.getToTransform().getExtent().getName(), event.getToTransform().getExtent())));
			PlayerParser.resetPlayer(player, player.getUniqueId());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-MoveEntityEvent!");
		}
	}

	private Transform<World> getTransform(WSLocation to) {
		return new Transform<>(((SpongeLocation) to).getHandledWorld(), to.toVector3d(), to.getRotation().toVector3().toDouble());
	}

	private WSLocation callEntityMoveEvent(MoveEntityEvent event, WSLocation from, WSLocation to, WSEntity entity) {
		WSEntityMoveEvent wetSpongeEvent = new WSEntityMoveEvent(entity, from, to);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
		if (event.isCancelled()) return to;
		return wetSpongeEvent.getTo();
	}

	private WSLocation callEntityTeleportEvent(MoveEntityEvent event, WSLocation from, WSLocation to, WSEntity entity) {
		WSEntityTeleportEvent wetSpongeEvent = new WSEntityTeleportEvent(entity, from, to);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
		if (event.isCancelled()) return to;
		return wetSpongeEvent.getTo();
	}

	private WSLocation callPlayerMoveEvent(MoveEntityEvent event, WSLocation from, WSLocation to, WSPlayer player) {
		WSPlayerMoveEvent wetSpongeEvent = new WSPlayerMoveEvent(player, from, to);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
		if (event.isCancelled()) return to;
		return wetSpongeEvent.getTo();
	}

	private WSLocation callPlayerTeleportEvent(MoveEntityEvent event, WSLocation from, WSLocation to, WSPlayer player) {
		WSPlayerTeleportEvent wetSpongeEvent = new WSPlayerTeleportEvent(player, from, to);
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
		if (event.isCancelled()) return to;
		return wetSpongeEvent.getTo();
	}


}
