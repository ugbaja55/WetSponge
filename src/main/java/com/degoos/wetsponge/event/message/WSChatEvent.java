package com.degoos.wetsponge.event.message;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.text.WSText;
import java.util.Optional;
import java.util.Set;

public class WSChatEvent extends WSEvent implements WSCancellable {

	private WSPlayer player;
	private WSText originalMessage, rawMessage, header, body, footer;
	private boolean sendToConsole;
	private Set<WSPlayer> targets;
	private boolean cancelled, textChanged;

	public WSChatEvent(WSPlayer player, WSText originalMessage, WSText rawMessage, WSText header, WSText body, WSText footer, boolean sendToConsole,
		Set<WSPlayer> targets) {
		this.player = player;
		this.originalMessage = originalMessage;
		this.rawMessage = rawMessage;
		this.header = header;
		this.body = body;
		this.footer = footer;
		this.sendToConsole = sendToConsole;
		this.targets = targets;
		this.cancelled = false;
		this.textChanged = false;
	}

	public Optional<WSPlayer> getPlayer() {
		return Optional.ofNullable(player);
	}

	public WSText getMessage() {
		return header.toBuilder().append(body, footer).build();
	}

	public WSText getOriginalMessage() {
		return originalMessage;
	}

	public WSText getRawMessage() {
		return rawMessage;
	}

	public WSText getHeader() {
		return header;
	}

	public void setHeader(WSText header) {
		this.header = header == null ? WSText.empty() : header;
		textChanged = true;
	}

	public WSText getBody() {
		return body;
	}

	public void setBody(WSText body) {
		this.body = body == null ? WSText.empty() : body;
		textChanged = true;
	}

	public WSText getFooter() {
		return footer;
	}

	public void setFooter(WSText footer) {
		this.footer = footer == null ? WSText.empty() : footer;
		textChanged = true;
	}

	public boolean isSendToConsole() {
		return sendToConsole;
	}

	public void setSendToConsole(boolean sendToConsole) {
		this.sendToConsole = sendToConsole;
	}

	public Set<WSPlayer> getTargets() {
		return targets;
	}

	public void setTargets(Set<WSPlayer> targets) {
		this.targets = targets;
	}

	public boolean hasTextChanged () {
		return textChanged;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
