package com.degoos.wetsponge.event.world;

import com.degoos.wetsponge.world.WSChunk;
import com.degoos.wetsponge.world.WSWorld;

public class WSChunkEvent extends WSWorldEvent {

	private WSChunk chunk;

	public WSChunkEvent(WSWorld world, WSChunk chunk) {
		super(world);
		this.chunk = chunk;
	}

	public WSChunk getChunk() {
		return chunk;
	}

}
