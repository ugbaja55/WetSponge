package com.degoos.wetsponge.event.entity.player.interact;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;

public class WSPlayerInteractEvent extends WSPlayerEvent implements WSCancellable {

    private boolean cancelled;

    public WSPlayerInteractEvent(WSPlayer player) {
        super(player);
        cancelled = false;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
