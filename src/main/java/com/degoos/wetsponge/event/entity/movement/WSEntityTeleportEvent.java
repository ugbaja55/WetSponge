package com.degoos.wetsponge.event.entity.movement;

import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerMoveEvent;
import com.degoos.wetsponge.event.entity.WSEntityEvent;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.event.entity.player.movement.WSPlayerTeleportEvent;

/**
 * This Event is exclusive for Sponge until Spigot wants to add a EntityTeleportEvent. This event will be called before the
 * {@link WSPlayerTeleportEvent} and {@link WSPlayerMoveEvent} and after the {@link WSEntityMoveEvent} event.
 */
public class WSEntityTeleportEvent extends WSEntityEvent implements WSCancellable {

    private WSLocation from, to;
    private boolean cancelled;

    public WSEntityTeleportEvent(WSEntity entity, WSLocation from, WSLocation to) {
        super(entity);
        this.from = from;
        this.to = to;
        this.cancelled = false;
    }


    public WSLocation getFrom() {
        return from;
    }

    public WSLocation getTo() {
        return to;
    }

    public void setTo(WSLocation to) {
        this.to = to;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
