package com.degoos.wetsponge.event.entity.player.interact;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;

public class WSPlayerInteractItemEvent extends WSPlayerInteractEvent {

	private WSItemStack itemStack;

	public WSPlayerInteractItemEvent(WSPlayer player, WSItemStack itemStack) {
		super(player);
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack;
	}

	public WSItemStack getItemStack() {
		return itemStack.clone();
	}

	public static class Primary extends WSPlayerInteractItemEvent {

		public Primary(WSPlayer player, WSItemStack itemStack) {
			super(player, itemStack);
		}

		public static class MainHand extends Primary {

			public MainHand(WSPlayer player, WSItemStack itemStack) {
				super(player, itemStack);
			}
		}

		public static class OffHand extends Primary {

			public OffHand(WSPlayer player, WSItemStack itemStack) {
				super(player, itemStack);
			}
		}
	}

	public static class Secondary extends WSPlayerInteractItemEvent {


		public Secondary(WSPlayer player, WSItemStack itemStack) {
			super(player, itemStack);
		}

		public static class MainHand extends Secondary {

			public MainHand(WSPlayer player, WSItemStack itemStack) {
				super(player, itemStack);
			}
		}

		public static class OffHand extends Secondary {

			public OffHand(WSPlayer player, WSItemStack itemStack) {
				super(player, itemStack);
			}
		}
	}
}
