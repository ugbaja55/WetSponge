package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.modifier.WSDamageModifier;
import com.degoos.wetsponge.enums.EnumDamageType;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.util.Validate;

import java.util.HashSet;
import java.util.Set;

public class WSEntityDamageEvent extends WSEntityEvent implements WSCancellable {

    private double baseDamage, firstFinalDamage;
    private boolean cancelled = false, hasFinalDamageChanged;
    private EnumDamageType damageType;
    private Set<WSDamageModifier> damageModifiers;

    public WSEntityDamageEvent(WSEntity entity, double baseDamage, Set<WSDamageModifier> damageModifiers, EnumDamageType damageType, double firstFinalDamage) {
        super(entity);
        this.baseDamage = baseDamage;
        this.damageModifiers = new HashSet<>(damageModifiers);
        this.damageType = damageType;

        hasFinalDamageChanged = false;
        this.firstFinalDamage = firstFinalDamage;
    }

    public double getBaseDamage() {
        return baseDamage;
    }

    public void setBaseDamage(double baseDamage) {
        this.baseDamage = baseDamage;
        this.hasFinalDamageChanged = true;
    }


    public Set<WSDamageModifier> getDamageModifiers() {
        hasFinalDamageChanged = true;
        return new HashSet<>(damageModifiers);
    }

    public double getFinalDamage() {
        if(!hasFinalDamageChanged) return firstFinalDamage;
        double damage = baseDamage;
        for (WSDamageModifier modifier : damageModifiers)
            damage = modifier
                    .getFunction()
                    .applyAsDouble(damage);
        return damage;
    }

    public void setDamageModifier(WSDamageModifier modifier) {
        Validate.notNull(modifier, "Modifier cannot be null!");
        if (damageModifiers.contains(modifier)) damageModifiers.remove(modifier);
        damageModifiers.add(modifier);
        hasFinalDamageChanged = true;
    }

    public EnumDamageType getDamageType() {
        return damageType;
    }

    public boolean willCauseDeath() {
        return ((WSLivingEntity) getEntity()).getHealth() <= getFinalDamage();
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }


}
