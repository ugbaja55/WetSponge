package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;

public class WSPlayerFoodLevelChangeEvent extends WSPlayerEvent implements WSCancellable {

	private int originalFoodLevel, foodLevel;
	private boolean cancelled;

	public WSPlayerFoodLevelChangeEvent(WSPlayer player, int originalFoodLevel, int foodLevel) {
		super(player);
		this.originalFoodLevel = originalFoodLevel;
		this.foodLevel = foodLevel;
		this.cancelled = false;
	}

	public double getOriginalFoodLevel() {
		return originalFoodLevel;
	}

	public int getFoodLevel() {
		return foodLevel;
	}

	public void setFoodLevel(int foodLevel) {
		this.foodLevel = Math.min(0, Math.max(20, foodLevel));
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
