package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.event.WSEvent;

public class WSEntityEvent extends WSEvent {

    private WSEntity entity;

    public WSEntityEvent(WSEntity entity) {
        this.entity = entity;
    }

    public WSEntity getEntity() {
        return entity;
    }

}
