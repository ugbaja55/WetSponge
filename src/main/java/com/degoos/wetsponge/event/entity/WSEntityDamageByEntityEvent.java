package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.modifier.WSDamageModifier;
import com.degoos.wetsponge.enums.EnumDamageType;
import java.util.Set;

public class WSEntityDamageByEntityEvent extends WSEntityDamageEvent {

	private WSEntity damager;

	public WSEntityDamageByEntityEvent(WSEntity entity, double baseDamage, Set<WSDamageModifier> damageModifiers, EnumDamageType damageType, WSEntity damager,
		double firstFinalDamage) {
		super(entity, baseDamage, damageModifiers, damageType, firstFinalDamage);
		this.damager = damager;
	}

	public WSEntity getDamager() {
		return damager;
	}
}
