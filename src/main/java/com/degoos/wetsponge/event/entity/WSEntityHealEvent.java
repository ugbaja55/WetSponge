package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.modifier.WSHealthModifier;
import com.degoos.wetsponge.enums.EnumHealingType;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.util.Validate;
import java.util.HashSet;
import java.util.Set;

public class WSEntityHealEvent extends WSEntityEvent implements WSCancellable {

	private double baseHealth, firstFinalHealth;
	private boolean cancelled = false, hasFinalHeathChanged;
	private EnumHealingType healingType;
	private Set<WSHealthModifier> healthModifiers;

	public WSEntityHealEvent(WSEntity entity, double baseHealth, Set<WSHealthModifier> healthModifiers, EnumHealingType healingType, double firstFinalHealth) {
		super(entity);
		this.baseHealth = baseHealth;
		this.healthModifiers = new HashSet<>(healthModifiers);
		this.healingType = healingType;
		this.firstFinalHealth = firstFinalHealth;
		hasFinalHeathChanged = false;
	}

	public WSEntityHealEvent(WSEntity entity, double baseHealth, Set<WSHealthModifier> healthModifiers, EnumHealingType healingType) {
		super(entity);
		this.baseHealth = baseHealth;
		this.healthModifiers = new HashSet<>(healthModifiers);
		this.healingType = healingType;
		this.firstFinalHealth = 0;
		hasFinalHeathChanged = true;
	}

	public double getBaseHealth() {
		return baseHealth;
	}

	public void setBaseHealth(double baseHealth) {
		this.baseHealth = baseHealth;
		hasFinalHeathChanged = true;
	}


	public Set<WSHealthModifier> getHealthModifiers() {
		hasFinalHeathChanged = true;
		return new HashSet<>(healthModifiers);
	}

	public double getFinalHealth() {
		if (!hasFinalHeathChanged) return firstFinalHealth;
		double damage = baseHealth;
		for (WSHealthModifier modifier : healthModifiers) damage = modifier.getFunction().applyAsDouble(damage);
		return damage;
	}

	public void setHealthModifier(WSHealthModifier modifier) {
		Validate.notNull(modifier, "Modifier cannot be null!");
		if (healthModifiers.contains(modifier)) healthModifiers.remove(modifier);
		healthModifiers.add(modifier);
		hasFinalHeathChanged = true;
	}

	public EnumHealingType getHealingType() {
		return healingType;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}


}
