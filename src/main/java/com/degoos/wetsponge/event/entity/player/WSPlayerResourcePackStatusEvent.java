package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumResourcePackStatus;

public class WSPlayerResourcePackStatusEvent extends WSPlayerEvent {

    private EnumResourcePackStatus status;

    public WSPlayerResourcePackStatusEvent(WSPlayer player, EnumResourcePackStatus status) {
        super(player);
        this.status = status;
    }

    public EnumResourcePackStatus getStatus() {
        return status;
    }
}
