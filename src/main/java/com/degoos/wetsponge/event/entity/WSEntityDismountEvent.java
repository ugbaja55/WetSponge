package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;

public class WSEntityDismountEvent extends WSEntityRideEvent {

	public WSEntityDismountEvent(WSEntity entity, WSEntity vehicle) {
		super(entity, vehicle);
	}
}
