package com.degoos.wetsponge.event.entity.player.connection;


import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.WSUser;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.event.EventHandler;

public class WSPlayerLoginEvent extends WSEvent implements WSCancellable {

	private WSUser user;
	private WSText cancelledMessageHeader, cancelledMessage, cancelledMessageFooter;
	private boolean cancelled;


	public WSPlayerLoginEvent (WSUser user, WSText cancelledMessageHeader, WSText cancelledMessage, WSText cancelledMessageFooter) {
		this.user = user;
		this.cancelledMessageHeader = cancelledMessageHeader;
		this.cancelledMessage = cancelledMessage;
		this.cancelledMessageFooter = cancelledMessageFooter;
		this.cancelled = false;
	}


	public WSUser getUser () {
		return user;
	}


	public WSText getCancelledMessageHeader () {
		return cancelledMessageHeader;
	}


	public void setCancelledMessageHeader (WSText cancelledMessageHeader) {
		Validate.notNull(cancelledMessageHeader, "CancelledMessageHeader cannot be null!");
		this.cancelledMessageHeader = cancelledMessageHeader;
	}


	public WSText getCancelledMessage () {
		return cancelledMessage;
	}


	public void setCancelledMessage (WSText cancelledMessage) {
		Validate.notNull(cancelledMessage, "CancelledMessage cannot be null!");
		this.cancelledMessage = cancelledMessage;
	}


	public WSText getCancelledMessageFooter () {
		return cancelledMessageFooter;
	}


	public void setCancelledMessageFooter (WSText cancelledMessageFooter) {
		Validate.notNull(cancelledMessageFooter, "CancelledMessageFooter cannot be null!");
		this.cancelledMessageFooter = cancelledMessageFooter;
	}


	@EventHandler
	public boolean isCancelled () {
		return cancelled;
	}


	@EventHandler
	public void setCancelled (boolean cancelled) {
		this.cancelled = cancelled;
	}
}
