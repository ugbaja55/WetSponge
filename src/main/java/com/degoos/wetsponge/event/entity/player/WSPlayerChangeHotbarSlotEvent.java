package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;

public class WSPlayerChangeHotbarSlotEvent extends WSPlayerEvent implements WSCancellable {

	private int previousSlot, newSlot;
	private boolean cancelled;

	public WSPlayerChangeHotbarSlotEvent(WSPlayer player, int previousSlot, int newSlot) {
		super(player);
		this.previousSlot = previousSlot;
		this.newSlot = newSlot;
		this.cancelled = false;
	}

	public int getPreviousSlot() {
		return previousSlot;
	}

	public int getNewSlot() {
		return newSlot;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
