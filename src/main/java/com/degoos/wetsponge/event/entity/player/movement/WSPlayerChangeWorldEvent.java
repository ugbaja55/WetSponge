package com.degoos.wetsponge.event.entity.player.movement;

import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.movement.WSEntityMoveEvent;

/**
 * This event will be called after the {@link WSEntityMoveEvent} event if the server is a Sponge server,
 * (@link {@link WSPlayerMoveEvent} and .
 */
public class WSPlayerChangeWorldEvent extends WSPlayerEvent {

    private WSWorld from, to;

    public WSPlayerChangeWorldEvent(WSPlayer player, WSWorld from, WSWorld to) {
        super(player);
        this.from = from;
        this.to = to;
    }

    public WSWorld getFrom() {
        return from;
    }

    public WSWorld getTo() {
        return to;
    }
}
