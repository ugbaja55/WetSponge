package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.world.WSLocation;

public class WSPlayerRespawnEvent extends WSPlayerEvent {

    private WSTransaction<WSLocation> locationTransform;
    private boolean isBedRespawn;


    public WSPlayerRespawnEvent(WSPlayer player, WSTransaction<WSLocation> locationTransform, boolean isBedRespawn) {
        super(player);
        this.locationTransform = locationTransform;
        this.isBedRespawn = isBedRespawn;
    }

    public WSTransaction<WSLocation> getLocationTransform() {
        return locationTransform;
    }

    public boolean isBedRespawn() {
        return isBedRespawn;
    }
}
