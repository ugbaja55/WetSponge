package com.degoos.wetsponge.event.entity.player.bed;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

public class WSPlayerLeaveBedEvent extends WSPlayerBedEvent {

    public WSPlayerLeaveBedEvent(WSPlayer player, WSBlock bedBlock) {
        super(player, bedBlock);
    }
}
