package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.event.WSEvent;

public class WSBlockEvent extends WSEvent {

    WSBlock block;

    public WSBlockEvent(WSBlock block) {
        this.block = block;
    }

    public WSBlock getBlock() {
        return block;
    }

    public void setBlock(WSBlock block) {
        this.block = block;
    }
}
