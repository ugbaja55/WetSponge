package com.degoos.wetsponge.event.block;

import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSLocation;

public class WSBlockChangeEvent extends WSEvent implements WSCancellable {

    private WSTransaction<WSBlockSnapshot> transaction;
    private WSLocation location;
    private boolean cancelled;

    public WSBlockChangeEvent(WSTransaction<WSBlockSnapshot> transaction, WSLocation location) {
        Validate.notNull(transaction, "Transaction cannot be null!");
        Validate.notNull(location, "Location cannot be null!");
        this.transaction = transaction;
        this.location = location;
        cancelled = false;
    }

    public WSTransaction<WSBlockSnapshot> getTransaction() {
        return transaction;
    }

    public WSLocation getLocation() {
        return location.clone();
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
