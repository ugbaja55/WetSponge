package com.degoos.wetsponge.event.plugin;

import com.degoos.wetsponge.plugin.WSBasePlugin;

/**
 * This event only works in {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}
 * due to {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge} doesn't have plugin events.
 */
public class WSBasePluginEnableEvent extends WSBasePluginEvent {

	public WSBasePluginEnableEvent(WSBasePlugin basePlugin, String pluginId) {
		super(basePlugin, pluginId);
	}
}
