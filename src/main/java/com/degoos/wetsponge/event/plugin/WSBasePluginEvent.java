package com.degoos.wetsponge.event.plugin;

import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.plugin.WSBasePlugin;

/**
 * These events only work in {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}
 * due to {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge} doesn't have plugin events.
 */
public class WSBasePluginEvent extends WSEvent {

	private WSBasePlugin basePlugin;
	private String pluginId;

	public WSBasePluginEvent(WSBasePlugin basePlugin, String pluginId) {
		this.basePlugin = basePlugin;
		this.pluginId = pluginId;
	}

	public Object getBasePlugin() {
		return basePlugin;
	}

	public String getPluginId() {
		return pluginId;
	}
}
