package com.degoos.wetsponge.event.command;


import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;

public class WSSendCommandEvent extends WSEvent implements WSCancellable {

	private String          command;
	private String[]        arguments;
	private WSCommandSource source;
	private boolean         cancelled;


	public WSSendCommandEvent (String command, String[] arguments, WSCommandSource source) {
		this.command = command;
		this.arguments = arguments;
		this.source = source;
		cancelled = false;
	}


	public String getCommand () {
		return command;
	}


	public void setCommand (String command) {
		this.command = command;
	}


	public String[] getArguments () {
		return arguments;
	}


	public void setArguments (String[] arguments) {
		this.arguments = arguments;
	}


	public WSCommandSource getSource () {
		return source;
	}


	@Override
	public boolean isCancelled () {
		return cancelled;
	}


	@Override
	public void setCancelled (boolean cancelled) {
		this.cancelled = cancelled;
	}
}
