package com.degoos.wetsponge.resource;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.sql.DriverManager;

public class SQLDriverUtils {
    public static void loadDrivers() {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            switch (WetSponge.getServerType()) {
                case SPIGOT:  		case PAPER_SPIGOT:
                    DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                    break;
                case SPONGE:
                    DriverManager.registerDriver(new org.h2.Driver());
                    DriverManager.registerDriver(new org.mariadb.jdbc.Driver());
                    break;
            }
        } catch (Throwable e) {
            InternalLogger.sendError(WSText.of("Error loading a JDBC driver"));
            e.printStackTrace();
        }
    }
}
