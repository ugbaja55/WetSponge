package com.degoos.wetsponge.resource;

import it.unimi.dsi.fastutil.objects.ObjectIterator;

public class WSObjectIterator<T> implements ObjectIterator<T> {

	private ObjectIterator<T> iterator;
	private T                 current;

	public WSObjectIterator(ObjectIterator<T> iterator) {
		this.iterator = iterator;
		this.current = null;
	}

	@Override
	public int skip(int n) {
		return iterator.skip(n);
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	@Override
	public T next() {
		return current = iterator.next();
	}


	public boolean hasCurrent() {
		return current != null;
	}

	public T current() {
		return current;
	}

	@Override
	public void remove() {
		iterator.remove();
		current = null;
	}
}
