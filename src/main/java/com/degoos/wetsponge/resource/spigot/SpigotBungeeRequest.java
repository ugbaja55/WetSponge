package com.degoos.wetsponge.resource.spigot;

import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

public class SpigotBungeeRequest<T> {

    private Player player;
    private String request;
    private CompletableFuture<T> completableFuture;

    public SpigotBungeeRequest(Player player, String request, CompletableFuture<T> completableFuture) {
        this.player = player;
        this.request = request;
        this.completableFuture = completableFuture;
    }

    public Player getPlayer() {
        return player;
    }


    public String getRequest() {
        return request;
    }


    public CompletableFuture<T> getCompletableFuture() {
        return completableFuture;
    }

}
