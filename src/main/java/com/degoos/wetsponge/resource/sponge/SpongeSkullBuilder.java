package com.degoos.wetsponge.resource.sponge;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.profile.property.ProfileProperty;

import java.net.URL;
import java.util.UUID;

public class SpongeSkullBuilder {

    public String getTexture(GameProfile gameProfile) {
        return gameProfile.getPropertyMap().get("textures").stream().findAny().map(ProfileProperty::getValue).orElse(null);
    }

    public static GameProfile setTexture(GameProfile gameProfile, String texture) {
        if (texture == null) gameProfile.getPropertyMap().removeAll("textures");
        else gameProfile.addProperty("textures", ProfileProperty.of("textures", texture));
        return gameProfile;
    }

    public static String getTexture(URL url) {
        if (url == null) return null;
        return String.format("{textures:{SKIN:{url:\"%s\"}}}", url.toExternalForm());
    }

    public static String getTexture(String url) {
        if (url == null) return null;
        return String.format("{textures:{SKIN:{url:\"%s\"}}}", url);
    }

    public static GameProfile getGameProfileByName(String name) {
        return GameProfile.of(UUID.randomUUID(), name);
    }

    public static GameProfile getEmptyGameProfile() {
        return GameProfile.of(UUID.randomUUID());
    }

    public static GameProfile getGameProfile(BlockState blockState) {
        return blockState.get(Keys.REPRESENTED_PLAYER).orElse(getEmptyGameProfile());
    }

    public static GameProfile getGameProfile(ItemStack itemStack) {
        return itemStack.get(Keys.REPRESENTED_PLAYER).orElse(getEmptyGameProfile());
    }


    //ITEMS

    public static ItemStack createItemStackByUnknownFormat(String string) {
        if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:"))
            return createItemStackByTexture(getTexture(string));
        else if (string.length() <= 16) return createItemStackByPlayerName(string);
        else return createItemStackByTexture(string);
    }

    public static ItemStack createItemStackByTexture(String texture) {
        ItemStack itemStack = ItemStack.of(ItemTypes.SKULL, 1);
        itemStack.offer(Keys.SKULL_TYPE, SkullTypes.PLAYER);
        itemStack.offer(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(itemStack), texture));
        return itemStack;
    }

    public static ItemStack createItemStackByURL(URL url) {
        ItemStack itemStack = ItemStack.of(ItemTypes.SKULL, 1);
        itemStack.offer(Keys.SKULL_TYPE, SkullTypes.PLAYER);
        itemStack.offer(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(itemStack), getTexture(url)));
        return itemStack;
    }

    public static ItemStack createItemStackByPlayerName(String name) {
        ItemStack itemStack = ItemStack.of(ItemTypes.SKULL, 1);
        itemStack.offer(Keys.SKULL_TYPE, SkullTypes.PLAYER);
        itemStack.offer(Keys.REPRESENTED_PLAYER, getGameProfileByName(name));
        return itemStack;
    }

    //ITEM UPDATE

    public static ItemStack updateItemStackByUnknownFormat(ItemStack itemStack, String string) {
        if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:"))
            return updateItemStackByTexture(itemStack, getTexture(string));
        else if (string.length() <= 16) return updateItemStackByPlayerName(itemStack, string);
        else return updateItemStackByTexture(itemStack, string);
    }

    public static ItemStack updateItemStackByTexture(ItemStack itemStack, String texture) {
        itemStack.offer(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(itemStack), texture));
        return itemStack;
    }

    public static ItemStack updateItemStackByURL(ItemStack itemStack, URL url) {
        itemStack.offer(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(itemStack), getTexture(url)));
        return itemStack;
    }

    public static ItemStack updateItemStackByPlayerName(ItemStack itemStack, String name) {
        itemStack.offer(Keys.REPRESENTED_PLAYER, getGameProfileByName(name));
        return itemStack;
    }

    //BLOCKS

    public static BlockState updateSkullByUnknownFormat(BlockState skull, String string) {
        if (string.toLowerCase().startsWith("http:") || string.toLowerCase().startsWith("https:"))
            return updateSkullByTexture(skull, getTexture(string));
        else if (string.length() <= 16) return updateSkullByPlayerName(skull, string);
        else return updateSkullByTexture(skull, string);
    }

    public static BlockState updateSkullByTexture(BlockState skull, String texture) {
        return skull.with(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(skull), texture)).orElse(skull);
    }

    public static BlockState updateSkullByURL(BlockState skull, URL url) {
        return skull.with(Keys.REPRESENTED_PLAYER, setTexture(getGameProfile(skull), getTexture(url))).orElse(skull);
    }

    public static BlockState updateSkullByPlayerName(BlockState skull, String name) {
        return skull.with(Keys.REPRESENTED_PLAYER, getGameProfileByName(name)).orElse(skull);
    }
}
