package com.degoos.wetsponge.resource;

import com.degoos.wetsponge.enums.EnumTextColor;

public class StringUtils {

    public static String stripColors(String string) {
        if (string == null) return string;
        for (EnumTextColor textColor : EnumTextColor.values())
            string = string.replace("\u00A7" + textColor.getId(), "");
        return string;
    }

}
