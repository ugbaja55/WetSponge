package com.degoos.wetsponge.particle;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3f;

import java.util.Collection;

/**
 * Represents a particle. {@link WSPlayer player}s can see them.
 */
public interface WSParticle {

    /**
     * Spawns a particle to all players in the list. Radius is (1, 1, 1) by default.
     *
     * @param location the center of the radius where particles are spawned.
     * @param speed    the speed of the particle.
     * @param amount   the amount of particles spawned.
     * @param players  the players.
     */
    void spawnParticle(WSLocation location, float speed, int amount, WSPlayer... players);

    /**
     * Spawns a particle to all players in the list. Radius is (1, 1, 1) by default.
     *
     * @param location the center of the radius where particles are spawned.
     * @param speed    the speed of the particle.
     * @param amount   the amount of particles spawned.
     * @param players  the players.
     */
    void spawnParticle(WSLocation location, float speed, int amount, Collection<WSPlayer> players);

    /**
     * Spawns a particle to all players in the selected area. Radius is (1, 1, 1) by default.
     *
     * @param location     the center of the radius where particles are spawned.
     * @param speed        the speed of the particle.
     * @param amount       the amount of particles spawned.
     * @param playerRadius the area.
     */
    void spawnParticle(WSLocation location, float speed, int amount, Vector3d playerRadius);

    /**
     * Spawns a particle to all players in the list.
     *
     * @param location the center of the radius where particles are spawned.
     * @param speed    the speed of the particle.
     * @param amount   the amount of particles spawned.
     * @param radius   the area where particles are spawned.
     * @param players  the players.
     */
    void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSPlayer... players);

    /**
     * Spawns a particle to all players in the list.
     *
     * @param location the center of the radius where particles are spawned.
     * @param speed    the speed of the particle.
     * @param amount   the amount of particles spawned.
     * @param radius   the area where particles are spawned.
     * @param players  the players.
     */
    void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Collection<WSPlayer> players);

    /**
     * Spawns a particle to all players in the selected area.
     *
     * @param location     the center of the radius where particles are spawned.
     * @param speed        the speed of the particle.
     * @param amount       the amount of particles spawned.
     * @param radius       the area where particles are spawned.
     * @param playerRadius the area.
     */
    void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Vector3d playerRadius);

    /**
     * Returns the old minecraft id of the particle. This is used
     * by {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}, in the version {@link com.degoos.wetsponge.enums.EnumServerVersion#MINECRAFT_OLD old}.
     *
     * @return the old minecraft id of the particle.
     */
    String getOldMinecraftId();


    /**
     * Returns the new minecraft id of the particle. This id is used by {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge} in
     * all versions. The format of this id is <code>minecraft:NAME</code>.
     *
     * @return the new minecraft id of the particle.
     */
    String getMinecraftId();

    /**
     * Returns the Spigot id of the particle. This is used
     * by {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}, in the version
     * {@link com.degoos.wetsponge.enums.EnumServerVersion#MINECRAFT_1_9 1.9} and later.
     *
     * @return the old minecraft id of the particle.
     */
    String getSpigotName();

}
