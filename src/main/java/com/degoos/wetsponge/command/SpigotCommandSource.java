package com.degoos.wetsponge.command;


import com.degoos.wetsponge.text.WSText;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.util.Set;
import java.util.stream.Collectors;

public class SpigotCommandSource implements WSCommandSource {

    private CommandSender sender;


    public SpigotCommandSource(CommandSender sender) {
        this.sender = sender;
    }


    @Override
    public String getName() {
        return sender.getName();
    }


    @Override
    public void sendMessage(String message) {
        getHandled().sendMessage(message);
    }


    @Override
    public boolean hasPermission(String name) {
        return sender.hasPermission(name);
    }

    @Override
    public Set<String> getPermissions() {
        return getHandled().getEffectivePermissions().stream().map(PermissionAttachmentInfo::getPermission).collect(Collectors.toSet());
    }



    @Override
    public void sendMessage(WSText text) {
        getHandled().sendMessage(text.toFormattingText());
    }


    @Override
    public void sendMessages(String... messages) {
        for (String message : messages) sendMessage(message);
    }


    @Override
    public void sendMessages(WSText... texts) {
        for (WSText text : texts) sendMessage(text);
    }

    @Override
    public void performCommand(String command) {
        Bukkit.dispatchCommand(getHandled(), command);
    }


    @Override
    public CommandSender getHandled() {
        return sender;
    }

}
