package com.degoos.wetsponge.command.ramified;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.util.Validate;

import java.util.List;

public abstract class WSSubcommand {

    private String name;
    private WSRamifiedCommand command;

    public WSSubcommand(String name, WSRamifiedCommand command) {
        Validate.notNull(name, "Name cannot be null!");
        Validate.notNull(command, "Command cannot be null!");
        this.name = name;
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public WSRamifiedCommand getCommand() {
        return command;
    }

    public abstract void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments);

    public abstract List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSSubcommand that = (WSSubcommand) o;

        return name.equalsIgnoreCase(that.name);
    }

    @Override
    public int hashCode() {
        return name.toLowerCase().hashCode();
    }
}
