package com.degoos.wetsponge.command.ramified;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.util.ListUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class WSRamifiedSubcommand extends WSSubcommand {

	private Set<WSSubcommand> subcommands;
	private WSSubcommand notFoundSubcommand;

	public WSRamifiedSubcommand(String name, WSSubcommand notFoundSubcommand, WSRamifiedCommand command, WSSubcommand... subcommands) {
		super(name, command);
		this.subcommands = ListUtils.toSet(subcommands);
		this.notFoundSubcommand = notFoundSubcommand;
	}

	public WSRamifiedSubcommand(String name, WSSubcommand notFoundSubcommand, WSRamifiedCommand command, Set<WSSubcommand> subcommands) {
		super(name, command);
		this.subcommands = subcommands;
		this.notFoundSubcommand = notFoundSubcommand;
	}

	public Set<WSSubcommand> getSubcommands() {
		return subcommands;
	}

	public void setSubcommands(Set<WSSubcommand> subcommands) {
		this.subcommands = subcommands;
	}

	public boolean addSubcommand(WSSubcommand subcommand) {
		return subcommands.add(subcommand);
	}

	public boolean removeSubcommand(WSSubcommand subcommand) {
		return subcommands.remove(subcommand);
	}

	public void clearSubcommands() {
		subcommands.clear();
	}

	public WSSubcommand getNotFoundSubcommand() {
		return notFoundSubcommand;
	}

	public void setNotFoundSubcommand(WSSubcommand notFoundSubcommand) {
		this.notFoundSubcommand = notFoundSubcommand;
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if (!beforeExecute(commandSource, command, arguments, remainingArguments)) return;
		if (remainingArguments.length == 0) {
			if (notFoundSubcommand != null) notFoundSubcommand.executeCommand(commandSource, command, arguments, remainingArguments);
			return;
		}
		Optional<WSSubcommand> optional = subcommands.stream().filter(subcommand -> subcommand.getName().equalsIgnoreCase(remainingArguments[0])).findAny();
		if (!optional.isPresent()) {
			if (notFoundSubcommand != null)
				notFoundSubcommand.executeCommand(commandSource, command, arguments, Arrays.copyOfRange(remainingArguments, 1, remainingArguments.length));
			return;
		}
		optional.get().executeCommand(commandSource, command, arguments, Arrays.copyOfRange(remainingArguments, 1, remainingArguments.length));
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if (remainingArguments.length == 0) return new ArrayList<>();
		return remainingArguments.length == 1 ? subcommands.stream().map(WSSubcommand::getName)
			.filter(name -> name.toLowerCase().startsWith(remainingArguments[0].toLowerCase()))
			.collect(Collectors.toList()) : subcommands.stream().filter(subcommand -> subcommand.getName().toLowerCase().equals(remainingArguments[0].toLowerCase()))
			       .findAny().map(subcommand -> subcommand.sendTab(commandSource, command, arguments,
				Arrays.copyOfRange(remainingArguments, 1, remainingArguments.length)))
			       .orElse(new ArrayList<>());
	}

	public boolean beforeExecute(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		return true;
	}
}
