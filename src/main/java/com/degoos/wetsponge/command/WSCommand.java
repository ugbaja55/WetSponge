package com.degoos.wetsponge.command;


import co.aikar.wetspongetimings.Timing;
import co.aikar.wetspongetimings.WetSpongeTimings;
import com.degoos.wetsponge.util.Validate;

import java.util.Arrays;
import java.util.List;

public abstract class WSCommand {

    private String       name;
    private String       description;
    private List<String> aliases;
    public  Timing       timings;


    public WSCommand(String name, String description, String... aliases) {
        Validate.notNull(name, "Name cannot be null!");
        Validate.notNull(description, "Description cannot be null!");
        this.name = name;
        this.description = description;
        this.aliases = Arrays.asList(aliases);
    }


    public abstract void executeCommand(WSCommandSource commandSource, String command, String[] arguments);

    public abstract List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments);


    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void addAlias(String alias) {
        aliases.add(alias);
    }

    public void removeAlias(String alias) {
        aliases.remove(alias);
    }

    public boolean hasAlias (String alias) {
        return aliases.stream().anyMatch(target -> target.equalsIgnoreCase(alias));
    }

    public void setDescription(String description) {
        Validate.notNull(description, "Description cannot be null!");
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSCommand wsCommand = (WSCommand) o;

        return name.equals(wsCommand.name);
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
