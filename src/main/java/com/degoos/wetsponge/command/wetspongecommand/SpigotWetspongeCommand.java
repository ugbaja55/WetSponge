package com.degoos.wetsponge.command.wetspongecommand;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.SpigotCommandSource;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.SpigotConsoleSource;
import com.degoos.wetsponge.parser.player.PlayerParser;
import java.util.Arrays;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SpigotWetspongeCommand implements CommandExecutor {


	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
		if (strings.length == 0) return true;
		WetSponge.getCommandManager().getCommand(strings[0]).ifPresent(target -> target
			.executeCommand(getCommandSource(commandSender), command.getName(), strings.length == 1 ? new String[0] : Arrays.copyOfRange(strings, 1, strings.length)));
		return true;
	}

	private WSCommandSource getCommandSource(CommandSender source) {
		if (source instanceof Player) return PlayerParser.getPlayer(((Player) source).getUniqueId()).orElse(null);
		if (source instanceof ConsoleCommandSender) return new SpigotConsoleSource((ConsoleCommandSender) source);
		else return new SpigotCommandSource(source);
	}
}
