package com.degoos.wetsponge.command.wetspongecommand.timings;

import co.aikar.wetspongetimings.Timings;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;
import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsPaste extends WSSubcommand {


	public WetSpongeSubcommandTimingsPaste(WSRamifiedCommand command) {
		super("paste", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		if (!Timings.isTimingsEnabled()) {
			source.sendMessage(WetSpongeMessages.getMessage("command.timings.notEnabled").orElse(WSText.empty()));
			return;
		}
		Timings.generateReport(source);

	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
