package com.degoos.wetsponge.command.wetspongecommand.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.text.WSText;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WetSpongeSubcommandPluginUnload extends WSSubcommand {

    private WSSubcommand notFoundSubcommand;

    public WetSpongeSubcommandPluginUnload(WSRamifiedCommand command, WSSubcommand notFoundSubcommand) {
        super("unload", command);
        this.notFoundSubcommand = notFoundSubcommand;
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        if (remainingArguments.length == 0) {
            notFoundSubcommand.executeCommand(commandSource, command, arguments, remainingArguments);
            return;
        }
        Optional<WSPlugin> optional = WSPluginManager.getInstance().getPlugin(remainingArguments[0]);
        if (!optional.isPresent()) {
            commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.common.notFound").orElse(WSText.empty()));
            return;
        }
        unloadPlugin(commandSource, optional.get());
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return WetSponge.getPluginManager()
                .getPluginFiles()
                .keySet()
                .stream()
                .filter(arg -> arg.toLowerCase().startsWith(remainingArguments[0].toLowerCase()))
                .collect(Collectors.toList());
    }

    public static void unloadPlugin(WSCommandSource source, WSPlugin plugin) {
        WSPluginManager.getInstance().getPlugins().stream().filter(target -> target.isDependency(plugin))
                .forEach(target -> unloadPlugin(source, target));
        source.sendMessage(WetSpongeMessages.getMessage("command.plugin.unload.unloading", "<PLUGIN>",
                plugin.getId()).orElse(WSText.empty()));
        WSPluginManager.getInstance().unloadPlugin(plugin);
        source.sendMessage(WetSpongeMessages.getMessage("command.plugin.unload.done", "<PLUGIN>",
                plugin.getId()).orElse(WSText.empty()));
    }
}
