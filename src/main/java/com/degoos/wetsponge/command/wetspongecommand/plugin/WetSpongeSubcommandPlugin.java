package com.degoos.wetsponge.command.wetspongecommand.plugin;

import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSRamifiedSubcommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

public class WetSpongeSubcommandPlugin extends WSRamifiedSubcommand {

    public WetSpongeSubcommandPlugin(WSSubcommand notFoundSubcommand, WSRamifiedCommand command) {
        super("plugin", notFoundSubcommand, command);
        addSubcommand(new WetSpongeSubcommandPluginLoad(command, notFoundSubcommand));
        addSubcommand(new WetSpongeSubcommandPluginUnload(command, notFoundSubcommand));
        addSubcommand(new WetSpongeSubcommandPluginReload(command, notFoundSubcommand));
        addSubcommand(new WetSpongeSubcommandPluginList(command));
    }
}
