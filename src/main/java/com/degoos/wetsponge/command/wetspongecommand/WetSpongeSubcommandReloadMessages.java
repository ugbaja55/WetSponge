package com.degoos.wetsponge.command.wetspongecommand;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.util.List;

public class WetSpongeSubcommandReloadMessages extends WSSubcommand {


    public WetSpongeSubcommandReloadMessages(WSRamifiedCommand command) {
        super("reloadMessages", command);
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        InternalLogger.sendInfo("Reloading messages...");
        WetSpongeMessages.load();
        InternalLogger.sendDone("All messages have been reloaded!");
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.reloadMessages.done").orElse(WSText.empty()));

    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return null;
    }
}
