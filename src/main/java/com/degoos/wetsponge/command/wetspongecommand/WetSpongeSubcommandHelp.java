package com.degoos.wetsponge.command.wetspongecommand;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandHelp extends WSSubcommand {

    public WetSpongeSubcommandHelp(WSRamifiedCommand command) {
        super("help", command);
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.commands.header").orElse(WSText.empty()).toBuilder().center().build());
        getCommand().getSubcommands().forEach(subcommand -> commandSource.sendMessage(WetSpongeMessages
                .getMessage("command.commands." + subcommand.getName())
                        .orElse(WSText.empty())
                        .toBuilder()
                        .hoverAction(WSShowTextAction
                                .of(WetSpongeMessages.getMessage("command.commands." + subcommand.getName() + "HoverAction")
                                        .orElse(WSText.empty()))).build()));
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.commands.footer").orElse(WSText.empty()).toBuilder().center().build());
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return new ArrayList<>();
    }
}
