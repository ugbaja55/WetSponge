package com.degoos.wetsponge.command.wetspongecommand.timings;

import co.aikar.wetspongetimings.Timings;
import co.aikar.wetspongetimings.WetSpongeTimingsFactory;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;
import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsCost extends WSSubcommand {


	public WetSpongeSubcommandTimingsCost(WSRamifiedCommand command) {
		super("cost", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		if (!Timings.isTimingsEnabled()) {
			source.sendMessage(WetSpongeMessages.getMessage("command.timings.notEnabled").orElse(WSText.empty()));
			return;
		}
		source.sendMessage(WetSpongeMessages.getMessage("command.timings.cost", "<COST>", String.valueOf(WetSpongeTimingsFactory.getCost())).orElse(WSText.empty()));
	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
