package com.degoos.wetsponge.command.wetspongecommand.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.text.WSText;

import java.util.*;
import java.util.stream.Collectors;

public class WetSpongeSubcommandPluginReload extends WSSubcommand {

    private WSSubcommand notFoundSubcommand;
    private Map<String, Set<String>> dependencies;

    public WetSpongeSubcommandPluginReload(WSRamifiedCommand command, WSSubcommand notFoundSubcommand) {
        super("reload", command);
        this.notFoundSubcommand = notFoundSubcommand;
        dependencies = new HashMap<>();
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        if (remainingArguments.length == 0) {
            notFoundSubcommand.executeCommand(commandSource, command, arguments, remainingArguments);
            return;
        }
        Optional<WSPlugin> optional = WSPluginManager.getInstance().getPlugin(remainingArguments[0]);
        if (!optional.isPresent()) {
            commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.common.notFound").orElse(WSText.empty()));
            return;
        }
        WSPlugin plugin = optional.get();
        String id = plugin.getId();
        reloadPluginPhase1(plugin);
        WetSpongeSubcommandPluginUnload.unloadPlugin(commandSource, plugin);
        reloadPluginPhase2(commandSource, id);
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return WetSponge.getPluginManager()
                .getPluginFiles()
                .keySet()
                .stream()
                .filter(arg -> arg.toLowerCase().startsWith(remainingArguments[0].toLowerCase()))
                .collect(Collectors.toList());
    }

    public void reloadPluginPhase1(WSPlugin plugin) {
        List<WSPlugin> dependencies = WSPluginManager.getInstance().getPlugins().stream()
                .filter(target -> target.isDependency(plugin)).collect(Collectors.toList());
        Set<String> names = dependencies.stream().map(WSPlugin::getId).collect(Collectors.toSet());
        this.dependencies.put(plugin.getId(), names);
        dependencies.forEach(this::reloadPluginPhase1);
    }

    public void reloadPluginPhase2(WSCommandSource source, String name) {
        Set<String> dependencies = this.dependencies.get(name);
        WetSpongeSubcommandPluginLoad.loadPlugin(source, name);
        dependencies.forEach(target -> reloadPluginPhase2(source, target));
    }
}
