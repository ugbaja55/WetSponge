package com.degoos.wetsponge.command.wetspongecommand.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.plugin.WSPluginDescription;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSOpenURLAction;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandPluginList extends WSSubcommand {

    public WetSpongeSubcommandPluginList(WSRamifiedCommand command) {
        super("list", command);
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.list.header").orElse(WSText.empty()).toBuilder().center().build());

        WetSponge.getPluginManager().getPlugins().forEach(plugin -> {
            WSPluginDescription description = plugin.getPluginDescription();

            WSText.Builder message = WetSpongeMessages.getMessage("command.plugin.list.plugin", "<PLUGIN>", plugin.getId())
                    .orElse(WSText.empty())
                    .toBuilder()
                    .hoverAction(WSShowTextAction.of(
                            WetSpongeMessages.getMessage("command.plugin.list.pluginHoverAction", "<NAME>", plugin.getId(), "<VERSION>", description.getVersion(),
                                    "<DESCRIPTION>",
                                    description.getDescription() == null ? "-" : description.getDescription(), "<AUTHORS>",
                                    description.getAuthors().isEmpty() ? "-" : description.getAuthors().toString(), "<WEBSITE>",
                                    description.getWebsite() == null ? "-" : description.getWebsite(), "<DEPENDENCIES>",
                                    description.getDepend().isEmpty() ? "-" : description.getDepend().toString(), "<SOFT_DEPENDENCIES>",
                                    description.getSoftDepend().isEmpty() ? "-" : description.getSoftDepend().toString()).orElse(WSText.empty())));

            try {
                message.clickAction(WSOpenURLAction.of(new URL(description.getWebsite())));
            } catch (Throwable ignore) {
            }

            commandSource.sendMessage(message.build());
        });
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.list.footer").orElse(WSText.empty()).toBuilder().center().build());
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return new ArrayList<>();
    }
}
