package com.degoos.wetsponge.command.wetspongecommand.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.text.WSText;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WetSpongeSubcommandPluginLoad extends WSSubcommand {

    private WSSubcommand notFoundSubcommand;

    public WetSpongeSubcommandPluginLoad(WSRamifiedCommand command, WSSubcommand notFoundSubcommand) {
        super("load", command);
        this.notFoundSubcommand = notFoundSubcommand;
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        if (remainingArguments.length == 0) {
            notFoundSubcommand.executeCommand(commandSource, command, arguments, remainingArguments);
            return;
        }
        loadPlugin(commandSource, remainingArguments[0]);
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return WetSponge.getPluginManager()
                .getPluginFiles()
                .keySet()
                .stream()
                .filter(arg -> arg.toLowerCase().startsWith(remainingArguments[0].toLowerCase()))
                .collect(Collectors.toList());
    }

    public static boolean loadPlugin(WSCommandSource commandSource, String name) {
        File pluginFile = null;
        for (File file : new File("WetSpongePlugins").listFiles()) {
            if (file.getName().equalsIgnoreCase(name + ".jar") || (file.getName().equalsIgnoreCase(name) && file.getName().endsWith(".jar"))) {
                pluginFile = file;
                break;
            }
        }
        if (pluginFile == null) {
            Optional<File> optional = WSPluginManager.getInstance().getPluginFile(name);
            if (optional.isPresent()) pluginFile = optional.get();
            else {
                commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.common.notFound").orElse(WSText.empty()));
                return false;
            }
        }
        commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.load.loading", "<PLUGIN>",
                pluginFile.getName()).orElse(WSText.empty()));
        switch (WSPluginManager.getInstance().loadPlugin(pluginFile)) {
            case ALREADY_LOADED:
                commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.load.alreadyLoaded").orElse(WSText.empty()));
                return false;
            case ERROR:
                commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.load.error").orElse(WSText.empty()));
                return false;
            case LOADED:
                commandSource.sendMessage(WetSpongeMessages.getMessage("command.plugin.load.done", "<PLUGIN>",
                        pluginFile.getName()).orElse(WSText.empty()));
            default:
                return true;
        }
    }
}
