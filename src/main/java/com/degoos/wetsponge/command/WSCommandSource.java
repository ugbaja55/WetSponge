package com.degoos.wetsponge.command;


import com.degoos.wetsponge.permission.WSPermisible;
import com.degoos.wetsponge.text.WSText;

public interface WSCommandSource extends WSPermisible {

	String getName();

	Object getHandled();

	void sendMessage(String message);

	void sendMessage(WSText text);

	void sendMessages(String... messages);

	void sendMessages(WSText... texts);

	void performCommand(String command);

}
