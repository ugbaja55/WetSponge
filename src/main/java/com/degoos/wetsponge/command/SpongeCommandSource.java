package com.degoos.wetsponge.command;


import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.HashSet;
import java.util.Set;

public class SpongeCommandSource implements WSCommandSource {

	private CommandSource source;


	public SpongeCommandSource(CommandSource sender) {
		this.source = sender;
	}


	@Override
	public String getName() {
		return source.getName();
	}


	@Override
	public boolean hasPermission(String name) {
		return source.hasPermission(name);
	}

	@Override
	public Set<String> getPermissions() {
		//todo: get player permissions set
		return new HashSet<>();
	}

	@Override
	public void sendMessage(String message) {
		getHandled().sendMessage(TextSerializers.LEGACY_FORMATTING_CODE.deserialize(message));
	}


	@Override
	public void sendMessage(WSText text) {
		getHandled().sendMessage(((SpongeText) text).getHandled());
	}


	@Override
	public void sendMessages(String... messages) {
		for (String message : messages) sendMessage(message);
	}


	@Override
	public void sendMessages(WSText... texts) {
		for (WSText text : texts) sendMessage(text);
	}

	@Override
	public void performCommand(String command) {
		Sponge.getGame().getCommandManager().process(getHandled(), command);
	}


	@Override
	public CommandSource getHandled() {
		return source;
	}

}
