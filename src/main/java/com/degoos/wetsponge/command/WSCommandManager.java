package com.degoos.wetsponge.command;


import co.aikar.wetspongetimings.TimingsManager;
import com.degoos.wetsponge.plugin.WSPlugin;
import java.util.*;
import java.util.stream.Collectors;

public class WSCommandManager {

    private static WSCommandManager ourInstance = new WSCommandManager();
    private Map<String, WSCommand> commands;


    private WSCommandManager() {
        commands = new HashMap<>();
    }


    public static WSCommandManager getInstance() {
        return ourInstance;
    }


    public boolean addCommand(WSCommand command) {
        String name = command.getName().toLowerCase();
        command.timings = TimingsManager.getCommandTiming("WetSponge", command);
        if (commands.containsKey(name)) return false;
        commands.put(name, command);
        return true;
    }

    public boolean addCommand(WSPlugin plugin, WSCommand command) {
        String name = command.getName().toLowerCase();
        command.timings = TimingsManager.getCommandTiming(plugin.getPluginDescription().getName(), command);
        if (commands.containsKey(name)) return false;
        commands.put(name, command);
        return true;
    }


    public Optional<WSCommand> getCommand(String name) {
        WSCommand command = commands.get(name.toLowerCase());
        if (command == null) return commands.values().stream().filter(target -> target.hasAlias(name)).findAny();
        return Optional.of(command);
    }


    public Set<WSCommand> getCommands() {
        return new HashSet<>(commands.values());
    }

    public boolean removeCommand(String name) {
        if (!commands.containsKey(name.toLowerCase())) return false;
        commands.remove(name.toLowerCase());
        return true;
    }

    public Set<String> getCommandsAndAliases() {
        Set<String> set = commands.values().stream().map(WSCommand::getName).collect(Collectors.toSet());
        commands.values().forEach(command -> set.addAll(command.getAliases()));
        return set;
    }

}
