package com.degoos.wetsponge.inventory.multiinventory;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryClickEvent;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.inventory.WSSlotPos;

public class MultiInventoryClickEvent extends WSEvent implements WSCancellable {

    private int inventoryNumber;
    private int clickedMultiInventorySlot;
    private WSSlotPos clickedInventorySlot;
    private WSInventory inventory;
    private MultiInventory multiInventory;
    private WSPlayer player;
    private WSInventoryClickEvent wetSpongeEvent;
    private boolean cancelled;


    public MultiInventoryClickEvent(WSSlotPos clickedInventorySlot, int clickedMultiInventorySlot, int invNumber, WSInventory inventory,
                                    MultiInventory multiInventory, WSPlayer player, WSInventoryClickEvent wetSpongeEvent) {
        this.clickedInventorySlot = clickedInventorySlot;
        this.clickedMultiInventorySlot = clickedMultiInventorySlot;
        this.inventoryNumber = invNumber;
        this.inventory = inventory;
        this.multiInventory = multiInventory;
        this.player = player;
        this.wetSpongeEvent = wetSpongeEvent;
        cancelled = false;
    }


    public WSInventoryClickEvent getWetSpongeEvent() {
        return wetSpongeEvent;
    }


    public int getInventoryNumber() {
        return inventoryNumber;
    }


    public WSInventory getInventory() {
        return inventory;
    }


    public MultiInventory getMultiInventory() {
        return multiInventory;
    }


    public WSPlayer getPlayer() {
        return player;
    }


    public int getClickedMultiInventorySlot() {
        return clickedMultiInventorySlot;
    }


    public WSSlotPos getClickedInventorySlot() {
        return clickedInventorySlot;
    }


    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }


    public boolean isCancelled() {
        return cancelled;
    }
}
