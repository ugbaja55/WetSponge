package com.degoos.wetsponge.inventory.multiinventory;


public class MultiInvEntry {

	private MultiInventory multiInventory;
	private int                  inventory;


	public MultiInvEntry (MultiInventory multiInventory, int inventory) {
		this.multiInventory = multiInventory;
		this.inventory = inventory;
	}


	public MultiInventory getMultiInventory () {
		return multiInventory;
	}


	public void setMultiInventory (PlayerMultiInventory multiInventory) {
		this.multiInventory = multiInventory;
	}


	public int getInventory () {
		return inventory;
	}


	public void setInventory (int inventory) {
		this.inventory = inventory;
	}
}
