package com.degoos.wetsponge.inventory.multiinventory;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.WSText;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class ModifiedPlayerMultiInventory extends PlayerMultiInventory implements ModifiedMultiInventory {

	private int[] slotMap;

	public ModifiedPlayerMultiInventory(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id, int[] slotMap) {
		super(name, rows, invAmount, player, id);
		this.slotMap = transformMap(slotMap, rows);
	}

	public ModifiedPlayerMultiInventory(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id, boolean hotBarIfUnique, int[] slotMap) {
		super(name, rows, invAmount, player, id, hotBarIfUnique);
		this.slotMap = transformMap(slotMap, rows);
	}

	public ModifiedPlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, int[] slotMap) {
		this(name, slots, rows, player, id, false, slotMap);
	}

	public ModifiedPlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, boolean hotBarIfUnique, int[] slotMap) {
		this(name, slots, rows, player, id, hotBarIfUnique, transformMap(slotMap, rows), true);
	}

	private ModifiedPlayerMultiInventory(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, boolean hotBarIfUnique, int[] slotMap, boolean real) {
		super(name, slots, rows, player, id, hotBarIfUnique, slotMap.length);
		this.slotMap = slotMap;
	}

	private static int[] transformMap(int[] slotMap, InventoryRows rows) {

		Set<Integer> set = new HashSet<>();
		int[] newSlotMap = new int[(int) Arrays.stream(slotMap).filter(slot -> slot < rows.getSlots()).count()];
		int index = 0;

		for (int i : slotMap) {
			if (i >= rows.getSlots()) continue;
			if (set.contains(i)) continue;
			newSlotMap[index] = i;
			set.add(i);
			index++;
		}

		return newSlotMap;
	}

	public void setItem(int slot, WSItemStack item) {
		int inventory = Math.floorDiv(slot, slotMap.length);
		int invSlot = slot - inventory * slotMap.length;
		setItem(inventory, invSlot, item);
	}

	public void setItem(int inventory, int invSlot, WSItemStack item) {
		super.setItem(inventory, slotMap[invSlot], item);
	}

	@Override
	public Optional<WSItemStack> getItem(int slot) {

		int inventory = Math.floorDiv(slot, slotMap.length);
		int invSlot = slot - inventory * slotMap.length;

		if (inventories.size() == 1) return inventories.get(0).getItem(slotMap[invSlot]);
		int invNumber = Math.floorDiv(slot, slotMap.length);
		if (inventories.size() <= invNumber) return Optional.empty();
		return inventories.get(invNumber).getItem(slotMap[invSlot]);
	}


	public int size() {
		return getInventories().size() * slotMap.length;
	}

	@Override
	public Optional<Integer> getSlot(int slot) {

		int inventory = Math.floorDiv(slot, getRows().getSlots());
		int invSlot = slot - inventory * getRows().getSlots();

		for (int i = 0; i < slotMap.length; i++)
			if (slotMap[i] == invSlot) return Optional.of(i + inventory * slotMap.length);
		return Optional.empty();
	}

	@Override
	public Optional<Integer> getRealSlot(int realSlot) {

		int inventory = Math.floorDiv(realSlot, slotMap.length);
		int invSlot = realSlot - inventory * slotMap.length;

		try {
			return Optional.of(slotMap[invSlot] + inventory * getRows().getSlots());
		} catch (Exception ex) {
			return Optional.empty();
		}
	}
}
