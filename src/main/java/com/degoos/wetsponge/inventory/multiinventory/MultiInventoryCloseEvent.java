package com.degoos.wetsponge.inventory.multiinventory;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;

public class MultiInventoryCloseEvent extends WSPlayerEvent {

    private MultiInventory multiInventory;


    public MultiInventoryCloseEvent(MultiInventory multiInventory, WSPlayer player) {
        super(player);
        this.multiInventory = multiInventory;
    }


    public MultiInventory getMultiInventory() {
        return multiInventory;
    }

}
