package com.degoos.wetsponge.inventory.multiinventory;

import java.util.Optional;

public interface ModifiedMultiInventory extends MultiInventory {

	Optional<Integer> getSlot(int realSlot);

	Optional<Integer> getRealSlot(int slot);

}
