package com.degoos.wetsponge.inventory;


public class WSSlotPos {

	private int column, row, maxColumns;


	public WSSlotPos(int column, int row) {
		this.column = column;
		this.row = row;
		this.maxColumns = 9;
	}

	public WSSlotPos(int column, int row, int maxColumns) {
		this.column = column;
		this.row = row;
		this.maxColumns = maxColumns;
	}


	public static WSSlotPos getBySlot(int columns, int slot) {
		return new WSSlotPos(slot % columns, slot / columns, columns);
	}


	public int getColumn() {
		return column;
	}


	public void setColumn(int column) {
		this.column = column;
	}


	public int getRow() {
		return row;
	}


	public void setRow(int row) {
		this.row = row;
	}


	public int getSlot() {
		if (row == -1 && column == -1) return -1;
		return maxColumns * row + column;
	}
}
