package com.degoos.wetsponge.inventory;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.enums.EnumInventoryRows;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import org.spongepowered.api.block.tileentity.carrier.Dispenser;
import org.spongepowered.api.block.tileentity.carrier.Dropper;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.type.CarriedInventory;
import org.spongepowered.api.text.serializer.TextSerializers;

public class SpongeInventory implements WSInventory {

	private IInventory inventory;
	private Container container;
	private Inventory spongeInventory;
	private InventoryDimension dimension;
	private boolean isContainer;


	public SpongeInventory(Inventory inventory) {
		Validate.notNull(inventory, "Inventory cannot be null!");
		isContainer = inventory instanceof Container;
		this.inventory = isContainer ? null : (IInventory) inventory;
		this.container = isContainer ? (Container) inventory : null;
		this.spongeInventory = inventory;
		this.dimension = inventory.getProperty(InventoryDimension.class, "inventorydimension").orElse(getDimensionByInventoryType());
	}

	/**
	 * Creates a new inventory.
	 *
	 * @param size the inventory size.
	 *
	 * @return the new inventory.
	 */
	@Deprecated
	public static SpongeInventory of(int size) {
		return new SpongeInventory(Inventory.builder().property("inventorydimension", new InventoryDimension(9, size / 9)).build(SpongeWetSponge.getInstance()));
	}

	/**
	 * Creates a new inventory.
	 *
	 * @param size the inventory size.
	 * @param playerInventory whether the inventory is a player inventory or not.
	 *
	 * @return the new inventory.
	 */
	@Deprecated
	public static SpongeInventory of(int size, boolean playerInventory) {
		return new SpongeInventory(Inventory.builder().property("inventorydimension", new InventoryDimension(size, size / 9)).build(SpongeWetSponge.getInstance()));
	}


	/**
	 * Creates a new inventory.
	 *
	 * @param size the inventory size.
	 * @param title the inventory title.
	 * @param playerInventory whether the inventory is a player inventory or not
	 *
	 * @return the new inventory.
	 */
	public static SpongeInventory of(int size, WSText title, boolean playerInventory) {
		if (title == null) return of(size, playerInventory);
		return new SpongeInventory(Inventory.builder().property("inventorydimension", new InventoryDimension(9, size / 9))
			.property(InventoryTitle.PROPERTY_NAME, new InventoryTitle(((SpongeText) title).getHandled())).build(SpongeWetSponge.getInstance()));
	}


	private InventoryDimension getDimensionByInventoryType() {
		if (inventory instanceof CarriedInventory) {
			Optional<?> optional = ((CarriedInventory) inventory).getCarrier();
			if (optional.isPresent()) {
				if (optional.get() instanceof Dispenser || optional.get() instanceof Dropper) return new InventoryDimension(3, 3);
			}
		}
		return new InventoryDimension(9, 3);
	}


	@Override
	public Optional<String> getName() {
		return spongeInventory.getProperty(InventoryTitle.class, InventoryTitle.PROPERTY_NAME).map(InventoryTitle::getValue)
			.map(TextSerializers.LEGACY_FORMATTING_CODE::serialize);
	}


	@Override
	public EnumInventoryRows getRows() {
		return EnumInventoryRows.getByRows(dimension.getRows());
	}


	@Override
	public int size() {
		return dimension.getRows() * dimension.getColumns();
	}

	@Override
	public void setItem(WSItemStack item, int slot) {
		if (isContainer) {
			Slot minecraftSlot = container.getSlot(slot);
			if (minecraftSlot == null) return;
			minecraftSlot.putStack((ItemStack) item.getHandled());
		} else inventory.setInventorySlotContents(slot, (ItemStack) item.getHandled());
	}

	@Override
	public void setItem(WSItemStack item, WSSlotPos slotPos) {
		setItem(item, slotPos.getSlot());
	}

	@Override
	public void addItem(WSItemStack item) {
		if (item == null || item.getQuantity() < 1) return;
		int quantity = item.getQuantity();
		Map<Integer, WSItemStack> items = getContentMap();
		for (WSItemStack itemStack : items.values()) {
			if (itemStack.isSimilar(item)) {
				int add = itemStack.getMaterial().getMaxStackQuantity() - itemStack.getQuantity();
				if (add > quantity) {
					itemStack.setQuantity(itemStack.getQuantity() + quantity).update();
					quantity = 0;
					break;
				} else {
					itemStack.setQuantity(itemStack.getMaterial().getMaxStackQuantity()).update();
					quantity -= add;
				}
			}
		}
		if (quantity > 0) for (int i = 0; i < getSize(); i++) {
			if (!items.containsKey(i)) {
				items.put(i, item.clone().setQuantity(quantity).update());
				break;
			}
		}

		setContents(items);
	}

	@Override
	public Optional<WSItemStack> getItem(int slot) {
		ItemStack itemStack;
		if (isContainer) {
			Slot minecraftSlot = container.getSlot(slot);
			if (minecraftSlot == null) return Optional.empty();
			itemStack = minecraftSlot.getStack();
		} else itemStack = inventory.getStackInSlot(slot);
		if (itemStack.isEmpty()) return Optional.empty();
		return Optional.of(new SpongeItemStack((org.spongepowered.api.item.inventory.ItemStack) (Object) itemStack));
	}

	@Override
	public Optional<WSItemStack> getItem(WSSlotPos slotPos) {
		return getItem(slotPos.getSlot());
	}

	@Override
	public void removeItem(int slot) {
		if (isContainer) {
			Slot minecraftSlot = container.getSlot(slot);
			if (minecraftSlot == null) return;
			minecraftSlot.putStack(ItemStack.EMPTY);
		} else inventory.removeStackFromSlot(slot);
	}

	@Override
	public void removeItem(WSSlotPos slotPos) {
		removeItem(slotPos.getSlot());
	}


	@Override
	public List<WSItemStack> getContent() {
		List<WSItemStack> list = new ArrayList<>();
		for (int i = 0; i < size(); i++) getItem(i).ifPresent(list::add);
		return list;
	}


	@Override
	public Map<Integer, WSItemStack> getContentMap() {
		Map<Integer, WSItemStack> map = new HashMap<>();
		for (int i = 0; i < size(); i++) {
			Optional<WSItemStack> optional = getItem(i);
			if (optional.isPresent()) map.put(i, optional.get());
		}
		return map;
	}


	@Override
	public void setContents(Map<Integer, WSItemStack> contents) {
		inventory.clear();
		contents.forEach((integer, itemStack) -> setItem(itemStack, integer));
	}

	@Override
	public int getSize() {
		return dimension.getColumns() * dimension.getRows();
	}

	@Override
	public void clear() {
		inventory.clear();
	}


	@Override
	public Inventory getHandled() {
		return spongeInventory;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeInventory that = (SpongeInventory) o;

		return spongeInventory.equals(that.spongeInventory);
	}

	@Override
	public int hashCode() {
		return spongeInventory.hashCode();
	}
}
