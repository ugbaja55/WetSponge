package com.degoos.wetsponge.inventory;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumInventoryRows;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.WSText;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface WSInventory {

    /**
     * Creates a new inventory.
     *
     * @param size the inventory size.
     * @return the new inventory.
     */
    public static WSInventory of(int size) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return SpigotInventory.of(size);
            case SPONGE:
                return SpongeInventory.of(size, false);
            default:
                return null;
        }
    }

    /**
     * Creates a new inventory.
     *
     * @param size  the inventory size.
     * @param title the inventory title.
     * @return the new inventory.
     */
    public static WSInventory of(int size, String title) {
        return of(size, WSText.getByFormattingText(title));
    }

    /**
     * Creates a new inventory.
     *
     * @param size  the inventory size.
     * @param title the inventory title.
     * @return the new inventory.
     */
    public static WSInventory of(int size, WSText title) {
        if (title == null) return of(size);
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return SpigotInventory.of(size, title);
            case SPONGE:
                return SpongeInventory.of(size, title, false);
            default:
                return null;
        }
    }


    /**
     * @return the name of the inventory, if present.
     */
    Optional<String> getName();

    /**
     * @return the rows of the inventory.
     */
    EnumInventoryRows getRows();

    /**
     * @return the size of the inventory. Equals to {@link #getRows()} * 9.
     */
    int size();

    /**
     * Sets a new instance of the {@link WSItemStack} into the inventory at the selected slot.
     * Any changes of the set {@link WSItemStack} after use this method won't be applied into the inventory.
     *
     * @param item the {@link WSItemStack} to set.
     * @param slot the slot.
     */
    void setItem(WSItemStack item, int slot);

    /**
     * Sets a new instance of the {@link WSItemStack} into the inventory at the selected slot.
     * Any changes of the set {@link WSItemStack} after use this method won't be applied into the inventory.
     *
     * @param item    the {@link WSItemStack} to set.
     * @param slotPos the slot position.
     */
    void setItem(WSItemStack item, WSSlotPos slotPos);


	/**
	 * Adds a new instance of the {@link WSItemStack} into the inventory.
	 * @param item the {@link WSItemStack} to set.
	 */
	void addItem(WSItemStack item);

    /**
     * This method returns a new instance of the {@link WSItemStack} from the selected slot.
     *
     * @param slot the selected slot.
     * @return a new instance of the {@link WSItemStack}.
     */
    Optional<WSItemStack> getItem(int slot);

    /**
     * This method returns a new instance of the {@link WSItemStack} from the selected slot.
     *
     * @param slotPos the selected slot position.
     * @return a new instance of the {@link WSItemStack}.
     */
    Optional<WSItemStack> getItem(WSSlotPos slotPos);

    /**
     * This method clears the ItemStack in the selected slot, if present.
     *
     * @param slot the selected slot.
     */
    void removeItem(int slot);

    /**
     * This method clears the ItemStack in the selected slot, if present.
     *
     * @param slotPos the selected slot position.
     */
    void removeItem(WSSlotPos slotPos);

    /**
     * This method returns all {@link WSItemStack}s from this inventory. All {@link WSItemStack} are new instances.
     *
     * @return all {@link WSItemStack}s from this inventory.
     */
    List<WSItemStack> getContent();

    /**
     * This method returns all {@link WSItemStack}s from this inventory. All {@link WSItemStack} are new instances.
     *
     * @return all {@link WSItemStack}s from this inventory.
     */
    Map<Integer, WSItemStack> getContentMap();

    /**
     * Sets the contents of the {@link WSInventory inventory}.
     * The keys of the map are the slots.
     * @param contents the content.
     */
    void setContents (Map<Integer, WSItemStack> contents);

    /**
     * Returns the size of the {@link WSInventory inventory}.
     * @return the size of the {@link WSInventory inventory}.
     */
    int getSize ();

    /**
     * Clears all the content of the {@link WSInventory inventory}.
     */
    void clear();

    /**
     * @return the handled inventory.
     */
    Object getHandled();

}
