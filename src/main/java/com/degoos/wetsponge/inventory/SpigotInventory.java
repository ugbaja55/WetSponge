package com.degoos.wetsponge.inventory;


import com.degoos.wetsponge.enums.EnumInventoryRows;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SpigotInventory implements WSInventory {

	private Inventory inventory;
	private EnumInventoryRows rows;


	public SpigotInventory(Inventory inventory) {
		Validate.notNull(inventory, "Inventory cannot be null!");
		this.inventory = inventory;
		this.rows = EnumInventoryRows.getBySize(inventory.getSize());
	}


	/**
	 * Creates a new inventory.
	 *
	 * @param size the inventory size.
	 *
	 * @return the new inventory.
	 */
	public static SpigotInventory of(int size) {
		return new SpigotInventory(Bukkit.createInventory(null, size));
	}


	/**
	 * Creates a new inventory.
	 *
	 * @param size the inventory size.
	 * @param title the inventory title.
	 *
	 * @return the new inventory.
	 */
	public static SpigotInventory of(int size, WSText title) {
		if (title == null) return of(size);
		String name = title.toFormattingText();
		return new SpigotInventory(Bukkit.createInventory(null, size, name.substring(0, Math.min(32, name.length()))));
	}


	@Override
	public Optional<String> getName() {
		return Optional.ofNullable(inventory.getName());
	}


	@Override
	public EnumInventoryRows getRows() {
		return rows;
	}


	@Override
	public int size() {
		return inventory.getSize();
	}


	@Override
	public void setItem(WSItemStack item, int slot) {
		Validate.notNull(item, "Item cannot be null!");
		inventory.setItem(slot, ((SpigotItemStack) item).getHandled().clone());
	}


	@Override
	public void setItem(WSItemStack item, WSSlotPos slotPos) {
		Validate.notNull(slotPos, "SlotPos cannot be null!");
		setItem(item, slotPos.getSlot());
	}

	@Override
	public void addItem(WSItemStack item) {
		inventory.addItem(((SpigotItemStack)item).getHandled());
	}


	@Override
	public Optional<WSItemStack> getItem(int slot) {
		return Optional.ofNullable(inventory.getItem(slot)).map(ItemStack::clone).map(SpigotItemStack::new);
	}


	@Override
	public Optional<WSItemStack> getItem(WSSlotPos slotPos) {
		Validate.notNull(slotPos, "SlotPos cannot be null!");
		return getItem(slotPos.getSlot());
	}


	@Override
	public void removeItem(int slot) {
		inventory.setItem(slot, null);
	}


	@Override
	public void removeItem(WSSlotPos slotPos) {
		Validate.notNull(slotPos, "SlotPos cannot be null!");
		inventory.setItem(slotPos.getSlot(), null);
	}


	@Override
	public List<WSItemStack> getContent() {
		ItemStack[] contents = inventory.getContents();
		if (contents.length == 0) return new ArrayList<>();
		return Arrays.stream(contents).filter(Objects::nonNull).map(ItemStack::clone).map(SpigotItemStack::new).collect(Collectors.toList());
	}


	@Override
	public Map<Integer, WSItemStack> getContentMap() {
		Map<Integer, WSItemStack> map = new HashMap<>();
		for (int i = 0; i < size(); i++) {
			Optional<WSItemStack> optional = getItem(i);
			if (optional.isPresent()) map.put(i, optional.get());
		}
		return map;
	}

	@Override
	public void setContents(Map<Integer, WSItemStack> contents) {
		inventory.clear();
		contents.forEach((integer, itemStack) -> setItem(itemStack, integer));
	}

	@Override
	public int getSize() {
		return inventory.getSize();
	}

	@Override
	public void clear() {
		inventory.clear();
	}


	@Override
	public Inventory getHandled() {
		return inventory;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotInventory that = (SpigotInventory) o;

		return inventory.equals(that.inventory);
	}

	@Override
	public int hashCode() {
		return inventory.hashCode();
	}
}
