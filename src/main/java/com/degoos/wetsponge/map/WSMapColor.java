package com.degoos.wetsponge.map;

import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumMapIllumination;
import com.degoos.wetsponge.util.Validate;
import java.awt.Color;

public class WSMapColor {

	private EnumMapBaseColor baseColor;
	private EnumMapIllumination illumination;

	public WSMapColor(EnumMapBaseColor baseColor, EnumMapIllumination illumination) {

		Validate.notNull(baseColor, "Base cannot be null!");
		this.baseColor = baseColor;
		this.illumination = illumination == null ? EnumMapIllumination.NORMAL : illumination;
	}

	public EnumMapBaseColor getBaseColor() {
		return baseColor;
	}

	public void setBaseColor(EnumMapBaseColor baseColor) {
		Validate.notNull(baseColor, "Base cannot be null!");
		this.baseColor = baseColor;
	}

	public EnumMapIllumination getIllumination() {
		return illumination;
	}

	public void setIllumination(EnumMapIllumination illumination) {
		this.illumination = illumination == null ? EnumMapIllumination.NORMAL : illumination;
	}

	public Color getColor() {
		return illumination.maskColor(baseColor.getColor());
	}

	public byte getMapColor() {
		return baseColor == EnumMapBaseColor.AIR ? 0 : (byte) ((baseColor.getId() * 4) + illumination.getId());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WSMapColor that = (WSMapColor) o;

		if (baseColor != that.baseColor) return false;
		return illumination == that.illumination;
	}

	@Override
	public int hashCode() {
		int result = baseColor.hashCode();
		result = 31 * result + illumination.hashCode();
		return result;
	}
}
