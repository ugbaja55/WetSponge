package com.degoos.wetsponge.world.edit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

/**
 * WetSponge on 30/09/2017 by IhToN.
 */
public interface WSRegionFormat {

	public InputStream getReader(InputStream inputStream) throws IOException;

	public OutputStream getWriter(OutputStream outputStream) throws IOException;

	public boolean isFormat(File file);

	public String getName();

	public Set<String> getAliases();
}
