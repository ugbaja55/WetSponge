package com.degoos.wetsponge.world.edit;

import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3i;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class WSSchematic {

	public static WSSchematic copy(Vector3i center, Vector3i pos1, Vector3i pos2, WSWorld world, boolean copyAir) {
		Validate.notNull(center, "Center cannot be null!");
		Validate.notNull(pos1, "Pos 1 cannot be null!");
		Validate.notNull(pos2, "Pos 2 cannot be null!");
		Validate.notNull(world, "World cannot be null!");
		return world.createSchematic(center, pos1, pos2, copyAir);
	}

	public static WSSchematic load(File file) {
		Validate.notNull(file, "File cannot be null!");
		Validate.isTrue(file.isFile(), "File must be a file!");
		try {
			DataInputStream inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
			Map<Vector3i, WSBlockType> relativeBlocks = new HashMap<>();
			Map<Vector3i, String> relativeTileEntities = new HashMap<>();

			int size = inputStream.readInt();
			for (int i = 0; i < size; i++) {
				Vector3i position = new Vector3i(inputStream.readInt(), inputStream.readInt(), inputStream.readInt());
				WSBlockType blockType = WSBlockTypes.getType(inputStream.readInt(), inputStream.readShort()).orElse(null);
				if (inputStream.readBoolean()) {
					String tileEntity = inputStream.readUTF();
					if (blockType != null) relativeTileEntities.put(position, tileEntity);
				}
				if (blockType != null) relativeBlocks.put(position, blockType);
			}
			inputStream.close();
			return new WSSchematic(relativeBlocks, relativeTileEntities);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was loading a schematic!");
			return null;
		}
	}

	private Map<Vector3i, WSBlockType> relativeBlocks;
	private Map<Vector3i, String> relativeTileEntities;

	public WSSchematic(Map<Vector3i, WSBlockType> relativeBlocks, Map<Vector3i, String> relativeTileEntities) {
		this.relativeBlocks = relativeBlocks == null ? new HashMap<>() : relativeBlocks;
		this.relativeTileEntities = relativeTileEntities == null ? new HashMap<>() : relativeTileEntities;
	}

	public Map<Vector3i, WSBlockType> getRelativeBlocks() {
		return relativeBlocks;
	}

	public void setRelativeBlocks(Map<Vector3i, WSBlockType> relativeBlocks) {
		this.relativeBlocks = relativeBlocks == null ? new HashMap<>() : relativeBlocks;
	}

	public Map<Vector3i, String> getRelativeTileEntities() {
		return relativeTileEntities;
	}

	public void setRelativeTileEntities(Map<Vector3i, String> relativeTileEntities) {
		this.relativeTileEntities = relativeTileEntities == null ? new HashMap<>() : relativeTileEntities;
	}

	public void paste(WSLocation center, boolean sendPacket, boolean async) {
		Validate.notNull(center, "Center cannot be null!");
		WSWorld world = center.getWorld();
		Vector3i vector = center.toVector3i();
		WSWorldQueue queue = WSWorldQueue.of(world);
		relativeBlocks.forEach((vector3i, blockType) -> queue.setBlock(vector.add(vector3i), blockType));
		queue.flush(sendPacket, async);
	}

	public void save(File file) {
		Validate.notNull(file, "File cannot be null!");
		try {
			if (file.exists()) file.delete();
			file.createNewFile();

			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			output.writeInt(relativeBlocks.size());
			relativeBlocks.forEach((vector3i, blockType) -> {
				try {
					output.writeInt(vector3i.getX());
					output.writeInt(vector3i.getY());
					output.writeInt(vector3i.getZ());
					output.writeInt(blockType.getId());
					output.writeShort(blockType instanceof WSDataValuable ? ((WSDataValuable) blockType).getDataValue() : 0);
					String tileEntity = relativeTileEntities.get(vector3i);
					output.writeBoolean(tileEntity != null);
					if (tileEntity != null) output.writeUTF(tileEntity);
				} catch (Exception ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was saving a schematic!");
				}
			});
			output.close();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was saving a schematic!");
		}
	}
}
