package com.degoos.wetsponge.world.generation.populator;

import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.generation.WSBlockVolume;

public interface WSGenerationPopulator {

    void populate(WSWorld world, WSBlockVolume volume);

}
