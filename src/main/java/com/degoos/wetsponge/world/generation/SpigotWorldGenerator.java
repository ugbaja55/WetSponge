package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.listener.spigot.SpigotWorldListener;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpigotWorld;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SpigotWorldGenerator extends ChunkGenerator implements WSWorldGenerator {

    private WSGenerationPopulator populator;

    public SpigotWorldGenerator() {
    }

    public SpigotWorldGenerator(WSGenerationPopulator populator) {
        this.populator = populator;
    }

    @Override
    public void setBaseGenerationPopulator(WSGenerationPopulator populator) {
        this.populator = populator;
        Bukkit.getWorlds().stream().filter(world -> equals(world.getGenerator()))
                .forEach(world -> SpigotWorldListener.refreshGenerator(world, world.getGenerator(), this.populator == null));
    }

    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }

    @Override
    public byte[] generate(World world, Random random, int x, int z) {
        return new byte[(int) Math.pow(2, 10)];
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        return Arrays.asList(new BlockPopulator[0]);
    }

    @Override
    public ChunkGenerator.ChunkData generateChunkData(World world, Random random, int x, int z, ChunkGenerator.BiomeGrid biome) {
        ChunkData data = Bukkit.getServer().createChunkData(world);
        if (populator != null) populator.populate(WorldParser.getOrCreateWorld(world.getName(), world),
                new SpigotBlockVolume(data, x, z, random, biome));
        return data;
    }

    @Override
    public ChunkGenerator getHandler() {
        return this;
    }

}
