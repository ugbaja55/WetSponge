package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;

/**
 * Represents a {@link com.degoos.wetsponge.world.WSWorld world}'s generator.
 * In {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}, WetSponge will inject a custom
 * generator to support changes, like in {@link com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge}.
 */
public interface WSWorldGenerator {

    /**
     * Sets the {@link WSGenerationPopulator}. This generator populator is
     * responsible for generating the base terrain of the chunk.
     *
     * @param populator The populator.
     */
    void setBaseGenerationPopulator(WSGenerationPopulator populator);

    /**
     * Returns the handled object.
     *
     * @return the handled object.
     */
    Object getHandler();
}
