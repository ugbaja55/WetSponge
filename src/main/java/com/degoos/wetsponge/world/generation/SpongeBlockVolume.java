package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.material.SpongeDataValueConverter;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.world.extent.MutableBlockVolume;

public class SpongeBlockVolume implements WSBlockVolume {

    private MutableBlockVolume blockVolume;

    public SpongeBlockVolume(MutableBlockVolume blockVolume) {
        this.blockVolume = blockVolume;
    }

    @Override
    public Vector3i getBlockMin() {
        return blockVolume.getBlockMin();
    }

    @Override
    public Vector3i getBlockMax() {
        return blockVolume.getBlockMax();
    }

    @Override
    public Vector3i getBlockSize() {
        return getBlockSize();
    }

    @Override
    public boolean containsBlock(int x, int y, int z) {
        return blockVolume.containsBlock(x, y, z);
    }

    @Override
    public WSBlockType getBlock(int x, int y, int z) {
        BlockState state = blockVolume.getBlock(x, y, z);
        if (state == null) return null;
        WSBlockType blockType = WSBlockTypes.getType(state.getId()).orElse(WSBlockTypes.AIR.getDefaultType());
        SpongeDataValueConverter.refresh(blockType, state);
        return blockType;
    }

    @Override
    public boolean setBlock(int x, int y, int z, WSBlockType blockType) {
        BlockType type = Sponge.getRegistry().getType(BlockType.class, blockType.getStringId()).orElse(BlockTypes.AIR);
        BlockState state = type.getDefaultState();
        state = SpongeDataValueConverter.update(blockType, state);
        return blockVolume.setBlock(x, y, z, state);
    }

    @Override
    public MutableBlockVolume getHandled() {
        return blockVolume;
    }


}
