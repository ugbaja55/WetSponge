package com.degoos.wetsponge.world;

/**
 * The WSLocatable class represents a object with a {@link WSLocation location}.
 */
public interface WSLocatable {

	/**
	 * Gets the {@link WSLocation} of this object.
	 * @return the location.
	 */
	WSLocation getLocation ();

	/**
	 * Gets the {@link WSWorld} of this object.
	 * @return the world.
	 */
	WSWorld getWorld ();

}
