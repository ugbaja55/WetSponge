package com.degoos.wetsponge.world;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntity;
import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunk;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import java.util.Set;
import java.util.stream.Collectors;
import org.spongepowered.api.world.Chunk;

public class SpongeChunk implements WSChunk {

	private Chunk chunk;


	public SpongeChunk(Chunk chunk) {
		this.chunk = chunk;
	}

	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(chunk.getWorld().getName(), chunk.getWorld());
	}

	@Override
	public int getX() {
		return chunk.getBlockMin().getX() / 16;
	}

	@Override
	public int getZ() {
		return chunk.getBlockMin().getZ() / 16;
	}

	@Override
	public WSBlock getBlock(int x, int y, int z) {
		return new SpongeLocation(chunk.getWorld(), chunk.getBlockMin().getX() + x, y, chunk.getBlockMin().getZ() + z, 0, 0).getBlock();
	}

	@Override
	public Set<WSEntity> getEntities() {
		return chunk.getEntities().stream().map(SpongeEntityParser::getWSEntity).collect(Collectors.toSet());
	}

	@Override
	public Set<WSTileEntity> getTileEntities() {
		return chunk.getTileEntities().stream().map(SpongeTileEntity::new).collect(Collectors.toSet());
	}

	@Override
	public boolean load(boolean generate) {
		return chunk.loadChunk(generate);
	}

	@Override
	public void addToUnloadQueue() {
		((SpongeWorld)getWorld()).getChunkProvider().queueUnload((net.minecraft.world.chunk.Chunk) chunk);
	}

	@Override
	public boolean isLoaded() {
		return chunk.isLoaded();
	}

	@Override
	public boolean canBeUnloaded() {
		return ((WSMixinChunk) chunk).canBeUnloaded();
	}

	@Override
	public void setCanBeUnloaded(boolean canBeUnloaded) {
		((WSMixinChunk) chunk).setCanBeUnloaded(canBeUnloaded);
	}

	@Override
	public boolean canBeSaved() {
		return ((WSMixinChunk) chunk).canBeSaved();
	}

	@Override
	public void setCanBeSaved(boolean canSave) {
		((WSMixinChunk) chunk).setCanBeSaved(canSave);
	}

	@Override
	public Chunk getHandled() {
		return chunk;
	}
}
