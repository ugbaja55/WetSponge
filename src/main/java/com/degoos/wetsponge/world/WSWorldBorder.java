package com.degoos.wetsponge.world;

import com.flowpowered.math.vector.Vector2d;

public interface WSWorldBorder {

	void copyPropertiesFrom(WSWorldBorder worldBorder);

	Vector2d getCenter();

	void setCenter(Vector2d center);

	void setCenter(double x, double z);

	double getDamageAmount();

	void setDamageAmount(double damageAmount);

	double getDamageThreshold();

	void setDamageThreshold(double damageThreshold);

	double getDiameter();

	void setDiameter(double diameter);

	void setDiameter(double diameter, long time);

	void setDiameter(double startDiameter, double endDiameter, long time);

	double getNewDiameter();

	int getWarningDistance();

	void setWarningDistance(int warningDistance);

	int getWarningTime();

	void setWaningTime(int waningTime);

	Object getHandled();
}

