package com.degoos.wetsponge.world;

import com.degoos.wetsponge.enums.EnumDifficulty;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Represents a {@link WSWorldProperties} unlinked to any world.
 */
public class UnlinkedWorldProperties implements WSWorldProperties {

	private boolean initialized, enabled, loadOnStartup, keepSpawnLoaded, generateSpawnOnLoad, pvpEnabled, raining, thundering, mapFeaturesEnabled, hardcore, commandsAllowed, generateBonusChest;
	private String worldName;
	private UUID uniqueId;
	private Vector3i spawnPosition;
	private long seed, totalTime, worldTime, worldBorderTimeRemaining;
	private int rainTime, thunderTime, worldBorderWarningTime, worldBorderWarningDistance;
	private EnumGameMode gameMode;
	private EnumDifficulty difficulty;
	private Vector3d worldBorderCenter;
	private double worldBorderDiameter, worldBorderTargetDiameter, worldBorderDamageThreshold, worldBolderDamageAmount;
	private Map<String, String> gameRules;
	private int maxHeight;

	public UnlinkedWorldProperties(String worldName) {
		this.worldName = worldName;
		this.uniqueId = null;
		this.spawnPosition = new Vector3i(0, 0, 0);
		this.gameMode = EnumGameMode.SURVIVAL;
		this.difficulty = EnumDifficulty.NORMAL;
		this.worldBorderCenter = new Vector3d(0, 0, 0);
		this.worldBorderDiameter = 60000000;
		this.gameRules = new HashMap<>();
		this.maxHeight = 255;
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public String getWorldName() {
		return worldName;
	}

	@Override
	public UUID getUniqueId() {
		return uniqueId;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public boolean loadOnStartup() {
		return loadOnStartup;
	}

	@Override
	public void setLoadOnStartup(boolean loadOnStartup) {
		this.loadOnStartup = loadOnStartup;
	}

	@Override
	public boolean doesKeepSpawnLoaded() {
		return keepSpawnLoaded;
	}

	@Override
	public void setKeepSpawnLoaded(boolean keepSpawnLoaded) {
		this.keepSpawnLoaded = keepSpawnLoaded;
	}

	@Override
	public boolean doesGenerateSpawnOnLoad() {
		return generateSpawnOnLoad;
	}

	@Override
	public void setGenerateSpawnOnLoad(boolean generateSpawnOnLoad) {
		this.generateSpawnOnLoad = generateSpawnOnLoad;
	}

	@Override
	public Vector3i getSpawnPosition() {
		return spawnPosition;
	}

	@Override
	public void setSpawnPosition(Vector3i spawnPosition) {
		this.spawnPosition = spawnPosition;
	}

	@Override
	public long getSeed() {
		return seed;
	}

	@Override
	public void setSeed(long seed) {
		this.seed = seed;
	}

	@Override
	public long getTotalTime() {
		return totalTime;
	}

	@Override
	public long getWorldTime() {
		return worldTime;
	}

	@Override
	public void setWorldTime(long worldTime) {
		this.worldTime = worldTime;
	}

	@Override
	public boolean isPVPEnabled() {
		return pvpEnabled;
	}

	@Override
	public void setPVPEnabled(boolean pvpEnabled) {
		this.pvpEnabled = pvpEnabled;
	}

	@Override
	public boolean isRaining() {
		return raining;
	}

	@Override
	public void setRaining(boolean raining) {
		this.raining = raining;
	}

	@Override
	public int getRainTime() {
		return rainTime;
	}

	@Override
	public void setRainTime(int rainTime) {
		this.rainTime = rainTime;
	}

	@Override
	public boolean isThundering() {
		return thundering;
	}

	@Override
	public void setThundering(boolean thundering) {
		this.thundering = thundering;
	}

	@Override
	public int getThunderTime() {
		return thunderTime;
	}

	@Override
	public void setThunderTime(int thunderTime) {
		this.thunderTime = thunderTime;
	}

	@Override
	public EnumGameMode getGameMode() {
		return gameMode;
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {
		this.gameMode = gameMode;
	}

	@Override
	public boolean usesMapFeatures() {
		return mapFeaturesEnabled;
	}

	@Override
	public void setMapFeaturesEnabled(boolean mapFeaturesEnabled) {
		this.mapFeaturesEnabled = mapFeaturesEnabled;
	}

	@Override
	public boolean isHardcore() {
		return hardcore;
	}

	@Override
	public void setHardcore(boolean hardcore) {
		this.hardcore = hardcore;
	}

	@Override
	public boolean areCommandsAllowed() {
		return commandsAllowed;
	}

	@Override
	public void setCommandsAllowed(boolean commandsAllowed) {
		this.commandsAllowed = commandsAllowed;
	}

	@Override
	public EnumDifficulty getDifficulty() {
		return difficulty;
	}

	@Override
	public void setDifficulty(EnumDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	@Override
	public boolean doesGenerateBonusChest() {
		return generateBonusChest;
	}

	@Override
	public Optional<String> getGameRule(String name) {
		return gameRules.keySet().stream().filter(gameRule -> gameRule.equalsIgnoreCase(name)).map(gameRule -> gameRules.get(gameRule)).findAny();
	}

	@Override
	public Map<String, String> getGameRules() {
		return gameRules;
	}

	@Override
	public void setGameRule(String name, String value) {
		gameRules.put(name, value);
	}

	@Override
	public boolean removeGameRule(String name) {
		gameRules.remove(name);
		return true;
	}

	@Override
	public int getMaxHeight() {
		return maxHeight;
	}
}
