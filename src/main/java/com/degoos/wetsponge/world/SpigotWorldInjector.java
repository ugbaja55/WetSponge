package com.degoos.wetsponge.world;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.mixin.spigot.MixinChunkProviderServer;
import com.degoos.wetsponge.mixin.spigot.WSChunkProviderAssist;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bukkit.World;

public class SpigotWorldInjector {

	protected static Object inject(World world) {
		try {
			Object   worldHandled       = HandledUtils.getWorldHandle(world);
			Class<?> worldClass         = NMSUtils.getNMSClass("World");
			Field    chunkProviderField = ReflectionUtils.setAccessible(worldClass.getDeclaredField("chunkProvider"));
			Object   chunkProvider      = chunkProviderField.get(worldHandled);
			Field[]  fields             = NMSUtils.getNMSClass("ChunkProviderServer").getDeclaredFields();

			Constructor constructor   = WSChunkProviderAssist.CHUNK_PROVIDER_CLASS.getConstructors()[0];
			Object wsChunkProvider = constructor.newInstance(new Object[constructor.getParameterTypes().length]);

			wsChunkProvider.getClass().getMethod("initLists", ArrayList.class).invoke(wsChunkProvider, new ArrayList<>());

			Arrays.stream(fields).forEach(field -> {
				try {
					ReflectionUtils.setAccessible(field);
					field.set(wsChunkProvider, field.get(chunkProvider));
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
			});

			/*if(WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Set<Long> set = new HashSet<Long>() {
					private List<Object> dontUnloadChunks = list;
					@Override
					public boolean add (Long o) {
						return MixinChunkProviderServer.hasChunkLongStatic(o, dontUnloadChunks) || super.add(o);
					}
				};
				chunkProvider.getClass().getField("unloadQueue").set(wsChunkProvider, set);
			}*/
			chunkProviderField.set(worldHandled, wsChunkProvider);
			return wsChunkProvider;
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
