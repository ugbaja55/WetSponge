package com.degoos.wetsponge.world;


import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.world.edit.WSSchematic;
import com.degoos.wetsponge.world.generation.WSWorldGenerator;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import java.io.File;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * The WSWorld class represents a loaded world of Minecraft.
 */
public interface WSWorld {

	/**
	 * @return the name of the {@link WSWorld world}.
	 */
	String getName();

	/**
	 * Returns an immutable {@link Set set} with all {@link WSEntity entitie}s of the {@link WSWorld world}, possibly only returning entities only in loaded areas.
	 *
	 * @return the {@link Set set} of {@link WSEntity entitie}s.
	 */
	Set<WSEntity> getEntities();

	/**
	 * Returns an immutable {@link Set set} with all {@link WSPlayer player}s of the {@link WSWorld world}.
	 *
	 * @return the {@link Set set} of {@link WSPlayer player}s
	 */
	Set<WSPlayer> getPlayers();

	/**
	 * Gets the {@link WSWorld world}'s {@link UUID UUID}.
	 *
	 * @return the unique id of the {@link WSWorld world}.
	 */
	UUID getUniqueId();

	/**
	 * Creates an {@link WSEntity entity} and puts it into the {@link WSWorld world}, if possible.
	 *
	 * @param entityClass the class of the {@link WSEntity entity} to spawn.
	 * @param location the {@link Vector3d location} where the {@link WSEntity entity} will be placed.
	 * @param <T> the class of the {@link WSEntity entity} to spawn.
	 *
	 * @return the spawned entity, if created.
	 */
	<T extends WSEntity> Optional<T> spawnEntity(Class<T> entityClass, Vector3d location);

	/**
	 * Creates an {@link WSEntity entity} and puts it into the {@link WSWorld world}, if possible.
	 *
	 * @param type the {@link EnumEntityType type} of the {@link WSEntity entity} to spawn.
	 * @param location the {@link Vector3d location} where the {@link WSEntity entity} will be placed.
	 *
	 * @return the spawned entity, if created and spawned.
	 */
	Optional<WSEntity> spawnEntity(EnumEntityType type, Vector3d location);

	/**
	 * Spawns an already created {@link WSEntity entity} in the {@link WSWorld world}.
	 *
	 * @param entity the {@link WSEntity entity} to spawn.
	 *
	 * @return whether the {@link WSEntity entity} was spawned.
	 */
	boolean spawnEntity(WSEntity entity);

	/**
	 * Creates an {@link WSEntity entity}, but doesn't put it into the {@link WSWorld world}. Although the entity won't spawn, the {@link Vector3d location} is required
	 * by Minecraft.
	 *
	 * @param entityClass the class of the {@link WSEntity entity} to spawn.
	 * @param location the location of the entity.
	 * @param <T> the class of the {@link WSEntity entity} to spawn.
	 *
	 * @return the created entity, if created and spawned.
	 */
	<T extends WSEntity> Optional<T> createEntity(Class<T> entityClass, Vector3d location);

	/**
	 * Creates an {@link WSEntity entity}, but doesn't put it into the {@link WSWorld world}. Although the entity won't spawn, the {@link Vector3d location} is required
	 * by Minecraft.
	 *
	 * @param type the {@link EnumEntityType type} of the {@link WSEntity entity} to spawn.
	 * @param location the location of the entity.
	 *
	 * @return the created entity, if created.
	 */
	Optional<WSEntity> createEntity(EnumEntityType type, Vector3d location);

	/**
	 * Returns the {@link WSWorld world}'s {@link WSWorldProperties properties}. The {@link WSWorldProperties properties} storages information about the {@link WSWorld
	 * world}.
	 *
	 * @return the {@link WSWorld world}'s {@link WSWorldProperties properties}.
	 */
	WSWorldProperties getProperties();

	/**
	 * Returns the {@link WSWorld world}'s {@link WSWorldGenerator generator}. The {@link WSWorldGenerator generator} creates new chunks when it's requied. In {@link
	 * com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}, WetSponge will inject a custom {@link WSWorldGenerator generator} to support changes, like in {@link
	 * com.degoos.wetsponge.enums.EnumServerType#SPONGE Sponge}.
	 *
	 * @return the {@link WSWorld world}'s {@link WSWorldGenerator generator}.
	 */
	WSWorldGenerator getGenerator();

	/**
	 * Returns a {@link WSChunk chunk} by its chunk location, if present.
	 *
	 * @param x the x location.
	 * @param z the z location.
	 *
	 * @return the {@link WSChunk chunk}, if present.
	 */
	Optional<WSChunk> getChunk(int x, int z);

	/**
	 * Returns a {@link WSChunk chunk} by a world location, if present.
	 *
	 * @param x the x location.
	 * @param z the z location.
	 *
	 * @return the {@link WSChunk chunk}, if present.
	 */
	Optional<WSChunk> getChunkAtLocation(int x, int z);


	/**
	 * Returns a {@link WSChunk chunk} by its chunk location, of loads if it's not loaded. If the boolean <code>generate</code> is <code>false</code> and the chunk is
	 * not
	 * generated, the {@link Optional optional} will be empty and the chunk won't load.
	 *
	 * @param x the x location.
	 * @param z the z location.
	 * @param shouldGenerate whether the chunk should generate.
	 *
	 * @return the loaded {@link WSChunk chunk}.
	 */
	Optional<WSChunk> loadChunk(int x, int z, boolean shouldGenerate);

	/**
	 * Returns a {@link WSChunk chunk} by a world location, of loads if it's not loaded. If the boolean <code>generate</code> is <code>false</code> and the chunk is not
	 * generated, the {@link Optional optional} will be empty and the chunk won't load.
	 *
	 * @param x the x location.
	 * @param z the z location.
	 * @param shouldGenerate whether the chunk should generate if it's its first load.
	 *
	 * @return the loaded {@link WSChunk chunk}.
	 */
	Optional<WSChunk> loadChunkAtLocation(int x, int z, boolean shouldGenerate);

	/**
	 * Returns a {@link Set set} with all loaded chunks.
	 *
	 * @return a {@link Set set} with all loaded chunks.
	 */
	Set<WSChunk> getLoadedChunks();

	/**
	 * Returns the {@link EnumBiomeType biome} of the selected location.
	 *
	 * @param x the x coord.
	 * @param y the y coord.
	 * @param z the z coord.
	 *
	 * @return a {@link EnumBiomeType biome}.
	 */
	EnumBiomeType getBiome(int x, int y, int z);

	/**
	 * Sets the target {@link EnumBiomeType biome} to the selected location.
	 *
	 * @param x the x coord.
	 * @param y the y coord.
	 * @param z the z coord.
	 * @param biome the {@link EnumBiomeType biome}.
	 */
	void setBiome(int x, int y, int z, EnumBiomeType biome);

	/**
	 * Returns whether the auto-save is enabled.
	 *
	 * @return whether the auto-save is enabled.
	 */
	boolean isAutoSave();

	/**
	 * Sets whether the auto-save is enabled.
	 *
	 * @param autoSave whether the auto-save is enabled.
	 */
	void setAutoSave(boolean autoSave);

	/**
	 * Saves the world. This will also saves the world if the auto-save is disable.
	 */
	void save();

	/**
	 * Unloads all the {@link WSChunk chunk}s in the unload queue of the {@link WSWorld world}.
	 */
	void flushUnloadedChunksQueue();

	/**
	 * Unloads all {@link WSChunk chunk}s at once. Use with caution!
	 */
	void shutdownAllChunks();

	/**
	 * Returns whether the {@link WSChunk chunk}s of the {@link WSWorld world} can be saved.
	 *
	 * @return whether the {@link WSChunk chunk} can be saved.
	 */
	boolean canBeSaved();

	/**
	 * Sets whether the {@link WSChunk chunk}s of the {@link WSWorld world} can be saved.
	 *
	 * @param canSave whether the {@link WSChunk chunk} can be saved.
	 */
	void setCanBeSaved(boolean canSave);

	/**
	 * Forces the world to save all loaded chunks. You must have {@link #canBeSaved()} on true.
	 */
	void saveAllChunks();

	/**
	 * Returns an immutable {@link WSBlockType block type} of the block on the selected {@link Vector3i position}. This is the fastest way to get a {@link WSBlockType
	 * block type}.
	 *
	 * @param position the {@link Vector3i position}.
	 *
	 * @return the {@link WSBlockType block type}.
	 */
	WSBlockType getBlockType(Vector3i position);

	/**
	 * Returns an immutable {@link WSBlockType block type} of the block on the selected {@link Vector3i position}. This is the fastest way to get a {@link WSBlockType
	 * block type}.
	 *
	 * @param x the x pos.
	 * @param y the y pos.
	 * @param z the z pos.
	 *
	 * @return the {@link WSBlockType block type}.
	 */
	WSBlockType getBlockType(int x, int y, int z);

	/**
	 * Returns an immutable {@link Map map} with all {@link WSBlockTypes block type}s in an area. This is the fastest way to get a {@link WSBlockType block type}. This
	 * method wont get air blocks.
	 *
	 * @param pos1 the pos1 {@link Vector3i vector}.
	 * @param pos2 the pos2 {@link Vector3i vector}.
	 *
	 * @return the {@link Map map}.
	 */
	Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2);

	/**
	 * Returns an immutable {@link Map map} with all {@link WSBlockTypes block type}s in an area. This is the fastest way to get a {@link WSBlockType block type}.
	 *
	 * @param pos1 the pos1 {@link Vector3i vector}.
	 * @param pos2 the pos2 {@link Vector3i vector}.
	 * @param getAir whether the method will get air blocks.
	 *
	 * @return the {@link Map map}.
	 */
	Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2, boolean getAir);

	/**
	 * Returns a new {@link WSSchematic schematic} with the data of the selected area.
	 *
	 * @param center the center of the {@link WSSchematic schematic}.
	 * @param pos1 the pos1 {@link Vector3i vector}.
	 * @param pos2 the pos2 {@link Vector3i vector}.
	 * @param copyAir whether the method will copy air blocks.
	 *
	 * @return a new {@link WSSchematic schematic}.
	 */
	WSSchematic createSchematic(Vector3i center, Vector3i pos1, Vector3i pos2, boolean copyAir);

	/**
	 * Returns immutable {@link Map map} with the map color of the highest block into the given area. {@link EnumMapBaseColor#AIR} will be ignored.
	 *
	 * @param pos1 the pos1 {@link Vector3i vector}.
	 * @param pos2 the pos2 {@link Vector3i vector}.
	 *
	 * @return the {@link Map map}.
	 */
	Map<Vector2i, EnumMapBaseColor> getMapColors(Vector3i pos1, Vector3i pos2);

	/**
	 * Triggers an {@link WSExplosion explosion} in the world.
	 *
	 * @param explosion the {@link WSExplosion explosion}.
	 */
	void triggerExplosion(WSExplosion explosion);

	/**
	 * Returns the {@link WSWorldBorder world border} of the {@link WSWorld world}.
	 *
	 * @return the {@link WSWorldBorder world border}
	 */
	WSWorldBorder getWorldBorder();

	/**
	 * Returns an immutable map with all {@link WSMapView map view}s and their map ids in the {@link WSWorld world}.
	 *
	 * These {@link WSMapView map view}s will replace the vanilla map when this one updates. All links will be lost when the {@link WSWorld world} unloads.
	 *
	 * @return immutable map with all {@link WSMapView map view}s and their map ids in the {@link WSWorld world}.
	 */
	Map<Integer, WSMapView> getMapViews();

	/**
	 * Returns the {@link WSMapView map view} linked to the selected map, if present.
	 *
	 * @param mapId the map id.
	 *
	 * @return the {@link WSMapView map view} linked to the selected map id, if present.
	 */
	Optional<WSMapView> getMapView(int mapId);

	/**
	 * Links a {@link WSMapView map view} to the selected map in the {@link WSWorld world}.
	 *
	 * @param mapId the id of the map.
	 * @param mapView the {@link WSMapView map view}.
	 */
	void putMapView(int mapId, WSMapView mapView);

	/**
	 * Unlinks the selected map in the {@link WSWorld world}.
	 *
	 * @param mapId the id of the map.
	 */
	void removeMapView(int mapId);

	/**
	 * Unlinks all maps in the {@link WSWorld world}.
	 */
	void clearMapViews();

	/**
	 * Returns the {@link WSWorld world} folder.
	 * @return the folder.
	 */
	File getWorldFolder ();

	/**
	 * Returns the server provider of the {@link WSWorld world}.
	 *
	 * @return the server provider.
	 */
	Object getChunkProvider();

	/**
	 * @return the handled world.
	 */
	Object getHandled();
}
