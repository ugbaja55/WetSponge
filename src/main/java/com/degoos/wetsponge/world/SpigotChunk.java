package com.degoos.wetsponge.world;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.tileentity.SpigotTileEntity;
import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.listener.spigot.SpigotWorldListener;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.reflection.HandledUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import org.bukkit.Chunk;

public class SpigotChunk implements WSChunk {

	private Chunk chunk;

	public SpigotChunk(Chunk chunk) {
		this.chunk = chunk;
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(chunk.getWorld().getName(), chunk.getWorld());
	}

	@Override
	public int getX() {
		return chunk.getX();
	}

	@Override
	public int getZ() {
		return chunk.getZ();
	}

	@Override
	public WSBlock getBlock(int x, int y, int z) {
		return new SpigotBlock(chunk.getBlock(x, y, z));
	}

	@Override
	public Set<WSEntity> getEntities() {
		return Arrays.stream(chunk.getEntities()).map(SpigotEntityParser::getWSEntity).collect(Collectors.toSet());
	}

	@Override
	public Set<WSTileEntity> getTileEntities() {
		return Arrays.stream(chunk.getTileEntities()).map(SpigotTileEntity::new).collect(Collectors.toSet());
	}

	@Override
	public boolean load(boolean generate) {
		return chunk.load(generate);
	}

	@Override
	public void addToUnloadQueue() {
		Object chunkProvider = getWorld().getChunkProvider();
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Object chunkHandled = HandledUtils.getHandle(chunk);
				chunkProvider.getClass().getMethod("unload", chunkHandled.getClass()).invoke(chunkProvider, chunkHandled);
			} else chunkProvider.getClass().getMethod("queueUnload", int.class, int.class).invoke(chunkProvider, getX(), getZ());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean isLoaded() {
		return chunk.isLoaded();
	}

	@Override
	public boolean canBeUnloaded() {
		return !SpigotWorldListener.dontUnloadChunks.contains(chunk);
	}

	@Override
	public void setCanBeUnloaded(boolean canBeUnloaded) {
		if (canBeUnloaded) SpigotWorldListener.dontUnloadChunks.remove(chunk);
		else SpigotWorldListener.dontUnloadChunks.add(chunk);
	}

	@Override
	public boolean canBeSaved() {
		Object object = getWorld().getChunkProvider();
		try {
			return !getList("getCantSave", object).contains(HandledUtils.getHandle(chunk));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return true;
		}
	}

	@Override
	public void setCanBeSaved(boolean canSave) {
		Object object = getWorld().getChunkProvider();
		try {
			if (canSave) getList("getCantSave", object).remove(HandledUtils.getHandle(chunk));
			else getList("getCantSave", object).add(HandledUtils.getHandle(chunk));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Chunk getHandled() {
		return chunk;
	}

	public Object getBukkitHandled() {
		return HandledUtils.getHandle(chunk);
	}

	private ArrayList getList(String getterName, Object object) throws Exception {
		return (ArrayList) object.getClass().getMethod(getterName).invoke(object);
	}
}
