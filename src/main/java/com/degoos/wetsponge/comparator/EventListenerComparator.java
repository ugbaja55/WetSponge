package com.degoos.wetsponge.comparator;


import co.aikar.wetspongetimings.TimedEventListener;
import com.degoos.wetsponge.event.WSListener;

import java.util.Comparator;

public class EventListenerComparator implements Comparator<TimedEventListener> {

	@Override
	public int compare(TimedEventListener o1, TimedEventListener o2) {
		return o1.getMethod().getDeclaredAnnotation(WSListener.class).priority().getPriority() -
		       o2.getMethod().getDeclaredAnnotation(WSListener.class).priority().getPriority();
	}
}
