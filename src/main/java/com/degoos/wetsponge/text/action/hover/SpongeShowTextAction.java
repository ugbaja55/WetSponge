package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import org.spongepowered.api.text.action.HoverAction;
import org.spongepowered.api.text.action.TextActions;

public class SpongeShowTextAction extends SpongeHoverAction implements WSShowTextAction {

    public SpongeShowTextAction(WSText text) {
        super(TextActions.showText(((SpongeText) text).getHandled()));
    }

    public SpongeShowTextAction(HoverAction.ShowText action) {
        super(action);
    }

    @Override
    public WSText getText() {
        return SpongeText.of(getHandled().getResult());
    }

    @Override
    public HoverAction.ShowText getHandled() {
        return (HoverAction.ShowText) super.getHandled();
    }

}
