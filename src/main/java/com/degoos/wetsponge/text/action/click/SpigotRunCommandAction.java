package com.degoos.wetsponge.text.action.click;

import net.md_5.bungee.api.chat.ClickEvent;

public class SpigotRunCommandAction extends SpigotClickAction implements WSRunCommandAction {

    public SpigotRunCommandAction(ClickEvent event) {
        super(event);
    }

    public SpigotRunCommandAction(String command) {
        super(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    }

    @Override
    public String getCommand() {
        return getHandled().getValue();
    }
}
