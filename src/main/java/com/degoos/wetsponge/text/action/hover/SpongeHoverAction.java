package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.action.SpongeTextAction;
import org.spongepowered.api.text.action.HoverAction;

public class SpongeHoverAction extends SpongeTextAction implements WSHoverAction {

    public static SpongeHoverAction of(HoverAction<?> action) {
        if (action instanceof HoverAction.ShowText) return new SpongeShowTextAction((HoverAction.ShowText) action);
        return new SpongeHoverAction(action);
    }

    public SpongeHoverAction(HoverAction<?> action) {
        super(action);
    }

    @Override
    public HoverAction<?> getHandled() {
        return (HoverAction) super.getHandled();
    }
}
