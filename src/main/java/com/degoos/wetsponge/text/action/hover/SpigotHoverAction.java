package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.action.SpigotTextAction;
import com.degoos.wetsponge.text.action.click.WSClickAction;
import net.md_5.bungee.api.chat.HoverEvent;

public class SpigotHoverAction extends SpigotTextAction implements WSHoverAction {

    public static SpigotHoverAction of(HoverEvent event) {
        switch (event.getAction()) {
            case SHOW_ACHIEVEMENT:
            case SHOW_ENTITY:
            case SHOW_ITEM:
            case SHOW_TEXT:
                new SpigotShowTextAction(event);
            default:
                return new SpigotHoverAction(event);
        }
    }


    private HoverEvent hoverEvent;


    public SpigotHoverAction(HoverEvent hoverEvent) {
        this.hoverEvent = hoverEvent;
    }

    @Override
    public HoverEvent getHandled() {
        return hoverEvent;
    }
}
