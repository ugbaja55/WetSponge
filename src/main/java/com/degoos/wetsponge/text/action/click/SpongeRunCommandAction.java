package com.degoos.wetsponge.text.action.click;

import org.spongepowered.api.text.action.ClickAction;
import org.spongepowered.api.text.action.TextActions;

public class SpongeRunCommandAction extends SpongeClickAction implements WSRunCommandAction {

    public SpongeRunCommandAction(String command) {
        super(TextActions.runCommand(command));
    }

    public SpongeRunCommandAction(ClickAction.RunCommand action) {
        super(action);
    }

    @Override
    public String getCommand() {
        return getHandled().getResult();
    }

    @Override
    public ClickAction.RunCommand getHandled() {
        return (ClickAction.RunCommand) super.getHandled();
    }
}
