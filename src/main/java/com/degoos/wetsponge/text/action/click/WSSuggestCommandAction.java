package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;

public interface WSSuggestCommandAction extends WSClickAction {

    static WSSuggestCommandAction of(String command) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotSuggestCommandAction(command);
            case SPONGE:
                return new SpongeSuggestCommandAction(command);
            default:
                return null;
        }
    }

    String getCommand();
}
