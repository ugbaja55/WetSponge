package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;

import java.net.URL;

public interface WSChangePageAction extends WSClickAction {

    static WSChangePageAction of(int page) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotChangePageAction(page);
            case SPONGE:
                return new SpongeChangePageAction(page);
            default:
                return null;
        }
    }

    int getPage();

}
