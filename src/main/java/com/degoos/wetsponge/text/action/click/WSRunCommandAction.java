package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.WetSponge;

public interface WSRunCommandAction extends WSClickAction {

	static WSRunCommandAction of(String command) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotRunCommandAction(command);
			case SPONGE:
				return new SpongeRunCommandAction(command);
			default:
				return null;
		}
	}

	String getCommand();
}
