package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.text.action.SpongeTextAction;
import org.spongepowered.api.text.action.ClickAction;

public class SpongeClickAction extends SpongeTextAction implements WSClickAction {

    public static SpongeClickAction of(ClickAction<?> action) {
        if (action instanceof ClickAction.OpenUrl) return new SpongeOpenURLAction((ClickAction.OpenUrl) action);
        else if (action instanceof ClickAction.RunCommand) return new SpongeRunCommandAction((ClickAction.RunCommand) action);
        else if (action instanceof ClickAction.SuggestCommand) return new SpongeSuggestCommandAction((ClickAction.SuggestCommand) action);
        else if (action instanceof ClickAction.ChangePage) return new SpongeChangePageAction((ClickAction.ChangePage) action);
        else return new SpongeClickAction(action);
    }

    public SpongeClickAction(ClickAction<?> action) {
        super(action);
    }

    @Override
    public ClickAction<?> getHandled() {
        return (ClickAction) super.getHandled();
    }
}
