package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class SpigotShowTextAction extends SpigotHoverAction implements WSShowTextAction {

    public SpigotShowTextAction(HoverEvent event) {
        super(event);
    }

    public SpigotShowTextAction(WSText text) {
        super(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(text.toFormattingText()).create()));
    }


    @Override
    public WSText getText() {
        return SpigotText.of(getHandled().getValue()[0]);
    }
}
