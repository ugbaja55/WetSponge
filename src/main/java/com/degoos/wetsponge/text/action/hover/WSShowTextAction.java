package com.degoos.wetsponge.text.action.hover;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSClickAction;

public interface WSShowTextAction extends WSHoverAction {

    static WSShowTextAction of(WSText text) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotShowTextAction(text);
            case SPONGE:
                return new SpongeShowTextAction(text);
            default:
                return null;
        }
    }

    WSText getText();

}
