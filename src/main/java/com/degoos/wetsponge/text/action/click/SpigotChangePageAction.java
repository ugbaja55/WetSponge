package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.util.NumericUtils;
import net.md_5.bungee.api.chat.ClickEvent;

public class SpigotChangePageAction extends SpigotClickAction implements WSChangePageAction {

    public SpigotChangePageAction(ClickEvent event) {
        super(event);
    }

    public SpigotChangePageAction(int page) {
        super(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, String.valueOf(page)));
    }


    @Override
    public int getPage() {
        return NumericUtils.isInteger(getHandled().getValue()) ? Integer.parseInt(getHandled().getValue()) : 0;
    }
}
