package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;

import java.net.URL;

public interface WSOpenURLAction extends WSClickAction {

    static WSOpenURLAction of(URL url) {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotOpenURLAction(url);
            case SPONGE:
                return new SpongeOpenURLAction(url);
            default:
                return null;
        }
    }

    URL getURL();

}
