package com.degoos.wetsponge.text.translation;


public interface WSTranslatable {

	/**
	 * Gets the {@link WSTranslation} of this object.
	 * @return the translation.
	 */
	WSTranslation getTranslation ();

}
