package com.degoos.wetsponge.text.translation;


import java.util.Locale;

public interface WSTranslation {

	String get ();

	String get (Locale locale);

	String get (Locale locale, Object... args);

	String get (Object... args);

	String getId ();

}
