package com.degoos.wetsponge.text.translation;

import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.util.Locale;

public class SpigotTranslation implements WSTranslation {

	private static Class<?> localeInstance = NMSUtils.getNMSClass("LocaleI18n");

	private String id;

	public SpigotTranslation(String id) {
		this.id = id;
	}

	@Override
	public String get() {
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "get", id);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Locale locale) {
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "get", id);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Locale locale, Object... args) {
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "a", id, args);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Object... args) {
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "a", id, args);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	@Override
	public String getId() {
		return id;
	}
}
