package com.degoos.wetsponge.config;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ConfigSection {

	private ConfigAccessor parent;
	private String node;

	public ConfigSection(ConfigAccessor parent, String node) {
		this.parent = parent;
		this.node = node + ".";
	}

	public void set(String node, Object value) {
		parent.set(this.node + node, value);
	}


	public Object get(String node) {
		return parent.get(this.node + node);
	}


	public String getString(String node) {
		Object object = get(node);
		return object != null && object instanceof String ? (String) object : "";
	}


	public String getString(String node, String def) {
		Object object = get(node);
		return object != null && object instanceof String ? (String) object : def;
	}


	public byte getByte(String node) {
		Object object = get(node);
		return object != null && object instanceof Byte ? (byte) object : 0;
	}


	public byte getByte(String node, byte def) {
		Object object = get(node);
		return object != null && object instanceof Byte ? (byte) object : def;
	}


	public short getShort(String node) {
		Object object = get(node);
		return object != null && object instanceof Short ? (short) object : 0;
	}


	public short getShort(String node, short def) {
		Object object = get(node);
		return object != null && object instanceof Short ? (short) object : def;
	}


	public int getInt(String node) {
		Object object = get(node);
		return object != null && object instanceof Integer ? (int) object : 0;
	}


	public int getInt(String node, int def) {
		Object object = get(node);
		return object != null && object instanceof Integer ? (int) object : def;
	}


	public long getLong(String node) {
		Object object = get(node);
		return object != null && object instanceof Long ? (long) object : 0;
	}


	public long getLong(String node, long def) {
		Object object = get(node);
		return object != null && object instanceof Long ? (long) object : def;
	}


	public float getFloat(String node) {
		Object object = get(node);
		return object != null && object instanceof Float ? (float) object : 0;
	}


	public float getFloat(String node, float def) {
		Object object = get(node);
		return object != null && object instanceof Float ? (float) object : def;
	}


	public double getDouble(String node) {
		Object object = get(node);
		return object != null && object instanceof Double ? (double) object : 0;
	}


	public double getDouble(String node, double def) {
		Object object = get(node);
		return object != null && object instanceof Double ? (double) object : def;
	}


	public boolean getBoolean(String node) {
		Object object = get(node);
		return object != null && object instanceof Boolean ? (Boolean) object : false;
	}


	public boolean getBoolean(String node, boolean def) {
		Object object = get(node);
		return object != null && object instanceof Boolean ? (Boolean) object : def;
	}


	public List<?> getList(String node) {
		Object val = get(node);
		return val != null && val instanceof List ? (List) val : null;
	}


	public List<?> getList(String node, List<?> def) {
		Object val = get(node);
		return (List) (val instanceof List ? val : def);
	}


	public List<String> getStringList(String path) {
		List list = this.getList(path);
		if (list == null) {
			return new ArrayList<>(0);
		} else {
			List<String> result = new ArrayList<>();
			Iterator var4 = list.iterator();

			while (true) {
				Object object;
				do {
					if (!var4.hasNext()) {
						return result;
					}

					object = var4.next();
				} while (!(object instanceof String) && !this.isPrimitiveWrapper(object));

				result.add(String.valueOf(object));
			}
		}
	}


	public List<String> getStringList(String node, List<String> def) {
		List<String> list = getStringList(node);
		return list == null || list.isEmpty() ? def : list;
	}


	private boolean isPrimitiveWrapper(Object input) {
		return input instanceof Integer ||
			input instanceof Boolean ||
			input instanceof Character ||
			input instanceof Byte ||
			input instanceof Short ||
			input instanceof Double ||
			input instanceof Long ||
			input instanceof Float;
	}


	public Set<String> getKeys() {
		return parent.getKeys().stream().filter(key -> key.startsWith(node)).map(key -> key.replaceFirst(node, "")).collect(Collectors.toSet());
	}


	public Set<String> getKeys(boolean deep) {
		if (deep) return getKeys();
		return getKeys()
			.stream()
			.map(string -> string.replace(".", "/").split("/")[0])
			.collect(Collectors.toSet());
	}

	public boolean contains(String node) {
		return get(node) != null;
	}

	public ConfigSection getSection(String node) {
		return new ConfigSection(parent, this.node + node);
	}
}
