package com.degoos.wetsponge.config;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.text.WSText;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Optional;

public class WetSpongeMessages {

	private static ConfigAccessor config;


	public static void load() {
		ConfigAccessor jarConfig = new ConfigAccessor(getResource("wetSpongeMessages.yml"));
		config = new ConfigAccessor(new File("wetSpongeMessages.yml"));
		jarConfig.getKeys(true).stream().filter(node -> !config.contains(node)).forEach(node -> config.set(node, jarConfig.getString(node)));
		config.getKeys(true).stream().filter(node -> !jarConfig.contains(node)).forEach(node -> config.set(node, null));
		config.save();
	}


	private static InputStream getResource(String filename) {
		if (filename == null) {
			throw new IllegalArgumentException("Filename cannot be null");
		} else {
			try {
				URL url = WetSponge.getServer().getClass().getClassLoader().getResource(filename);
				if (url == null) {
					return null;
				} else {
					URLConnection connection = url.openConnection();
					connection.setUseCaches(false);
					return connection.getInputStream();
				}
			} catch (IOException var4) {
				return null;
			}
		}
	}


	public static Optional<WSText> getMessage(String node, Object... replaces) {
		return Optional.ofNullable(config.getString(node)).map(value -> {
			for (int i = 0; i + 1 < replaces.length; i += 2)
				value = value
					.replace(replaces[i].toString(), replaces[i + 1] instanceof WSText ? ((WSText) replaces[i + 1]).toFormattingText() : replaces[i + 1].toString());
			return value;
		}).map(value -> value.replace('&', '\u00A7')).map(WSText::getByFormattingText);
	}

}
