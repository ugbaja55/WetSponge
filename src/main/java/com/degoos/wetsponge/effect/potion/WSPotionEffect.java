package com.degoos.wetsponge.effect.potion;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.enums.EnumServerType;

public interface WSPotionEffect {

    public static Builder builder() {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return new SpigotPotionEffect.Builder();
            case SPONGE:
                return new SpongePotionEffect.Builder();
            default:
                return null;
        }
    }

    EnumPotionEffectType getType();

    int getDuration();

    int getAmplifier();

    boolean isAmbient();

    boolean getShowParticles();

    Object getHandled();

    interface Builder {


        EnumPotionEffectType type();

        Builder type(EnumPotionEffectType type);

        int duration();

        Builder duration(int duration);

        int amplifier();

        Builder amplifier(int amplifier);

        boolean ambient();

        Builder ambient(boolean ambient);

        boolean showParticles();

        Builder showParticles(boolean showParticles);

        WSPotionEffect build();

    }

}
