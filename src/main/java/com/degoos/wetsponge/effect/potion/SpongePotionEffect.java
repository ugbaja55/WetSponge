package com.degoos.wetsponge.effect.potion;

import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.effect.potion.PotionEffectTypes;

public class SpongePotionEffect implements WSPotionEffect {

    private PotionEffect potionEffect;
    private EnumPotionEffectType type;

    public SpongePotionEffect(EnumPotionEffectType type, int duration, int amplifier, boolean ambient, boolean showParticles) {
        Validate.notNull(type, "Type cannot be null!");
        potionEffect = PotionEffect.builder().potionType(Sponge.getRegistry().getType(PotionEffectType.class, type.name())
                .orElse(PotionEffectTypes.SPEED)).duration(duration).amplifier(amplifier).ambience(ambient).particles(showParticles).build();
        this.type = type;
    }

    public SpongePotionEffect(PotionEffect potionEffect) {
        Validate.notNull(potionEffect, "Potion effect cannot be null!");
        this.potionEffect = potionEffect;
        this.type = EnumPotionEffectType.getById(potionEffect.getType().getId()).orElseThrow(NullPointerException::new);
    }

    @Override
    public EnumPotionEffectType getType() {
        return type;
    }

    @Override
    public int getDuration() {
        return potionEffect.getDuration();
    }

    @Override
    public int getAmplifier() {
        return potionEffect.getAmplifier();
    }

    @Override
    public boolean isAmbient() {
        return potionEffect.isAmbient();
    }

    @Override
    public boolean getShowParticles() {
        return potionEffect.getShowParticles();
    }

    @Override
    public PotionEffect getHandled() {
        return potionEffect;
    }


    protected static class Builder implements WSPotionEffect.Builder {

        private EnumPotionEffectType type;
        private int duration, amplifier;
        private boolean ambient, showParticles;

        @Override
        public EnumPotionEffectType type() {
            return type;
        }

        @Override
        public WSPotionEffect.Builder type(EnumPotionEffectType type) {
            this.type = type;
            return this;
        }

        @Override
        public int duration() {
            return duration;
        }

        @Override
        public WSPotionEffect.Builder duration(int duration) {
            this.duration = duration;
            return this;
        }

        @Override
        public int amplifier() {
            return amplifier;
        }

        @Override
        public WSPotionEffect.Builder amplifier(int amplifier) {
            this.amplifier = amplifier;
            return this;
        }

        @Override
        public boolean ambient() {
            return ambient;
        }

        @Override
        public WSPotionEffect.Builder ambient(boolean ambient) {
            this.ambient = ambient;
            return this;
        }

        @Override
        public boolean showParticles() {
            return showParticles;
        }

        @Override
        public WSPotionEffect.Builder showParticles(boolean showParticles) {
            this.showParticles = showParticles;
            return this;
        }

        @Override
        public WSPotionEffect build() {
            return new SpongePotionEffect(type, duration, amplifier, ambient, showParticles);
        }
    }
}
