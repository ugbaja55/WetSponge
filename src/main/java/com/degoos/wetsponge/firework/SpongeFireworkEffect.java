package com.degoos.wetsponge.firework;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumFireworkShape;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.item.FireworkEffect;
import org.spongepowered.api.item.FireworkShape;
import org.spongepowered.api.item.FireworkShapes;
import org.spongepowered.api.util.Color;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeFireworkEffect implements WSFireworkEffect {

    public static WSFireworkEffect.Builder builder() {
        return new Builder(FireworkEffect.builder());
    }

    private FireworkEffect effect;

    public SpongeFireworkEffect(FireworkEffect effect) {
        Validate.notNull(effect, "Effect cannot be null!");
        this.effect = effect;
    }

    @Override
    public boolean flickers() {
        return effect.flickers();
    }

    @Override
    public boolean hasTrail() {
        return effect.hasTrail();
    }

    @Override
    public List<WSColor> getColors() {
        return effect.getColors().stream().map(color -> WSColor.ofRGB(color.getRgb())).collect(Collectors.toList());
    }

    @Override
    public List<WSColor> getFadeColors() {
        return effect.getFadeColors().stream().map(color -> WSColor.ofRGB(color.getRgb())).collect(Collectors.toList());
    }

    @Override
    public EnumFireworkShape getShape() {
        Optional<EnumFireworkShape> optional = EnumFireworkShape.getBySpongeName(effect.getShape().getId());
        if (!optional.isPresent()) {
            InternalLogger.sendError("Cannot found EnumFireworkShape for "
                    + effect.getShape().getId() + ". Using the default one: " + EnumFireworkShape.BALL + ".");
            return EnumFireworkShape.BALL;
        }
        return optional.get();
    }

    @Override
    public Builder toBuilder() {
        return new Builder(FireworkEffect.builder().from(effect));
    }

    @Override
    public WSFireworkEffect clone() {
        return toBuilder().build();
    }

    @Override
    public FireworkEffect getHandled() {
        return effect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpongeFireworkEffect that = (SpongeFireworkEffect) o;

        return effect.equals(that.effect);
    }

    @Override
    public int hashCode() {
        return effect.hashCode();
    }

    protected static class Builder implements WSFireworkEffect.Builder {

        private FireworkEffect.Builder builder;

        public Builder(FireworkEffect.Builder builder) {
            this.builder = builder;
        }

        @Override
        public WSFireworkEffect.Builder trail(boolean trail) {
            builder.trail(trail);
            return this;
        }

        @Override
        public WSFireworkEffect.Builder flicker(boolean flicker) {
            builder.flicker(flicker);
            return this;
        }

        @Override
        public WSFireworkEffect.Builder color(WSColor color) {
            builder.color(Color.ofRgb(color.toRGB()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder colors(WSColor... colors) {
            builder.colors(ListUtils.toArray(Color.class, Arrays.stream(colors).map(color -> Color.ofRgb(color.toRGB())).collect(Collectors.toList())));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder colors(Collection<WSColor> colors) {
            builder.colors(colors.stream().map(color -> Color.ofRgb(color.toRGB())).collect(Collectors.toList()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fade(WSColor color) {
            builder.fade(Color.ofRgb(color.toRGB()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fades(WSColor... colors) {
            builder.fades(ListUtils.toArray(Color.class, Arrays.stream(colors).map(color -> Color.ofRgb(color.toRGB())).collect(Collectors.toList())));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fades(Collection<WSColor> colors) {
            builder.fades(colors.stream().map(color -> Color.ofRgb(color.toRGB())).collect(Collectors.toList()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder shape(EnumFireworkShape shape) {
            Optional<FireworkShape> optional = Sponge.getRegistry().getType(FireworkShape.class, shape.name());
            if (!optional.isPresent()) {
                InternalLogger.sendError("Cannot found FireworkShape (Sponge) for "
                        + shape + ". Using the default one: " + FireworkShapes.BALL.getId() + ".");
                builder.shape(FireworkShapes.BALL);
            } else builder.shape(optional.get());
            return this;
        }

        @Override
        public WSFireworkEffect build() {
            return new SpongeFireworkEffect(builder.build());
        }
    }
}
