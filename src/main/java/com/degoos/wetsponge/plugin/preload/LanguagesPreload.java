package com.degoos.wetsponge.plugin.preload;

import com.degoos.wetsponge.plugin.WSPluginDescription;
import com.degoos.wetsponge.util.InternalLogger;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class LanguagesPreload extends Preload {


	public LanguagesPreload() {
		super("Languages");
	}

	@Override
	public File onLoad(WSPluginDescription description, File file) {
		try {
			URL url = new URL("https://degoos.com/resources/Languages.jar");
			URLConnection connection = url.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			connection.connect();
			file.delete();
			file.createNewFile();
			ReadableByteChannel rbc = Channels.newChannel(connection.getInputStream());
			FileOutputStream fos = new FileOutputStream(file);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			rbc.close();
			fos.close();
			return file;
		} catch (Exception ex) {
			InternalLogger.sendError("An error has occurred while WetSponge was preloading Languages!");
			//InternalLogger.printException(ex, "An error has occurred while WetSponge was preloading Languages!");
			return null;
		}
	}
}
