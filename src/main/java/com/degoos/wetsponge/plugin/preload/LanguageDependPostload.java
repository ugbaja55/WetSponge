package com.degoos.wetsponge.plugin.preload;

import com.degoos.wetsponge.plugin.WSPlugin;

public class LanguageDependPostload {


	public static boolean check(WSPlugin plugin) {
		try {
			return (Boolean) plugin.getClassLoader().findClass("com.degoos.languages.util.PluginUtils").getMethod("initProcess", WSPlugin.class).invoke(null, plugin);
		} catch (Exception ignore) {
			return false;
		}
	}

}
