package com.degoos.wetsponge.plugin;

import java.util.List;

public interface WSBasePlugin {

	String getName();

	String getVersion();

	String getDescription();

	String getUrl();

	List<String> getAuthors();

	List<String> getDependencies();

	List<String> getSoftDependencies();

	Object getHandled();
}
