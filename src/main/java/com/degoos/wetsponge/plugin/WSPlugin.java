package com.degoos.wetsponge.plugin;


import co.aikar.wetspongetimings.TimingHandler;
import co.aikar.wetspongetimings.Timings;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.util.Validate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.stream.Collectors;

public class WSPlugin {

    static {
        LogManager.getLogManager().reset();
    }

    private WSPluginClassLoader classLoader;
    private WSPluginDescription pluginDescription;
    private String id;
    private boolean enabled;
    private File dataFolder;
    private Set<WSPlugin> dependencies, enabledSoftDependencies;
    private WSPluginLogger logger;
    public TimingHandler timings;


    public WSPlugin() {

    }


    protected void init(WSPluginClassLoader classLoader, WSPluginDescription pluginDescription) {
        Validate.notNull(classLoader, "Classloader cannot be null!");
        Validate.notNull(pluginDescription, "Plugin description cannot be null!");
        this.classLoader = classLoader;
        this.pluginDescription = pluginDescription;
        this.id = pluginDescription.getName();
        this.logger = new WSPluginLogger(id);

        this.dataFolder = new File(WetSponge.getPluginManager().getPluginFolder(), id);
        if (!dataFolder.exists()) dataFolder.mkdirs();
        else if (dataFolder.isFile()) {
            dataFolder.delete();
            dataFolder.mkdirs();
        }

        dependencies = pluginDescription.getDepend().stream().map(target -> WSPluginManager.getInstance().getPlugin(target).orElse(null)).filter(Objects::nonNull)
                .collect(Collectors.toSet());
        enabledSoftDependencies = pluginDescription.getSoftDepend().stream().map(target -> WSPluginManager.getInstance().getPlugin(target).orElse(null))
                .filter(Objects::nonNull).collect(Collectors.toSet());

        enabled = false;
    }


    public void onEnable() {

    }


    public void onDisable() {

    }


    public WSPluginClassLoader getClassLoader() {
        return classLoader;
    }


    public WSPluginDescription getPluginDescription() {
        return pluginDescription;
    }


    public String getId() {
        return id;
    }

    public File getDataFolder() {
        return dataFolder;
    }

    public WSPluginLogger getLogger() {
        return logger;
    }

    public boolean isEnabled() {
        return enabled;
    }


    public void setEnabled(boolean enabled) {
        if (this.enabled == enabled) return;
        this.enabled = enabled;
        if (enabled) onEnable();
        else onDisable();
    }

    public Set<WSPlugin> getDependencies() {
        return new HashSet<>(dependencies);
    }

    public boolean isDependency(WSPlugin plugin) {
        return dependencies.contains(plugin);
    }

    public Set<WSPlugin> getEnabledSoftDependencies() {
        return enabledSoftDependencies;
    }

    public boolean isSoftDependency(WSPlugin plugin) {
        return enabledSoftDependencies.contains(plugin);
    }

    public InputStream getResource(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("Filename cannot be null");
        } else {
            try {
                URL url = classLoader.getResource(filename);
                if (url == null) {
                    return null;
                } else {
                    URLConnection connection = url.openConnection();
                    connection.setUseCaches(false);
                    return connection.getInputStream();
                }
            } catch (IOException var4) {
                return null;
            }
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSPlugin wsPlugin = (WSPlugin) o;

        return id.equals(wsPlugin.id);
    }


    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
