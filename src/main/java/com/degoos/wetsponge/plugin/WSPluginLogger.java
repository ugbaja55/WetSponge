package com.degoos.wetsponge.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

public class WSPluginLogger {

    private final WSText banner;
    private static final WSText close = WSText.builder("] ").color(EnumTextColor.YELLOW).build();

    public WSPluginLogger(String id) {
        banner = WSText.builder("[" + id + " -> ").color(EnumTextColor.YELLOW).build();
    }

    public void sendError(String string) {
        sendError(WSText.of(string));
    }

    public void sendWarning(String string) {
        sendWarning(WSText.of(string));
    }

    public void sendInfo(String string) {
        sendInfo(WSText.of(string));
    }

    public void sendDone(String string) {
        sendDone(WSText.of(string));
    }

    public void sendError(WSText text) {
        send(text, WSText.builder("Error").color(EnumTextColor.RED).build(), EnumTextColor.RED);
    }

    public void sendWarning(WSText text) {
        send(text, WSText.builder("Warning").color(EnumTextColor.LIGHT_PURPLE).build(), EnumTextColor.LIGHT_PURPLE);
    }

    public void sendInfo(WSText text) {
        send(text, WSText.builder("Info").color(EnumTextColor.AQUA).build(), EnumTextColor.AQUA);
    }

    public void sendDone(WSText text) {
        send(text, WSText.builder("Done").color(EnumTextColor.GREEN).build(), EnumTextColor.GREEN);
    }


    public void send(WSText text, WSText type, EnumTextColor color) {
        WetSponge.getServer().getConsole().sendMessage(banner.toBuilder().append(type).append(close)
                .append(WSText.builder("").color(color).append(text).build()).build());
    }

}
