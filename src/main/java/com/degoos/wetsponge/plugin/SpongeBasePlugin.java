package com.degoos.wetsponge.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.plugin.meta.PluginDependency;

public class SpongeBasePlugin implements WSBasePlugin {

	private Object plugin;
	public PluginContainer container;

	public SpongeBasePlugin(Object plugin) {
		this.plugin = plugin;
		container = Sponge.getPluginManager().fromInstance(plugin).orElse(null);
	}


	@Override
	public String getName() {
		return container.getName();
	}

	@Override
	public String getVersion() {
		return container.getVersion().orElse(null);
	}

	@Override
	public String getDescription() {
		return container.getDescription().orElse(null);
	}

	@Override
	public String getUrl() {
		return container.getUrl().orElse(null);
	}

	@Override
	public List<String> getAuthors() {
		return container.getAuthors();
	}

	@Override
	public List<String> getDependencies() {
		List<String> list = new ArrayList<>();
		for (PluginDependency dependency : container.getDependencies())
			if (!dependency.isOptional()) list.add(dependency.getId());
		return list;
	}

	@Override
	public List<String> getSoftDependencies() {
		List<String> list = new ArrayList<>();
		for (PluginDependency dependency : container.getDependencies())
			if (dependency.isOptional()) list.add(dependency.getId());
		return list;
	}

	@Override
	public Object getHandled() {
		return plugin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SpongeBasePlugin that = (SpongeBasePlugin) o;
		return Objects.equals(plugin, that.plugin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(plugin);
	}

	@Override
	public String toString() {
		return "{BasePlugin " + getName() + " version " + getVersion() + "}";
	}
}
