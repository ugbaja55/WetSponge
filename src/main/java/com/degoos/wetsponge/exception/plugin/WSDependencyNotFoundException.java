package com.degoos.wetsponge.exception.plugin;


public class WSDependencyNotFoundException extends Exception {

	public WSDependencyNotFoundException (final Throwable cause) {
		super(cause);
	}


	public WSDependencyNotFoundException () {
	}


	public WSDependencyNotFoundException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSDependencyNotFoundException (final String message) {
		super(message);
	}

}
