package com.degoos.wetsponge.exception.plugin;


public class WSInvalidDescriptionException extends Exception {

	public WSInvalidDescriptionException (final Throwable cause) {
		super(cause);
	}


	public WSInvalidDescriptionException () {
	}


	public WSInvalidDescriptionException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSInvalidDescriptionException (final String message) {
		super(message);
	}

}
