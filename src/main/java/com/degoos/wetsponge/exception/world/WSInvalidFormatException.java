package com.degoos.wetsponge.exception.world;

/**
 * WetSponge on 30/09/2017 by IhToN.
 */
public class WSInvalidFormatException extends WSDataException {

	public WSInvalidFormatException(String msg) {
		super(msg);
	}

}
