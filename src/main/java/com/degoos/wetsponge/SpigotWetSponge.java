package com.degoos.wetsponge;


import com.degoos.wetsponge.command.wetspongecommand.SpigotWetspongeCommand;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.loader.SpigotListenerLoader;
import com.degoos.wetsponge.loader.WetSpongeLoader;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.resource.spigot.SpigotBungeeCord;
import com.degoos.wetsponge.server.SpigotServer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ServerUtils;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class SpigotWetSponge extends JavaPlugin implements WetSpongeLoader {

	private static SpigotWetSponge instance;


	public static SpigotWetSponge getInstance() {
		return instance;
	}


	@Override
	public void onEnable() {
		instance = this;
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					long millis = System.currentTimeMillis();

					String serverVersion = Bukkit.getServer().getClass().getPackage().getName();
					EnumServerVersion version = EnumServerVersion.getBySpigotVersionName(serverVersion.substring(serverVersion.lastIndexOf(".") + 1));
					EnumServerType type = EnumServerType.SPIGOT;
					try {
						Class.forName("com.destroystokyo.paper.PaperConfig");
						type = EnumServerType.PAPER_SPIGOT;
					} catch (ClassNotFoundException e) {
					}

					WetSponge.load(getDescription().getVersion(), type, instance, new SpigotServer(Bukkit.getServer()), version, new SpigotBungeeCord(), ServerUtils
						.getMainThread());

					InternalLogger.sendInfo("Loading WetSponge " + getDescription().getVersion() + "...");

					InternalLogger
						.sendInfo(WSText.builder("Using version ").append(WSText.builder("SPIGOT " + version.name()).color(EnumTextColor.GREEN).build()).build());

					InternalLogger.sendInfo("Loading entities.");
					SpigotEntityParser.load();
					Arrays.stream(EnumEntityType.values()).forEach(EnumEntityType::load);
					InternalLogger.sendInfo("Loading Spigot listeners.");
					SpigotListenerLoader.load();
					InternalLogger.sendInfo("Loading common.");
					WetSponge.loadCommon();

					getCommand("wetSpongeCmd").setExecutor(new SpigotWetspongeCommand());

					double secs = (System.currentTimeMillis() - millis) / 1000D;
					InternalLogger.sendDone(WSText.builder("WetSponge has been loaded in ").append(WSText.builder(String.valueOf(secs)).color(EnumTextColor.RED).build())
						.append(WSText.builder(" seconds!").color(EnumTextColor.GREEN).build()).build());
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was loading!");
				}
			}
		}.runTaskLater(this, 1);
	}


	@Override
	public void onDisable() {
		try {
			WetSponge.unloadCommon();
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was unloading!");
		}
	}
}
