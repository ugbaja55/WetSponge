package com.degoos.wetsponge.parser.player;

import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import java.util.Optional;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;

public class SpongePlayerParser {

	protected static Optional<WSPlayer> checkPlayer(String name) {
		return Sponge.getServer().getPlayer(name).map(SpongePlayer::new);
	}

	protected static Optional<WSPlayer> checkPlayer(UUID uuid) {
		return Sponge.getServer().getPlayer(uuid).map(SpongePlayer::new);
	}

	protected static WSPlayer newInstance(Object player) {
		return new SpongePlayer((Player) player);
	}

}
