package com.degoos.wetsponge.parser.packet;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketCloseWindows;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketEntityAction;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketPlayerTryUseItemOnBlock;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketUpdateSign;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketUseEntity;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketAnimation;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketBlockChange;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketCloseWindows;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityHeadLook;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityLook;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityLookMove;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityMetadata;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityProperties;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityRelMove;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketEntityTeleport;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketHeldItemChange;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketMaps;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketMultiBlockChange;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketServerInfo;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSetSlot;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSignEditorOpen;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSpawnExperienceOrb;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSpawnGlobalEntity;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSpawnMob;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSpawnObject;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketSpawnPlayer;
import com.degoos.wetsponge.packet.play.server.SpigotSPacketWindowItems;
import com.degoos.wetsponge.util.reflection.NMSUtils;

public class SpigotPacketParser {

	public static WSPacket parse(Object packet) {
		//SERVER
		if (NMSUtils.getNMSClass("PacketPlayOutAnimation").isInstance(packet)) return new SpigotSPacketAnimation(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutBlockChange").isInstance(packet)) return new SpigotSPacketBlockChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutMultiBlockChange").isInstance(packet)) return new SpigotSPacketMultiBlockChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityWeather").isInstance(packet)) return new SpigotSPacketSpawnGlobalEntity(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityExperienceOrb").isInstance(packet)) return new SpigotSPacketSpawnExperienceOrb(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntityLiving").isInstance(packet)) return new SpigotSPacketSpawnMob(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSpawnEntity").isInstance(packet)) return new SpigotSPacketSpawnObject(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutNamedEntitySpawn").isInstance(packet)) return new SpigotSPacketSpawnPlayer(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityDestroy").isInstance(packet)) return new SpigotSPacketDestroyEntities(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutCloseWindow").isInstance(packet)) return new SpigotSPacketCloseWindows(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutOpenSignEditor").isInstance(packet)) return new SpigotSPacketSignEditorOpen(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMoveLook").isInstance(packet)) return new SpigotSPacketEntityLookMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutEntityLook").isInstance(packet)) return new SpigotSPacketEntityLook(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutRelEntityMove").isInstance(packet)) return new SpigotSPacketEntityRelMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntity").isInstance(packet)) return new SpigotSPacketEntityLookMove(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutHeldItemSlot").isInstance(packet)) return new SpigotSPacketHeldItemChange(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityMetadata").isInstance(packet)) return new SpigotSPacketEntityMetadata(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityTeleport").isInstance(packet)) return new SpigotSPacketEntityTeleport(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutEntityHeadRotation").isInstance(packet)) return new SpigotSPacketEntityHeadLook(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutUpdateAttributes").isInstance(packet)) return new SpigotSPacketEntityProperties(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutMap").isInstance(packet)) return new SpigotSPacketMaps(packet);
		if (NMSUtils.getNMSClass("PacketStatusOutServerInfo").isInstance(packet)) return new SpigotSPacketServerInfo(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutWindowItems").isInstance(packet)) return new SpigotSPacketWindowItems(packet);
		if (NMSUtils.getNMSClass("PacketPlayOutSetSlot").isInstance(packet)) return new SpigotSPacketSetSlot(packet);

		//CLIENT
		if (NMSUtils.getNMSClass("PacketPlayInCloseWindow").isInstance(packet)) return new SpigotCPacketCloseWindows(packet);
		if (NMSUtils.getNMSClass("PacketPlayInUpdateSign").isInstance(packet)) return new SpigotCPacketUpdateSign(packet);
		if (NMSUtils.getNMSClass("PacketPlayInUseEntity").isInstance(packet)) return new SpigotCPacketUseEntity(packet);
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) && NMSUtils.getNMSClass("PacketPlayInUseItem").isInstance(packet))
			return new SpigotCPacketPlayerTryUseItemOnBlock(packet);
		if (NMSUtils.getNMSClass("PacketPlayInEntityAction").isInstance(packet)) return new SpigotCPacketEntityAction(packet);

		return new SpigotPacket(packet) {
			@Override
			public void update() {
			}

			@Override
			public void refresh() {
			}

			@Override
			public boolean hasChanged() {
				return false;
			}
		};
	}

}
