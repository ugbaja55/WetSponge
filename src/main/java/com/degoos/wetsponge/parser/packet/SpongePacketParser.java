package com.degoos.wetsponge.parser.packet;

import com.degoos.wetsponge.material.WSDataValuable;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.client.SpigotCPacketEntityAction;
import com.degoos.wetsponge.packet.play.client.SpongeCPacketCloseWindows;
import com.degoos.wetsponge.packet.play.client.SpongeCPacketPlayerTryUseItemOnBlock;
import com.degoos.wetsponge.packet.play.client.SpongeCPacketUpdateSign;
import com.degoos.wetsponge.packet.play.client.SpongeCPacketUseEntity;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketAnimation;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketBlockChange;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketCloseWindows;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntity;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityHeadLook;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityLook;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityLookMove;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityMetadata;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityProperties;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityRelMove;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketEntityTeleport;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketHeldItemChange;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketMaps;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketMultiBlockChange;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketServerInfo;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSetSlot;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSignEditorOpen;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSpawnExperienceOrb;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSpawnGlobalEntity;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSpawnMob;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSpawnObject;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketSpawnPlayer;
import com.degoos.wetsponge.packet.play.server.SpongeSPacketWindowItems;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketCloseWindow;
import net.minecraft.network.play.client.CPacketEntityAction;
import net.minecraft.network.play.client.CPacketPlayerTryUseItemOnBlock;
import net.minecraft.network.play.client.CPacketUpdateSign;
import net.minecraft.network.play.client.CPacketUseEntity;
import net.minecraft.network.play.server.SPacketAnimation;
import net.minecraft.network.play.server.SPacketBlockChange;
import net.minecraft.network.play.server.SPacketCloseWindow;
import net.minecraft.network.play.server.SPacketDestroyEntities;
import net.minecraft.network.play.server.SPacketEntity;
import net.minecraft.network.play.server.SPacketEntityHeadLook;
import net.minecraft.network.play.server.SPacketEntityMetadata;
import net.minecraft.network.play.server.SPacketEntityProperties;
import net.minecraft.network.play.server.SPacketEntityTeleport;
import net.minecraft.network.play.server.SPacketHeldItemChange;
import net.minecraft.network.play.server.SPacketMaps;
import net.minecraft.network.play.server.SPacketMultiBlockChange;
import net.minecraft.network.play.server.SPacketSetSlot;
import net.minecraft.network.play.server.SPacketSignEditorOpen;
import net.minecraft.network.play.server.SPacketSpawnExperienceOrb;
import net.minecraft.network.play.server.SPacketSpawnGlobalEntity;
import net.minecraft.network.play.server.SPacketSpawnMob;
import net.minecraft.network.play.server.SPacketSpawnObject;
import net.minecraft.network.play.server.SPacketSpawnPlayer;
import net.minecraft.network.play.server.SPacketWindowItems;
import net.minecraft.network.status.server.SPacketServerInfo;

public class SpongePacketParser {

	public static WSPacket parse(Packet<?> packet) {
		//SERVER
		if (packet instanceof SPacketAnimation) return new SpongeSPacketAnimation(packet);
		if (packet instanceof SPacketBlockChange) return new SpongeSPacketBlockChange(packet);
		if (packet instanceof SPacketMultiBlockChange) return new SpongeSPacketMultiBlockChange(packet);
		if (packet instanceof SPacketSpawnGlobalEntity) return new SpongeSPacketSpawnGlobalEntity(packet);
		if (packet instanceof SPacketSpawnExperienceOrb) return new SpongeSPacketSpawnExperienceOrb(packet);
		if (packet instanceof SPacketSpawnMob) return new SpongeSPacketSpawnMob(packet);
		if (packet instanceof SPacketSpawnObject) return new SpongeSPacketSpawnObject(packet);
		if (packet instanceof SPacketSpawnPlayer) return new SpongeSPacketSpawnPlayer(packet);
		if (packet instanceof SPacketDestroyEntities) return new SpongeSPacketDestroyEntities(packet);
		if (packet instanceof SPacketCloseWindow) return new SpongeSPacketCloseWindows(packet);
		if (packet instanceof SPacketSignEditorOpen) return new SpongeSPacketSignEditorOpen(packet);
		if (packet instanceof SPacketEntity.S17PacketEntityLookMove) return new SpongeSPacketEntityLookMove(packet);
		if (packet instanceof SPacketEntity.S16PacketEntityLook) return new SpongeSPacketEntityLook(packet);
		if (packet instanceof SPacketEntity.S15PacketEntityRelMove) return new SpongeSPacketEntityRelMove(packet);
		if (packet instanceof SPacketEntity) return new SpongeSPacketEntity(packet);
		if (packet instanceof SPacketHeldItemChange) return new SpongeSPacketHeldItemChange(packet);
		if (packet instanceof SPacketEntityMetadata) return new SpongeSPacketEntityMetadata(packet);
		if (packet instanceof SPacketEntityTeleport) return new SpongeSPacketEntityTeleport(packet);
		if (packet instanceof SPacketEntityHeadLook) return new SpongeSPacketEntityHeadLook(packet);
		if (packet instanceof SPacketEntityProperties) return new SpongeSPacketEntityProperties(packet);
		if (packet instanceof SPacketMaps) return new SpongeSPacketMaps(packet);
		if (packet instanceof SPacketServerInfo) return new SpongeSPacketServerInfo(packet);
		if (packet instanceof SPacketWindowItems) return new SpongeSPacketWindowItems(packet);
		if (packet instanceof SPacketSetSlot) return new SpongeSPacketSetSlot(packet);

		//CLIENT
		if (packet instanceof CPacketCloseWindow) return new SpongeCPacketCloseWindows(packet);
		if (packet instanceof CPacketUpdateSign) return new SpongeCPacketUpdateSign(packet);
		if (packet instanceof CPacketPlayerTryUseItemOnBlock) return new SpongeCPacketPlayerTryUseItemOnBlock(packet);
		if (packet instanceof CPacketUseEntity) return new SpongeCPacketUseEntity(packet);
		if (packet instanceof CPacketEntityAction) return new SpigotCPacketEntityAction(packet);

		return new SpongePacket(packet) {
			@Override
			public void update() {
			}

			@Override
			public void refresh() {
			}

			@Override
			public boolean hasChanged() {
				return false;
			}
		};
	}

	public static IBlockState getBlockState(WSBlockType material) {
		try {
			return Block.getBlockById(material.getId()).getStateFromMeta(material instanceof WSDataValuable ? ((WSDataValuable) material).getDataValue() : 0);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static WSBlockType getMaterial(IBlockState blockState) {
		try {
			Block block = blockState.getBlock();
			int id = Block.REGISTRY.getIDForObject(block);
			return WSBlockTypes.getType(id, block.getMetaFromState(blockState)).orElse(new WSBlockType(id, "", 64));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

}
