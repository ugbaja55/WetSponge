package com.degoos.wetsponge.parser.entity;

import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class SpigotEntityData {

    private EntityType spongeEntityType;
    private EnumEntityType entityType;
    private Class<? extends Entity> entityClass;

    public SpigotEntityData(EntityType spongeEntityType, EnumEntityType entityType, Class<? extends Entity> entityClass) {
        Validate.notNull(spongeEntityType, "Sponge entity type cannot be null!");
        Validate.notNull(entityType, "Entity type cannot be null!");
        Validate.notNull(entityClass, "Entity class cannot be null!");
        this.spongeEntityType = spongeEntityType;
        this.entityType = entityType;
        this.entityClass = entityClass;
    }

    public SpigotEntityData(EntityType spongeEntityType, EnumEntityType entityType) {
        Validate.notNull(spongeEntityType, "Sponge entity type cannot be null!");
        Validate.notNull(entityType, "Entity type cannot be null!");
        this.spongeEntityType = spongeEntityType;
        this.entityType = entityType;
        this.entityClass = spongeEntityType.getEntityClass();
    }

    public EntityType getSpongeEntityType() {
        return spongeEntityType;
    }

    public void setSpongeEntityType(EntityType spongeEntityType) {
        Validate.notNull(spongeEntityType, "Sponge entity type cannot be null!");
        this.spongeEntityType = spongeEntityType;
    }

    public EnumEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EnumEntityType entityType) {
        Validate.notNull(entityType, "Entity type cannot be null!");
        this.entityType = entityType;
    }

    public Class<? extends Entity> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<? extends Entity> entityClass) {
        Validate.notNull(entityClass, "Entity class cannot be null!");
        this.entityClass = entityClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpigotEntityData that = (SpigotEntityData) o;

        return entityType == that.entityType;
    }

    @Override
    public int hashCode() {
        return entityType.hashCode();
    }
}
