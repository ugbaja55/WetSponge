package com.degoos.wetsponge.bar;

import com.degoos.wetsponge.entity.living.player.SpigotPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;
import java.util.stream.Collectors;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

public class SpigotBossBar implements WSBossBar {

	private BossBar bossBar;

	public SpigotBossBar(BossBar bossBar) {
		this.bossBar = bossBar;
	}

	@Override
	public SpigotBossBar addPlayer(WSPlayer player) {
		bossBar.addPlayer(((SpigotPlayer) player).getHandled());
		return this;
	}

	@Override
	public SpigotBossBar removePlayer(WSPlayer player) {
		bossBar.removePlayer(((SpigotPlayer) player).getHandled());
		return this;
	}

	@Override
	public SpigotBossBar clearPlayers() {
		bossBar.getPlayers().forEach(bossBar::removePlayer);
		return this;
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return bossBar.getPlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}

	@Override
	public EnumBossBarColor getColor() {
		return EnumBossBarColor.getByName(bossBar.getColor().name()).orElse(EnumBossBarColor.PURPLE);
	}

	@Override
	public SpigotBossBar setColor(EnumBossBarColor color) {
		bossBar.setColor(BarColor.valueOf(color.name()));
		return this;
	}

	@Override
	public EnumBossBarOverlay getOverlay() {
		return EnumBossBarOverlay.getBySpigotName(bossBar.getStyle().name()).orElse(EnumBossBarOverlay.PROGRESS);
	}

	@Override
	public SpigotBossBar setOverlay(EnumBossBarOverlay overlay) {
		bossBar.setStyle(BarStyle.valueOf(overlay.getSpigotName()));
		return this;
	}

	@Override
	public float getPercent() {
		return (float) bossBar.getProgress();
	}

	@Override
	public SpigotBossBar setPercent(float percent) {
		bossBar.setProgress(percent);
		return this;
	}

	@Override
	public boolean shouldCreateFog() {
		return bossBar.hasFlag(BarFlag.CREATE_FOG);
	}

	@Override
	public SpigotBossBar setCreateFog(boolean createFog) {
		if (createFog) bossBar.addFlag(BarFlag.CREATE_FOG);
		else bossBar.removeFlag(BarFlag.CREATE_FOG);
		return this;
	}

	@Override
	public boolean shouldDarkenSky(boolean darkenSky) {
		return bossBar.hasFlag(BarFlag.DARKEN_SKY);
	}

	@Override
	public SpigotBossBar setDarkenSky(boolean darkenSky) {
		if (darkenSky) bossBar.addFlag(BarFlag.DARKEN_SKY);
		else bossBar.removeFlag(BarFlag.DARKEN_SKY);
		return this;
	}

	@Override
	public boolean shouldPlayEndBossMusic() {
		return bossBar.hasFlag(BarFlag.PLAY_BOSS_MUSIC);
	}

	@Override
	public SpigotBossBar setPlayEndBossMusic(boolean playEndBossMusic) {
		if (playEndBossMusic) bossBar.addFlag(BarFlag.PLAY_BOSS_MUSIC);
		else bossBar.removeFlag(BarFlag.PLAY_BOSS_MUSIC);
		return this;
	}

	@Override
	public boolean isVisible() {
		return bossBar.isVisible();
	}

	@Override
	public SpigotBossBar setVisible(boolean visible) {
		bossBar.setVisible(visible);
		return this;
	}

	@Override
	public WSText getName() {
		return WSText.getByFormattingText(bossBar.getTitle());
	}

	@Override
	public SpigotBossBar setName(WSText name) {
		bossBar.setTitle(name.toFormattingText());
		return this;
	}

	@Override
	public BossBar getHandled() {
		return bossBar;
	}

	protected static class Builder implements WSBossBar.Builder {

		private EnumBossBarColor color;
		private EnumBossBarOverlay overlay;
		private float percent;
		private boolean createFog, darkenSky, endBossMusic, visible;
		private WSText name;

		Builder() {
			color = EnumBossBarColor.PURPLE;
			overlay = EnumBossBarOverlay.PROGRESS;
			percent = 1;
			createFog = darkenSky = endBossMusic = visible = false;
			name = WSText.of("");
		}

		@Override
		public Builder color(EnumBossBarColor color) {
			this.color = color;
			return this;
		}

		@Override
		public Builder overlay(EnumBossBarOverlay overlay) {
			this.overlay = overlay;
			return this;
		}

		@Override
		public Builder percent(float percent) {
			this.percent = percent;
			return this;
		}

		@Override
		public Builder createFog(boolean createFog) {
			this.createFog = createFog;
			return this;
		}

		@Override
		public Builder darkenSky(boolean darkenSky) {
			this.darkenSky = darkenSky;
			return this;
		}

		@Override
		public Builder playEndBossMusic(boolean endBossMusic) {
			this.endBossMusic = endBossMusic;
			return this;
		}

		@Override
		public Builder visible(boolean visible) {
			this.visible = visible;
			return this;
		}

		@Override
		public Builder name(WSText name) {
			this.name = name;
			return this;
		}

		@Override
		public WSBossBar build() {
			BossBar bar = Bukkit.createBossBar(name.toFormattingText(), BarColor.valueOf(color.name()), BarStyle.valueOf(overlay.getSpigotName()));
			bar.setVisible(visible);
			bar.setProgress(percent);
			if (createFog) bar.addFlag(BarFlag.CREATE_FOG);
			if (darkenSky) bar.addFlag(BarFlag.DARKEN_SKY);
			if (endBossMusic) bar.addFlag(BarFlag.PLAY_BOSS_MUSIC);
			return new SpigotBossBar(bar);
		}
	}
}
