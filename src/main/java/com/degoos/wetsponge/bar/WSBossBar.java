package com.degoos.wetsponge.bar;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;

public interface WSBossBar {

	public static Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return new SpigotBossBar.Builder();
				else return new OldSpigotBossBar.Builder();
			case SPONGE:
				return new SpongeBossBar.Builder();
			default:
				return null;
		}
	}


	WSBossBar addPlayer(WSPlayer player);

	WSBossBar removePlayer(WSPlayer player);

	WSBossBar clearPlayers();

	Set<WSPlayer> getPlayers();

	EnumBossBarColor getColor();

	WSBossBar setColor(EnumBossBarColor color);

	EnumBossBarOverlay getOverlay();

	WSBossBar setOverlay(EnumBossBarOverlay overlay);

	float getPercent();

	WSBossBar setPercent(float percent);

	boolean shouldCreateFog();

	WSBossBar setCreateFog(boolean createFog);

	boolean shouldDarkenSky(boolean darkenSky);

	WSBossBar setDarkenSky(boolean darkenSky);

	boolean shouldPlayEndBossMusic();

	WSBossBar setPlayEndBossMusic(boolean playEndBossMusic);

	boolean isVisible();

	WSBossBar setVisible(boolean visible);

	WSText getName();

	WSBossBar setName(WSText name);

	Object getHandled();

	public interface Builder {

		Builder color(EnumBossBarColor color);

		Builder overlay(EnumBossBarOverlay overlay);

		Builder percent(float percent);

		Builder createFog(boolean createFog);

		Builder darkenSky(boolean darkenSky);

		Builder playEndBossMusic(boolean endBossMusic);

		Builder visible(boolean visible);

		Builder name(WSText name);

		WSBossBar build();
	}

}
