package com.degoos.wetsponge.loader;


import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.listener.sponge.SpongeBlockListener;
import com.degoos.wetsponge.listener.sponge.SpongeChatListener;
import com.degoos.wetsponge.listener.sponge.SpongeEntityListener;
import com.degoos.wetsponge.listener.sponge.SpongeExplosionListener;
import com.degoos.wetsponge.listener.sponge.SpongeInventoryListener;
import com.degoos.wetsponge.listener.sponge.SpongeMovementListener;
import com.degoos.wetsponge.listener.sponge.SpongePlayerConnectionListener;
import com.degoos.wetsponge.listener.sponge.SpongePlayerGeneralListener;
import com.degoos.wetsponge.listener.sponge.SpongePlayerInteractListener;
import com.degoos.wetsponge.listener.sponge.SpongeSendCommandListener;
import com.degoos.wetsponge.listener.sponge.SpongeServerListener;
import com.degoos.wetsponge.listener.sponge.SpongeSignListener;
import com.degoos.wetsponge.listener.sponge.SpongeWorldListener;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.EventManager;

public class SpongeListenerLoader {

	private static boolean loaded = false;


	public static void load() {
		if (loaded) return;
		EventManager manager = Sponge.getEventManager();
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeWorldListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeSendCommandListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeInventoryListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongePlayerConnectionListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeMovementListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongePlayerGeneralListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeEntityListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeSignListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeBlockListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongePlayerInteractListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeServerListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeExplosionListener());
		manager.registerListeners(SpongeWetSponge.getInstance(), new SpongeChatListener());
		loaded = true;
	}
}
