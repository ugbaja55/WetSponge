package com.degoos.wetsponge.loader;


import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.listener.spigot.SpigotBlockListener;
import com.degoos.wetsponge.listener.spigot.SpigotChatListener;
import com.degoos.wetsponge.listener.spigot.SpigotEntityListener;
import com.degoos.wetsponge.listener.spigot.SpigotExplosionListener;
import com.degoos.wetsponge.listener.spigot.SpigotInventoryListener;
import com.degoos.wetsponge.listener.spigot.SpigotMovementListener;
import com.degoos.wetsponge.listener.spigot.SpigotNewVersionListener;
import com.degoos.wetsponge.listener.spigot.SpigotPlayerConnectionListener;
import com.degoos.wetsponge.listener.spigot.SpigotPlayerGeneralListener;
import com.degoos.wetsponge.listener.spigot.SpigotPlayerInteractListener;
import com.degoos.wetsponge.listener.spigot.SpigotPluginListener;
import com.degoos.wetsponge.listener.spigot.SpigotSendCommandListener;
import com.degoos.wetsponge.listener.spigot.SpigotServerListener;
import com.degoos.wetsponge.listener.spigot.SpigotSignListener;
import com.degoos.wetsponge.listener.spigot.SpigotTabCompleteListener;
import com.degoos.wetsponge.listener.spigot.SpigotWorldListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class SpigotListenerLoader {

	private static boolean loaded = false;


	public static void load() {
		if (loaded) return;
		PluginManager manager = Bukkit.getPluginManager();
		manager.registerEvents(new SpigotWorldListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotSendCommandListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotInventoryListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotPlayerConnectionListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotMovementListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotPlayerGeneralListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotEntityListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotSignListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotBlockListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotPlayerInteractListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotServerListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotExplosionListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotPluginListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new SpigotChatListener(), SpigotWetSponge.getInstance());
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			manager.registerEvents(new SpigotNewVersionListener(), SpigotWetSponge.getInstance());
			manager.registerEvents(new SpigotTabCompleteListener(), SpigotWetSponge.getInstance());
		}

		loaded = true;
	}
}
