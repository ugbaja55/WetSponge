package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.WSItemStack;

import java.util.Optional;

public interface WSTrade {

    public static Builder builder() {
        switch (WetSponge.getServerType()) {
            case SPIGOT:  		case PAPER_SPIGOT:
                return WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? new SpigotTrade.Builder() : new OldSpigotTrade.Builder();
            case SPONGE:
                return new SpongeTrade.Builder();
            default:
                return null;
        }
    }

    int getUses();

    int getMaxUses();

    WSItemStack getResult();

    WSItemStack getFirstItem();

    Optional<WSItemStack> getSecondItem();

    boolean doesGrantExperience();

    Object getHandled();

    public interface Builder {

        Builder uses(int uses);

        Builder maxUses(int maxUses);

        Builder result(WSItemStack result);

        Builder firstItem(WSItemStack firstItem);

        Builder secondItem(WSItemStack secondItem);

        Builder canGrantExperience(boolean grantExperience);

        WSTrade build();

    }


}
