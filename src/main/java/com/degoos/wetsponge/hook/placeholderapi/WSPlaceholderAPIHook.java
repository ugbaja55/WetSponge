package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.entity.living.player.WSPlayer;

public interface WSPlaceholderAPIHook {

	String getIdentifier();

	String onPlaceholderRequest(WSPlayer player, String identifier);
}
