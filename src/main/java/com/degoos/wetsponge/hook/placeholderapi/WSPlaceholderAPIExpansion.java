package com.degoos.wetsponge.hook.placeholderapi;

public interface WSPlaceholderAPIExpansion extends WSPlaceholderAPIHook {

	String getPlugin();

	String getAuthor();

	String getVersion();
}
