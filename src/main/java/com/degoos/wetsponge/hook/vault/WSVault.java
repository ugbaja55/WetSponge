package com.degoos.wetsponge.hook.vault;

import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.hook.WSHook;
import java.util.Optional;

public abstract class WSVault extends WSHook {

	public WSVault(EnumServerType... serverTypes) {
		super("Vault", serverTypes);
	}

	public abstract Optional<WSVaultEconomy> getEconomy();
}
