package co.aikar.timings;

public class WSPaperSpigotTimings extends FullServerTickHandler {


	public WSPaperSpigotTimings() {
		super();
	}

	@Override
	public FullServerTickHandler startTiming() {
		co.aikar.wetspongetimings.TimingsManager.FULL_SERVER_TICK.startTiming();
		return (FullServerTickHandler) super.startTiming();
	}

	@Override
	public void stopTiming() {
		co.aikar.wetspongetimings.TimingsManager.FULL_SERVER_TICK.stopTiming();
		super.stopTiming();
	}
}
