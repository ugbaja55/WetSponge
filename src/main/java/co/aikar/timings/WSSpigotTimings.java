package co.aikar.timings;

import org.spigotmc.CustomTimingsHandler;

public class WSSpigotTimings extends CustomTimingsHandler {

	public WSSpigotTimings(String name) {
		super(name);
	}

	public void startTiming() {
		co.aikar.wetspongetimings.TimingsManager.FULL_SERVER_TICK.startTiming();
		super.startTiming();
	}

	public void stopTiming() {
		co.aikar.wetspongetimings.TimingsManager.FULL_SERVER_TICK.stopTiming();
		super.stopTiming();
	}


}
