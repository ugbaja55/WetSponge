/*
 * This file is licensed under the MIT License (MIT).
 *
 * Copyright (c) 2014 Daniel Ennis <http://aikar.co>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package co.aikar.wetspongetimings;


import co.aikar.wetspongeutils.LoadingMap;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.plugin.WSPluginClassLoader;
import com.degoos.wetsponge.plugin.WSPluginManager;
import com.degoos.wetsponge.server.WSServer;
import com.degoos.wetsponge.util.InternalLogger;
import com.google.common.base.Function;
import com.google.common.collect.EvictingQueue;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class TimingsManager {

	static final Map<TimingIdentifier, TimingHandler> TIMING_MAP = Collections.synchronizedMap(LoadingMap.newHashMap(new Function<TimingIdentifier, TimingHandler>() {
		@Override
		public TimingHandler apply(TimingIdentifier id) {
			return (id.protect ? new UnsafeTimingHandler(id) : new TimingHandler(id));
		}
	}, 256, .5F));
	public static final FullServerTickHandler FULL_SERVER_TICK = new FullServerTickHandler();
	public static final TimingHandler TIMINGS_TICK = Timings.ofSafe("Timings Tick", FULL_SERVER_TICK);
	public static final Timing PLUGIN_GROUP_HANDLER = Timings.ofSafe("Plugins");
	public static final Timing PLUGIN_SCHEDULER_HANDLER = WetSpongeTimingsFactory.ofSafe("Plugin Scheduler");
	public static final Timing PLUGIN_EVENT_HANDLER = WetSpongeTimingsFactory.ofSafe("Plugin Events");
	public static List<String> hiddenConfigs = new ArrayList<>();
	public static boolean privacy = false;

	static final Collection<TimingHandler> HANDLERS = new ConcurrentLinkedQueue<>();
	static final Collection<TimingHistory.MinuteReport> MINUTE_REPORTS = new ConcurrentLinkedQueue<>();

	static EvictingQueue<TimingHistory> HISTORY = EvictingQueue.create(12);
	static TimingHandler CURRENT;
	static long timingStart = System.currentTimeMillis();
	static long historyStart = System.currentTimeMillis();
	static boolean needsFullReset = false;
	static boolean needsRecheckEnabled = false;


	public TimingsManager() {
	}


	/**
	 * Resets all timing data on the next tick
	 */
	static void reset() {
		needsFullReset = true;
	}


	/**
	 * Ticked every tick by CraftBukkit to count the number of times a timer caused TPS loss.
	 */
	static void tick() {
		if (Timings.isTimingsEnabled()) {
			boolean violated = FULL_SERVER_TICK.isViolated();

			for (TimingHandler handler : new ArrayList<>(HANDLERS)) {
				if (handler.isSpecial()) {
					// We manually call this
					continue;
				}
				handler.processTick(violated);
			}

			TimingHistory.timedTicks++;
			// Generate TPS/Ping/Tick reports every minute
		}
	}


	static void stopServer() {
		Timings.setTimingsEnabled(false);
		recheckEnabled();
	}


	static void recheckEnabled() {
		synchronized (TIMING_MAP) {
			for (TimingHandler timings : TIMING_MAP.values()) {
				timings.checkEnabled();
			}
		}
		needsRecheckEnabled = false;
	}


	static void resetTimings() {
		if (needsFullReset) {
			// Full resets need to re-check every handlers enabled state
			// Timing map can be modified from async so we must sync on it.
			synchronized (TIMING_MAP) {
				for (TimingHandler timings : TIMING_MAP.values()) {
					timings.reset(true);
				}
			}
			InternalLogger.sendInfo("Timings Reset");
			HISTORY.clear();
			needsFullReset = false;
			needsRecheckEnabled = false;
			timingStart = System.currentTimeMillis();
		} else {
			// Soft resets only need to act on timings that have done something
			// Handlers can only be modified on main thread.
			for (TimingHandler timings : HANDLERS) {
				timings.reset(false);
			}
		}

		HANDLERS.clear();
		MINUTE_REPORTS.clear();

		TimingHistory.resetTicks(true);
		historyStart = System.currentTimeMillis();
	}


	static TimingHandler getHandler(String group, String name, Timing parent, boolean protect) {
		return TIMING_MAP.get(new TimingIdentifier(group, name, parent, protect));
	}


	/**
	 * <p>Due to access restrictions, we need a helper method to get a Command TimingHandler with String group</p> <p> Plugins should never call this
	 *
	 * @param pluginName Plugin this command is associated with
	 * @param command Command to get timings for
	 *
	 * @return TimingHandler
	 */
	public static Timing getCommandTiming(String pluginName, WSCommand command) {
		WSPlugin plugin = null;
		final WSServer server = WetSponge.getServer();
		if (!(server == null || pluginName == null || "minecraft".equals(pluginName) || "bukkit".equals(pluginName) || "spigot".equalsIgnoreCase(pluginName) ||
			"paper".equals(pluginName))) {
			plugin = WSPluginManager.getInstance().getPlugin(pluginName).orElse(null);
		}
		if (plugin == null) {
			// Plugin is passing custom fallback prefix, try to look up by class loader
			plugin = getPluginByClassloader(command.getClass());
		}
		if (plugin == null) {
			return Timings.ofSafe("Command: " + pluginName + ":" + command.getName());
		}

		return Timings.ofSafe(plugin, "Command: " + pluginName + ":" + command.getName());
	}


	/**
	 * Looks up the class loader for the specified class, and if it is a PluginClassLoader, return the Plugin that created this class.
	 *
	 * @param clazz Class to check
	 *
	 * @return Plugin if created by a plugin
	 */
	public static WSPlugin getPluginByClassloader(Class<?> clazz) {
		if (clazz == null) {
			return null;
		}
		final ClassLoader classLoader = clazz.getClassLoader();
		if (classLoader instanceof WSPluginClassLoader) {
			WSPluginClassLoader pluginClassLoader = (WSPluginClassLoader) classLoader;
			return pluginClassLoader.getPlugin();
		}
		return null;
	}
}
