package co.aikar.wetspongetimings;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.WSConsoleSource;
import com.degoos.wetsponge.rcon.WSRconSource;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import com.google.common.collect.Lists;

import java.util.List;

@SuppressWarnings ("WeakerAccess")
public class TimingsReportListener {
	private final List<WSCommandSource> senders;
	private final Runnable              onDone;
	private       String                timingsURL;


	public TimingsReportListener (WSCommandSource senders) {
		this(senders, null);
	}


	public TimingsReportListener (WSCommandSource sender, Runnable onDone) {
		this(Lists.newArrayList(sender), onDone);
	}


	public TimingsReportListener (List<WSCommandSource> senders) {
		this(senders, null);
	}


	public TimingsReportListener (List<WSCommandSource> senders, Runnable onDone) {
		Validate.notNull(senders);
		Validate.notEmpty(senders);

		this.senders = Lists.newArrayList(senders);
		this.onDone = onDone;
	}


	public String getTimingsURL () {
		return timingsURL;
	}


	public void done () {
		done(null);
	}


	public void done (String url) {
		this.timingsURL = url;
		if (onDone != null) {
			onDone.run();
		}
		for (WSCommandSource sender : senders) {
			if (sender instanceof TimingsReportListener) {
				((TimingsReportListener) sender).done();
			}
		}
	}


	public void sendMessage (WSText message) {
		senders.forEach((sender) -> sender.sendMessage(message));
	}


	public void addConsoleIfNeeded () {
		boolean hasConsole = false;
		for (WSCommandSource sender : this.senders) {
			if (sender instanceof WSConsoleSource || sender instanceof WSRconSource) {
				hasConsole = true;
			}
		}
		if (!hasConsole) {
			this.senders.add(WetSponge.getServer().getConsole());
		}
	}
}
