/*
 * This file is licensed under the MIT License (MIT).
 *
 * Copyright (c) 2014 Daniel Ennis <http://aikar.co>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aikar.wetspongetimings;

import co.aikar.wetspongeutils.JSONUtil;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSOpenURLAction;
import com.degoos.wetsponge.util.InternalLogger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;
import org.jooq.tools.json.JSONValue;

@SuppressWarnings({"rawtypes", "SuppressionAnnotation"})
class TimingsExport extends Thread {

	private final TimingsReportListener listeners;
	private final Map                   out;
	private final TimingHistory[]       history;
	private static       long                  lastReport       = 0;
	final static         List<WSCommandSource> requestingReport = new ArrayList<>();
	private static final String                PREFIX           = "[WetSponge] ";

	private TimingsExport(TimingsReportListener listeners, Map out, TimingHistory[] history) {
		super("Timings paste thread");
		this.listeners = listeners;
		this.out = out;
		this.history = history;
	}

	private static String getServerVersion() {
		return "WetSponge" + " for " + WetSponge.getServerType() + " (" + WetSponge.getVersion() + ")";
	}


	/**
	 * Checks if any pending reports are being requested, and builds one if needed.
	 */
	static void reportTimings() {
		if (requestingReport.isEmpty()) {
			return;
		}
		TimingsReportListener listeners = new TimingsReportListener(requestingReport);
		listeners.addConsoleIfNeeded();

		requestingReport.clear();
		long       now            = System.currentTimeMillis();
		final long lastReportDiff = now - lastReport;
		if (lastReportDiff < 60000) {
			listeners.sendMessage(WSText
				.of(PREFIX + "Please wait at least 1 minute in between Timings reports. (" + (int) ((60000 - lastReportDiff) / 1000) + " seconds)", EnumTextColor.RED));
			listeners.done();
			return;
		}
		final long lastStartDiff = now - TimingsManager.timingStart;
		if (lastStartDiff < 180000) {
			listeners.sendMessage(WSText.of(PREFIX +
				"Please wait at least 3 minutes before generating a Timings report. Unlike Timings v1, v2 benefits from longer timings and is not as useful with short" +
				" " + "timings. (" + (int) ((180000 - lastStartDiff) / 1000) + " seconds)", EnumTextColor.RED));
			listeners.done();
			return;
		}
		listeners.sendMessage(WSText.of(PREFIX + "Preparing Timings Report...", EnumTextColor.GREEN));
		lastReport = now;
		Map parent = JSONUtil.createObject(
			// Get some basic system details about the server
			JSONUtil.pair("version", getServerVersion()), JSONUtil.pair("maxplayers", WetSponge.getServer().getServerInfo().getMaxPlayers()),
			JSONUtil.pair("start", TimingsManager.timingStart / 1000), JSONUtil.pair("end", System.currentTimeMillis() / 1000),
			JSONUtil.pair("sampletime", (System.currentTimeMillis() - TimingsManager.timingStart) / 1000));
		if (!TimingsManager.privacy) {
			JSONUtil.appendObjectData(parent, JSONUtil.pair("server", WetSponge.getServer().getServerInfo().getServerName()),
				JSONUtil.pair("motd", WetSponge.getServer().getServerInfo().getMotd().getText()),
				JSONUtil.pair("online-mode", WetSponge.getServer().getServerInfo().isOnlineMode()),
				JSONUtil.pair("icon", WetSponge.getServer().getServerInfo().getBase64ServerIcon()));
		}

		final Runtime runtime     = Runtime.getRuntime();
		RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();

		parent.put("system", JSONUtil.createObject(JSONUtil.pair("timingcost", getCost()), JSONUtil.pair("name", System.getProperty("os.name")),
			JSONUtil.pair("version", System.getProperty("os.version")), JSONUtil.pair("jvmversion", System.getProperty("java.version")),
			JSONUtil.pair("arch", System.getProperty("os.arch")), JSONUtil.pair("maxmem", runtime.maxMemory()), JSONUtil.pair("cpu", runtime.availableProcessors()),
			JSONUtil.pair("runtime", ManagementFactory.getRuntimeMXBean().getUptime()), JSONUtil.pair("flags", String.join(" ", runtimeBean.getInputArguments())),
			JSONUtil.pair("gc", JSONUtil.toObjectMapper(ManagementFactory.getGarbageCollectorMXBeans(),
				input -> JSONUtil.pair(input.getName(), JSONUtil.toArray(input.getCollectionCount(), input.getCollectionTime()))))));

		Set<WSBlockType>    tileEntityTypeSet = new HashSet<>();
		Set<EnumEntityType> entityTypeSet     = new HashSet<>();

		int             size    = TimingsManager.HISTORY.size();
		TimingHistory[] history = new TimingHistory[size + 1];
		int             i       = 0;
		for (TimingHistory timingHistory : TimingsManager.HISTORY) {
			tileEntityTypeSet.addAll(timingHistory.tileEntityTypeSet);
			entityTypeSet.addAll(timingHistory.entityTypeSet);
			history[i++] = timingHistory;
		}

		history[i] = new TimingHistory(); // Current snapshot
		tileEntityTypeSet.addAll(history[i].tileEntityTypeSet);
		entityTypeSet.addAll(history[i].entityTypeSet);

		Map handlers = JSONUtil.createObject();
		for (TimingIdentifier.TimingGroup group : TimingIdentifier.GROUP_MAP.values()) {
			for (TimingHandler id : group.handlers) {
				if (!id.isTimed() && !id.isSpecial()) {
					continue;
				}
				handlers.put(id.id, JSONUtil.toArray(group.id, id.name));
			}
		}

		parent.put("idmap", JSONUtil
			.createObject(JSONUtil.pair("groups", JSONUtil.toObjectMapper(TimingIdentifier.GROUP_MAP.values(), group -> JSONUtil.pair(group.id, group.name))),
				JSONUtil.pair("handlers", handlers),
				JSONUtil.pair("worlds", JSONUtil.toObjectMapper(TimingHistory.worldMap.entrySet(), input -> JSONUtil.pair(input.getValue(), input.getKey()))),
				JSONUtil.pair("tileentity", JSONUtil.toObjectMapper(tileEntityTypeSet, input -> JSONUtil.pair(input.getId(), input.getStringId()))),
				JSONUtil.pair("entity", JSONUtil.toObjectMapper(entityTypeSet, input -> JSONUtil.pair(input.getTypeId(), input.getName())))));

		// Information about loaded plugins

		parent.put("plugins", JSONUtil.toObjectMapper(WetSponge.getPluginManager().getPlugins(), plugin -> JSONUtil.pair(plugin.getId(), JSONUtil
			.createObject(JSONUtil.pair("version", plugin.getPluginDescription().getVersion()),
				JSONUtil.pair("description", String.valueOf(plugin.getPluginDescription().getDescription()).trim()),
				JSONUtil.pair("website", plugin.getPluginDescription().getWebsite()),
				JSONUtil.pair("authors", String.join(",", plugin.getPluginDescription().getAuthors()))))));

		// Information on the users Config
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				parent.put("config", co.aikar.wetspongetimings.SpigotTimingsExport.addConfigs());
				break;
			case SPONGE:
				parent.put("config", JSONUtil.objectBuilder().add("sponge", co.aikar.wetspongeutils.SpongeTimingsUtils.serializeConfigNode()));
				break;
			default:
				break;
		}

		new TimingsExport(listeners, parent, history).start();
	}

	static long getCost() {
		// Benchmark the users System.nanotime() for cost basis
		int           passes   = 100;
		TimingHandler SAMPLER1 = Timings.ofSafe("Timings Sampler 1");
		TimingHandler SAMPLER2 = Timings.ofSafe("Timings Sampler 2");
		TimingHandler SAMPLER3 = Timings.ofSafe("Timings Sampler 3");
		TimingHandler SAMPLER4 = Timings.ofSafe("Timings Sampler 4");
		TimingHandler SAMPLER5 = Timings.ofSafe("Timings Sampler 5");
		TimingHandler SAMPLER6 = Timings.ofSafe("Timings Sampler 6");

		long start = System.nanoTime();
		for (int i = 0; i < passes; i++) {
			SAMPLER1.startTiming();
			SAMPLER2.startTiming();
			SAMPLER3.startTiming();
			SAMPLER3.stopTiming();
			SAMPLER4.startTiming();
			SAMPLER5.startTiming();
			SAMPLER6.startTiming();
			SAMPLER6.stopTiming();
			SAMPLER5.stopTiming();
			SAMPLER4.stopTiming();
			SAMPLER2.stopTiming();
			SAMPLER1.stopTiming();
		}
		long timingsCost = (System.nanoTime() - start) / passes / 6;
		SAMPLER1.reset(true);
		SAMPLER2.reset(true);
		SAMPLER3.reset(true);
		SAMPLER4.reset(true);
		SAMPLER5.reset(true);
		SAMPLER6.reset(true);
		return timingsCost;
	}

	@Override
	public void run() {
		out.put("data", JSONUtil.toArrayMapper(history, TimingHistory::export));

		String response   = null;
		String timingsURL = null;
		try {
			HttpURLConnection con = (HttpURLConnection) new URL("http://timings.aikar.co/post").openConnection();
			//HttpURLConnection con = (HttpURLConnection) new URL("https://timings.atalgaba.com/post").openConnection();
			con.setDoOutput(true);
			String hostName = "BrokenHost";
			try {
				hostName = InetAddress.getLocalHost().getHostName();
			} catch (Exception ignored) {}
			con.setRequestProperty("User-Agent", "Paper/" + WetSponge.getServer().getServerInfo().getServerName() + "/" + hostName);
			//con.setRequestProperty("User-Agent", "WetSponge/" + WetSponge.getServer().getServerInfo().getServerName() + "/" + hostName);
			con.setRequestMethod("POST");
			con.setInstanceFollowRedirects(false);

			OutputStream request = new GZIPOutputStream(con.getOutputStream()) {{
				this.def.setLevel(7);
			}};

			request.write(JSONValue.toJSONString(out).getBytes("UTF-8"));
			request.close();

			response = getResponse(con);

			if (con.getResponseCode() != 302) {
				listeners
					.sendMessage(WSText.builder(PREFIX + "Upload Error: " + con.getResponseCode() + ": " + con.getResponseMessage()).color(EnumTextColor.RED).build());
				listeners.sendMessage(WSText.builder(PREFIX + "Check your logs for more information").color(EnumTextColor.RED).build());
				if (response != null) {
					InternalLogger.sendError(response);
				}
				return;
			}

			timingsURL = con.getHeaderField("Location");
			listeners.sendMessage(WSText.builder(PREFIX).color(EnumTextColor.YELLOW).append(WSText.builder("View Timings Report: ").color(EnumTextColor.GREEN)
				.append(WSText.builder(timingsURL).clickAction(WSOpenURLAction.of(new URL(timingsURL))).build()).build()).build());

			if (response != null && !response.isEmpty()) {
				InternalLogger.sendInfo(PREFIX + "Timing Response: " + response);
			}
		} catch (IOException ex) {
			listeners.sendMessage(WSText.builder(PREFIX + "Error uploading timings, check your logs for more information").color(EnumTextColor.RED).build());
			if (response != null) {
				InternalLogger.sendError(response);
			}
			InternalLogger.sendError(PREFIX + "Could not paste timings: " + ex.toString());
		} finally {
			this.listeners.done(timingsURL);
		}
	}

	private String getResponse(HttpURLConnection con) throws IOException {
		InputStream is = null;
		try {
			is = con.getInputStream();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			byte[] b = new byte[1024];
			int    bytesRead;
			while ((bytesRead = is.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			return bos.toString();

		} catch (IOException ex) {
			listeners.sendMessage(WSText.builder(PREFIX + "Error uploading timings, check your logs for more information").color(EnumTextColor.RED).build());
			InternalLogger.sendWarning(con.getResponseMessage() + ": " + ex);
			return null;
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}
}
