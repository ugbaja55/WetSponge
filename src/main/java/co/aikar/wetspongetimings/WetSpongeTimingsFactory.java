/*
 * This file is part of Sponge, licensed under the MIT License (MIT).
 *
 * Copyright (c) SpongePowered <https://www.spongepowered.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package co.aikar.wetspongetimings;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.util.InternalLogger;
import com.google.common.collect.EvictingQueue;

import javax.annotation.Nullable;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class WetSpongeTimingsFactory implements TimingsFactory {

	private final int     MAX_HISTORY_FRAMES = 12;
	public final Timing NULL_HANDLER       = new NullTimingHandler();
	private       boolean timingsEnabled     = false;
	private       boolean verboseEnabled     = true;
	private       int     historyInterval    = -1;
	private       int     historyLength      = -1;
	private final boolean moduleEnabled;


	public WetSpongeTimingsFactory () {
		this.moduleEnabled = true;
		init();
	}


	public void init () {
		setVerboseTimingsEnabled(true);
		setTimingsEnabled(this.moduleEnabled);
		setHistoryInterval(300);
		setHistoryLength(3600);
	}


	private static String timeSummary (int seconds) {
		String time = "";
		if (seconds > 60 * 60) {
			time += TimeUnit.SECONDS.toHours(seconds) + "h";
			seconds /= 60;
		}

		if (seconds > 0) {
			time += TimeUnit.SECONDS.toMinutes(seconds) + "m";
		}
		return time;
	}


	@Override
	public Timing of(WSPlugin plugin, String name, @Nullable Timing groupHandler) {
		return TimingsManager.getHandler(plugin.getId(), name, groupHandler, true);
	}


	@Override
	public boolean isTimingsEnabled () {
		return this.timingsEnabled;
	}


	@Override
	public void setTimingsEnabled (boolean enabled) {
		if (!this.moduleEnabled) {
			return;
		}
		this.timingsEnabled = enabled;
		reset();
	}


	@Override
	public boolean isVerboseTimingsEnabled () {
		return this.verboseEnabled;
	}


	@Override
	public void setVerboseTimingsEnabled (boolean enabled) {
		this.verboseEnabled = enabled;
		TimingsManager.needsRecheckEnabled = true;
	}


	@Override
	public int getHistoryInterval () {
		return this.historyInterval;
	}


	@Override
	public void setHistoryInterval (int interval) {
		this.historyInterval = Math.max(20 * 60, interval);
		// Recheck the history length with the new Interval
		if (this.historyLength != -1) {
			setHistoryLength(this.historyLength);
		}
	}


	@Override
	public int getHistoryLength () {
		return this.historyLength;
	}


	@Override
	public void setHistoryLength (int length) {
		// Cap at 12 History Frames, 1 hour at 5 minute frames.
		int maxLength = historyInterval * MAX_HISTORY_FRAMES;
		// For special cases of servers with special permission to bypass the max.
		// This max helps keep data file sizes reasonable for processing on Aikar's Timing parser side.
		// Setting this will not help you bypass the max unless Aikar has added an exception on the API side.
		if (System.getProperty("timings.bypassMax") != null) {
			maxLength = Integer.MAX_VALUE;
		}
		historyLength = Math.max(Math.min(maxLength, length), historyInterval);
		Queue<TimingHistory> oldQueue = TimingsManager.HISTORY;
		int                  frames   = (getHistoryLength() / getHistoryInterval());
		if (length > maxLength) {
			InternalLogger.sendWarning("Timings Length too high. Requested " +
			                           length +
			                           ", max is " +
			                           maxLength +
			                           ". To get longer history, you must increase your interval. Set Interval to " +
			                           Math.ceil(length / MAX_HISTORY_FRAMES) +
			                           " to achieve this length.");
		}
		TimingsManager.HISTORY = EvictingQueue.create(frames);
		if (oldQueue != null)
			TimingsManager.HISTORY.addAll(oldQueue);
	}


	@Override
	public void reset () {
		TimingsManager.reset();
	}


	@Override
	public void generateReport (WSCommandSource sender) {
		if (sender == null) {
			sender = WetSponge.getServer().getConsole();
		}
		TimingsExport.requestingReport.add(sender);
		TimingsExport.reportTimings();
	}


	public static long getCost () {
		return TimingsExport.getCost();
	}


	public static TimingHandler ofSafe (String name) {
		return ofSafe(null, name, null);
	}


	public static Timing ofSafe(WSPlugin plugin, String name) {
		return ofSafe(plugin != null ? plugin.getId() : "Minecraft - Invalid Plugin", name);
	}


	public static TimingHandler ofSafe(String name, Timing groupHandler) {
		return ofSafe(null, name, groupHandler);
	}


	public static TimingHandler ofSafe (String groupName, String name) {
		return TimingsManager.getHandler(groupName, name, null, false);
	}


	public static TimingHandler ofSafe(String groupName, String name, Timing groupHandler) {
		return TimingsManager.getHandler(groupName, name, groupHandler, false);
	}

}
