package co.aikar.wetspongetimings;


import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.Map;

import static co.aikar.wetspongeutils.JSONUtil.*;

/**
 * WetSponge on 09/07/2017 by IhToN.
 */
public class SpigotTimingsExport {
	public static Map addConfigs () {
		Map configs = createObject();
		configs.put("spigot", mapAsJSON(Bukkit.spigot().getConfig(), null));
		configs.put("bukkit", mapAsJSON(Bukkit.spigot().getConfig(), null));
		configs.put("paper", mapAsJSON(Bukkit.spigot().getConfig(), null));
		return configs;
	}


	private static JSONObject mapAsJSON (ConfigurationSection config, String parentKey) {

		JSONObject object = new JSONObject();
		for (String key : config.getKeys(false)) {
			String fullKey = (parentKey != null ? parentKey + "." + key : key);
			if (fullKey.equals("database") || fullKey.equals("settings.bungeecord-addresses") || TimingsManager.hiddenConfigs.contains(fullKey)) {
				continue;
			}
			final Object val = config.get(key);

			object.put(key, valAsJSON(val, fullKey));
		}
		return object;
	}


	private static Object valAsJSON (Object val, final String parentKey) {
		if (!(val instanceof MemorySection)) {
			if (val instanceof List) {
				Iterable<Object> v = (Iterable<Object>) val;
				return toArrayMapper(v, input -> valAsJSON(input, parentKey));
			}
			else {
				return val.toString();
			}
		}
		else {
			return mapAsJSON((ConfigurationSection) val, parentKey);
		}
	}
}
