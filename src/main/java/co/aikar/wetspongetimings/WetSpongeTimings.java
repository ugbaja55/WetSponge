/*
 * This file is part of Sponge, licensed under the MIT License (MIT).
 *
 * Copyright (c) SpongePowered <https://www.spongepowered.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aikar.wetspongetimings;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.material.blockType.WSBlockType;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;

public final class WetSpongeTimings {

	public static final Timing playerListTimer = WetSpongeTimingsFactory.ofSafe("Player List");
	public static final Timing connectionTimer = WetSpongeTimingsFactory.ofSafe("Connection Handler");
	public static final Timing tickablesTimer = WetSpongeTimingsFactory.ofSafe("Tickables");
	public static final Timing schedulerTimer = WetSpongeTimingsFactory.ofSafe("Scheduler");
	public static final Timing chunkIOTickTimer = WetSpongeTimingsFactory.ofSafe("ChunkIOTick");
	public static final Timing timeUpdateTimer = WetSpongeTimingsFactory.ofSafe("Time Update");
	public static final Timing serverCommandTimer = WetSpongeTimingsFactory.ofSafe("Server Command");
	public static final Timing worldSaveTimer = WetSpongeTimingsFactory.ofSafe("World Save");

	public static final Timing processQueueTimer = WetSpongeTimingsFactory.ofSafe("processQueue");

	public static final Timing playerCommandTimer = WetSpongeTimingsFactory.ofSafe("playerCommand");

	public static final Timing entityActivationCheckTimer = WetSpongeTimingsFactory.ofSafe("entityActivationCheck");
	public static final Timing checkIfActiveTimer = WetSpongeTimingsFactory.ofSafe("checkIfActive");

	public static final Timing antiXrayUpdateTimer = WetSpongeTimingsFactory.ofSafe("anti-xray - update");
	public static final Timing antiXrayObfuscateTimer = WetSpongeTimingsFactory.ofSafe("anti-xray - obfuscate");

	public static final Timing dataGetManipulator = WetSpongeTimingsFactory.ofSafe("## getManipulator");
	public static final Timing dataGetOrCreateManipulator = WetSpongeTimingsFactory.ofSafe("## getOrCreateManipulator");
	public static final Timing dataOfferManipulator = WetSpongeTimingsFactory.ofSafe("## offerData");
	public static final Timing dataOfferMultiManipulators = WetSpongeTimingsFactory.ofSafe("## offerManipulators");
	public static final Timing dataRemoveManipulator = WetSpongeTimingsFactory.ofSafe("## removeManipulator");
	public static final Timing dataSupportsManipulator = WetSpongeTimingsFactory.ofSafe("## supportsManipulator");
	public static final Timing dataOfferKey = WetSpongeTimingsFactory.ofSafe("## offerKey");
	public static final Timing dataGetByKey = WetSpongeTimingsFactory.ofSafe("## getKey");
	public static final Timing dataGetValue = WetSpongeTimingsFactory.ofSafe("## getValue");
	public static final Timing dataSupportsKey = WetSpongeTimingsFactory.ofSafe("## supportsKey");
	public static final Timing dataRemoveKey = WetSpongeTimingsFactory.ofSafe("## removeKey");

	public static final Timing TRACKING_PHASE_UNWINDING = WetSpongeTimingsFactory.ofSafe("## unwindPhase");

	private WetSpongeTimings() {
	}

	/**
	 * Gets a timer associated with a plugins tasks.
	 *
	 * @param task the task
	 * @param period the period
	 *
	 * @return the timing
	 */
	public static Timing getPluginTaskTimings(WSTask task, long period) {
		if (task.isAsynchronous()) {
			return null;
		}
		WSPlugin plugin = task.getPlugin();

		String name = "Task: " + task.getCallerStackTraceElement() + "(" + task.getUniqueId().toString() + ")";
		if (period > 0) {
			name += " (interval:" + period + ") ";
		} else {
			name += " (Single) ";
		}
		if (task.isAsynchronous()) {
			name += "(Async)";
		} else {
			name += "(Sync)";
		}

		return WetSpongeTimingsFactory.ofSafe(plugin, name);
	}

	/**
	 * Get a named timer for the specified entity type to track type specific timings.
	 *
	 * @param entity the entity
	 *
	 * @return the timing
	 */
	public static Timing getEntityTiming(WSEntity entity) {
		EnumEntityType type = entity.getEntityType();
		String entityType = type != null ? type.getName() : entity.getClass().getName();
		return WetSpongeTimingsFactory.ofSafe("Minecraft", "## tickEntity - " + entityType);
	}

	/**
	 * Get a named timer for the specified tile entity type to track type specific timings.
	 *
	 * @param entity the entity
	 *
	 * @return the timing
	 */
	public static Timing getTileEntityTiming(WSTileEntity entity) {
		WSBlock type = entity.getBlock();
		String entityType = type != null ? type.getStringId() : entity.getClass().getName();
		return WetSpongeTimingsFactory.ofSafe("Minecraft", "## tickTileEntity - " + entityType);
	}


	public static Timing getPluginTimings(WSPlugin plugin, String context) {
		return WetSpongeTimingsFactory.ofSafe(plugin.getId(), context, TimingsManager.PLUGIN_EVENT_HANDLER);
	}

	public static Timing getPluginSchedulerTimings(WSPlugin plugin) {
		return WetSpongeTimingsFactory.ofSafe(plugin.getId(), TimingsManager.PLUGIN_SCHEDULER_HANDLER);
	}

	public static Timing getCancelTasksTimer() {
		return WetSpongeTimingsFactory.ofSafe("Cancel Tasks");
	}

	public static Timing getCancelTasksTimer(WSPlugin plugin) {
		return WetSpongeTimingsFactory.ofSafe(plugin, "Cancel Tasks");
	}

	public static void stopServer() {
		TimingsManager.stopServer();
	}

	public static Timing getBlockTiming(WSBlock block) {
		WSBlockType type = block.getBlockType();
		return WetSpongeTimingsFactory.ofSafe("## Scheduled Block: " + type != null ? type.getStringId() : block.getHandled().getClass().getName());
	}
}
