<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.degoos</groupId>
    <artifactId>WetSponge</artifactId>
    <version>0.7.3-ALPHA</version>
    <name>wetsponge</name>
    <description>Custom API to develop Spigot AND Sponge Plugins.</description>

    <properties>
        <!-- github server corresponds to entry in ~/.m2/settings.xml -->
        <github.global.server>github</github.global.server>
    </properties>

    <build>
        <!-- Filter only plugin.yml -->
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>*</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>*</exclude>
                </excludes>
            </resource>
        </resources>


        <defaultGoal>install</defaultGoal>
        <directory>${project.basedir}/target</directory>
        <finalName>${project.artifactId}-${project.version}</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.5</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <nonFilteredFileExtensions>
                        <nonFilteredFileExtension>nbs</nonFilteredFileExtension>
                    </nonFilteredFileExtensions>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <finalName>${project.artifactId}-${project.version}</finalName>
                    <attach>false</attach>
                    <appendAssemblyId>false</appendAssemblyId>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <altDeploymentRepository>internal.repo::default::file://${project.basedir}/../MavenRepo
                    </altDeploymentRepository>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10</version>
                <configuration>
                    <show>private</show>
                    <nohelp>true</nohelp>
                    <sourcepath>${basedir}/src/main/java/com/degoos/wetsponge</sourcepath>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.10</version>
                <configuration>
                    <!--<stylesheetfile>${basedir}/src/main/javadoc/stylesheet.css</stylesheetfile>-->
                    <sourcepath>${basedir}/src/main/java/com/degoos/wetsponge</sourcepath>
                    <show>public</show>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <repositories>
        <repository>
            <id>sponge</id>
            <url>http://repo.spongepowered.org/maven</url>
        </repository>
        <repository>
            <id>com.degoos.mvn.repo</id>
            <url>https://degoos.com/mvn-repo/</url>
        </repository>
    </repositories>

    <dependencies>
        <!-- Spigot -->
        <dependency>
            <groupId>org.spigotmc</groupId>
            <artifactId>spigot</artifactId>
            <version>LATEST</version>
            <scope>system</scope>
            <systemPath>${project.basedir}/src/main/lib/spigot-1.12.jar</systemPath>
        </dependency>
        <!-- Sponge -->
        <dependency>
            <groupId>org.spongepowered</groupId>
            <artifactId>spongeapi</artifactId>
            <version>6.0.0</version>
            <scope>provided</scope>
        </dependency>
        <!-- Flow math -->
        <dependency>
            <groupId>com.flowpowered</groupId>
            <artifactId>flow-math</artifactId>
            <version>1.0.3</version>
        </dependency>
        <!-- SnakeYAML -->
        <dependency>
            <groupId>org.yaml</groupId>
            <artifactId>snakeyaml</artifactId>
            <version>1.17</version>
        </dependency>
        <!-- ViaVersion -->
        <dependency>
            <groupId>us.myles</groupId>
            <artifactId>ViaVersion</artifactId>
            <version>LATEST</version>
            <scope>system</scope>
            <systemPath>${project.basedir}/src/main/lib/viaversion-1.1.1-SNAPSHOT.jar</systemPath>
        </dependency>

        <!-- UnitTesting -->
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock</artifactId>
            <version>1.6.4</version>
            <type>pom</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-api-mockito</artifactId>
            <version>1.6.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-junit4</artifactId>
            <version>1.6.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-module-junit4-rule</artifactId>
            <version>1.6.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.powermock</groupId>
            <artifactId>powermock-classloading-xstream</artifactId>
            <version>1.6.4</version>
            <scope>test</scope>
        </dependency>

        <!-- Jooq -->
        <dependency>
            <groupId>org.jooq</groupId>
            <artifactId>jooq</artifactId>
            <version>3.9.3</version>
        </dependency>
        <!-- Javax Persistence -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>javax.persistence</artifactId>
            <version>2.1.0</version>
        </dependency>
    </dependencies>
</project>